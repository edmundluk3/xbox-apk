.class final Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;
.super Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
.source "AutoValue_BeamChannelListViewState.java"


# instance fields
.field private final content:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation
.end field

.field private final dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

.field private final error:Ljava/lang/Throwable;

.field private final isLoading:Z

.field private final isLoadingMore:Z


# direct methods
.method constructor <init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/Throwable;ZZLcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V
    .locals 0
    .param p1    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "isLoading"    # Z
    .param p4, "isLoadingMore"    # Z
    .param p5, "dialogViewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;",
            "Ljava/lang/Throwable;",
            "ZZ",
            "Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "content":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->content:Lcom/google/common/collect/ImmutableList;

    .line 25
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->error:Ljava/lang/Throwable;

    .line 26
    iput-boolean p3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoading:Z

    .line 27
    iput-boolean p4, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoadingMore:Z

    .line 28
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .line 29
    return-void
.end method


# virtual methods
.method public content()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->content:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 75
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 76
    check-cast v0, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    .line 77
    .local v0, "that":Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->content:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->error:Ljava/lang/Throwable;

    if-nez v3, :cond_4

    .line 78
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->error()Ljava/lang/Throwable;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoading:Z

    .line 79
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoadingMore:Z

    .line 80
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    if-nez v3, :cond_5

    .line 81
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 77
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->content:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 78
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->error()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 81
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    :cond_6
    move v1, v2

    .line 83
    goto :goto_0
.end method

.method public error()Ljava/lang/Throwable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    const v5, 0xf4243

    .line 88
    const/4 v0, 0x1

    .line 89
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->content:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 91
    mul-int/2addr v0, v5

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->error:Ljava/lang/Throwable;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 93
    mul-int/2addr v0, v5

    .line 94
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoading:Z

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    xor-int/2addr v0, v1

    .line 95
    mul-int/2addr v0, v5

    .line 96
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoadingMore:Z

    if-eqz v1, :cond_3

    :goto_3
    xor-int/2addr v0, v3

    .line 97
    mul-int/2addr v0, v5

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    if-nez v1, :cond_4

    :goto_4
    xor-int/2addr v0, v2

    .line 99
    return v0

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->content:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    move v1, v4

    .line 94
    goto :goto_2

    :cond_3
    move v3, v4

    .line 96
    goto :goto_3

    .line 98
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode()I

    move-result v2

    goto :goto_4
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoading:Z

    return v0
.end method

.method public isLoadingMore()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoadingMore:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BeamChannelListViewState{content="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->content:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoadingMore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->isLoadingMore:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dialogViewState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;->dialogViewState:Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
