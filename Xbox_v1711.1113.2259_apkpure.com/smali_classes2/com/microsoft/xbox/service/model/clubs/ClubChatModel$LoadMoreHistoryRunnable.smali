.class Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ClubChatModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMoreHistoryRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private initMessageId:J

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;J)V
    .locals 0
    .param p2, "initMessageId"    # J

    .prologue
    .line 690
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 691
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;->initMessageId:J

    .line 692
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Integer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 700
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;->initMessageId:J

    const/16 v1, 0x1e

    invoke-static {v0, v2, v3, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;JI)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 687
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;->buildData()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 710
    const-wide/16 v0, 0x26ac

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 705
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$500(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 706
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 696
    return-void
.end method
