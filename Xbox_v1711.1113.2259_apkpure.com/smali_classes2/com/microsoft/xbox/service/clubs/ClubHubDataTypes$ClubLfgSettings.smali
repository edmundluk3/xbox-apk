.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubLfgSettings"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;
    .locals 1

    .prologue
    .line 849
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubLfgSettings$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubLfgSettings$Builder;-><init>()V

    return-object v0
.end method

.method public static isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Z
    .locals 1
    .param p0, "settings"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 836
    if-eqz p0, :cond_0

    .line 837
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 838
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 839
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 836
    :goto_0
    return v0

    .line 839
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 843
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubLfgSettings$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubLfgSettings$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end method

.method public abstract join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;
.end method

.method public abstract view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end method
