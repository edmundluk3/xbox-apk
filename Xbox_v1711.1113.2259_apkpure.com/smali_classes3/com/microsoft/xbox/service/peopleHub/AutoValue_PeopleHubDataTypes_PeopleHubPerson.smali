.class final Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson;
.super Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;
.source "AutoValue_PeopleHubDataTypes_PeopleHubPerson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZZZZLcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Lcom/microsoft/xbox/service/model/UserStatus;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "addedDateTimeUtc"    # Ljava/util/Date;
    .param p2, "displayName"    # Ljava/lang/String;
    .param p3, "displayPicRaw"    # Ljava/lang/String;
    .param p4, "gamerScore"    # J
    .param p6, "gamertag"    # Ljava/lang/String;
    .param p7, "isBroadcasting"    # Z
    .param p8, "isFavorite"    # Z
    .param p9, "isFollowedByCaller"    # Z
    .param p10, "isFollowingCaller"    # Z
    .param p11, "isIdentityShared"    # Z
    .param p12, "multiplayerSummary"    # Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    .param p13, "preferredColor"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .param p14, "presenceState"    # Lcom/microsoft/xbox/service/model/UserStatus;
    .param p15, "presenceText"    # Ljava/lang/String;
    .param p16, "realName"    # Ljava/lang/String;
    .param p17, "xuid"    # J

    .prologue
    .line 24
    invoke-direct/range {p0 .. p18}, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_PeopleHubPerson;-><init>(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZZZZLcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Lcom/microsoft/xbox/service/model/UserStatus;Ljava/lang/String;Ljava/lang/String;J)V

    .line 25
    return-void
.end method
