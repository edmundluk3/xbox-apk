.class public final enum Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;
.super Ljava/lang/Enum;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaHubBroadcastProvider"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

.field public static final enum Beam:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

.field public static final enum Mixer:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

.field public static final enum Twitch:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 319
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    const-string v1, "Mixer"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->Mixer:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .line 320
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    const-string v1, "Beam"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->Beam:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .line 321
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    const-string v1, "Twitch"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->Twitch:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    .line 318
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->Mixer:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->Beam:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->Twitch:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->$VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 318
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 318
    const-class v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->$VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    return-object v0
.end method
