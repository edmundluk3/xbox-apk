.class public final enum Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;
.super Ljava/lang/Enum;
.source "ClubChatManagementServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    .line 11
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubChatManagementServiceStub;

    return-object v0
.end method


# virtual methods
.method public refreshClubChatTicket(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "clubId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "userId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 17
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 18
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 20
    const/4 v0, 0x1

    return v0
.end method
