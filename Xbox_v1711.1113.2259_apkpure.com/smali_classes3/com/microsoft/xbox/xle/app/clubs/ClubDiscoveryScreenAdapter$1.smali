.class Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$1;
.super Ljava/lang/Object;
.source "ClubDiscoveryScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAcceptClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->acceptInvite(J)V

    .line 82
    return-void
.end method

.method public onIgnoreClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreInvite(J)V

    .line 87
    return-void
.end method

.method public onProfileClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->goToClub(J)V

    .line 77
    return-void
.end method
