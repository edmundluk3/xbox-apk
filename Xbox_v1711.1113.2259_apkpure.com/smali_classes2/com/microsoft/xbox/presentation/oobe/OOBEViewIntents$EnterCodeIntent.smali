.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;
.super Ljava/lang/Object;
.source "OOBEViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EnterCodeIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;
    .locals 2
    .param p0, "code"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x6L
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 20
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->sizeRange(IILjava/lang/CharSequence;)V

    .line 21
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_EnterCodeIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_EnterCodeIntent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract code()Ljava/lang/String;
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
