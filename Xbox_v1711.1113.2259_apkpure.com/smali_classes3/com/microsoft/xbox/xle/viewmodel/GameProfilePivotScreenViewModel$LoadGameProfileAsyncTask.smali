.class Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfilePivotScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGameProfileAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$1;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 104
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->shouldRefresh(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 121
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 110
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->onLoadGameProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 112
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->onLoadGameProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 133
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 100
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;Z)Z

    .line 128
    return-void
.end method
