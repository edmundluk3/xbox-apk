.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SnapHolder"
.end annotation


# instance fields
.field private final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 3
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    const v2, 0x7f0b0022

    .line 579
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;-><init>(Landroid/view/View;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 580
    const v0, 0x7f0e00df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 581
    const v0, 0x7f0e07cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 582
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->setRowHeight(II)V

    .line 583
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->setImageWidth(Landroid/widget/ImageView;II)V

    .line 584
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 6
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    .line 588
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 590
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v3

    .line 592
    .local v3, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getAppMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 593
    .local v2, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getBoxArtUri()Ljava/lang/String;

    move-result-object v1

    .line 594
    .local v1, "imageUrl":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 595
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f070fca

    :goto_0
    invoke-virtual {v5, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 596
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackground(Landroid/view/View;)V

    .line 603
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v3, v2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->bindRowTitle(Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 604
    return-void

    .line 595
    :cond_1
    const v4, 0x7f070fdb

    goto :goto_0

    .line 598
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 599
    .local v0, "defaultRid":I
    :goto_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v5, 0x7f0e001d

    invoke-virtual {v4, v5, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 600
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v4, v1, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 601
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->updateImageBackground(Landroid/widget/ImageView;)V

    goto :goto_1

    .line 598
    .end local v0    # "defaultRid":I
    :cond_3
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 608
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->npi:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    .line 609
    .local v0, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v0, :cond_0

    .line 610
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedOnSnappedNPM(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 612
    :cond_0
    return-void
.end method
