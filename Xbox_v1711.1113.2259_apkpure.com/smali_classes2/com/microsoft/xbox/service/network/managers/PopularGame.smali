.class public Lcom/microsoft/xbox/service/network/managers/PopularGame;
.super Ljava/lang/Object;
.source "PopularGame.java"


# instance fields
.field public bingId:Ljava/lang/String;

.field public date:Ljava/util/Date;

.field public friendsPlayed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private imageUrl:Ljava/lang/String;

.field private isNowPlaying:Z

.field public name:Ljava/lang/String;

.field public titleId:J

.field public xuids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFriendsPlayedCount()I
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getFriendsPlayedString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 32
    const/4 v1, 0x0

    .line 33
    .local v1, "result":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 36
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 37
    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 41
    .end local v2    # "s":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 43
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    :cond_2
    return-object v1
.end method

.method public getImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getIsNowPlaying()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->isNowPlaying:Z

    return v0
.end method

.method public setImageUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->imageUrl:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setIsNowPlaying(Z)V
    .locals 0
    .param p1, "isNowPlaying"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->isNowPlaying:Z

    .line 48
    return-void
.end method
