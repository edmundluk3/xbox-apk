.class public Lcom/flipkart/chatheads/ui/UpArrowLayout;
.super Landroid/view/ViewGroup;
.source "UpArrowLayout.java"


# instance fields
.field private arrowDrawable:I

.field private arrowView:Landroid/widget/ImageView;

.field private final mMatchParentChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final pointTo:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 20
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    .line 23
    sget v0, Lcom/flipkart/chatheads/R$drawable;->chat_top_arrow:I

    iput v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowDrawable:I

    .line 27
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->init()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    .line 23
    sget v0, Lcom/flipkart/chatheads/R$drawable;->chat_top_arrow:I

    iput v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowDrawable:I

    .line 32
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->init()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    .line 23
    sget v0, Lcom/flipkart/chatheads/R$drawable;->chat_top_arrow:I

    iput v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowDrawable:I

    .line 37
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->init()V

    .line 38
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->removeView(Landroid/view/View;)V

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->createArrowView()Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    .line 54
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->addView(Landroid/view/View;)V

    .line 55
    return-void
.end method

.method private updatePointer()V
    .locals 4

    .prologue
    .line 184
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int v0, v2, v3

    .line 185
    .local v0, "x":I
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    iget v1, v2, Landroid/graphics/Point;->y:I

    .line 186
    .local v1, "y":I
    int-to-float v2, v0

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 187
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 189
    :cond_0
    int-to-float v2, v1

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    .line 190
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 193
    :cond_1
    return-void
.end method


# virtual methods
.method protected createArrowView()Landroid/widget/ImageView;
    .locals 4

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowDrawable:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 59
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 60
    .local v1, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 61
    return-object v1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 206
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 211
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getArrowDrawable()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowDrawable:I

    return v0
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "parentWidthMeasureSpec"    # I
    .param p3, "widthUsed"    # I
    .param p4, "parentHeightMeasureSpec"    # I
    .param p5, "heightUsed"    # I

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 164
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, p3, v3}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildMeasureSpec(III)I

    move-result v1

    .line 165
    .local v1, "childWidthMeasureSpec":I
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p4, p5, v3}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 169
    .local v0, "childHeightMeasureSpec":I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 170
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 197
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, p2

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, p3

    invoke-virtual {v2, p2, p3, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 198
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 199
    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 200
    .local v0, "child":Landroid/view/View;
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    if-ne v0, v2, :cond_0

    .line 198
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 201
    :cond_0
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p3

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v3

    invoke-virtual {v0, p2, v2, p4, p5}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 204
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 20
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 67
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildCount()I

    move-result v12

    .line 69
    .local v12, "count":I
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    .line 70
    .local v18, "measureSpec":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    move/from16 v0, v18

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/widget/ImageView;->measure(II)V

    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v8

    .line 72
    .local v8, "arrowViewMeasuredHeight":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v19

    .line 73
    .local v19, "size":I
    move/from16 v0, v19

    if-le v0, v8, :cond_0

    .line 74
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v8

    sub-int v19, v19, v2

    .line 75
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    move/from16 v0, v19

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 80
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v2, v4, :cond_1

    .line 81
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    if-eq v2, v4, :cond_3

    :cond_1
    const/16 v17, 0x1

    .line 83
    .local v17, "measureMatchParentChildren":Z
    :goto_0
    const/4 v15, 0x0

    .line 84
    .local v15, "maxHeight":I
    const/16 v16, 0x0

    .line 85
    .local v16, "maxWidth":I
    const/4 v10, 0x0

    .line 87
    .local v10, "childState":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v12, :cond_6

    .line 88
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 89
    .local v3, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    if-ne v3, v2, :cond_4

    .line 87
    :cond_2
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 81
    .end local v3    # "child":Landroid/view/View;
    .end local v10    # "childState":I
    .end local v13    # "i":I
    .end local v15    # "maxHeight":I
    .end local v16    # "maxWidth":I
    .end local v17    # "measureMatchParentChildren":Z
    :cond_3
    const/16 v17, 0x0

    goto :goto_0

    .line 90
    .restart local v3    # "child":Landroid/view/View;
    .restart local v10    # "childState":I
    .restart local v13    # "i":I
    .restart local v15    # "maxHeight":I
    .restart local v16    # "maxWidth":I
    .restart local v17    # "measureMatchParentChildren":Z
    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_2

    .line 91
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p1

    move/from16 v6, p2

    invoke-virtual/range {v2 .. v7}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 92
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    .line 94
    .local v14, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 93
    move/from16 v0, v16

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 96
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 95
    invoke-static {v15, v2}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 97
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    move-result v2

    invoke-static {v10, v2}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->combineMeasuredStates(II)I

    move-result v10

    .line 98
    if-eqz v17, :cond_2

    .line 99
    iget v2, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    iget v2, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    .line 101
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 109
    .end local v3    # "child":Landroid/view/View;
    .end local v14    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v15, v2}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 110
    invoke-virtual/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getSuggestedMinimumWidth()I

    move-result v2

    move/from16 v0, v16

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 113
    move/from16 v0, v16

    move/from16 v1, p1

    invoke-static {v0, v1, v10}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->resolveSizeAndState(III)I

    move-result v2

    shl-int/lit8 v4, v10, 0x10

    .line 114
    move/from16 v0, p2

    invoke-static {v15, v0, v4}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->resolveSizeAndState(III)I

    move-result v4

    .line 113
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setMeasuredDimension(II)V

    .line 117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 118
    const/4 v2, 0x1

    if-le v12, v2, :cond_9

    .line 119
    const/4 v13, 0x0

    :goto_3
    if-ge v13, v12, :cond_9

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->mMatchParentChildren:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 122
    .restart local v3    # "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 126
    .local v14, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v14, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_7

    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getMeasuredWidth()I

    move-result v2

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v4

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v2, v4

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 137
    .local v11, "childWidthMeasureSpec":I
    :goto_4
    iget v2, v14, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_8

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getMeasuredHeight()I

    move-result v2

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v2, v4

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v2, v4

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 149
    .local v9, "childHeightMeasureSpec":I
    :goto_5
    invoke-virtual {v3, v11, v9}, Landroid/view/View;->measure(II)V

    .line 119
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 131
    .end local v9    # "childHeightMeasureSpec":I
    .end local v11    # "childWidthMeasureSpec":I
    :cond_7
    iget v2, v14, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v4

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    move/from16 v0, p1

    invoke-static {v0, v2, v4}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildMeasureSpec(III)I

    move-result v11

    .restart local v11    # "childWidthMeasureSpec":I
    goto :goto_4

    .line 143
    :cond_8
    iget v2, v14, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v4

    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v0, p2

    invoke-static {v0, v2, v4}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getChildMeasureSpec(III)I

    move-result v9

    .restart local v9    # "childHeightMeasureSpec":I
    goto :goto_5

    .line 154
    .end local v3    # "child":Landroid/view/View;
    .end local v9    # "childHeightMeasureSpec":I
    .end local v11    # "childWidthMeasureSpec":I
    .end local v14    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setMeasuredDimension(II)V

    .line 155
    invoke-direct/range {p0 .. p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->updatePointer()V

    .line 157
    return-void
.end method

.method public pointTo(II)V
    .locals 1
    .param p1, "viewX"    # I
    .param p2, "viewY"    # I

    .prologue
    .line 173
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    iput p1, v0, Landroid/graphics/Point;->x:I

    .line 174
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo:Landroid/graphics/Point;

    iput p2, v0, Landroid/graphics/Point;->y:I

    .line 175
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getMeasuredHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->updatePointer()V

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->invalidate()V

    .line 180
    return-void
.end method

.method public removeAllViews()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 217
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->addView(Landroid/view/View;)V

    .line 218
    return-void
.end method

.method public setArrowDrawable(I)V
    .locals 0
    .param p1, "arrowDrawable"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/flipkart/chatheads/ui/UpArrowLayout;->arrowDrawable:I

    .line 46
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->init()V

    .line 47
    return-void
.end method
