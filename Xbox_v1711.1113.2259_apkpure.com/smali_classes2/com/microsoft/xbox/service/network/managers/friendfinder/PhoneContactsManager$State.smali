.class public final enum Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;
.super Ljava/lang/Enum;
.source "PhoneContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum AddPhoneNumberDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum DisplayGameIsBetterDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum DisplayPhoneContactsDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum Error:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum OptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum OptInDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum OptOut:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum OptOutDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum ServerOptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum UploadPhoneContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field public static final enum VerifyPhoneTokenDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Unknown:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "OptOutDialog"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptOutDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "OptOut"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptOut:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "OptInDialog"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptInDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "AddPhoneNumberDialog"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->AddPhoneNumberDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "VerifyPhoneTokenDialog"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->VerifyPhoneTokenDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "UploadPhoneContacts"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->UploadPhoneContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "ServerOptIn"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->ServerOptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "DisplayPhoneContactsDialog"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayPhoneContactsDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "DisplayGameIsBetterDialog"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayGameIsBetterDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "OptIn"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 53
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const-string v1, "Error"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Error:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 41
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Unknown:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptOutDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptOut:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptInDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->AddPhoneNumberDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->VerifyPhoneTokenDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->UploadPhoneContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->ServerOptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayPhoneContactsDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayGameIsBetterDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Error:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    return-object v0
.end method
