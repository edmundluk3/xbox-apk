.class Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSocialActionItemSocialInfoRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
        ">;>;"
    }
.end annotation


# instance fields
.field private actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation
.end field

.field private final slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/util/ArrayList;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 1
    .param p3, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4403
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4404
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->items:Ljava/util/ArrayList;

    .line 4405
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 4406
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .line 4407
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4397
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->items:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v18

    if-nez v18, :cond_b

    .line 4412
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 4413
    .local v10, "pathsForSummary":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 4414
    .local v9, "pathsForLikeInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->items:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .line 4415
    .local v4, "item":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-object/from16 v19, v0

    sget-object v20, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 4416
    if-eqz v4, :cond_0

    iget-object v0, v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->rootPath:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    iget-object v0, v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 4417
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getPathForSummaryRequest()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4418
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->getPathForLikeInfoRequest()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4423
    .end local v4    # "item":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    :cond_1
    const/4 v13, 0x0

    .line 4424
    .local v13, "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    const/4 v8, 0x0

    .line 4425
    .local v8, "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 4426
    .local v17, "summaries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 4427
    .local v6, "likes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;>;"
    const/4 v15, 0x0

    .line 4428
    .local v15, "startingIndex":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v15, v0, :cond_4

    .line 4429
    add-int/lit8 v18, v15, 0x32

    add-int/lit8 v18, v18, -0x1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 4430
    .local v3, "endingIndex":I
    new-instance v18, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;

    new-instance v19, Ljava/util/ArrayList;

    invoke-virtual {v10, v15, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct/range {v18 .. v19}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;-><init>(Ljava/util/List;)V

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;->getCommentsServiceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;)Ljava/lang/String;

    move-result-object v12

    .line 4431
    .local v12, "postBodyForSummary":Ljava/lang/String;
    new-instance v18, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;

    new-instance v19, Ljava/util/ArrayList;

    invoke-virtual {v9, v15, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct/range {v18 .. v19}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;-><init>(Ljava/util/List;)V

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;->getCommentsServiceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;)Ljava/lang/String;

    move-result-object v11

    .line 4433
    .local v11, "postBodyForLikeInfo":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v12}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getLikeDataInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    move-result-object v14

    .line 4434
    .local v14, "socialSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    if-eqz v14, :cond_2

    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    .line 4435
    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 4438
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getMeLikeInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;

    move-result-object v7

    .line 4439
    .local v7, "meLike":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    if-eqz v7, :cond_3

    iget-object v0, v7, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 4440
    iget-object v0, v7, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4445
    .end local v7    # "meLike":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .end local v14    # "socialSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :cond_3
    :goto_2
    add-int/lit8 v15, v3, 0x1

    .line 4446
    goto :goto_1

    .line 4442
    :catch_0
    move-exception v2

    .line 4443
    .local v2, "e":Ljava/lang/Exception;
    const-string v18, "GetSocialActionItemSocialInfoRunner"

    const-string v19, "Social Info Request Failed"

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 4448
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "endingIndex":I
    .end local v11    # "postBodyForLikeInfo":Ljava/lang/String;
    .end local v12    # "postBodyForSummary":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_5

    .line 4449
    new-instance v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    .end local v13    # "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    invoke-direct {v13}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;-><init>()V

    .line 4450
    .restart local v13    # "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    move-object/from16 v0, v17

    iput-object v0, v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    .line 4453
    :cond_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_6

    .line 4454
    new-instance v8, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;

    .end local v8    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    invoke-direct {v8}, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;-><init>()V

    .line 4455
    .restart local v8    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    iput-object v6, v8, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    .line 4458
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->items:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_b

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .line 4459
    .restart local v4    # "item":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    if-eqz v4, :cond_7

    .line 4460
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_9

    if-eqz v13, :cond_9

    iget-object v0, v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    if-eqz v19, :cond_9

    .line 4461
    iget-object v0, v13, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_8
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_9

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 4463
    .local v16, "sum":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    move-object/from16 v20, v0

    iget-object v0, v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->id:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 4464
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->update(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)V

    goto :goto_3

    .line 4468
    .end local v16    # "sum":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    :cond_9
    if-eqz v8, :cond_7

    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v19

    if-nez v19, :cond_7

    .line 4469
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_a
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    .line 4471
    .local v5, "like":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    if-eqz v5, :cond_a

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;->parentPath:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;->parentPath:Ljava/lang/String;

    move-object/from16 v20, v0

    iget-object v0, v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->id:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 4472
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    goto :goto_4

    .line 4480
    .end local v4    # "item":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .end local v5    # "like":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .end local v6    # "likes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;>;"
    .end local v8    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .end local v9    # "pathsForLikeInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "pathsForSummary":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v13    # "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    .end local v15    # "startingIndex":I
    .end local v17    # "summaries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;>;"
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetSocialActionItemSocialInfoRunner;->items:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    return-object v18
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4496
    const-wide/16 v0, 0xd1c

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 4490
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4486
    return-void
.end method
