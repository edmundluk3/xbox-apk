.class public Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;
.super Ljava/lang/Object;
.source "ProgrammingAction.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "Action"
.end annotation


# static fields
.field public static Activity:Ljava/lang/String;

.field public static Album:Ljava/lang/String;

.field public static App:Ljava/lang/String;

.field public static Aritst:Ljava/lang/String;

.field public static Episode:Ljava/lang/String;

.field public static Game:Ljava/lang/String;

.field public static Movie:Ljava/lang/String;

.field public static Season:Ljava/lang/String;

.field public static TV:Ljava/lang/String;


# instance fields
.field public Parameters:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Parameters"
        required = false
    .end annotation
.end field

.field public Target:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Target"
    .end annotation
.end field

.field public type:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "type"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "GameMarketplace/Xbox"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Game:Ljava/lang/String;

    .line 11
    const-string v0, "VideoMarketplace/Movie"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Movie:Ljava/lang/String;

    .line 12
    const-string v0, "VideoMarketplace/Series"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->TV:Ljava/lang/String;

    .line 13
    const-string v0, "VideoMarketplace/Season"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Season:Ljava/lang/String;

    .line 14
    const-string v0, "VideoMarketplace/Episode"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Episode:Ljava/lang/String;

    .line 15
    const-string v0, "MusicMarketplace/Album"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Album:Ljava/lang/String;

    .line 16
    const-string v0, "MusicMarketplace/Artist"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Aritst:Ljava/lang/String;

    .line 17
    const-string v0, "AppMarketplace/App"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->App:Ljava/lang/String;

    .line 18
    const-string v0, "AppMarketplace/Activity"

    sput-object v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Activity:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
