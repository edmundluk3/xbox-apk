.class public Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "StoreAutoSuggestItemListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private defaultResourceIdToIndexTable:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resourceId:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "rowViewResourceId"    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 28
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    .line 32
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->resourceId:I

    .line 33
    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromStoreAutoSuggestItem(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 82
    .local v0, "resourceId":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 38
    move-object v4, p2

    .line 40
    .local v4, "v":Landroid/view/View;
    if-nez v4, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 42
    .local v5, "vi":Landroid/view/LayoutInflater;
    iget v6, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->resourceId:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;-><init>()V

    .line 45
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;
    const v6, 0x7f0e079f

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    iput-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    .line 46
    const v6, 0x7f0e07a0

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->primaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 47
    const v6, 0x7f0e07a1

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->secondaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    const v6, 0x7f0e07a2

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->smartGlassIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 50
    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 55
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromStoreAutoSuggestItem(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 58
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v3

    .line 60
    .local v3, "resId":I
    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    instance-of v6, v6, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;

    if-eqz v6, :cond_1

    .line 61
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;

    .line 62
    .local v1, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setImageURI2(Ljava/lang/String;I)V

    .line 73
    .end local v1    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;
    :goto_1
    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->primaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->smartGlassIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 76
    return-object v4

    .line 52
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;
    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v3    # "resId":I
    :cond_0
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;
    goto :goto_0

    .line 64
    .restart local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v3    # "resId":I
    :cond_1
    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    instance-of v6, v6, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    if-eqz v6, :cond_2

    .line 65
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    .line 66
    .local v1, "img":Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setImageURI2(Ljava/lang/String;I)V

    .line 67
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultAspectXY(I)[I

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setAspectXY([I)V

    goto :goto_1

    .line 70
    .end local v1    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;
    :cond_2
    sget-object v6, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;->TAG:Ljava/lang/String;

    const-string v7, "Unknown image type"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
