.class public Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "GameDetailsProgressPhoneScreenAdapter.java"


# instance fields
.field private gameclipList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation
.end field

.field private gameclipsNoDataTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private gameclipsView:Landroid/widget/LinearLayout;

.field private moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 38
    const v0, 0x7f0e0612

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->screenBody:Landroid/view/View;

    .line 39
    const v0, 0x7f0e0613

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    .line 42
    const v0, 0x7f0e0615

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsView:Landroid/widget/LinearLayout;

    .line 43
    const v0, 0x7f0e0616

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    const v0, 0x7f0e0614

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsNoDataTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    return-object v0
.end method

.method private addGameClipItem(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 8
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    const/4 v7, 0x0

    .line 117
    if-nez p1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsView:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 122
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 123
    .local v2, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v4, 0x1

    const/high16 v5, 0x40c00000    # 6.0f

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 124
    .local v1, "itemGap":F
    float-to-int v4, v1

    invoke-virtual {v2, v7, v7, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->createViewHolder(Landroid/content/Context;)Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;

    move-result-object v0

    .line 127
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->itemView:Landroid/view/View;

    .line 128
    .local v3, "view":Landroid/view/View;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->getGamerTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->updateWithData(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Ljava/lang/String;)V

    .line 129
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$3;

    invoke-direct {v4, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v4, :cond_2

    .line 140
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$4;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$4;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_2
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v4, :cond_3

    .line 148
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$5;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$5;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    :cond_3
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v4, :cond_4

    .line 156
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$6;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$6;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    :cond_4
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v4, :cond_5

    .line 164
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$7;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$7;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    :cond_5
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v4, :cond_6

    .line 172
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$8;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$8;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    :cond_6
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    if-eqz v4, :cond_0

    .line 180
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$9;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$9;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    :cond_0
    return-void
.end method

.method public updateViewOverride()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->getShouldHideScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/GameDetailsProgressPhoneScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->updateLoadingIndicator(Z)V

    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->showNoContentTextForGameClips()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsNoDataTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 96
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsView:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->getGameClips()Ljava/util/ArrayList;

    move-result-object v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->shouldUpdateLikeControl()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->resetLikeControlUpdate()V

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->getGameClips()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipList:Ljava/util/ArrayList;

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 100
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 102
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->addGameClipItem(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    goto :goto_2

    .line 92
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipsNoDataTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_1

    .line 105
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->gameclipList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_5

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0

    .line 108
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;->moreGameClipsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto/16 :goto_0
.end method
