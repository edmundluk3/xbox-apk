.class public abstract Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter;
.super Ljava/lang/Object;
.source "CommonActionsAndResults.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/common/CommonActionsAndResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LoadMoreActionWithParameter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "parameter":Ljava/lang/Object;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 89
    new-instance v0, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_LoadMoreActionWithParameter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/common/AutoValue_CommonActionsAndResults_LoadMoreActionWithParameter;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public abstract parameter()Ljava/lang/Object;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter<TT;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
