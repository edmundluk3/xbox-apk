.class public Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "SettingsNotificationsPageAdapter.java"


# instance fields
.field private final globalHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final messageHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifAchievement:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubDemotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubInviteRequest:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubInvited:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubJoined:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubNewMember:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubPromotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubRecommendation:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubReport:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifClubTransfer:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifFavBroadcast:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifFavOnline:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifLfg:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifMessage:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifPartyInvite:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifTournamentMatch:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifTournamentState:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final notifTournamentTeam:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    .line 40
    const v0, 0x7f0e09f8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->globalHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 41
    const v0, 0x7f0e09f9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavOnline:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 42
    const v0, 0x7f0e09fa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavBroadcast:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 43
    const v0, 0x7f0e09fb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifMessage:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 44
    const v0, 0x7f0e09fc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->messageHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 45
    const v0, 0x7f0e09fd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifPartyInvite:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 46
    const v0, 0x7f0e09fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifLfg:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 47
    const v0, 0x7f0e0a00

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubJoined:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 48
    const v0, 0x7f0e0a01

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubNewMember:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 49
    const v0, 0x7f0e0a02

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubPromotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 50
    const v0, 0x7f0e0a03

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubDemotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 51
    const v0, 0x7f0e0a04

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInviteRequest:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 52
    const v0, 0x7f0e0a05

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubRecommendation:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 53
    const v0, 0x7f0e0a06

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubReport:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 54
    const v0, 0x7f0e0a07

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInvited:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 55
    const v0, 0x7f0e0a08

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubTransfer:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 56
    const v0, 0x7f0e09ff

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifAchievement:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 57
    const v0, 0x7f0e0a09

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentState:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 58
    const v0, 0x7f0e0a0a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentMatch:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 59
    const v0, 0x7f0e0a0b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentTeam:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->updateViewOverride()V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->globalHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavOnline:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavBroadcast:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifMessage:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->messageHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifLfg:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifPartyInvite:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubJoined:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubNewMember:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubPromotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubDemotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInviteRequest:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubRecommendation:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubReport:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInvited:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubTransfer:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifAchievement:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentState:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentMatch:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentTeam:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 102
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setGlobalHoverChat(Z)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsFavoriteOnlineEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$10(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubDemotionEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$11(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubInviteRequestEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$12(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubRecommendationEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$13(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubReportEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$14(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubInvitedEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$15(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubTransferEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$16(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsAchievementsEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$17(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsTournamentStatusEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$18(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsTournamentMatchEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$19(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsTournamentTeamEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsFavoriteBroadcastEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsMessageEnabled(Z)V

    .line 76
    if-nez p2, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->messageHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->globalHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->messageHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->globalHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 83
    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setMessageHoverChatEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsLfgEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsPartyInviteEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubJoinedEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubNewMemberEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$new$9(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsClubPromotionEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isPushNotificationsBusy()Z

    move-result v0

    .line 108
    .local v0, "pushNotifyBusy":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->globalHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isGlobalHoverChatEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 109
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->globalHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsMessageEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavOnline:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsFavoriteOnlineEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 112
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavOnline:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavBroadcast:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsFavoriteBroadcastEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 115
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifFavBroadcast:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifMessage:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsMessageEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 118
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifMessage:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_3

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->messageHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isMessageHoverChatEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 121
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->messageHoverChat:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsMessageEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    move v1, v2

    :goto_4
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifLfg:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsLfgEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 124
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifLfg:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_5

    move v1, v2

    :goto_5
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 126
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifPartyInvite:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationPartyInviteVisible()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v3

    :goto_6
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifPartyInvite:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsPartyInviteEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 128
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifPartyInvite:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_7

    move v1, v2

    :goto_7
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubJoined:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubJoinedEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 131
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubJoined:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_8

    move v1, v2

    :goto_8
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubNewMember:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubNewMemberEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 134
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubNewMember:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_9

    move v1, v2

    :goto_9
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubPromotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubPromotionEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 137
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubPromotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_a

    move v1, v2

    :goto_a
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubDemotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubDemotionEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 140
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubDemotion:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_b

    move v1, v2

    :goto_b
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInviteRequest:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubInviteRequestEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 143
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInviteRequest:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_c

    move v1, v2

    :goto_c
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubRecommendation:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubRecommendationEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 146
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubRecommendation:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_d

    move v1, v2

    :goto_d
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubReport:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubReportEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 149
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubReport:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_e

    move v1, v2

    :goto_e
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInvited:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubInvitedEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 152
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubInvited:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_f

    move v1, v2

    :goto_f
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubTransfer:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsClubTransferEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 155
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifClubTransfer:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_10

    move v1, v2

    :goto_10
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 157
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifAchievement:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsAchievementsEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 158
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifAchievement:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_11

    move v1, v2

    :goto_11
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 160
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentState:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationTournamentStatusEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 161
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentState:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_12

    move v1, v2

    :goto_12
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentMatch:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationTournamentMatchEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 164
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentMatch:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_13

    move v1, v2

    :goto_13
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 166
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentTeam:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationTournamentTeamEnabled()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;->notifTournamentTeam:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-nez v0, :cond_14

    :goto_14
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 168
    return-void

    :cond_0
    move v1, v3

    .line 109
    goto/16 :goto_0

    :cond_1
    move v1, v3

    .line 112
    goto/16 :goto_1

    :cond_2
    move v1, v3

    .line 115
    goto/16 :goto_2

    :cond_3
    move v1, v3

    .line 118
    goto/16 :goto_3

    :cond_4
    move v1, v3

    .line 121
    goto/16 :goto_4

    :cond_5
    move v1, v3

    .line 124
    goto/16 :goto_5

    .line 126
    :cond_6
    const/16 v1, 0x8

    goto/16 :goto_6

    :cond_7
    move v1, v3

    .line 128
    goto/16 :goto_7

    :cond_8
    move v1, v3

    .line 131
    goto/16 :goto_8

    :cond_9
    move v1, v3

    .line 134
    goto/16 :goto_9

    :cond_a
    move v1, v3

    .line 137
    goto/16 :goto_a

    :cond_b
    move v1, v3

    .line 140
    goto/16 :goto_b

    :cond_c
    move v1, v3

    .line 143
    goto/16 :goto_c

    :cond_d
    move v1, v3

    .line 146
    goto/16 :goto_d

    :cond_e
    move v1, v3

    .line 149
    goto/16 :goto_e

    :cond_f
    move v1, v3

    .line 152
    goto/16 :goto_f

    :cond_10
    move v1, v3

    .line 155
    goto/16 :goto_10

    :cond_11
    move v1, v3

    .line 158
    goto :goto_11

    :cond_12
    move v1, v3

    .line 161
    goto :goto_12

    :cond_13
    move v1, v3

    .line 164
    goto :goto_13

    :cond_14
    move v2, v3

    .line 167
    goto :goto_14
.end method
