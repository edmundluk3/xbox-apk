.class Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;
.super Ljava/util/concurrent/FutureTask;
.source "ReceiverSessionTask.java"

# interfaces
.implements Lcom/microsoft/playready/networkdevice/IReceiverSessionTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Lcom/microsoft/playready/networkdevice/IReceiverSession;",
        ">;",
        "Lcom/microsoft/playready/networkdevice/IReceiverSessionTask;"
    }
.end annotation


# instance fields
.field mWorker:Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;)V
    .locals 0
    .param p1, "worker"    # Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 28
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;->mWorker:Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;

    .line 29
    return-void
.end method


# virtual methods
.method public addReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;->mWorker:Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->addReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V

    .line 35
    return-void
.end method

.method public removeReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionTask;->mWorker:Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->removeReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V

    .line 41
    return-void
.end method
