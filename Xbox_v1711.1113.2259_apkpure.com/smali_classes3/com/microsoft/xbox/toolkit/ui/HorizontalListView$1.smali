.class Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;
.super Landroid/database/DataSetObserver;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    monitor-enter v1

    .line 150
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$002(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;Z)Z

    .line 151
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getEmptyView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->setEmptyView(Landroid/view/View;)V

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->invalidate()V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->requestLayout()V

    .line 155
    return-void

    .line 151
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$100(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->invalidate()V

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->requestLayout()V

    .line 162
    return-void
.end method
