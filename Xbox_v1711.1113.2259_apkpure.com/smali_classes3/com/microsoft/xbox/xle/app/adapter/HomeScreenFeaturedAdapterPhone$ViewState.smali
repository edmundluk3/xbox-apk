.class final enum Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;
.super Ljava/lang/Enum;
.source "HomeScreenFeaturedAdapterPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

.field public static final enum CONTENT:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

.field public static final enum EMPTY:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 190
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->CONTENT:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->EMPTY:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    .line 189
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->CONTENT:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->EMPTY:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 189
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 189
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewState;

    return-object v0
.end method
