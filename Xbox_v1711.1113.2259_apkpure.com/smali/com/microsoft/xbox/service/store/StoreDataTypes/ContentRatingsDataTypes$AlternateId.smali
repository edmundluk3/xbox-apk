.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;
.super Ljava/lang/Object;
.source "ContentRatingsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlternateId"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final idType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IdType"
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Value"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "idType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "value"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 507
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 508
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 510
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->idType:Ljava/lang/String;

    .line 511
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->value:Ljava/lang/String;

    .line 512
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 516
    if-ne p1, p0, :cond_1

    .line 522
    :cond_0
    :goto_0
    return v1

    .line 518
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;

    if-nez v3, :cond_2

    move v1, v2

    .line 519
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 521
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;

    .line 522
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->idType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->idType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->value:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->value:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 528
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->hashCode:I

    if-nez v0, :cond_0

    .line 529
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->hashCode:I

    .line 530
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->idType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->hashCode:I

    .line 531
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->value:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->hashCode:I

    .line 534
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 539
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
