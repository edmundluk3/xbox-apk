.class final Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$2;
.super Ljava/lang/Object;
.source "UTCAppLaunch.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch;->trackDelegatedSigninComplete(Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$result:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$2;->val$result:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 128
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 129
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Result"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$2;->val$result:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 130
    const-string v1, "SignIn - Delegate Key Complete"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 131
    return-void
.end method
