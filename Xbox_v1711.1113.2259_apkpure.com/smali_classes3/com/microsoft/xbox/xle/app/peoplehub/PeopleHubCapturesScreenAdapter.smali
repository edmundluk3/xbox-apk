.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "PeopleHubCapturesScreenAdapter.java"


# instance fields
.field private captures:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private filterSpinner:Landroid/widget/Spinner;

.field private filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

.field private noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 41
    const v0, 0x7f0e064e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->screenBody:Landroid/view/View;

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    .line 44
    const v0, 0x7f0e0651

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0301d8

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;-><init>(Landroid/content/Context;IZLcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f020163

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 48
    const v0, 0x7f0e0650

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 49
    const v0, 0x7f0e064f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 51
    const v0, 0x7f0e0652

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onResume()V

    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->restoreListPosition()V

    .line 79
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 93
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x1090008

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->values()[Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->notifyDataSetChanged()V

    .line 126
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->cleanupLikeClicks()V

    .line 137
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStop()V

    .line 138
    return-void
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->updateLoadingIndicator(Z)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getNoContentText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->clear()V

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getCaptures()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->captures:Ljava/util/List;

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->captures:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->captures:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 64
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/ICapture;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .end local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/ICapture;
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->add(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    goto :goto_0

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->shouldUpdateLikeControl()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->resetLikeControlUpdate()V

    .line 70
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->notifyDataSetChanged()V

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 73
    return-void
.end method
