.class public Lcom/microsoft/xbox/smartglass/ClientResolution;
.super Ljava/lang/Object;
.source "ClientResolution.java"


# instance fields
.field public final dpiX:S

.field public final dpiY:S

.field public final nativeHeight:S

.field public final nativeWidth:S


# direct methods
.method public constructor <init>(SSSS)V
    .locals 0
    .param p1, "nativeWidth"    # S
    .param p2, "nativeHeight"    # S
    .param p3, "dpiX"    # S
    .param p4, "dpiY"    # S

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-short p1, p0, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeWidth:S

    .line 27
    iput-short p2, p0, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeHeight:S

    .line 28
    iput-short p3, p0, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiX:S

    .line 29
    iput-short p4, p0, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiY:S

    .line 30
    return-void
.end method
