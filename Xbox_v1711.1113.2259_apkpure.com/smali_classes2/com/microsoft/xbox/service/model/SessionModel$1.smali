.class Lcom/microsoft/xbox/service/model/SessionModel$1;
.super Ljava/lang/Object;
.source "SessionModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/model/SessionModel;->leaveSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/SessionModel;

.field final synthetic val$mode:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/model/SessionModel;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel$1;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/SessionModel$1;->val$mode:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel$1;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->access$100(Lcom/microsoft/xbox/service/model/SessionModel;)Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel$1;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->access$200(Lcom/microsoft/xbox/service/model/SessionModel;)V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel$1;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->access$300(Lcom/microsoft/xbox/service/model/SessionModel;)Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel$1;->val$mode:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->shutdownSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 255
    return-void
.end method
