.class Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "SystemSettingsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetFriendFinderSettingsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/xle/app/FriendFinderSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .prologue
    .line 448
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;->this$0:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 449
    iput-object p2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;->caller:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 450
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 454
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFriendFinderSettings()Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;->buildData()Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 468
    const-wide/16 v0, 0x1389

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/xle/app/FriendFinderSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 463
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/xle/app/FriendFinderSettings;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetFriendFinderSettingsRunner;->caller:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->access$400(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 464
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 459
    return-void
.end method
