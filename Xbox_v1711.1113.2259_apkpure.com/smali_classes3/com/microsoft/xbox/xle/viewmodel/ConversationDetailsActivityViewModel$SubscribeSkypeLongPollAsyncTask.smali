.class Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ConversationDetailsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubscribeSkypeLongPollAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final edfRegId:Ljava/lang/String;

.field private final model:Lcom/microsoft/xbox/service/model/MessageModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "edfRegId"    # Ljava/lang/String;

    .prologue
    .line 1096
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1093
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->model:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 1097
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->edfRegId:Ljava/lang/String;

    .line 1098
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1102
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->model:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->edfRegId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->subscribeSkypeLongPoll(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1092
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1111
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1092
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 1107
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1125
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$800(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    .line 1132
    :goto_0
    return-void

    .line 1129
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$900()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SubscribeSkypeLongPollAsyncTask failed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1092
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1121
    return-void
.end method
