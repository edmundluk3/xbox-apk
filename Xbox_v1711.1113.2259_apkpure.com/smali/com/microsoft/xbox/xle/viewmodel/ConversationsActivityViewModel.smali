.class public Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
.source "ConversationsActivityViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;",
        "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
        ">;"
    }
.end annotation


# static fields
.field private static final REQUEST_OVERLAY_PERM_CODE:I = 0x4d2


# instance fields
.field private final TAG:Ljava/lang/String;

.field private addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

.field private forceInitialReload:Z

.field hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private isAddingUserToBlockList:Z

.field private isLoadingProfileNeverList:Z

.field private isRemovingUserFromBlockList:Z

.field private final listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ListenerManager",
            "<",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private loadProfileNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

.field private neverList:Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

.field private profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

.field private shouldForceUpdateSummaryAdapter:Z

.field private summaryToOpenAsHoverChat:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 55
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    .line 68
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getMessagesAdapter(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 92
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->presenceUpdateHandler:Ljava/lang/Runnable;

    .line 94
    new-instance v0, Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ListenerManager;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    .line 96
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->blockSenderInternal(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->onAddUserToBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isAddingUserToBlockList:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->onRemoveUserFromBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isRemovingUserFromBlockList:Z

    return p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->onLoadProfileNeverListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$802(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isLoadingProfileNeverList:Z

    return p1
.end method

.method private blockSenderInternal(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 3
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    const/4 v1, 0x1

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "blocking current sender"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;->cancel()V

    .line 151
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;->load(Z)V

    .line 153
    return-void

    .line 145
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->updateProfileAndPresenceDataAsync()V

    return-void
.end method

.method static synthetic lambda$processSenderXuid$1(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
    .param p1, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 237
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->navigateToConversationDetails(Ljava/lang/String;)V

    return-void
.end method

.method private navigateToConversationDetails(Ljava/lang/String;)V
    .locals 6
    .param p1, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 520
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Navigating to conversation details for user: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->inboxConversationSelected(Ljava/lang/String;)V

    .line 524
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v1, p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addMessageInstance(Landroid/content/Context;Ljava/lang/String;)V

    .line 530
    :goto_0
    return-void

    .line 526
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 527
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSenderXuid(Ljava/lang/String;)V

    .line 528
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method private onAddUserToBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "blockUserXuid"    # Ljava/lang/String;

    .prologue
    .line 428
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onAddUserToBlockListCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isAddingUserToBlockList:Z

    .line 431
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$3;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 444
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updateAdapter()V

    .line 445
    return-void

    .line 435
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User added to never list"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNeverListData()Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->neverList:Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 440
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to add user to block list"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const v0, 0x7f070758

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->showError(I)V

    goto :goto_0

    .line 431
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadProfileNeverListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 468
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadMessageDetailsCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isLoadingProfileNeverList:Z

    .line 471
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$3;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 482
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updateAdapter()V

    .line 483
    return-void

    .line 475
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNeverListData()Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->neverList:Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 479
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to get block list"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onRemoveUserFromBlockListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "blockUserXuid"    # Ljava/lang/String;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onRemoveUserFromBlockListCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isRemovingUserFromBlockList:Z

    .line 451
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$3;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 464
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updateAdapter()V

    .line 465
    return-void

    .line 455
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User removed from never list"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNeverListData()Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->neverList:Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 460
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to remove user from block list"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const v0, 0x7f07075a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->showError(I)V

    goto :goto_0

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private processSenderXuid(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 3
    .param p1, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 234
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSenderXuid()Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "senderXuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 237
    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 239
    :cond_0
    return-void
.end method


# virtual methods
.method public addOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 333
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->addListener(Ljava/lang/Object;)V

    .line 334
    return-void
.end method

.method public blockSender(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 7
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 156
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 158
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07073e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07073d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070738

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$1;

    invoke-direct {v4, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070736

    .line 164
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    .line 158
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 167
    return-void

    .line 156
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearForceUpdateSummaryAdapter()V
    .locals 1

    .prologue
    .line 660
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter:Z

    .line 661
    return-void
.end method

.method public deleteSkypeConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 7
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 205
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 207
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07038e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07038d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070738

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;

    invoke-direct {v4, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070736

    .line 213
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    .line 207
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 216
    return-void

    .line 205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConversationSentTimeString(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)Ljava/lang/String;
    .locals 4
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .param p2, "forNarrator"    # Z

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastSent:Ljava/util/Date;

    if-nez v3, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-object v2

    .line 119
    :cond_1
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 121
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getMostRecentMessageWithText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-result-object v1

    .line 122
    .local v1, "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    if-eqz v3, :cond_0

    .line 126
    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    .line 127
    .local v0, "lastSent":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 128
    invoke-static {v0, p2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToDurationSinceNow(Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 130
    :cond_2
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getLocalizedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 132
    .end local v0    # "lastSent":Ljava/util/Date;
    .end local v1    # "message":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    :cond_3
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastSent:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 133
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastSent:Ljava/util/Date;

    invoke-static {v2, p2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToDurationSinceNow(Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 135
    :cond_4
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastSent:Ljava/util/Date;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getLocalizedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getListState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getMessageModel()Lcom/microsoft/xbox/service/model/MessageModel;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    return-object v0
.end method

.method public getSLSConversationList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getUnReadMessageCount()I

    move-result v0

    return v0
.end method

.method public hasMoreItemsToLoad()Z
    .locals 1

    .prologue
    .line 323
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->moreConversationExists()Z

    move-result v0

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getIsLoadingMessageList()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isAddingUserToBlockList:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isRemovingUserFromBlockList:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isLoadingProfileNeverList:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isLoadingAttachment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isLoadingGameClip()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->isLoadingConversationDetail:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 317
    :goto_0
    return v0

    .line 318
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUserBlocked(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z
    .locals 2
    .param p1, "conversation"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 664
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->neverList:Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->neverList:Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    const/4 v0, 0x1

    .line 669
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v2, 0x0

    .line 277
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->forceInitialReload:Z

    if-eqz v1, :cond_0

    .line 278
    const/4 p1, 0x1

    .line 279
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->forceInitialReload:Z

    .line 282
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v1, :cond_1

    .line 283
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 286
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->isResyncNeeded()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 288
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    .line 291
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationProfilePresenceUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 293
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 294
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isLoadingConversation()Z

    move-result v1

    if-nez v1, :cond_3

    .line 295
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    .line 298
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadProfileNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

    if-eqz v1, :cond_4

    .line 299
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadProfileNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->cancel()V

    .line 301
    :cond_4
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadProfileNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

    .line 302
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadProfileNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->load(Z)V

    .line 304
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MessageModel;->clearCurrentConversation()V

    .line 305
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v1, :cond_5

    .line 306
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {p0, v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)V

    .line 308
    :cond_5
    return-void
.end method

.method public loadConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)V
    .locals 2
    .param p1, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .param p2, "forceRefresh"    # Z

    .prologue
    .line 673
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 674
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->cancel()V

    .line 677
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    .line 678
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->load(Z)V

    .line 679
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->setCurrentConversation(Ljava/lang/String;)V

    .line 680
    return-void
.end method

.method public loadMoreItemsAsync()V
    .locals 2

    .prologue
    .line 328
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    .line 329
    return-void
.end method

.method public navigateToConversationDetails(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 6
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 506
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackReadConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 507
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 508
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 509
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Navigating to conversation details for user: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->inboxConversationSelected(Ljava/lang/String;)V

    .line 513
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addMessageInstance(Landroid/content/Context;Ljava/lang/String;)V

    .line 517
    :goto_0
    return-void

    .line 515
    :cond_0
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->NavigateTo(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public navigateToFriendsPickerScreen()V
    .locals 7

    .prologue
    .line 683
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 684
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Navigating to Friend Selector"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 686
    .local v1, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putIsGroupConversation(Z)V

    .line 687
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 688
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 689
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 691
    :cond_0
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/FriendsSelectorScreen;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 700
    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v1    # "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 693
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v3, "User has no privilege to send message"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v2

    .line 695
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f07061d

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 696
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f07061c

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 697
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    const v6, 0x7f0707c7

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 694
    invoke-interface {v2, v3, v4, v5, v6}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 196
    const/16 v0, 0x4d2

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->summaryToOpenAsHoverChat:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->conversationOpenedFromContextMenu(Ljava/lang/String;)V

    .line 198
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->summaryToOpenAsHoverChat:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addMessageInstance(Landroid/content/Context;Ljava/lang/String;)V

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getMessagesAdapter(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 102
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 225
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 226
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 227
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 228
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->isForceReload()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->forceInitialReload:Z

    .line 230
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->processSenderXuid(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 231
    return-void

    .line 226
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 248
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onStopOverride()V

    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->presenceUpdateHandler:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 253
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->Handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->presenceUpdateHandler:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->addUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$AddUserToNeverListAsyncTask;->cancel()V

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->cancel()V

    .line 264
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadProfileNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

    if-eqz v0, :cond_3

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadProfileNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$LoadProfileNeverListAsyncTask;->cancel()V

    .line 268
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    if-eqz v0, :cond_4

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->cancel()V

    .line 272
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->cancelLoadAttachments()V

    .line 273
    return-void
.end method

.method protected onUpdateFinished()V
    .locals 5

    .prologue
    const v4, 0x7f0704c4

    .line 487
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageData:Lcom/microsoft/xbox/service/model/UpdateType;

    const-wide/16 v2, 0xbea

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_2

    .line 489
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->showError(I)V

    .line 496
    :cond_0
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDetailsData:Lcom/microsoft/xbox/service/model/UpdateType;

    const-wide/16 v2, 0xbeb

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 497
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->showError(I)V

    .line 502
    :cond_1
    :goto_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onUpdateFinished()V

    .line 503
    return-void

    .line 491
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 492
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updateAdapter()V

    goto :goto_0

    .line 498
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    const-wide/16 v2, 0xbed

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    const v0, 0x7f070759

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->showError(I)V

    goto :goto_1
.end method

.method public openInHoverChat(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 2
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 179
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 181
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->conversationOpenedFromContextMenu(Ljava/lang/String;)V

    .line 183
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addMessageInstance(Landroid/content/Context;Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 185
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->summaryToOpenAsHoverChat:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 186
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const/16 v1, 0x4d2

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->requestDrawOverlayViewPermission(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method public removeOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 338
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->removeListener(Ljava/lang/Object;)V

    .line 339
    return-void
.end method

.method public shouldForceUpdateSummaryAdapter()Z
    .locals 1

    .prologue
    .line 656
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter:Z

    return v0
.end method

.method public shouldShowHoverChatOption()Z
    .locals 1

    .prologue
    .line 191
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canRequestDrawOverlayViews()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unblockUser(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 2
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->cancel()V

    .line 173
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->removeUserToNeverListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$RemoveUserToNeverListAsyncTask;->load(Z)V

    .line 175
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackBlockUserState(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Z)V

    .line 176
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v4, 0x1

    .line 342
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 344
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$3;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 420
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected update type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updateAdapter()V

    .line 425
    :goto_2
    return-void

    .line 346
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 347
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Conversation Delete Failed with exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 349
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 350
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 351
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 353
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 355
    :cond_5
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter:Z

    goto :goto_1

    .line 360
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 361
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Message Delete Failed with exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 363
    :cond_6
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 364
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getSLSMessageList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 365
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 366
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 370
    :cond_7
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter:Z

    goto/16 :goto_1

    .line 375
    :pswitch_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-nez v2, :cond_8

    .line 376
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "update not final, ignore update"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 380
    :cond_8
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_9

    .line 381
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 398
    :goto_3
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter:Z

    goto/16 :goto_1

    .line 382
    :cond_9
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_a

    .line 383
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_3

    .line 384
    :cond_a
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_b

    .line 385
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_3

    .line 387
    :cond_b
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 389
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 390
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isLoadingConversation()Z

    move-result v2

    if-nez v2, :cond_c

    .line 391
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessages()V

    .line 392
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updatePresenceDataForConversations(Z)V

    goto :goto_3

    .line 394
    :cond_c
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Loading conversations"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 401
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :pswitch_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-nez v2, :cond_d

    .line 402
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "update not final, ignore update"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 406
    :cond_d
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter:Z

    goto/16 :goto_1

    .line 409
    :pswitch_4
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->updatePresenceDataForConversations(Z)V

    .line 410
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter:Z

    goto/16 :goto_1

    .line 413
    :pswitch_5
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-nez v2, :cond_0

    .line 415
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->setShouldScrollToBottom()V

    .line 416
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->load()V

    .line 417
    const-string v2, ""

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->setMessageBody(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
