.class final synthetic Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

.field private final arg$2:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private final arg$3:J

.field private final arg$4:J


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;JJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$1:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$2:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iput-wide p3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$3:J

    iput-wide p5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$4:J

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;JJ)Landroid/widget/PopupMenu$OnMenuItemClickListener;
    .locals 8

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;JJ)V

    return-object v1
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$1:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$2:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$3:J

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$$Lambda$15;->arg$4:J

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->lambda$null$4(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;JJLandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
