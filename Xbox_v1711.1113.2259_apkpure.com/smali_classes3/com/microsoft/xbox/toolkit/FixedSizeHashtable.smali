.class public Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;
.super Ljava/lang/Object;
.source "FixedSizeHashtable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private count:I

.field private fifo:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable",
            "<TK;TV;>.KeyTuple;>;"
        }
    .end annotation
.end field

.field private hashtable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final maxSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSize"    # I

    .prologue
    .line 38
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->fifo:Ljava/util/PriorityQueue;

    .line 36
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->count:I

    .line 40
    iput p1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->maxSize:I

    .line 42
    if-gtz p1, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 45
    :cond_0
    return-void
.end method

.method private cleanupIfNecessary()V
    .locals 5

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 100
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    move-result v1

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->fifo:Ljava/util/PriorityQueue;

    invoke-virtual {v4}, Ljava/util/PriorityQueue;->size()I

    move-result v4

    if-ne v1, v4, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 102
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    move-result v1

    iget v4, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->maxSize:I

    if-le v1, v4, :cond_2

    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->fifo:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;

    .line 105
    .local v0, "oldest":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    move-result v1

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->fifo:Ljava/util/PriorityQueue;

    invoke-virtual {v4}, Ljava/util/PriorityQueue;->size()I

    move-result v4

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_2
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    goto :goto_1

    .end local v0    # "oldest":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    :cond_0
    move v1, v3

    .line 100
    goto :goto_0

    .restart local v0    # "oldest":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    :cond_1
    move v1, v3

    .line 107
    goto :goto_2

    .line 109
    .end local v0    # "oldest":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    :cond_2
    return-void
.end method


# virtual methods
.method public elements()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 96
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 50
    if-nez p2, :cond_1

    .line 63
    :goto_1
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 55
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 58
    :cond_2
    iget v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->count:I

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->fifo:Ljava/util/PriorityQueue;

    new-instance v1, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;

    iget v2, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->count:I

    invoke-direct {v1, p0, p1, v2}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;-><init>(Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;Ljava/lang/Object;I)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->cleanupIfNecessary()V

    goto :goto_1
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 74
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const/4 v0, 0x0

    .line 78
    .local v0, "keyTuple":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->fifo:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 80
    .local v1, "queueIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "keyTuple":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    check-cast v0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;

    .line 82
    .restart local v0    # "keyTuple":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;->access$000(Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 87
    :cond_1
    const-string v2, "Queue should contain an item with the given key."

    invoke-static {v2, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/FixedSizeHashtable;->fifo:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 89
    return-void

    .line 71
    .end local v0    # "keyTuple":Lcom/microsoft/xbox/toolkit/FixedSizeHashtable$KeyTuple;, "Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;"
    .end local v1    # "queueIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/toolkit/FixedSizeHashtable<TK;TV;>.KeyTuple;>;"
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
