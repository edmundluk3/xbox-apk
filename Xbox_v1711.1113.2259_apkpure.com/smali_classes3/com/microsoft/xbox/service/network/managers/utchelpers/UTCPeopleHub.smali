.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;
.super Ljava/lang/Object;
.source "UTCPeopleHub.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsListItemType;,
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;,
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$FriendsFilter;,
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$AchievementsFilter;
    }
.end annotation


# static fields
.field public static final ACTION_NAMES:[Ljava/lang/String;

.field private static final FILTER_KEY:Ljava/lang/String; = "filter"

.field private static final ITEM_TYPE_KEY:Ljava/lang/String; = "itemType"

.field private static final PEOPLETABSCROLLEDPATTERN:Ljava/lang/String; = "012"

.field private static final PEOPLETABSELECTEDPATTERN:Ljava/lang/String; = "02"

.field public static final TAB_NAMES:[Ljava/lang/String;

.field private static currentSocialFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

.field private static currentTab:Ljava/lang/String;

.field private static previousTab:Ljava/lang/String;

.field private static targetXUID:Ljava/lang/String;

.field public static trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->targetXUID:Ljava/lang/String;

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "People Hub - Info View"

    aput-object v1, v0, v2

    const-string v1, "People Hub - Activity View"

    aput-object v1, v0, v3

    const-string v1, "People Hub - Achievements View"

    aput-object v1, v0, v4

    const-string v1, "People Hub - Friends View"

    aput-object v1, v0, v5

    const-string v1, "People Hub - Captures View"

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->TAB_NAMES:[Ljava/lang/String;

    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "People Hub - Info View"

    aput-object v1, v0, v2

    const-string v1, "People Hub - Activity View"

    aput-object v1, v0, v3

    const-string v1, "People Hub - Achievements View"

    aput-object v1, v0, v4

    const-string v1, "People Hub - Friends View"

    aput-object v1, v0, v5

    const-string v1, "People Hub - Captures View"

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->ACTION_NAMES:[Ljava/lang/String;

    .line 85
    const-string v0, "People Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentTab:Ljava/lang/String;

    .line 86
    const-string v0, "People Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->previousTab:Ljava/lang/String;

    .line 87
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->ALL:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentSocialFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 115
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 141
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->targetXUID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->previousTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 29
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->previousTab:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 29
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentTab:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentSocialFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    return-object v0
.end method

.method public static getCurrentTab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentTab:Ljava/lang/String;

    return-object v0
.end method

.method public static getPreviousTab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->previousTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic lambda$trackCapturesFilterAction$0(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V
    .locals 3
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;

    .prologue
    .line 270
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 271
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Filter"

    invoke-interface {p0}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;->getTelemetryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 272
    const-string v2, "ProfileType"

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->targetXUID:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "YouProfile"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 274
    const-string v1, "Game Captures Filter"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 275
    return-void

    .line 272
    :cond_0
    const-string v1, "MeProfile"

    goto :goto_0
.end method

.method static synthetic lambda$trackFriendsFilterAction$1(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V
    .locals 3
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;

    .prologue
    .line 283
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 284
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Filter"

    invoke-interface {p0}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;->getTelemetryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 285
    const-string v2, "ProfileType"

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->targetXUID:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "YouProfile"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 287
    const-string v1, "People Hub - Friends Filter"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 288
    return-void

    .line 285
    :cond_0
    const-string v1, "MeProfile"

    goto :goto_0
.end method

.method public static resetTracking(Ljava/lang/String;)V
    .locals 1
    .param p0, "toPage"    # Ljava/lang/String;

    .prologue
    .line 103
    const-string v0, "People Hub"

    if-eq p0, v0, :cond_0

    const-string v0, "People Hub - Achievements View"

    if-eq p0, v0, :cond_0

    const-string v0, "People Hub - Activity View"

    if-eq p0, v0, :cond_0

    const-string v0, "People Hub - Captures View"

    if-eq p0, v0, :cond_0

    const-string v0, "People Hub - Friends View"

    if-eq p0, v0, :cond_0

    const-string v0, "People Hub - Info View"

    if-eq p0, v0, :cond_0

    .line 110
    const-string v0, "People Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentTab:Ljava/lang/String;

    .line 111
    const-string v0, "People Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->previousTab:Ljava/lang/String;

    .line 113
    :cond_0
    return-void
.end method

.method public static setSocialFilter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;)V
    .locals 0
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .prologue
    .line 90
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->currentSocialFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 91
    return-void
.end method

.method public static setTargetXUID(Ljava/lang/String;)V
    .locals 0
    .param p0, "userXUID"    # Ljava/lang/String;

    .prologue
    .line 393
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->targetXUID:Ljava/lang/String;

    .line 394
    return-void
.end method

.method public static trackAchievementsFilterAction(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;)V
    .locals 1
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 246
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$8;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$8;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 263
    return-void
.end method

.method public static trackBlockClicked(Ljava/lang/String;)V
    .locals 1
    .param p0, "targetXUID"    # Ljava/lang/String;

    .prologue
    .line 321
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$10;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$10;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 329
    return-void
.end method

.method public static trackBlockUnblockConfirmed(Ljava/lang/String;Z)V
    .locals 1
    .param p0, "targetXUID"    # Ljava/lang/String;
    .param p1, "isBlock"    # Z

    .prologue
    .line 335
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$11;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$11;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 347
    return-void
.end method

.method public static trackCapturesFilterAction(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V
    .locals 1
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;

    .prologue
    .line 269
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 276
    return-void
.end method

.method public static trackCompareGameAction(Ljava/lang/String;)V
    .locals 1
    .param p0, "titleID"    # Ljava/lang/String;

    .prologue
    .line 229
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$7;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 239
    return-void
.end method

.method public static trackCompareWithFriendsAction()V
    .locals 1

    .prologue
    .line 190
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$4;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 196
    return-void
.end method

.method public static trackCustomizeAction()V
    .locals 1

    .prologue
    .line 202
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$5;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 208
    return-void
.end method

.method public static trackFriendsFilterAction(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V
    .locals 1
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;

    .prologue
    .line 282
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 289
    return-void
.end method

.method public static trackRealNameSettingAction()V
    .locals 1

    .prologue
    .line 178
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$3;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 184
    return-void
.end method

.method public static trackSelectFromFriendsListAction(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;)V
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 295
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$9;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$9;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 315
    return-void
.end method

.method public static trackSendMessageAction()V
    .locals 1

    .prologue
    .line 214
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$6;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$6;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 223
    return-void
.end method

.method public static trackShareLeaderboard(Ljava/lang/String;)V
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 354
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$12;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$12;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 364
    return-void
.end method

.method public static trackShareMyLeaderboard()V
    .locals 1

    .prologue
    .line 370
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackShareLeaderboard(Ljava/lang/String;)V

    .line 371
    return-void
.end method

.method public static trackUseCustomGamerPic()V
    .locals 1

    .prologue
    .line 377
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$13;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$13;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 383
    return-void
.end method
