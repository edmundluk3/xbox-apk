.class public Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
.super Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
.source "GameProgressXboxoneAchievementsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProgressXboxoneAchievementsItem"
.end annotation


# instance fields
.field public achievementType:Ljava/lang/String;

.field public mediaAssets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;",
            ">;"
        }
    .end annotation
.end field

.field public progressState:Ljava/lang/String;

.field public progression:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;

.field public rewards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;",
            ">;"
        }
    .end annotation
.end field

.field public serviceConfigId:Ljava/lang/String;

.field public timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

.field public titleAssociations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TitleAssociation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;-><init>()V

    return-void
.end method

.method private getPercentage(DD)I
    .locals 9
    .param p1, "current"    # D
    .param p3, "total"    # D

    .prologue
    const/16 v1, 0x64

    .line 258
    const-wide/16 v4, 0x0

    cmpl-double v4, p3, v4

    if-lez v4, :cond_2

    .line 259
    div-double v4, p1, p3

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v2, v4, v6

    .line 260
    .local v2, "percent":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 261
    .local v0, "finalPercent":I
    :goto_0
    if-ltz v0, :cond_2

    .line 262
    if-le v0, v1, :cond_0

    move v0, v1

    .line 269
    .end local v0    # "finalPercent":I
    .end local v2    # "percent":D
    :cond_0
    :goto_1
    return v0

    .line 260
    .restart local v2    # "percent":D
    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    goto :goto_0

    .line 269
    .end local v2    # "percent":D
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getPercentage(II)I
    .locals 8
    .param p1, "current"    # I
    .param p2, "total"    # I

    .prologue
    const/16 v1, 0x64

    .line 243
    if-lez p2, :cond_2

    .line 244
    int-to-double v4, p1

    int-to-double v6, p2

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v2, v4, v6

    .line 245
    .local v2, "percent":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 246
    .local v0, "finalPercent":I
    :goto_0
    if-ltz v0, :cond_2

    .line 247
    if-le v0, v1, :cond_0

    move v0, v1

    .line 254
    .end local v0    # "finalPercent":I
    .end local v2    # "percent":D
    :cond_0
    :goto_1
    return v0

    .line 245
    .restart local v2    # "percent":D
    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    goto :goto_0

    .line 254
    .end local v2    # "percent":D
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getAchievementIconUri()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->mediaAssets:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->mediaAssets:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->getMediaAssetUri()Ljava/lang/String;

    move-result-object v0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGamerscore()Ljava/lang/String;
    .locals 5

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    .local v0, "gamerscore":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 163
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;

    .line 164
    .local v1, "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->type:Ljava/lang/String;

    const-string v4, "Gamerscore"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 165
    iget-object v0, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->value:Ljava/lang/String;

    .line 171
    .end local v1    # "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    :cond_1
    return-object v0
.end method

.method public getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progressState:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v0

    return-object v0
.end method

.method public getRemainingTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResponseType()Lcom/microsoft/xbox/service/network/managers/SLSResponseType;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->XboxOne:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    return-object v0
.end method

.method public getTimeUnlocked()Ljava/util/Date;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progression:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progression:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;->timeUnlocked:Ljava/util/Date;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRewards()Z
    .locals 5

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "hasRewards":Z
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 178
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;

    .line 179
    .local v1, "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->type:Ljava/lang/String;

    const-string v4, "gamerscore"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 180
    const/4 v0, 0x1

    goto :goto_0

    .line 185
    .end local v1    # "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    :cond_1
    return v0
.end method

.method public isExpired()Z
    .locals 3

    .prologue
    .line 200
    const/4 v0, 0x0

    .line 201
    .local v0, "expired":Z
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 203
    const/4 v0, 0x1

    .line 207
    :cond_0
    return v0
.end method

.method protected setPercentageComplete()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 217
    iput v3, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->percentageComplete:I

    .line 218
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v1, v2, :cond_1

    .line 219
    const/16 v1, 0x64

    iput v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->percentageComplete:I

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->InProgress:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progression:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progression:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;->requirements:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progression:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;->requirements:Ljava/util/ArrayList;

    .line 224
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 226
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progression:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementProgression;->requirements:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;

    .line 227
    .local v0, "progressData":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;
    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->getOperationType()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;->SUM:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;

    if-eq v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->getOperationType()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;->MAXIMUM:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;

    if-ne v1, v2, :cond_0

    :cond_2
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->current:Ljava/lang/String;

    .line 229
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->target:Ljava/lang/String;

    .line 230
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 231
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->getValueType()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->INTEGER:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    if-ne v1, v2, :cond_3

    .line 232
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->current:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->target:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getPercentage(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->percentageComplete:I

    goto :goto_0

    .line 233
    :cond_3
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->getValueType()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->DOUBLE:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    if-ne v1, v2, :cond_0

    .line 234
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->current:Ljava/lang/String;

    invoke-static {v1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseDouble(Ljava/lang/String;D)D

    move-result-wide v2

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->target:Ljava/lang/String;

    invoke-static {v1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseDouble(Ljava/lang/String;D)D

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getPercentage(DD)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->percentageComplete:I

    goto :goto_0
.end method
