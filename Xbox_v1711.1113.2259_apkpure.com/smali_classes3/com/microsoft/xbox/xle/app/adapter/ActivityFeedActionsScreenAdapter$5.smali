.class Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;
.super Ljava/lang/Object;
.source "ActivityFeedActionsScreenAdapter.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 331
    if-nez p9, :cond_1

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->clearShouldScrollToBottomOnLayoutChange()V

    .line 336
    :goto_0
    if-eq p5, p9, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->shouldScrollToBottomOnLayoutChange()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 344
    :cond_0
    return-void

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setShouldScrollToBottomOnLayoutChange()V

    goto :goto_0
.end method
