.class Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "SkypeMessagingDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OpenDataBaseTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Landroid/database/sqlite/SQLiteDatabase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)V
    .locals 1

    .prologue
    .line 78
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;->this$0:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    .line 79
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 80
    return-void
.end method


# virtual methods
.method protected doInBackground()Landroid/database/sqlite/SQLiteDatabase;
    .locals 6

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OpenDataBaseTask::started"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$100(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v2

    if-nez v2, :cond_0

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OpenDataBaseTask::dbHelper is null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;->this$0:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Landroid/content/Context;)V

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$200(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;)V

    .line 94
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OpenDataBaseTask::try to enable write ahead logging"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$100(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;->setWriteAheadLoggingEnabled(Z)V

    .line 96
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OpenDataBaseTask::enabled write ahead logging"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    const/4 v0, 0x0

    .line 109
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OpenDataBaseTask::try to open the database"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$100(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;)Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 111
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OpenDataBaseTask::opened the database"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 116
    :goto_1
    return-object v0

    .line 97
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v1

    .line 100
    .local v1, "ex":Ljava/lang/NoSuchMethodError;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SQLite DBHelper does not support setWriteAheadLoggingEnabled on this version of android"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    .end local v1    # "ex":Ljava/lang/NoSuchMethodError;
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catch_1
    move-exception v1

    .line 114
    .local v1, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Could not open the SQLite database"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;->doInBackground()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "sqLiteDatabase"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 121
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OpenDataBaseTask::onPostExecute started"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$300(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OpenDataBaseTask::completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OpenDataBaseTask::setReady completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 77
    check-cast p1, Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$OpenDataBaseTask;->onPostExecute(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method
