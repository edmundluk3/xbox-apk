.class public interface abstract Lcom/microsoft/xbox/XLEComponent;
.super Ljava/lang/Object;
.source "XLEComponent.java"

# interfaces
.implements Lcom/microsoft/xbox/XLEAppGraph;


# annotations
.annotation runtime Ldagger/Component;
    modules = {
        Lcom/microsoft/xbox/XLEAppModule;,
        Lcom/microsoft/xbox/data/service/ReleaseServiceModule;,
        Lcom/microsoft/xbox/data/ReleaseDataModule;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation
