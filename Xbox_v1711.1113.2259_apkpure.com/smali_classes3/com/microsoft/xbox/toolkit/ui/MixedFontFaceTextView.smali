.class public Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;
.super Landroid/widget/TextView;
.source "MixedFontFaceTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;
    }
.end annotation


# static fields
.field private static final nbsp:Ljava/lang/String; = "\u00a0"


# instance fields
.field protected formatString:Ljava/lang/String;

.field protected formatTypefaceSpan:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

.field protected mainTypefaceSpan1:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

.field protected mainTypefaceSpan2:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

.field protected unformattedString:Ljava/lang/String;

.field protected willPadFormatStringWithNBSP:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 138
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->willPadFormatStringWithNBSP:Z

    .line 139
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 140
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 143
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->getFormatSpans(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 144
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->getFormatStrings(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 145
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->buildFormattedSpannable()Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method


# virtual methods
.method protected buildFormattedSpannable()Landroid/text/Spannable;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x21

    .line 210
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->unformattedString:Ljava/lang/String;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 212
    .local v2, "resultString":Ljava/lang/String;
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 213
    .local v4, "spannable":Landroid/text/Spannable;
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 214
    .local v1, "positionOfFormatString":I
    if-lez v1, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 216
    move v0, v1

    .line 217
    .local v0, "firstSpanEnd":I
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int v3, v0, v6

    .line 219
    .local v3, "secondSpanEnd":I
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->mainTypefaceSpan1:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-interface {v4, v6, v11, v0, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 220
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatTypefaceSpan:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-interface {v4, v6, v0, v3, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 222
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 223
    move v5, v3

    .line 224
    .local v5, "thirdSpanStart":I
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->mainTypefaceSpan2:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-interface {v4, v6, v5, v7, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 234
    .end local v0    # "firstSpanEnd":I
    .end local v3    # "secondSpanEnd":I
    .end local v5    # "thirdSpanStart":I
    :cond_0
    :goto_0
    return-object v4

    .line 227
    :cond_1
    if-nez v1, :cond_0

    .line 229
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatTypefaceSpan:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-interface {v4, v6, v11, v7, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 230
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->mainTypefaceSpan1:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    invoke-interface {v4, v6, v7, v8, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method protected getFormatSpans(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x7fc00000    # NaNf

    .line 168
    sget-object v7, Lcom/microsoft/xboxone/smartglass/R$styleable;->MixedFontFaceTextView:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 169
    .local v0, "array":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 171
    .local v4, "mainFontFaceName":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    .line 172
    .local v5, "mainTextSize":F
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v7

    invoke-virtual {v7, p1, v4}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 173
    .local v6, "mainTypeface":Landroid/graphics/Typeface;
    new-instance v7, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-direct {v7, p0, v4, v6, v5}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;-><init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;F)V

    iput-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->mainTypefaceSpan1:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    .line 174
    new-instance v7, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-direct {v7, p0, v4, v6, v5}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;-><init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;F)V

    iput-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->mainTypefaceSpan2:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    .line 177
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "formatFontFaceName":Ljava/lang/String;
    const/4 v7, 0x5

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    .line 179
    .local v2, "formatTextSize":F
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v7

    invoke-virtual {v7, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 180
    .local v3, "formatTypeface":Landroid/graphics/Typeface;
    new-instance v7, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-direct {v7, p0, v1, v3, v2}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;-><init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;F)V

    iput-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatTypefaceSpan:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    .line 182
    const/4 v7, 0x6

    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->willPadFormatStringWithNBSP:Z

    .line 184
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 186
    .end local v1    # "formatFontFaceName":Ljava/lang/String;
    .end local v2    # "formatTextSize":F
    .end local v3    # "formatTypeface":Landroid/graphics/Typeface;
    .end local v4    # "mainFontFaceName":Ljava/lang/String;
    .end local v5    # "mainTextSize":F
    .end local v6    # "mainTypeface":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method

.method protected getFormatStrings(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 189
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->MixedFontFaceTextView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 190
    .local v0, "array":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 191
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->unformattedString:Ljava/lang/String;

    .line 192
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    .line 193
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->unformattedString:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 197
    const-string v1, ""

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->unformattedString:Ljava/lang/String;

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 201
    const-string v1, ""

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    .line 204
    :cond_2
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->willPadFormatStringWithNBSP:Z

    if-eqz v1, :cond_3

    .line 205
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u00a0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u00a0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    .line 207
    :cond_3
    return-void
.end method

.method public setFormatTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatTypefaceSpan:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatTypefaceSpan:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->setOverrideColor(I)V

    .line 246
    :cond_0
    return-void
.end method

.method public updateText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "mainTextString"    # Ljava/lang/String;
    .param p2, "formatTextString"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->unformattedString:Ljava/lang/String;

    .line 150
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->unformattedString:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 153
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->unformattedString:Ljava/lang/String;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 157
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    .line 160
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->willPadFormatStringWithNBSP:Z

    if-eqz v0, :cond_2

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u00a0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u00a0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->formatString:Ljava/lang/String;

    .line 164
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->buildFormattedSpannable()Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    return-void
.end method
