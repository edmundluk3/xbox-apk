.class public final enum Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
.super Ljava/lang/Enum;
.source "TransmitterCertificate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Feature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public static final enum PerStreamKeys:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public static final enum Receiver:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public static final enum Revocation:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public static final enum SecureClock:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public static final enum SharedCertificate:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public static final enum Transmitter:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public static final enum Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;


# instance fields
.field private feature:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 29
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    const-string v1, "Transmitter"

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Transmitter:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 32
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    const-string v1, "Receiver"

    invoke-direct {v0, v1, v6, v6}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Receiver:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 35
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    const-string v1, "SharedCertificate"

    invoke-direct {v0, v1, v7, v7}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->SharedCertificate:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 38
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    const-string v1, "SecureClock"

    invoke-direct {v0, v1, v8, v8}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->SecureClock:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 41
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    const-string v1, "Revocation"

    const/4 v2, 0x5

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Revocation:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 44
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    const-string v1, "PerStreamKeys"

    const/4 v2, 0x6

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->PerStreamKeys:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 23
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Transmitter:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Receiver:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->SharedCertificate:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->SecureClock:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Revocation:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->PerStreamKeys:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->$VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "featureType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->feature:I

    .line 50
    iput p3, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->feature:I

    .line 51
    return-void
.end method

.method static fromValue(I)Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 60
    sget-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 62
    .local v0, "retVal":Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    iget v1, v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->feature:I

    if-gt v1, p0, :cond_0

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->SecureClock:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    iget v1, v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->feature:I

    if-gt p0, v1, :cond_0

    .line 64
    invoke-static {}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->values()[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    move-result-object v1

    aget-object v0, v1, p0

    .line 67
    :cond_0
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Revocation:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    iget v1, v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->feature:I

    if-ne p0, v1, :cond_2

    .line 69
    sget-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->Revocation:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 76
    :cond_1
    :goto_0
    return-object v0

    .line 71
    :cond_2
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->PerStreamKeys:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    iget v1, v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->feature:I

    if-ne p0, v1, :cond_1

    .line 73
    sget-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->PerStreamKeys:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->$VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    invoke-virtual {v0}, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    return-object v0
.end method


# virtual methods
.method getFeature()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->feature:I

    return v0
.end method
