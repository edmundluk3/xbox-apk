.class public final Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes;
.super Ljava/lang/Object;
.source "EditorialDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;,
        Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type cannot be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
