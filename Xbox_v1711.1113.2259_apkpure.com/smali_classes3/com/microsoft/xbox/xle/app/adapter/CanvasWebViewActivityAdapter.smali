.class public Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "CanvasWebViewActivityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;,
        Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;
    }
.end annotation


# static fields
.field private static final CHROME_ANIMATION_DURATION:I = 0x14d


# instance fields
.field private animateOutChromeTask:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;

.field private cancelButton:Landroid/view/View;

.field private canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

.field private canvasFragmentInitialized:Z

.field private canvasStarted:Z

.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private collapsedNowPlayingBar:Landroid/view/View;

.field private final collapsedNowPlayingBarHeight:I

.field private collapsedNowPlayingBarIsOffScreen:Z

.field private companionBar:Landroid/widget/RelativeLayout;

.field private final companionBarHeight:I

.field private companionContentHasBeenShown:Z

.field private companionDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private companionNameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private connectButton:Lcom/microsoft/xbox/xle/ui/ConnectButton;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private final menuBarHeight:I

.field private menuDots:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

.field private miniChromeShown:Z

.field private refreshButton:Landroid/view/View;

.field private splashBodySwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private startTime:J

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 85
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f09004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBarHeight:I

    .line 86
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBarHeight:I

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarHeight:I

    .line 87
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0901c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuBarHeight:I

    .line 92
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragmentInitialized:Z

    .line 94
    const v0, 0x7f0e021f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 95
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->addCanvasView()V

    .line 96
    const v0, 0x7f0e0224

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->mProgressBar:Landroid/widget/ProgressBar;

    .line 97
    const v0, 0x7f0e0221

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionNameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 98
    const v0, 0x7f0e0222

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 99
    const v0, 0x7f0e0229

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBar:Landroid/widget/RelativeLayout;

    .line 100
    const v0, 0x7f0e022a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 101
    const v0, 0x7f0e022b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ConnectButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->connectButton:Lcom/microsoft/xbox/xle/ui/ConnectButton;

    .line 102
    const v0, 0x7f0e0226

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->cancelButton:Landroid/view/View;

    .line 103
    const v0, 0x7f0e0227

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->refreshButton:Landroid/view/View;

    .line 104
    const v0, 0x7f0e022c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuDots:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    .line 105
    const v0, 0x7f0e0223

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->splashBodySwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 106
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getCollapsedAppBarView()Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBar:Landroid/view/View;

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->logBICompanionStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->showFullChrome(ZZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateOutChromeTask:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->showMiniChrome()V

    return-void
.end method

.method private addCanvasView()V
    .locals 5

    .prologue
    .line 181
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 183
    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-direct {v3}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    .line 185
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 186
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "R_PACKAGE_NAME"

    const-class v4, Lcom/microsoft/xboxone/smartglass/R;

    invoke-virtual {v4}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->setArguments(Landroid/os/Bundle;)V

    .line 189
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 190
    .local v1, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 191
    .local v2, "fragmentTransaction":Landroid/app/FragmentTransaction;
    const v3, 0x7f0e0228

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-virtual {v2, v3, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 192
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 193
    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 194
    return-void

    .line 181
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "fragmentManager":Landroid/app/FragmentManager;
    .end local v2    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private animateFullChromeIn()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x14d

    .line 410
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBarHeight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 411
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$7;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$7;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 417
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarIsOffScreen:Z

    .line 419
    return-void
.end method

.method private animateFullChromeOut()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x14d

    .line 424
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 425
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarHeight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$8;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$8;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 431
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarIsOffScreen:Z

    .line 432
    return-void
.end method

.method private animateMiniChromeIn()V
    .locals 4

    .prologue
    .line 436
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuDots:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x14d

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuBarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 437
    return-void
.end method

.method private animateMiniChromeOut()V
    .locals 4

    .prologue
    .line 441
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuDots:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x14d

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuBarHeight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 442
    return-void
.end method

.method private clearCanvasView()V
    .locals 4

    .prologue
    .line 344
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    if-eqz v2, :cond_1

    .line 345
    const-string v2, "CanvasWebViewActiivty"

    const-string v3, "destroying canvasView "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragmentInitialized:Z

    if-eqz v2, :cond_0

    .line 348
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getWebViewListener()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->removeListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V

    .line 352
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 353
    .local v0, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 354
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 356
    :try_start_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 359
    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :goto_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    .line 368
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragmentInitialized:Z

    .line 370
    .end local v0    # "fragmentManager":Landroid/app/FragmentManager;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_1
    return-void

    .line 360
    .restart local v0    # "fragmentManager":Landroid/app/FragmentManager;
    .restart local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private getWebView()Landroid/view/View;
    .locals 3

    .prologue
    .line 388
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    if-eqz v2, :cond_0

    .line 389
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 390
    .local v1, "view":Landroid/view/View;
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 391
    check-cast v0, Landroid/view/ViewGroup;

    .line 392
    .local v0, "group":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 393
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 397
    .end local v0    # "group":Landroid/view/ViewGroup;
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private hideChrome()V
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBar:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionBarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTranslationY(F)V

    .line 403
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuDots:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuBarHeight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->setTranslationY(F)V

    .line 404
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBar:Landroid/view/View;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarHeight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 405
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarIsOffScreen:Z

    .line 406
    return-void
.end method

.method private loadLandingPage()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->setInitialLoad()V

    .line 155
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragmentInitialized:Z

    if-nez v1, :cond_1

    .line 158
    const-string v1, "Canvas"

    const-string v2, "not initialized, initialize"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "allowedUrlPrefixes":[Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getWhitelistUrls()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getWhitelistUrls()[Ljava/lang/String;

    move-result-object v0

    .line 165
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getLaunchUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getWhitelistTitleIds()[I

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->initialize(Ljava/lang/String;[Ljava/lang/String;[I)V

    .line 166
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getWebViewListener()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V

    .line 167
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragmentInitialized:Z

    .line 168
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasStarted:Z

    .line 170
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->logBICompanionStart()V

    .line 177
    .end local v0    # "allowedUrlPrefixes":[Ljava/lang/String;
    :goto_1
    return-void

    .line 163
    .restart local v0    # "allowedUrlPrefixes":[Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/String;

    goto :goto_0

    .line 174
    .end local v0    # "allowedUrlPrefixes":[Ljava/lang/String;
    :cond_1
    const-string v1, "Canvas"

    const-string v2, "canvas ready, just launch the url"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getLaunchUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->loadUrl(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private logBICompanionStart()V
    .locals 3

    .prologue
    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCompanionIsHelp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 374
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCompanionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionId()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackCompanionStart(Ljava/lang/String;Ljava/util/UUID;)V

    .line 375
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->startTime:J

    .line 377
    :cond_0
    return-void
.end method

.method private logBICompanionStop()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 380
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCompanionIsHelp()Z

    move-result v4

    if-nez v4, :cond_0

    .line 381
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->startTime:J

    cmp-long v4, v4, v2

    if-nez v4, :cond_1

    move-wide v0, v2

    .line 382
    .local v0, "duration":J
    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->startTime:J

    .line 383
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCompanionId()Ljava/lang/String;

    move-result-object v3

    long-to-int v4, v0

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionId()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackCompanionExit(Ljava/lang/String;ILjava/util/UUID;)V

    .line 385
    .end local v0    # "duration":J
    :cond_0
    return-void

    .line 381
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->startTime:J

    sub-long v0, v4, v6

    goto :goto_0
.end method

.method private showFullChrome(ZZ)V
    .locals 1
    .param p1, "animateMiniChrome"    # Z
    .param p2, "force"    # Z

    .prologue
    .line 447
    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->miniChromeShown:Z

    if-eqz v0, :cond_2

    .line 448
    :cond_0
    if-eqz p1, :cond_1

    .line 449
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateMiniChromeOut()V

    .line 451
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateFullChromeIn()V

    .line 452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->miniChromeShown:Z

    .line 454
    :cond_2
    return-void
.end method

.method private showMiniChrome()V
    .locals 1

    .prologue
    .line 458
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->miniChromeShown:Z

    if-nez v0, :cond_0

    .line 459
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateFullChromeOut()V

    .line 460
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateMiniChromeIn()V

    .line 461
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->miniChromeShown:Z

    .line 463
    :cond_0
    return-void
.end method

.method private stopCanvasView()V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    if-eqz v0, :cond_1

    .line 332
    const-string v0, "CanvasWebViewActiivty"

    const-string v1, "stopping canvasView "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragmentInitialized:Z

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getWebViewListener()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->removeListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V

    .line 338
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasStarted:Z

    .line 341
    :cond_1
    return-void
.end method


# virtual methods
.method public onApplicationPause()V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onApplicationPause()V

    .line 200
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->logBICompanionStop()V

    .line 203
    const-string v0, "Canvas"

    const-string v1, "on application pause is called, stop the canvas view"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->stopCanvasView()V

    .line 205
    return-void
.end method

.method public onApplicationResume()V
    .locals 4

    .prologue
    .line 209
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onApplicationResume()V

    .line 213
    const-string v0, "Canvas"

    const-string v1, "on application resume is called, add canvas view back"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    if-nez v0, :cond_0

    .line 215
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->addCanvasView()V

    .line 221
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 229
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 309
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onDestroy()V

    .line 311
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->logBICompanionStop()V

    .line 312
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->clearCanvasView()V

    .line 318
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarIsOffScreen:Z

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->collapsedNowPlayingBarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$6;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 327
    :cond_0
    const-string v0, "CanvasWebViewActivityAdapter"

    const-string v1, "onDestroy stopping all components"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 233
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onPause()V

    .line 236
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->show()V

    .line 238
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->cancelButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->refreshButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuDots:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->connectButton:Lcom/microsoft/xbox/xle/ui/ConnectButton;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->onPause()V

    .line 243
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->getWebView()Landroid/view/View;

    move-result-object v0

    .line 244
    .local v0, "webview":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateOutChromeTask:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;

    if-eqz v1, :cond_1

    .line 249
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateOutChromeTask:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->stop()V

    .line 251
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 255
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onResume()V

    .line 257
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    .line 268
    .local v0, "closeCompanionHandler":Landroid/view/View$OnClickListener;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->cancelButton:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->refreshButton:Landroid/view/View;

    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$3;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->menuDots:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$4;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$4;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->getWebView()Landroid/view/View;

    move-result-object v1

    .line 286
    .local v1, "webview":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 287
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$5;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$5;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 304
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->connectButton:Lcom/microsoft/xbox/xle/ui/ConnectButton;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->onResume()V

    .line 305
    return-void
.end method

.method public updateViewOverride()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCanvasState()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->ordinal()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 111
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCanvasState()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Splash:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCanvasInternalStateIsLoading()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionNameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCompanionTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCompanionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCanvasState()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Webview:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    if-ne v0, v2, :cond_5

    .line 115
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionContentHasBeenShown:Z

    if-nez v0, :cond_0

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->show()V

    .line 117
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->hideChrome()V

    .line 118
    invoke-direct {p0, v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->showFullChrome(ZZ)V

    .line 119
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateOutChromeTask:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->animateOutChromeTask:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->start()V

    .line 121
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->companionContentHasBeenShown:Z

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->connectButton:Lcom/microsoft/xbox/xle/ui/ConnectButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/ConnectButton;->refresh()V

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->miniChromeShown:Z

    if-nez v0, :cond_4

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->show()V

    .line 136
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->splashBodySwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getShowError()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->Error:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->ordinal()I

    move-result v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 138
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasStarted:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragmentInitialized:Z

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 140
    const-string v0, "Canvas"

    const-string v1, "canvas stopped, try to restart restart"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasFragment:Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getWebViewListener()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->addListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V

    .line 142
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->canvasStarted:Z

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->drainNeedToLoadActivity()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 148
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;->loadLandingPage()V

    .line 150
    :cond_2
    return-void

    .line 111
    :cond_3
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 128
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->hide()V

    goto :goto_1

    .line 133
    :cond_5
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->hide()V

    goto :goto_1

    .line 136
    :cond_6
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->Normal:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->ordinal()I

    move-result v0

    goto :goto_2
.end method
