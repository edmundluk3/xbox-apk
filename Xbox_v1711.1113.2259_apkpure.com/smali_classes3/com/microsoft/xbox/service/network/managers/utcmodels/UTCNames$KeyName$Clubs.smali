.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyName$Clubs;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Clubs"
.end annotation


# static fields
.field public static final Available:Ljava/lang/String; = "Available"

.field public static final ChatMessageType:Ljava/lang/String; = "ChatMessageType"

.field public static final ClubLogoSource:Ljava/lang/String; = "ClubLogoSource"

.field public static final ClubName:Ljava/lang/String; = "ClubName"

.field public static final ClubType:Ljava/lang/String; = "ClubType"

.field public static final Created:Ljava/lang/String; = "Created"

.field public static final ReportCreatorXuid:Ljava/lang/String; = "ReportCreatorXuid"

.field public static final ReportItemId:Ljava/lang/String; = "ReportItemId"

.field public static final ReportReason:Ljava/lang/String; = "ReportReason"

.field public static final ReportReporterXuid:Ljava/lang/String; = "ReportReporterXuid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
