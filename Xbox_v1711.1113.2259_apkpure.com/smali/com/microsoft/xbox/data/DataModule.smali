.class public Lcom/microsoft/xbox/data/DataModule;
.super Ljava/lang/Object;
.source "DataModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/microsoft/xbox/data/repository/RepositoryModule;,
        Lcom/microsoft/xbox/data/service/ServiceModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideFacebookManager()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 27
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    return-object v0
.end method

.method provideSystemSettingsModel()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 21
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    return-object v0
.end method
