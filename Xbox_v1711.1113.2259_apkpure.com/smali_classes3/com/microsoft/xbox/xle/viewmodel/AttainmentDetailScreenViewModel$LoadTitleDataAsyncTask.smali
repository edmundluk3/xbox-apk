.class Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AttainmentDetailScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadTitleDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$1;

    .prologue
    .line 423
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 427
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 428
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->shouldRefresh(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .line 429
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefresh360GameProgressAchievements()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .line 430
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefresh360GameProgressEarnedAchievements()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 428
    :goto_0
    return v0

    .line 430
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 457
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/microsoft/xbox/service/model/TitleHubModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 458
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    new-array v1, v7, [Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360Achievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->merge(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 459
    new-array v1, v7, [Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360EarnedAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->merge(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 461
    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 452
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 435
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 436
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 437
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 448
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 423
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 441
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Z)Z

    .line 443
    return-void
.end method
