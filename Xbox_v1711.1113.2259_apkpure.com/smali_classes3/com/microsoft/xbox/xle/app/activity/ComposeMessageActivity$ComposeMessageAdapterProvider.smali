.class Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$ComposeMessageAdapterProvider;
.super Ljava/lang/Object;
.source "ComposeMessageActivity.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComposeMessageAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$ComposeMessageAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$1;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$ComposeMessageAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;)V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 117
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    .line 118
    .local v0, "composeMessageViewModel":Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getComposeMessageAdapter(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
