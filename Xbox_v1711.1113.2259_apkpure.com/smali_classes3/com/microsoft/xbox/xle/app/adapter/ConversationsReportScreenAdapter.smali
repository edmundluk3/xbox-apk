.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ConversationsReportScreenAdapter.java"


# instance fields
.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private filterSpinner:Landroid/widget/Spinner;

.field private saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 26
    const v0, 0x7f0e02e4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->screenBody:Landroid/view/View;

    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;

    .line 29
    const v0, 0x7f0e0497

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 30
    const v0, 0x7f0e0498

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;

    .line 32
    const v0, 0x7f0e0496

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 33
    const v0, 0x7f0e0495

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter$2;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter$3;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsReportScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    :cond_1
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method
