.class public final Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;
.super Ljava/lang/Object;
.source "HoverChatHeadImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HoverChatHeadImageDownloaderFactory"
.end annotation


# instance fields
.field private final userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V
    .locals 0
    .param p1, "userSummaryRepository"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    .line 53
    return-void
.end method


# virtual methods
.method public get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 57
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 59
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_0

    .line 60
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;->get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 62
    .restart local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 63
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;->get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;

    move-result-object v0

    goto :goto_0
.end method

.method public get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 69
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 70
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;)V

    return-object v0
.end method

.method public get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;
    .locals 3
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;

    invoke-direct {v0, p1, v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;)V

    .line 80
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;)V

    goto :goto_0
.end method
