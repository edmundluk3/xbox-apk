.class public final enum Lcom/microsoft/xbox/service/activityHub/ActivityHubService;
.super Ljava/lang/Enum;
.source "ActivityHubService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/activityHub/IActivityHubService;


# annotations
.annotation build Landroid/support/annotation/WorkerThread;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubService;",
        ">;",
        "Lcom/microsoft/xbox/service/activityHub/IActivityHubService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

.field private static final CONTRACT_VERSION:I = 0x1

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static final TRENDING_GLOBAL_ENDPOINT_FORMAT:Ljava/lang/String; = "https://activityhub.xboxlive.com/hot/global?maxItems=%d"

.field private static final TRENDING_TAG_ENDPOINT_FORMAT:Ljava/lang/String; = "https://activityhub.xboxlive.com/hot/lfgtags/scid/%1$s?maxItems=%2$s&type=trending"

.field private static final TRENDING_TITLE_ITEMS_ENDPOINT_FORMAT:Ljava/lang/String; = "https://activityhub.xboxlive.com/hot/titles/%s?maxItems=%d"

.field private static final TRENDING_TOPICS_ENDPOINT_FORMAT:Ljava/lang/String; = "https://activityhub.xboxlive.com/topics/?maxItems=%d"

.field private static final TRENDING_USER_ITEMS_ENDPOINT_FORMAT:Ljava/lang/String; = "https://activityhub.xboxlive.com/hot/users/xuid(%s)?maxItems=%d"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    .line 35
    new-array v0, v3, [Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    .line 40
    const-class v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->TAG:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->STATIC_HEADERS:Ljava/util/List;

    .line 54
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 58
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static getCategoryFilterCallable(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService$$Lambda$2;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    goto :goto_0
.end method

.method private static getHeaders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v1, Ljava/util/ArrayList;

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->STATIC_HEADERS:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 129
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getContentRestrictions()Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "contentRestrictions":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v3, "x-xbl-contentrestrictions"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_0
    return-object v1
.end method

.method static synthetic lambda$getCategoryFilterCallable$1(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 139
    invoke-static {}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getCategoryFilterCallable$2(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "categoryFilters"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 140
    invoke-static {}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingPostCategories;->with(Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingPostCategories;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getTrendingTags$3(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 155
    invoke-static {}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getTrendingTopicItems$0(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "topicId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingPostTopic;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingPostTopic;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityHub/ActivityHubService;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityHub/ActivityHubService;

    return-object v0
.end method


# virtual methods
.method public getTrendingCategoryItems(ILjava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;
    .locals 6
    .param p1, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrendingCategoryItems(maxItems:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " categoryFilters:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 66
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 67
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 69
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://activityhub.xboxlive.com/hot/global?maxItems=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->getCategoryFilterCallable(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;

    .line 71
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;

    return-object v1
.end method

.method public getTrendingTags(ILjava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;
    .locals 6
    .param p1, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 146
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrendingTags(maxItems:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " scid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 149
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 150
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 152
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://activityhub.xboxlive.com/hot/lfgtags/scid/%1$s?maxItems=%2$s&type=trending"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService$$Lambda$4;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    return-object v1
.end method

.method public getTrendingTitleItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    .locals 6
    .param p1, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 112
    .local p3, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrendingTitleItems(maxItems:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " titleId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " categoryFilters:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 115
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 116
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 117
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 119
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://activityhub.xboxlive.com/hot/titles/%s?maxItems=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->getCategoryFilterCallable(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    .line 121
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    return-object v1
.end method

.method public getTrendingTopicItems(ILjava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    .locals 6
    .param p1, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "topicId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 79
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrendingTopicItems(maxItems:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " topicId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 82
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 83
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 85
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://activityhub.xboxlive.com/topics/?maxItems=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService$$Lambda$1;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;

    return-object v1
.end method

.method public getTrendingUserItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    .locals 6
    .param p1, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 95
    .local p3, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrendingUserItems(maxItems:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " xuid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " categoryFilters:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 98
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 99
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 100
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 102
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://activityhub.xboxlive.com/hot/users/xuid(%s)?maxItems=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/microsoft/xbox/service/activityHub/ActivityHubService;->getCategoryFilterCallable(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    .line 104
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    return-object v1
.end method
