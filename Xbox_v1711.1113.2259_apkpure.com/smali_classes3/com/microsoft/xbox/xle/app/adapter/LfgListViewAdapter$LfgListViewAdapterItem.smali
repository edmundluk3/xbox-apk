.class public Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;
.super Ljava/lang/Object;
.source "LfgListViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LfgListViewAdapterItem"
.end annotation


# instance fields
.field public final handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final itemType:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 0
    .param p1, "itemType"    # Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 369
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->itemType:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    .line 370
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItem;->handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 371
    return-void
.end method
