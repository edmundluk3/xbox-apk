.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "GameProfileLfgScreenViewModel.java"


# static fields
.field private static final MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field public static final TAG:Ljava/lang/String;

.field private static final TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;


# instance fields
.field private clubId:Ljava/lang/Long;

.field private curLfgLanguage:Ljava/lang/String;

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

.field private final getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

.field private personSummaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private serviceConfigId:Ljava/lang/String;

.field private titleId:J

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    .line 63
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    .line 64
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 59
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    .line 60
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 72
    const-string v2, "all"

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->curLfgLanguage:Ljava/lang/String;

    .line 77
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 78
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 79
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedClubId()Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->clubId:Ljava/lang/Long;

    .line 81
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    .line 82
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getLfgLanguages()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    .line 84
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->personSummaries:Ljava/util/Map;

    .line 85
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfileLfgScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 87
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 88
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 89
    return-void

    .line 81
    :cond_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic lambda$load$0(ZLcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Lio/reactivex/SingleSource;
    .locals 6
    .param p0, "forceRefresh"    # Z
    .param p1, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 174
    iget-wide v2, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    .line 176
    .local v0, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {v0, p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->rxLoadGameProgressAchievements(ZLcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 178
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    .line 181
    :goto_0
    return-object v1

    .line 180
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not load titleModel for title id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$load$1(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;Lcom/google/common/base/Optional;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;
    .param p1, "success"    # Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->loadLfgSessions(Z)V

    .line 201
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->updateAdapter()V

    .line 202
    return-void
.end method

.method private loadLfgSessions(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "ServiceConfigId is null, waiting for title info to load before making calls to MPSD"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :goto_0
    return-void

    .line 215
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 216
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Loading LFG sessions"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V

    goto :goto_0

    .line 219
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Showing cached LFG session"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method

.method private navigateToCreateDialog()V
    .locals 4

    .prologue
    .line 343
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 344
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 346
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->clubId:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->clubId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedClubId(Ljava/lang/Long;)V

    .line 350
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 351
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedScid(Ljava/lang/String;)V

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-eqz v1, :cond_2

    .line 355
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMultiplayerHandleFilters(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V

    .line 358
    :cond_2
    const-class v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsScreen;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 359
    return-void
.end method

.method private onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 483
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 484
    .local v0, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->personSummaries:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 487
    .end local v0    # "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->updateAdapter()V

    .line 488
    return-void
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDateFilters()Ljava/util/List;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    const/4 v0, 0x7

    .line 390
    .local v0, "DAYS_IN_WEEK":I
    new-instance v2, Ljava/util/ArrayList;

    const/16 v5, 0x9

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 391
    .local v2, "dateFilters":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706ef

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706f0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706f1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706f2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    new-instance v3, Ljava/text/SimpleDateFormat;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 397
    .local v3, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 399
    .local v1, "cal":Ljava/util/Calendar;
    const/4 v4, 0x2

    .local v4, "i":I
    :goto_0
    const/4 v5, 0x7

    if-ge v4, v5, :cond_0

    .line 400
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 401
    const/4 v5, 0x6

    invoke-virtual {v1, v5, v4}, Ljava/util/Calendar;->add(II)V

    .line 402
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 405
    :cond_0
    return-object v2
.end method

.method public getLanguageFilters()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 410
    .local v1, "languageFilters":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    .line 411
    .local v0, "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->displayName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 413
    .end local v0    # "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    :cond_0
    return-object v1
.end method

.method public getPersonSummaries()Ljava/util/Map;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->getTags()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 383
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->getTags()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 8
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 152
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "load forceRefresh: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 156
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 157
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateOrJoinLFG()Z

    move-result v3

    if-nez v3, :cond_2

    .line 158
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 209
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->updateAdapter()V

    .line 210
    return-void

    .line 160
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 162
    if-eqz p1, :cond_3

    .line 163
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 167
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/microsoft/xbox/service/model/TitleHubModel;->rxLoad(ZJ)Lio/reactivex/Single;

    move-result-object v2

    .line 172
    .local v2, "titleDataSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$$Lambda$2;->lambdaFactory$(Z)Lio/reactivex/functions/Function;

    move-result-object v4

    .line 173
    invoke-virtual {v3, v4}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 185
    .local v0, "achievementsSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/google/common/base/Optional<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;>;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 187
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v0, v4}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v4

    .line 188
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v5

    .line 189
    invoke-virtual {v4, v5}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v4

    .line 185
    invoke-virtual {v3, v4}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 206
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->loadLfgSessions(Z)V

    goto :goto_0
.end method

.method public navigateToAccountSettings()V
    .locals 1

    .prologue
    .line 479
    const-string v0, "https://account.xbox.com/settings"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->NavigateToUri(Landroid/net/Uri;)V

    .line 480
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 120
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 121
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onDestroy()V

    .line 122
    return-void
.end method

.method public onLoadMultiplayerHandlesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 226
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLoadMultiplayerHandlesCompleted - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 229
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 230
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 263
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->updateAdapter()V

    .line 264
    return-void

    .line 234
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    .line 235
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 237
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 238
    .local v1, "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->data:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 239
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 235
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 242
    .restart local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 243
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v2, :cond_3

    .line 244
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 247
    :cond_3
    new-instance v2, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v2, v1, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 248
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    goto :goto_0

    .line 254
    .end local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 255
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "onLoadMultiplayerHandlesCompleted Failed"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 260
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "onLoadMultiplayerHandlesCompleted called when SCID is null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 230
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfileLfgScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 107
    :cond_0
    return-void
.end method

.method public onStartOverride()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfileLfgScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 100
    :cond_0
    return-void
.end method

.method public onStopOverride()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 115
    :cond_0
    return-void
.end method

.method public onTagsSelected(Ljava/util/List;)V
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 362
    .local p1, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->getTags()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 363
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 365
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;-><init>()V

    .line 366
    .local v0, "filters":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    .line 367
    .local v1, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->addTag(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    goto :goto_0

    .line 370
    .end local v1    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    :cond_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .line 371
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V

    .line 373
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->updateAdapter()V

    .line 375
    .end local v0    # "filters":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
    :cond_2
    return-void
.end method

.method public onTitleDataLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 267
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTitleDataLoadCompleted - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 295
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->updateAdapter()V

    .line 296
    return-void

    .line 272
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    .line 273
    .local v0, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v0, :cond_1

    .line 274
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "found scid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    .line 277
    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 278
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Loading LFG Session"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    .line 281
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V

    .line 282
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 290
    .end local v0    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :cond_1
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 291
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onTitleDataLoadCompleted Failed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onUpdateLFGSessionCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/UpdateType;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "type"    # Lcom/microsoft/xbox/service/model/UpdateType;

    .prologue
    .line 299
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUpdateLFGSessionCompleted - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/UpdateType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 316
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->forceRefresh()V

    .line 318
    :goto_0
    return-void

    .line 303
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070b6d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "errorMessage":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 312
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707ba

    .line 313
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 312
    invoke-interface {v1, v0, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 306
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070e27

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 307
    goto :goto_1

    .line 309
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 300
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 304
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDateFilter(I)V
    .locals 7
    .param p1, "selectedDateIndex"    # I

    .prologue
    const/4 v6, 0x0

    const/16 v4, 0xe

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 417
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 418
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-nez v2, :cond_0

    .line 419
    new-instance v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .line 422
    :cond_0
    if-nez p1, :cond_3

    .line 423
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setScheduledTimeFilter(Ljava/util/Date;)V

    .line 424
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setNowOnly(Z)V

    .line 445
    :cond_1
    :goto_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v5, v3, v4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V

    .line 447
    :cond_2
    return-void

    .line 425
    :cond_3
    if-ne p1, v5, :cond_4

    .line 426
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setScheduledTimeFilter(Ljava/util/Date;)V

    .line 427
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setNowOnly(Z)V

    goto :goto_0

    .line 428
    :cond_4
    const/4 v2, 0x2

    if-lt p1, v2, :cond_1

    .line 429
    add-int/lit8 v1, p1, -0x2

    .line 430
    .local v1, "dayOffset":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setNowOnly(Z)V

    .line 432
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 433
    .local v0, "cal":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 434
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 437
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 438
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 439
    const/16 v2, 0xd

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 440
    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    .line 441
    const v2, 0x5265c00

    mul-int/2addr v2, v1

    invoke-virtual {v0, v4, v2}, Ljava/util/Calendar;->add(II)V

    .line 442
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setScheduledTimeFilter(Ljava/util/Date;)V

    goto :goto_0
.end method

.method public setLanguageFilter(I)V
    .locals 5
    .param p1, "pos"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 450
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 451
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-nez v1, :cond_0

    .line 452
    new-instance v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .line 454
    :cond_0
    if-ltz p1, :cond_3

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge p1, v4, :cond_1

    move v3, v2

    :cond_1
    and-int/2addr v1, v3

    if-eqz v1, :cond_2

    .line 455
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    .line 456
    .local v0, "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->languageCode()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->curLfgLanguage:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 457
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->languageCode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->curLfgLanguage:Ljava/lang/String;

    .line 458
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->languageCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setLanguage(Ljava/lang/String;)V

    .line 459
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->MPSD_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->serviceConfigId:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V

    .line 463
    .end local v0    # "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 454
    goto :goto_0
.end method

.method public showCreateLfgDialog()V
    .locals 6

    .prologue
    .line 331
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 334
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateOrJoinLFG()Z

    move-result v2

    if-eqz v0, :cond_1

    .line 335
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07049d

    .line 336
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0706ba

    .line 337
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 333
    invoke-static {v2, v1, v3, v4}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->navigateToCreateDialog()V

    .line 340
    :cond_0
    return-void

    .line 335
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public showTagPickerDialog()V
    .locals 6

    .prologue
    .line 466
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 467
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 468
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 469
    :goto_0
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;

    move-result-object v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    .line 471
    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->enableAll(J)Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    move-result-object v3

    .line 468
    invoke-virtual {v1, v0, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showTagPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V

    .line 476
    :goto_1
    return-void

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .line 469
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->getTags()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 473
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not show tag picker for title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->titleId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 127
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 129
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 142
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 143
    return-void

    .line 131
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->onTitleDataLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 134
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->onLoadMultiplayerHandlesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 138
    :pswitch_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->onUpdateLFGSessionCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/UpdateType;)V

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
