.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->onAddStream(Lorg/webrtc/MediaStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

.field final synthetic val$stream:Lorg/webrtc/MediaStream;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;Lorg/webrtc/MediaStream;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    .prologue
    .line 870
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->val$stream:Lorg/webrtc/MediaStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 873
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/PeerConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 884
    :cond_0
    :goto_0
    return-void

    .line 876
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->val$stream:Lorg/webrtc/MediaStream;

    iget-object v0, v0, Lorg/webrtc/MediaStream;->audioTracks:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gt v0, v1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->val$stream:Lorg/webrtc/MediaStream;

    iget-object v0, v0, Lorg/webrtc/MediaStream;->videoTracks:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v1, :cond_3

    .line 877
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Weird-looking stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->val$stream:Lorg/webrtc/MediaStream;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    goto :goto_0

    .line 881
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->val$stream:Lorg/webrtc/MediaStream;

    iget-object v0, v0, Lorg/webrtc/MediaStream;->audioTracks:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 882
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;->val$stream:Lorg/webrtc/MediaStream;

    iget-object v1, v1, Lorg/webrtc/MediaStream;->audioTracks:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
