.class public Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;
.super Ljava/lang/Object;
.source "PartyAndLfgListViewAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HeaderListItem"
.end annotation


# instance fields
.field public final count:Ljava/lang/Integer;

.field public volatile transient hashCode:I

.field public final text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "count"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->text:Ljava/lang/String;

    .line 462
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->count:Ljava/lang/Integer;

    .line 463
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 467
    if-ne p1, p0, :cond_1

    .line 473
    :cond_0
    :goto_0
    return v1

    .line 469
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 470
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 472
    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;

    .line 473
    .local v0, "other":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->text:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->text:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->count:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->count:Ljava/lang/Integer;

    .line 474
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 480
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 481
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->hashCode:I

    .line 482
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->text:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->hashCode:I

    .line 483
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->count:Ljava/lang/Integer;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->hashCode:I

    .line 486
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 491
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
