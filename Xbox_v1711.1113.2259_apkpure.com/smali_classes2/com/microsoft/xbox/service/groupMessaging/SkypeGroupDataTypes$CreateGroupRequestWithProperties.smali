.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequestWithProperties;
.super Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequest;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateGroupRequestWithProperties"
.end annotation


# instance fields
.field private final properties:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "topic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "members":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequest;-><init>(Ljava/util/List;)V

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;-><init>(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequestWithProperties;->properties:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequestWithProperties;->properties:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;

    invoke-static {v0, p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;->access$102(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;Ljava/lang/String;)Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
