.class public Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "SearchResultsActivityViewModel.java"


# static fields
.field public static final MAX_SEARCH_RESULT_ITEMS:I = 0x1f4


# instance fields
.field private isLoadMoreFinished:Z

.field private isLoading:Z

.field private model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

.field private searchTag:Ljava/lang/String;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 30
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 35
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSearchDataResultAdapter(Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 36
    sget-object v1, Lcom/microsoft/xbox/service/model/StoreModel;->INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/StoreModel;->getSearchModel()Lcom/microsoft/xbox/service/model/StoreSearchModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    .line 37
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSearchTag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    .line 39
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoading:Z

    .line 40
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoadMoreFinished:Z

    .line 42
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 43
    return-void
.end method

.method private doSearch(ZLjava/lang/String;)V
    .locals 1
    .param p1, "forceRefresh"    # Z
    .param p2, "searchTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 182
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoading:Z

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->doSearch(ZLjava/lang/String;)V

    .line 186
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->updateAdapter()V

    .line 187
    return-void
.end method

.method private isNewSearchNeeded(Ljava/lang/String;)Z
    .locals 2
    .param p1, "searchTag"    # Ljava/lang/String;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    .line 192
    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNullCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    .line 192
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public NavigateToSearchResultDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 138
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0
.end method

.method public NavigateToSearchResultDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 152
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method public getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    return-object v0
.end method

.method public getSearchTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoading:Z

    return v0
.end method

.method public isLoadMoreFinished()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoadMoreFinished:Z

    return v0
.end method

.method public isNeedLoadMore()Z
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    .line 197
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->getHasMoreToLoad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    .line 196
    :goto_0
    return v0

    .line 199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 73
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->doSearch(ZLjava/lang/String;)V

    .line 75
    return-void
.end method

.method public loadMore()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoading:Z

    .line 161
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->loadMoreSearchResults()V

    .line 163
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSearchDataResultAdapter(Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 48
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSearchTag(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 82
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 87
    return-void
.end method

.method protected onUpdateFinished()V
    .locals 4

    .prologue
    .line 128
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    const-wide/16 v2, 0xfb4

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    const v0, 0x7f070b62

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->showError(I)V

    .line 134
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onUpdateFinished()V

    .line 135
    return-void
.end method

.method public search(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchTag"    # Ljava/lang/String;

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isNewSearchNeeded(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchHelper;->checkValidSearchTag(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSearchTag(Ljava/lang/String;)V

    .line 176
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->searchTag:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->doSearch(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v3, 0x0

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 91
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 119
    const-string v1, "SearchDataResultActivityViewModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpceted update type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->updateAdapter()V

    .line 124
    return-void

    .line 93
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 94
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoading:Z

    .line 95
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    if-nez v1, :cond_0

    .line 96
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 99
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 102
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 105
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 110
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->model:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->getSearchResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 111
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoading:Z

    .line 112
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoadMoreFinished:Z

    goto :goto_0

    .line 114
    :cond_4
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;->isLoadMoreFinished:Z

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
