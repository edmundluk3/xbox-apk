.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$2;
.super Ljava/lang/Object;
.source "PeopleHubInfoScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->banFromClub(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$2;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const-string v0, "Should not be possible due to all requests being singular"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 401
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 392
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 393
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$2;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    const v1, 0x7f07022a

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;I)V

    .line 394
    return-void
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 386
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070275

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$2;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v5, v5, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 387
    .local v0, "success":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$2;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Ljava/lang/String;)V

    .line 388
    return-void
.end method
