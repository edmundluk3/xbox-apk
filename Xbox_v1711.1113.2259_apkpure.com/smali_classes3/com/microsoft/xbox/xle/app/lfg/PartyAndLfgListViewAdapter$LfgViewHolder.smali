.class public Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PartyAndLfgListViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LfgViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private headerLayout:Landroid/widget/RelativeLayout;

.field private imageHeaderView:Landroid/widget/RelativeLayout;

.field private needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private rowHeader:Landroid/widget/RelativeLayout;

.field private rowItem:Landroid/widget/RelativeLayout;

.field private startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

.field private tagsList:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

.field private tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

.field private titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;Landroid/view/View;)V
    .locals 6
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .line 210
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 212
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0764

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    .line 213
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e075d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->imageHeaderView:Landroid/widget/RelativeLayout;

    .line 214
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e075f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 215
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e075e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 216
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0765

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 217
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0767

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 218
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0768

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 220
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0760

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 221
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0762

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 222
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0761

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 224
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0763

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->rowItem:Landroid/widget/RelativeLayout;

    .line 225
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0769

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    .line 227
    new-instance v2, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    invoke-direct {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    .line 229
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c0149

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 230
    .local v0, "backgroundColor":I
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 232
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    if-eqz v1, :cond_0

    .line 235
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v2

    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v0, v2, v5}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 237
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    const v3, 0x7f0e0766

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    .line 238
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    return-void

    :cond_0
    move v2, v0

    .line 235
    goto :goto_0
.end method

.method private bindTags(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 6
    .param p1, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 326
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 328
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergeTags(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;J)Z

    move-result v0

    .line 331
    .local v0, "shouldRefreshTags":Z
    if-eqz v0, :cond_0

    .line 332
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 333
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagUtil:Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->getMergedTags()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 334
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    .line 337
    .end local v0    # "shouldRefreshTags":Z
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;)V
    .locals 13
    .param p1, "lfgListItem"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v12, 0x7f020122

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 245
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 247
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->lfg:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 248
    .local v2, "lfg":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 250
    .local v1, "host":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->rowHeader:Landroid/widget/RelativeLayout;

    invoke-static {v6, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 251
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->rowItem:Landroid/widget/RelativeLayout;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 252
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->rowItem:Landroid/widget/RelativeLayout;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 253
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 254
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->imageHeaderView:Landroid/widget/RelativeLayout;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 255
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 256
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 258
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 259
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 260
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 261
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 263
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getStartTimeString()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 264
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->startTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 266
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 267
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->titleHostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 269
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v6

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v5

    .line 272
    .local v5, "selectedTitle":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v5, :cond_0

    iget-object v6, v5, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 273
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->titleGameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v7, v5, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-virtual {v6, v7, v12, v12}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 277
    .end local v5    # "selectedTitle":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :cond_0
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 278
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->sessionOwners()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 280
    if-eqz v1, :cond_1

    .line 281
    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    iget-object v6, v6, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->primaryColor:Ljava/lang/String;

    invoke-static {v6}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v4

    .line 282
    .local v4, "primaryColor":I
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 283
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->tagsListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setSelectedColor(I)V

    .line 284
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->clubTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v7, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->secondaryColor:Ljava/lang/String;

    invoke-static {v7}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setBackgroundColor(I)V

    .line 288
    .end local v4    # "primaryColor":I
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 290
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 291
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->text()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 300
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->isXuidConfirmed(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 301
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v8, 0x7f0706df

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 311
    :goto_1
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->bindTags(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 313
    return-void

    .line 293
    :cond_2
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 296
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 297
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->postedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v6, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 303
    :cond_4
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getRemainingNeedCount()I

    move-result v3

    .line 304
    .local v3, "needValue":I
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getConfirmedCount()I

    move-result v0

    .line 306
    .local v0, "confirmedCount":I
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->needCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-lez v3, :cond_5

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    :goto_2
    invoke-static {v7, v6, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 307
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->haveCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_1

    .line 306
    :cond_5
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v8, 0x7f0706e1

    invoke-virtual {v6, v8}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 190
    check-cast p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 317
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->access$300(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 318
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->access$400(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;

    .line 319
    .local v0, "item":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->access$300(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;

    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->lfg:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 323
    :cond_0
    return-void
.end method
