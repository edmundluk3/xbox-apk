.class public abstract Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;
.super Ljava/lang/Object;
.source "RtaDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/rta/RtaDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RtaPayload"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaPayload$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaPayload$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/Object;)Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;
    .locals 1
    .param p0, "eventPayload"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106
    new-instance v0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaPayload;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaPayload;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public abstract eventPayload()Ljava/lang/Object;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
