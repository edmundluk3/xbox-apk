.class public Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;
.super Landroid/widget/LinearLayout;
.source "XboxRemoteControl.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IViewUpdate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;,
        Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;
    }
.end annotation


# static fields
.field private static final STATE_IE_CONTROLS:I = 0x1

.field private static final STATE_NON_IE_CONTROLS:I = 0x0

.field private static final STATE_TV_CONTROLS:I = 0x2

.field private static final TAG:Ljava/lang/String; = "dpad"


# instance fields
.field private final VIBRATE_PATTERN:[J

.field private appInputContainer:Landroid/view/View;

.field private appInputSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private bButton:Landroid/view/View;

.field private final dPadContainer:Landroid/view/View;

.field private displayHeight:I

.field private displayWidth:I

.field private dpadLogic:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

.field private ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

.field private jackButton:Landroid/view/View;

.field private jillButton:Landroid/view/View;

.field private mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

.field private previousSentTouchFrame:Lcom/microsoft/xbox/smartglass/TouchFrame;

.field private touchPointerIds:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private xButton:Landroid/view/View;

.field private xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

.field private xenonButton:Landroid/view/View;

.field private yButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v7, 0x1

    const/4 v5, -0x1

    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const/4 v4, 0x2

    new-array v4, v4, [J

    fill-array-data v4, :array_0

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->VIBRATE_PATTERN:[J

    .line 68
    sget-object v4, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->NONE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dpadLogic:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    .line 73
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 74
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030289

    invoke-virtual {v2, v4, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 76
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 77
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    const v4, 0x7f0e0c07

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xenonButton:Landroid/view/View;

    .line 80
    const v4, 0x7f0e0c08

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jackButton:Landroid/view/View;

    .line 81
    const v4, 0x7f0e0c09

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jillButton:Landroid/view/View;

    .line 82
    const v4, 0x7f0e0c0e

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->bButton:Landroid/view/View;

    .line 83
    const v4, 0x7f0e0c0c

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xButton:Landroid/view/View;

    .line 84
    const v4, 0x7f0e0c0a

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->yButton:Landroid/view/View;

    .line 85
    const v4, 0x7f0e0c04

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 86
    const v4, 0x7f0e0c02

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->appInputContainer:Landroid/view/View;

    .line 87
    const v4, 0x7f0e0c03

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->appInputSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 88
    const v4, 0x7f0e07ff

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/MediaButtons;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    .line 89
    const v4, 0x7f0e07f6

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .line 90
    const v4, 0x7f0e0c05

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dPadContainer:Landroid/view/View;

    .line 92
    new-instance v4, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    .line 93
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->initialize(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;)V

    .line 95
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->initialize(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;Z)V

    .line 97
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$1;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    new-instance v3, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$2;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V

    .line 141
    .local v3, "xenonButtonGestureListener":Landroid/view/GestureDetector$SimpleOnGestureListener;
    new-instance v0, Landroid/view/GestureDetector;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v0, v4, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 142
    .local v0, "gestureDetector":Landroid/view/GestureDetector;
    invoke-virtual {v0, v7}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 143
    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 144
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xenonButton:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$3;

    invoke-direct {v5, p0, v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$3;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/GestureDetector;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 157
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jackButton:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jackButton:Landroid/view/View;

    sget-object v7, Lcom/microsoft/xbox/smartglass/GamePadButtons;->View:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-direct {v5, p0, v6, v7}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/View;Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 158
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jillButton:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jillButton:Landroid/view/View;

    sget-object v7, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Menu:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-direct {v5, p0, v6, v7}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/View;Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 159
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->bButton:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->bButton:Landroid/view/View;

    sget-object v7, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadB:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-direct {v5, p0, v6, v7}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/View;Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 160
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xButton:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xButton:Landroid/view/View;

    sget-object v7, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadX:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-direct {v5, p0, v6, v7}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/View;Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 161
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->yButton:Landroid/view/View;

    new-instance v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->yButton:Landroid/view/View;

    sget-object v7, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadY:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-direct {v5, p0, v6, v7}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/View;Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 164
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->updateView(Ljava/lang/Object;)V

    .line 165
    return-void

    .line 41
    :array_0
    .array-data 8
        0x0
        0x32
    .end array-data
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->vibrate()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->touchPointerIds:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;
    .param p1, "x1"    # F
    .param p2, "x2"    # F
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/microsoft/xbox/smartglass/TouchAction;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->buildTouchPoint(FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Lcom/microsoft/xbox/smartglass/TouchFrame;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/TouchFrame;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->sendTouchFrame(Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    return-void
.end method

.method private buildTouchPoint(FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "id"    # I
    .param p4, "touchAction"    # Lcom/microsoft/xbox/smartglass/TouchAction;

    .prologue
    .line 428
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isDPadXInverted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->displayWidth:I

    int-to-float v0, v0

    sub-float p1, v0, p1

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isDPadYInverted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 433
    iget v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->displayHeight:I

    int-to-float v0, v0

    sub-float p2, v0, p2

    .line 438
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/smartglass/TouchPoint;

    add-int/lit8 v1, p3, 0x1

    int-to-short v1, v1

    float-to-int v2, p1

    float-to-int v3, p2

    invoke-direct {v0, v1, p4, v2, v3}, Lcom/microsoft/xbox/smartglass/TouchPoint;-><init>(SLcom/microsoft/xbox/smartglass/TouchAction;II)V

    return-object v0
.end method

.method private captureTouchFrames()V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dpadLogic:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    sget-object v1, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CONSOLE_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    if-eq v0, v1, :cond_0

    .line 320
    const-string v0, "dpad"

    const-string v1, "using console-side dpad logic"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->displayWidth:I

    .line 323
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->displayHeight:I

    .line 324
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->touchPointerIds:Ljava/util/Vector;

    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dPadContainer:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 423
    sget-object v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CONSOLE_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dpadLogic:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    .line 425
    :cond_0
    return-void
.end method

.method private cleanupListeners()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xenonButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jackButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->jillButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->bButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dPadContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->bButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->yButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 191
    return-void
.end method

.method private sendTouchFrame(Lcom/microsoft/xbox/smartglass/TouchFrame;)V
    .locals 1
    .param p1, "touchFrame"    # Lcom/microsoft/xbox/smartglass/TouchFrame;

    .prologue
    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->previousSentTouchFrame:Lcom/microsoft/xbox/smartglass/TouchFrame;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->touchFramesEqual(Lcom/microsoft/xbox/smartglass/TouchFrame;Lcom/microsoft/xbox/smartglass/TouchFrame;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->asyncSendTouchFrame(Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    .line 452
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->previousSentTouchFrame:Lcom/microsoft/xbox/smartglass/TouchFrame;

    goto :goto_0
.end method

.method private touchFramesEqual(Lcom/microsoft/xbox/smartglass/TouchFrame;Lcom/microsoft/xbox/smartglass/TouchFrame;)Z
    .locals 6
    .param p1, "f1"    # Lcom/microsoft/xbox/smartglass/TouchFrame;
    .param p2, "f2"    # Lcom/microsoft/xbox/smartglass/TouchFrame;

    .prologue
    const/4 v3, 0x0

    .line 464
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 481
    :cond_0
    :goto_0
    return v3

    .line 470
    :cond_1
    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p2, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 471
    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 472
    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/TouchPoint;

    .line 473
    .local v1, "p1":Lcom/microsoft/xbox/smartglass/TouchPoint;
    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/smartglass/TouchPoint;

    .line 474
    .local v2, "p2":Lcom/microsoft/xbox/smartglass/TouchPoint;
    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->touchPointsEqual(Lcom/microsoft/xbox/smartglass/TouchPoint;Lcom/microsoft/xbox/smartglass/TouchPoint;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 471
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 478
    .end local v1    # "p1":Lcom/microsoft/xbox/smartglass/TouchPoint;
    .end local v2    # "p2":Lcom/microsoft/xbox/smartglass/TouchPoint;
    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private touchPointsEqual(Lcom/microsoft/xbox/smartglass/TouchPoint;Lcom/microsoft/xbox/smartglass/TouchPoint;)Z
    .locals 2
    .param p1, "p1"    # Lcom/microsoft/xbox/smartglass/TouchPoint;
    .param p2, "p2"    # Lcom/microsoft/xbox/smartglass/TouchPoint;

    .prologue
    .line 485
    iget-short v0, p1, Lcom/microsoft/xbox/smartglass/TouchPoint;->id:S

    iget-short v1, p2, Lcom/microsoft/xbox/smartglass/TouchPoint;->id:S

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/microsoft/xbox/smartglass/TouchPoint;->x:I

    iget v1, p2, Lcom/microsoft/xbox/smartglass/TouchPoint;->x:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/microsoft/xbox/smartglass/TouchPoint;->y:I

    iget v1, p2, Lcom/microsoft/xbox/smartglass/TouchPoint;->y:I

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/TouchPoint;->action:Lcom/microsoft/xbox/smartglass/TouchAction;

    iget-object v1, p2, Lcom/microsoft/xbox/smartglass/TouchPoint;->action:Lcom/microsoft/xbox/smartglass/TouchAction;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateBottomContainerVisibility()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 237
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->shouldShowMediaProgress()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isIeRunning()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isTvEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 238
    .local v0, "shouldShowButtonContainer":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->appInputContainer:Landroid/view/View;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 239
    return-void

    .end local v0    # "shouldShowButtonContainer":Z
    :cond_1
    move v0, v1

    .line 237
    goto :goto_0

    .line 238
    .restart local v0    # "shouldShowButtonContainer":Z
    :cond_2
    const/4 v1, 0x4

    goto :goto_1
.end method

.method private updateIeView()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->appInputSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->update()V

    .line 229
    return-void
.end method

.method private updateNonIeView()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->appInputSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->updateView(Ljava/lang/Object;)V

    .line 234
    return-void
.end method

.method private useAlternatedPadHandler()V
    .locals 4

    .prologue
    .line 250
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dpadLogic:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    sget-object v3, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CLIENT_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    if-eq v2, v3, :cond_0

    .line 251
    const-string v2, "dpad"

    const-string v3, "using client-side dpad logic"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    new-instance v1, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$4;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V

    .line 304
    .local v1, "gestureListener":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v0, Landroid/view/GestureDetector;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 305
    .local v0, "gestureDetector":Landroid/view/GestureDetector;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dPadContainer:Landroid/view/View;

    new-instance v3, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$5;

    invoke-direct {v3, p0, v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$5;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/GestureDetector;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 311
    sget-object v2, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CLIENT_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->dpadLogic:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    .line 313
    .end local v0    # "gestureDetector":Landroid/view/GestureDetector;
    .end local v1    # "gestureListener":Landroid/view/GestureDetector$OnGestureListener;
    :cond_0
    return-void
.end method

.method private vibrate()V
    .locals 3

    .prologue
    .line 243
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const-string v2, "vibrator"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 244
    .local v0, "v":Landroid/os/Vibrator;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->VIBRATE_PATTERN:[J

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 247
    :cond_0
    return-void
.end method


# virtual methods
.method public gotoControlMode()V
    .locals 1

    .prologue
    .line 197
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 199
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->updateBottomContainerVisibility()V

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isIeRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->updateIeView()V

    .line 206
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->updateNonIeView()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->onResume()V

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->load(Z)V

    .line 172
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->onPause()V

    .line 178
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->cleanupListeners()V

    .line 179
    return-void
.end method

.method public updateTvView(Z)V
    .locals 2
    .param p1, "tvEnabled"    # Z

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->updateBottomContainerVisibility()V

    .line 222
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 223
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 224
    return-void

    .line 222
    .end local v0    # "visibility":I
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateView(Ljava/lang/Object;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->xboxRemoteViewModel:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->useClientSideDPadHandler()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->useAlternatedPadHandler()V

    .line 216
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->gotoControlMode()V

    .line 217
    return-void

    .line 213
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->captureTouchFrames()V

    goto :goto_0
.end method
