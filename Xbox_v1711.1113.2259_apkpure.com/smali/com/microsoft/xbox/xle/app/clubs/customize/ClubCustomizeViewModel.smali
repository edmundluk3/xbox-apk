.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubCustomizeViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;
.implements Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;
.implements Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$TitleLoadAsyncTask;
    }
.end annotation


# static fields
.field private static final MAX_CHARACTER_DESCRIPTION_COUNT:I = 0x3e8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private clubColorPicker:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

.field private clubDescriptionDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

.field private clubDisplayImageDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

.field private clubNameSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

.field private colorList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingColors:Z

.field private isLoadingColorsError:Z

.field profileColorsRepository:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private tagSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;

.field private final titleData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private titleImageLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private final titleImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private titleLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubCustomizeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 95
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleImages:Ljava/util/List;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleData:Ljava/util/List;

    .line 98
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleImageLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 99
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->colorList:Ljava/util/List;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColorsError:Z

    .line 103
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V

    .line 104
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->onTitlesLoaded(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private isCustomImageUrl(Ljava/lang/String;)Z
    .locals 1
    .param p1, "imageUrl"    # Ljava/lang/String;

    .prologue
    .line 334
    if-eqz p1, :cond_0

    const-string v0, "file"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$launchTitleSelection$3(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Ljava/util/List;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
    .param p1, "titleInfoList"    # Ljava/util/List;

    .prologue
    .line 275
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 276
    .local v0, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .line 277
    .local v1, "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 279
    .end local v1    # "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 280
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setAssociatedTitles(Ljava/util/Set;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 285
    :cond_1
    return-void
.end method

.method static synthetic lambda$loadOverride$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Lcom/google/common/collect/ImmutableList;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
    .param p1, "colors"    # Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColors:Z

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->colorList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->colorList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 133
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->updateAdapter()V

    .line 134
    return-void
.end method

.method static synthetic lambda$loadOverride$1(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
    .param p1, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to download profile colors"

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColors:Z

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColorsError:Z

    .line 139
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->updateAdapter()V

    .line 140
    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .prologue
    .line 281
    const v0, 0x7f0701c3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 282
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating club titles"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method static synthetic lambda$onBackgroundImageSelected$7(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .prologue
    .line 327
    const v0, 0x7f0701bd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 328
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating background image"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    return-void
.end method

.method static synthetic lambda$onClubNameSelected$8(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 340
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    const v0, 0x7f0701bf

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 342
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating club name"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_0
    return-void
.end method

.method static synthetic lambda$onDisplayImageSelected$6(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .prologue
    .line 316
    const v0, 0x7f0701c1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 317
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating club display image"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    return-void
.end method

.method static synthetic lambda$onEditTextTextCompleted$5(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .prologue
    .line 304
    const v0, 0x7f0701c0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 305
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating club description"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    return-void
.end method

.method static synthetic lambda$onTagsSelected$4(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .prologue
    .line 295
    const v0, 0x7f0701c2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 296
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating club tags"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    return-void
.end method

.method static synthetic lambda$setCurrentColor$9(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .prologue
    .line 350
    const v0, 0x7f0701be

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 351
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating club color"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void
.end method

.method private onTitlesLoaded(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 382
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 403
    :goto_0
    return-void

    .line 386
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 387
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 388
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 389
    .local v0, "data":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleData:Ljava/util/List;

    new-instance v3, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;Z)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleImages:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->getBoxArt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 395
    .end local v0    # "data":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->updateTitleData()V

    .line 396
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->updateAdapter()V

    goto :goto_0

    .line 400
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get the title data."

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 382
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateTitleData()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 356
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v7

    .line 357
    .local v7, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 358
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->associatedTitles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 360
    .local v0, "associatedTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->unsortedEqual(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 361
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 362
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleImages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 364
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$TitleLoadAsyncTask;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    invoke-direct {v6, p0, v8, v4}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$TitleLoadAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Ljava/util/Collection;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$1;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 379
    .end local v0    # "associatedTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 377
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleImages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0
.end method


# virtual methods
.method public closeColorPickerDialog()V
    .locals 2

    .prologue
    .line 420
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubColorPicker:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 421
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubColorPicker:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .line 422
    return-void
.end method

.method public finishCustomization()V
    .locals 4

    .prologue
    .line 456
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubDone(J)V

    .line 458
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 460
    .local v0, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getFromScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;

    if-eqz v1, :cond_0

    .line 461
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->goBack()V

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;ZJ)V

    goto :goto_0
.end method

.method public getClubBackgroundImage()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 200
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 201
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getClubColor()I
    .locals 4
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    const v3, 0x7f0c0010

    .line 179
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 180
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 181
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    .line 182
    .local v1, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v2

    .line 184
    .end local v1    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :goto_0
    return v2

    .line 182
    .restart local v1    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_0

    .line 184
    .end local v1    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_0
.end method

.method public getClubDescription()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 212
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getClubDisplayImage()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 195
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getClubName()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 169
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getClubTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getSocialTags()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentColorObject()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 2

    .prologue
    .line 431
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 432
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 433
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    .line 435
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProfileColorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->colorList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTitleImages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleImages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColors:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCustomizeNameEnabled()Z
    .locals 3

    .prologue
    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 174
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public launchBackgroundImageSelection()V
    .locals 6

    .prologue
    .line 225
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 226
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubAddBackgroundImage(J)V

    .line 230
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubBackgroundImage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v2, 0x1

    .line 232
    .local v2, "removeBackgroundEnabled":Z
    :goto_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    invoke-direct {v1, v4, v5, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;-><init>(JZLcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 233
    .local v1, "params":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;
    const-class v3, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeImagePicker;

    invoke-virtual {p0, v3, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 234
    return-void

    .line 230
    .end local v1    # "params":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;
    .end local v2    # "removeBackgroundEnabled":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public launchClubRename()V
    .locals 5

    .prologue
    .line 439
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 440
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 441
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubEditName(J)V

    .line 442
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne v1, v2, :cond_0

    .line 443
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubNameSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    .line 444
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubNameSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 453
    :goto_0
    return-void

    .line 446
    :cond_0
    const-string v1, "Club rename only supported for secret/hidden clubs"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 447
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const v2, 0x7f0701b7

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0

    .line 450
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const v2, 0x7f070b6d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 451
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club was null when trying to rename"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public launchColorSelection()V
    .locals 4

    .prologue
    .line 406
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColorsError:Z

    if-eqz v1, :cond_0

    .line 407
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const v2, 0x7f07039f

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 416
    :goto_0
    return-void

    .line 409
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 410
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 411
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubChangeColor(J)V

    .line 413
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubColorPicker:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .line 414
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubColorPicker:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public launchDescriptionSelection()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    .line 250
    .local v8, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubEditDescription(J)V

    .line 252
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    .line 254
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/16 v4, 0x3e8

    .line 257
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubColor()I

    move-result v6

    move-object v5, v2

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubDescriptionDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .line 258
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubDescriptionDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 263
    :goto_0
    return-void

    .line 260
    :cond_0
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 261
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Null club while editing description"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public launchDisplayImageSelection()V
    .locals 6

    .prologue
    .line 216
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 217
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubDisplayImage(J)V

    .line 220
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;

    move-result-object v3

    invoke-direct {v1, v2, v4, v5, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;-><init>(Landroid/content/Context;JLcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubDisplayImageDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    .line 221
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubDisplayImageDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 222
    return-void
.end method

.method public launchTagsSelection()V
    .locals 5

    .prologue
    .line 237
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 238
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubEditTags(J)V

    .line 240
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->value()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v1, v2, v3, p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;-><init>(Landroid/content/Context;Ljava/util/Set;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog$TagsSelectedListener;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->tagSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;

    .line 241
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->tagSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 246
    :goto_0
    return-void

    .line 243
    :cond_0
    const v1, 0x7f070b6d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->showError(I)V

    .line 244
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Null club while editing tags"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public launchTitleSelection()V
    .locals 5

    .prologue
    .line 266
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v2

    if-nez v2, :cond_1

    .line 267
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 268
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 269
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCustomizeClubEditGames(J)V

    .line 271
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->titleData:Ljava/util/List;

    const/16 v3, 0x19

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;-><init>(Ljava/util/List;ILcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 286
    .local v1, "params":Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;
    const-class v2, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreen;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 288
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "params":Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;
    :cond_1
    return-void
.end method

.method protected loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 118
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Detail:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColors:Z

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isLoadingColorsError:Z

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->profileColorsRepository:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;

    .line 125
    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->getAllProfileColors()Lio/reactivex/Single;

    move-result-object v1

    .line 126
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 127
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 128
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 124
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 142
    return-void
.end method

.method public onBackgroundImageSelected(Ljava/lang/String;)V
    .locals 2
    .param p1, "imageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 323
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isCustomImageUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setLocalClubBackgroundImage(Ljava/lang/String;)V

    .line 331
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setClubBackgroundImage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    goto :goto_0
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 146
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->updateAdapter()V

    .line 164
    return-void

    .line 150
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 151
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 153
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->updateTitleData()V

    goto :goto_0

    .line 158
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 159
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club model changed to invalid state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onClubNameSelected(Ljava/lang/String;)V
    .locals 2
    .param p1, "clubName"    # Ljava/lang/String;

    .prologue
    .line 338
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubNameSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setClubName(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 345
    return-void
.end method

.method public onDisplayImageSelected(Ljava/lang/String;)V
    .locals 2
    .param p1, "imageUrl"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isCustomImageUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubDisplayImageDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 312
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setLocalClubDisplayImage(Ljava/lang/String;)V

    .line 320
    :goto_0
    return-void

    .line 314
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubDisplayImageDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setClubDisplayImage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    goto :goto_0
.end method

.method public onEditTextTextCompleted(Ljava/lang/String;)V
    .locals 2
    .param p1, "updatedText"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubDescriptionDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 303
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setClubDescription(Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 307
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubCustomizeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 109
    return-void
.end method

.method public onTagsSelected(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 292
    .local p1, "selectedTags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->tagSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->tagSelectionDialog:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeTagPickerDialog;

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setSocialTags(Ljava/util/Set;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 298
    return-void
.end method

.method public setCurrentColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
    .locals 2
    .param p1, "color"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setClubPreferredColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 353
    return-void
.end method
