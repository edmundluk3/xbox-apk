.class public final Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;
.super Ljava/lang/Object;
.source "ClubWatchRepositoryImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;


# static fields
.field private static final BEAM_QUERY:Ljava/lang/String; = "id:in:"

.field private static final MAX_PAGE_SIZE:I = 0x64

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final beamService:Lcom/microsoft/xbox/data/service/beam/BeamService;

.field private final clubModelManager:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

.field private final dataMapper:Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;

.field private final mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 0
    .param p1, "beamService"    # Lcom/microsoft/xbox/data/service/beam/BeamService;
    .param p2, "mediaHubService"    # Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .param p3, "dataMapper"    # Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;
    .param p4, "clubModelManager"    # Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .param p5, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->beamService:Lcom/microsoft/xbox/data/service/beam/BeamService;

    .line 46
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    .line 47
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;

    .line 48
    iput-object p4, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    .line 49
    iput-object p5, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 50
    return-void
.end method

.method static synthetic lambda$load$3(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;Landroid/support/v4/util/Pair;)Lio/reactivex/SingleSource;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;
    .param p1, "p"    # Landroid/support/v4/util/Pair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 64
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    iget-object v1, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/common/collect/ImmutableList;

    .line 66
    .local v1, "mediaHubBroadcasts":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;>;"
    invoke-static {v1}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {v0}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 68
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 70
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 71
    invoke-virtual {v2}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 72
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->beamService:Lcom/microsoft/xbox/data/service/beam/BeamService;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/data/service/beam/BeamService;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 74
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 75
    invoke-interface {v3}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 76
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {v0}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 78
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 79
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 80
    invoke-virtual {v2}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object v2

    .line 66
    return-object v2
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;)Z
    .locals 4
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p1, "mediaHubBroadcast"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->associatedTitles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->value()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;->titleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$null$1(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .param p0, "broadcastList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id:in:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-static {v1, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;)Z
    .locals 2
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p1, "beamServiceChannel"    # Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->audience()Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;->adult:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public load(J)Lio/reactivex/Single;
    .locals 5
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Single",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 54
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    const/4 v1, 0x0

    .line 57
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;->rxLoad(ZLjava/lang/Long;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->mediaHubService:Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    sget-object v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;->Mixer:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;

    const/16 v3, 0x64

    .line 58
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p1, p2, v2, v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->forClubAndProvider(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcastProvider;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getBroadcasts(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 59
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 56
    invoke-static {v0, v1, v2}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 61
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 56
    return-object v0
.end method
