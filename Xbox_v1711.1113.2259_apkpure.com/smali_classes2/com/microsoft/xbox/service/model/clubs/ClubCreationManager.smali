.class public Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
.super Ljava/lang/Object;
.source "ClubCreationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;,
        Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;,
        Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;
    }
.end annotation


# static fields
.field public static final MAX_CLUB_NAME_LENGTH:I = 0x7fffffff

.field public static final MIN_CLUB_NAME_LENGTH:I = 0x4

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

.field private final clubInformationListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;

.field private clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

.field private final clubNameReservedListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;

.field private clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

.field private final createdListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;

.field private selectedClubName:Ljava/lang/String;

.field private selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 1
    .param p1, "clubsByOwner"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .param p2, "clubNameReservation"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .param p3, "clubCreation"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubInformationListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;

    .line 85
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservedListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;

    .line 86
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->createdListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;

    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    .line 88
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    .line 89
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 90
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->name()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubName:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;)V
    .locals 0
    .param p1, "clubInformationListener"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubNameReservedListener"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "createdListener"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 75
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 76
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 77
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubInformationListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;

    .line 78
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservedListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;

    .line 79
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->createdListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;

    .line 80
    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubInformationListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    return-object p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservedListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v0
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->createdListener:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;

    return-object v0
.end method

.method private getNameErrorReasonForCode(I)I
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 162
    sparse-switch p1, :sswitch_data_0

    .line 177
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 164
    :sswitch_0
    const v0, 0x7f070190

    goto :goto_0

    .line 166
    :sswitch_1
    const v0, 0x7f07018f

    goto :goto_0

    .line 168
    :sswitch_2
    const v0, 0x7f0701a4

    goto :goto_0

    .line 170
    :sswitch_3
    const v0, 0x7f070188

    goto :goto_0

    .line 172
    :sswitch_4
    const v0, 0x7f0701a6

    goto :goto_0

    .line 174
    :sswitch_5
    const v0, 0x7f070194

    goto :goto_0

    .line 162
    nop

    :sswitch_data_0
    .sparse-switch
        0x3ed -> :sswitch_2
        0x3ee -> :sswitch_3
        0x3ef -> :sswitch_5
        0x3f0 -> :sswitch_4
        0x3f2 -> :sswitch_0
        0x3ff -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public createClub()V
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getSelectedClubName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getSelectedClubType()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateClubSubmit(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    .line 208
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getSelectedClubName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getSelectedClubType()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->execute()V

    .line 210
    :cond_0
    return-void
.end method

.method public getClubCreationNameErrorReason()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->code()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getNameErrorReasonForCode(I)I

    move-result v0

    .line 158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMaximumHiddenClubs()I
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->maximumSecretClubs()I

    move-result v0

    return v0
.end method

.method public getMaximumOpenAndClosedClubs()I
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->maximumOpenAndClosedClubs()I

    move-result v0

    return v0
.end method

.method public getNameReservationErrorReason()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->code()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getNameErrorReasonForCode(I)I

    move-result v0

    .line 149
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRemainingHiddenClubs()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingSecretClubs()I

    move-result v0

    return v0
.end method

.method public getRemainingOpenAndClosedClubs()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingOpenAndClosedClubs()I

    move-result v0

    return v0
.end method

.method public getSelectedClubName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubName:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedClubType()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    return-object v0
.end method

.method public isSelectedClubNameValid()Z
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubName:Ljava/lang/String;

    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 139
    :goto_0
    return v0

    .line 140
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadClubsInformation(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 202
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$1;)V

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;->load(Z)V

    .line 203
    return-void
.end method

.method public resetClubName()V
    .locals 1

    .prologue
    .line 181
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubName:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public setClubName(Ljava/lang/String;)V
    .locals 3
    .param p1, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 185
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 188
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubName:Ljava/lang/String;

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eq v0, v1, :cond_0

    .line 190
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;

    invoke-direct {v0, p0, p1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$1;)V

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->execute()V

    .line 193
    :cond_0
    iput-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 194
    return-void
.end method

.method public setSelectedClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z
    .locals 4
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 94
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 95
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 110
    const-string v0, "Unknown club type selected"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 112
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->selectedClubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 113
    iput-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubAccountsResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 114
    const/4 v0, 0x1

    :cond_1
    :goto_1
    return v0

    .line 98
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingOpenAndClosedClubs()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 103
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubsByOwnerResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingSecretClubs()I

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    iput-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->clubNameReservationResponse:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
