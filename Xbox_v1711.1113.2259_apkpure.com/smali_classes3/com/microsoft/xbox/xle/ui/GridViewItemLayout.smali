.class public Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;
.super Landroid/widget/LinearLayout;
.source "GridViewItemLayout.java"


# static fields
.field private static maxRowHeight:[I

.field private static numColumns:I


# instance fields
.field private position:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public static initItemLayout(II)V
    .locals 2
    .param p0, "numColumns"    # I
    .param p1, "itemCount"    # I

    .prologue
    .line 41
    sput p0, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->numColumns:I

    .line 42
    const/4 v0, 0x0

    .line 43
    .local v0, "numRows":I
    rem-int v1, p1, p0

    if-nez v1, :cond_0

    .line 44
    div-int v0, p1, p0

    .line 48
    :goto_0
    new-array v1, v0, [I

    sput-object v1, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->maxRowHeight:[I

    .line 49
    return-void

    .line 46
    :cond_0
    div-int v1, p1, p0

    add-int/lit8 v0, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 55
    sget v2, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->numColumns:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    sget-object v2, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->maxRowHeight:[I

    if-nez v2, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->position:I

    sget v3, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->numColumns:I

    div-int v1, v2, v3

    .line 62
    .local v1, "rowIndex":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->getMeasuredHeight()I

    move-result v0

    .line 64
    .local v0, "measuredHeight":I
    sget-object v2, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->maxRowHeight:[I

    aget v2, v2, v1

    if-le v0, v2, :cond_2

    .line 65
    sget-object v2, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->maxRowHeight:[I

    aput v0, v2, v1

    .line 68
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->getMeasuredWidth()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->maxRowHeight:[I

    aget v3, v3, v1

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/GridViewItemLayout;->setMeasuredDimension(II)V

    goto :goto_0
.end method
