.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AdapterBaseWithRecyclerView.java"


# static fields
.field protected static final LIST_VIEW_ANIMATION_NAME:Ljava/lang/String; = "ListView"


# instance fields
.field protected listView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 22
    return-void
.end method

.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 26
    return-void
.end method

.method private saveListPosition()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v4

    .line 49
    .local v4, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v5, :cond_1

    .line 50
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v1

    .line 51
    .local v1, "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    const/4 v0, 0x0

    .line 52
    .local v0, "index":I
    instance-of v5, v1, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v5, :cond_2

    .line 53
    check-cast v1, Landroid/support/v7/widget/LinearLayoutManager;

    .end local v1    # "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    .line 57
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 58
    .local v3, "v":Landroid/view/View;
    if-nez v3, :cond_3

    .line 59
    .local v2, "offset":I
    :goto_1
    invoke-virtual {v4, v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setListPosition(II)V

    .line 62
    .end local v0    # "index":I
    .end local v2    # "offset":I
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    return-void

    .line 54
    .restart local v0    # "index":I
    .restart local v1    # "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    :cond_2
    instance-of v5, v1, Landroid/support/v7/widget/GridLayoutManager;

    if-eqz v5, :cond_0

    .line 55
    check-cast v1, Landroid/support/v7/widget/GridLayoutManager;

    .end local v1    # "lm":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    invoke-virtual {v1}, Landroid/support/v7/widget/GridLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    goto :goto_0

    .line 58
    .restart local v3    # "v":Landroid/view/View;
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method protected abstract getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
.end method

.method protected abstract getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 35
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 36
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->saveListPosition()V

    .line 42
    return-void
.end method

.method protected restoreListPosition()V
    .locals 4

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    .line 66
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListPosition()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListOffset()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPositionWithOffset(Landroid/support/v7/widget/RecyclerView;II)V

    .line 70
    :cond_0
    return-void
.end method

.method protected setListView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p1, "listView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 31
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 79
    :cond_0
    return-void
.end method
