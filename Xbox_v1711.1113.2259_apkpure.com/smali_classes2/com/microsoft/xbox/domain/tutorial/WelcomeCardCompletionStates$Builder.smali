.class public abstract Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.super Ljava/lang/Object;
.source "WelcomeCardCompletionStates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
.end method

.method public abstract clubWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.end method

.method public abstract facebookWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.end method

.method public abstract redeemWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.end method

.method public abstract setupWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.end method

.method public abstract shopWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.end method

.method public abstract suggestedFriendsWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.end method
