.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
.super Ljava/lang/Object;
.source "RecommendationListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecommendationList"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final items:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;",
            ">;"
        }
    .end annotation
.end field

.field public final pagingInfo:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PagingInfo"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Title"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;)V
    .locals 0
    .param p1, "recommendationTitle"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "pagingInfo"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;",
            ">;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 74
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->title:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->items:Ljava/util/List;

    .line 76
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->pagingInfo:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;

    .line 77
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    .line 83
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    if-nez v3, :cond_2

    move v1, v2

    .line 84
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 86
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 87
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->items:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->items:Ljava/util/List;

    .line 88
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getItems()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->items:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->items:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 112
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 94
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->hashCode:I

    if-nez v0, :cond_0

    .line 95
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->hashCode:I

    .line 96
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->hashCode:I

    .line 97
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->items:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->hashCode:I

    .line 99
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
