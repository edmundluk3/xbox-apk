.class public final enum Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;
.super Ljava/lang/Enum;
.source "ApplicationBarManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppBarState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

.field public static final enum FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

.field public static final enum HALF:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

.field public static final enum HIDE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    const-string v1, "HALF"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->HALF:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->HIDE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->HALF:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->HIDE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->$VALUES:[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 62
    const-class v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->$VALUES:[Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    return-object v0
.end method
