.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubSearchDataTypes_ClubSearchResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultDescription:Ljava/lang/String;

.field private defaultDisplayImageUrl:Ljava/lang/String;

.field private defaultId:Ljava/lang/String;

.field private defaultName:Ljava/lang/String;

.field private defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

.field private defaultScore:Ljava/lang/Float;

.field private defaultTags:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTitles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final descriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final idAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final nameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final preferredColorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation
.end field

.field private final scoreAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final tagsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final titlesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 5
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultDisplayImageUrl:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultScore:Ljava/lang/Float;

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultTags:Lcom/google/common/collect/ImmutableList;

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultTitles:Lcom/google/common/collect/ImmutableList;

    .line 39
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 41
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    .line 45
    const-class v0, Ljava/lang/Float;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->scoreAdapter:Lcom/google/gson/TypeAdapter;

    .line 46
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    .line 47
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->titlesAdapter:Lcom/google/gson/TypeAdapter;

    .line 48
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->preferredColorAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;
    .locals 11
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v10, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v10, :cond_0

    .line 111
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 112
    const/4 v0, 0x0

    .line 168
    :goto_0
    return-object v0

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 116
    .local v1, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 117
    .local v2, "name":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 118
    .local v3, "description":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultDisplayImageUrl:Ljava/lang/String;

    .line 119
    .local v4, "displayImageUrl":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultScore:Ljava/lang/Float;

    .line 120
    .local v5, "score":Ljava/lang/Float;
    iget-object v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultTags:Lcom/google/common/collect/ImmutableList;

    .line 121
    .local v6, "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultTitles:Lcom/google/common/collect/ImmutableList;

    .line 122
    .local v7, "titles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 123
    .local v8, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 124
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v9

    .line 125
    .local v9, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v10, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v10, :cond_1

    .line 126
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 129
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 163
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 129
    :sswitch_0
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v10, "description"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v10, "displayImageUrl"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v10, "score"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v10, "tags"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v10, "titles"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v10, "preferredColor"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    .line 131
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "id":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 132
    .restart local v1    # "id":Ljava/lang/String;
    goto :goto_1

    .line 135
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "name":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 136
    .restart local v2    # "name":Ljava/lang/String;
    goto/16 :goto_1

    .line 139
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "description":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 140
    .restart local v3    # "description":Ljava/lang/String;
    goto/16 :goto_1

    .line 143
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "displayImageUrl":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 144
    .restart local v4    # "displayImageUrl":Ljava/lang/String;
    goto/16 :goto_1

    .line 147
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->scoreAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "score":Ljava/lang/Float;
    check-cast v5, Ljava/lang/Float;

    .line 148
    .restart local v5    # "score":Ljava/lang/Float;
    goto/16 :goto_1

    .line 151
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    check-cast v6, Lcom/google/common/collect/ImmutableList;

    .line 152
    .restart local v6    # "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 155
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->titlesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "titles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    check-cast v7, Lcom/google/common/collect/ImmutableList;

    .line 156
    .restart local v7    # "titles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 159
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->preferredColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    check-cast v8, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 160
    .restart local v8    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    goto/16 :goto_1

    .line 167
    .end local v9    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 168
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult;

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V

    goto/16 :goto_0

    .line 129
    :sswitch_data_0
    .sparse-switch
        -0x7a79030a -> :sswitch_3
        -0x66ca7c04 -> :sswitch_2
        -0x6552249e -> :sswitch_7
        -0x340fd6e5 -> :sswitch_6
        0xd1b -> :sswitch_0
        0x337a8b -> :sswitch_1
        0x363419 -> :sswitch_5
        0x6833e92 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDescription"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 60
    return-object p0
.end method

.method public setDefaultDisplayImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDisplayImageUrl"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultDisplayImageUrl:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public setDefaultId(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultId"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public setDefaultName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultName"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public setDefaultPreferredColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPreferredColor"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 80
    return-object p0
.end method

.method public setDefaultScore(Ljava/lang/Float;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultScore"    # Ljava/lang/Float;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultScore:Ljava/lang/Float;

    .line 68
    return-object p0
.end method

.method public setDefaultTags(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "defaultTags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultTags:Lcom/google/common/collect/ImmutableList;

    .line 72
    return-object p0
.end method

.method public setDefaultTitles(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "defaultTitles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->defaultTitles:Lcom/google/common/collect/ImmutableList;

    .line 76
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    if-nez p2, :cond_0

    .line 86
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 107
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 90
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 92
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 94
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 96
    const-string v0, "displayImageUrl"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->displayImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->displayImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 98
    const-string v0, "score"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->scoreAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->score()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 100
    const-string v0, "tags"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 102
    const-string v0, "titles"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->titlesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 104
    const-string v0, "preferredColor"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->preferredColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 106
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchResult$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;)V

    return-void
.end method
