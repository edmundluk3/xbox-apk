.class public Lcom/microsoft/xbox/xle/app/activity/GameDetailHeaderScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "GameDetailHeaderScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const-string v0, "GameDetailHeader"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 9
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameDetailHeaderScreen;->onCreateContentView()V

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameDetailHeaderScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 14
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 18
    const v0, 0x7f030111

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameDetailHeaderScreen;->setContentView(I)V

    .line 19
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method
