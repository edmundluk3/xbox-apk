.class public final Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;
.super Ljava/lang/Object;
.source "ServiceModule_ProvideGsonConvertorFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lretrofit2/converter/gson/GsonConverterFactory;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/service/ServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/ServiceModule;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/ServiceModule;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;->module:Lcom/microsoft/xbox/data/service/ServiceModule;

    .line 19
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/ServiceModule;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/ServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/ServiceModule;",
            ")",
            "Ldagger/internal/Factory",
            "<",
            "Lretrofit2/converter/gson/GsonConverterFactory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;-><init>(Lcom/microsoft/xbox/data/service/ServiceModule;)V

    return-object v0
.end method

.method public static proxyProvideGsonConvertorFactory(Lcom/microsoft/xbox/data/service/ServiceModule;)Lretrofit2/converter/gson/GsonConverterFactory;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/ServiceModule;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/ServiceModule;->provideGsonConvertorFactory()Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;->get()Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    return-object v0
.end method

.method public get()Lretrofit2/converter/gson/GsonConverterFactory;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;->module:Lcom/microsoft/xbox/data/service/ServiceModule;

    .line 24
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/ServiceModule;->provideGsonConvertorFactory()Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 23
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/converter/gson/GsonConverterFactory;

    return-object v0
.end method
