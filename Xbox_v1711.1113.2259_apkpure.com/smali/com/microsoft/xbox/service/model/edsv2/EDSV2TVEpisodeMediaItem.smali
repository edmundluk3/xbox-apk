.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2TVEpisodeMediaItem.java"


# instance fields
.field public Duration:Ljava/lang/String;

.field private DurationInMMSS:Ljava/lang/String;

.field public EpisodeNumber:I

.field public Networks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;",
            ">;"
        }
    .end annotation
.end field

.field public ParentalRating:Ljava/lang/String;

.field public ParentalRatingSystem:Ljava/lang/String;

.field public SeasonNumber:I

.field public ZuneId:Ljava/lang/String;

.field private network:Ljava/lang/String;

.field private parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ParentSeason"
    .end annotation
.end field

.field private parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ParentSeries"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 35
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->AllTimeAverageRating:F

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->AllTimeAverageRating:F

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Duration:Ljava/lang/String;

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->EpisodeNumber:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->EpisodeNumber:I

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Genres:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Genres:Ljava/util/List;

    move-object v0, p1

    .line 40
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Networks:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Networks:Ljava/util/ArrayList;

    move-object v0, p1

    .line 41
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ParentalRating:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ParentalRating:Ljava/lang/String;

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ParentalRatingSystem:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ParentalRatingSystem:Ljava/lang/String;

    move-object v0, p1

    .line 43
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->SeasonNumber:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->SeasonNumber:I

    move-object v0, p1

    .line 44
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ZuneId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ZuneId:Ljava/lang/String;

    move-object v0, p1

    .line 45
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    .line 46
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 48
    :cond_0
    const/16 v0, 0x3eb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->setMediaItemTypeFromInt(I)V

    .line 50
    return-void
.end method


# virtual methods
.method public getAirings()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Airings:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 2

    .prologue
    .line 118
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->DurationInMMSS:Ljava/lang/String;

    .line 123
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->DurationInMMSS:Ljava/lang/String;

    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Duration:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->durationStringToSeconds(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getTimeStringMMSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->DurationInMMSS:Ljava/lang/String;

    goto :goto_0
.end method

.method public getEpisodeNumber()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->EpisodeNumber:I

    return v0
.end method

.method public getNetwork()Ljava/lang/String;
    .locals 4

    .prologue
    .line 91
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->network:Ljava/lang/String;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Networks:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Networks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->Networks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;

    .line 94
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;->Name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 97
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->network:Ljava/lang/String;

    .line 100
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->network:Ljava/lang/String;

    return-object v2
.end method

.method public getParentSeason()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    return-object v0
.end method

.method public getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    return-object v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ParentalRating:Ljava/lang/String;

    return-object v0
.end method

.method public getSeasonCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->ID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSeasonNumber()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->SeasonNumber:I

    return v0
.end method

.method public getSeasonTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->Name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSeriesCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ID:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->ZuneId:Ljava/lang/String;

    return-object v0
.end method

.method public setParentSeaon(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 105
    return-void
.end method

.method public setParentSeries(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    .line 84
    return-void
.end method
