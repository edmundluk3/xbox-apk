.class Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "FeedItemActionsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemRootRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$1;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;-><init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)V

    return-void
.end method

.method private getXuilds(Ljava/util/ArrayList;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    const/4 v1, 0x0

    .line 188
    .local v1, "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 189
    new-instance v1, Ljava/util/HashSet;

    .end local v1    # "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 190
    .restart local v1    # "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .line 191
    .local v0, "action":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->xuid:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 194
    .end local v0    # "action":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    :cond_0
    return-object v1
.end method

.method private mergeWithProfileUsers(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    .local p1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    .local p2, "profileUsers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;>;"
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 173
    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 174
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    .line 175
    .local v2, "profileUser":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->id:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 177
    .end local v2    # "profileUser":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .line 178
    .local v0, "action":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    .line 179
    .restart local v2    # "profileUser":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;
    if-eqz v2, :cond_1

    .line 180
    iput-object v2, v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;->profileUser:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    goto :goto_1

    .line 184
    .end local v0    # "action":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .end local v1    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;>;"
    .end local v2    # "profileUser":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;
    :cond_2
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    .line 142
    .local v1, "mgr":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->access$100(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->access$200(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->getErrorCode()J

    move-result-wide v6

    invoke-interface {v1, v5, v6, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFeedItemActions(Ljava/lang/String;J)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v3

    .line 143
    .local v3, "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    if-eqz v3, :cond_1

    .line 144
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->access$200(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->getActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->getXuilds(Ljava/util/ArrayList;)Ljava/util/Set;

    move-result-object v4

    .line 145
    .local v4, "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 148
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->access$300()[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getProfileUsers(Ljava/util/Collection;[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;

    move-result-object v2

    .line 149
    .local v2, "profileUsers":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    if-eqz v2, :cond_0

    .line 150
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->access$200(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->getActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;->profileUsers:Ljava/util/ArrayList;

    invoke-direct {p0, v5, v6}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->mergeWithProfileUsers(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v2    # "profileUsers":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->reverseLists()V

    .line 158
    .end local v4    # "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    return-object v3

    .line 152
    .restart local v4    # "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->access$400()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Failed getting profile setting"

    invoke-static {v5, v6, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->buildData()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->access$200(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->getErrorCode()J

    move-result-wide v0

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;->this$0:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 164
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 137
    return-void
.end method
