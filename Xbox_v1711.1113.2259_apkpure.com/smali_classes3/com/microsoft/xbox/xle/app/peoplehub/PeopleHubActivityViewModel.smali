.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "PeopleHubActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private gamerTag:Ljava/lang/String;

.field private headerBackgroundColor:I

.field private isLoadingProfile:Z

.field private loadProfileAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

.field private profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 29
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "profileXuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 31
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleHubActivityScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 32
    return-void

    .line 30
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->isLoadingProfile:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->updateAdapter()V

    return-void
.end method


# virtual methods
.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->gamerTag:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderBackgroundColor()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->headerBackgroundColor:I

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->isLoadingProfile:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->loadProfileAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->loadProfileAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;->cancel()V

    .line 69
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->loadProfileAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->loadProfileAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;->load(Z)V

    .line 71
    return-void
.end method

.method public onLoadUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const v3, 0x7f0c0149

    .line 74
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onLoadUserProfile Completed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->isLoadingProfile:Z

    .line 77
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 93
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->updateAdapter()V

    .line 94
    return-void

    .line 81
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->gamerTag:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfilePreferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    .line 83
    .local v0, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColor()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->headerBackgroundColor:I

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    .line 88
    .end local v0    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :pswitch_1
    const-string v1, ""

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->gamerTag:Ljava/lang/String;

    .line 89
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->headerBackgroundColor:I

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleHubActivityScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 49
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->loadProfileAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;->loadProfileAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel$LoadProfileAsyncTask;->cancel()V

    .line 56
    :cond_0
    return-void
.end method
