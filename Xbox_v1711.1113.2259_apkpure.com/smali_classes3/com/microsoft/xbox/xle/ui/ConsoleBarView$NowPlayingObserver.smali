.class Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;
.super Ljava/lang/Object;
.source "ConsoleBarView.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NowPlayingObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 688
    return-void
.end method


# virtual methods
.method public register()V
    .locals 1

    .prologue
    .line 691
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 692
    return-void
.end method

.method public unRegister()V
    .locals 1

    .prologue
    .line 695
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 696
    return-void
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 700
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    if-eqz v4, :cond_0

    .line 701
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 702
    .local v1, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    .line 703
    .local v2, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    .line 705
    .local v0, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    sget-object v4, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v4, :cond_1

    .line 706
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$800(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;I)V

    .line 718
    .end local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .end local v1    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .end local v2    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_0
    return-void

    .line 707
    .restart local v0    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .restart local v1    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .restart local v2    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v2, v4, :cond_2

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    if-eq v2, v4, :cond_2

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v2, v4, :cond_0

    .line 708
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$900(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Ljava/util/Hashtable;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 709
    .local v3, "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    if-nez v3, :cond_3

    .line 710
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .end local v3    # "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$1000(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;-><init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V

    .line 711
    .restart local v3    # "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->load(Z)V

    .line 712
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$NowPlayingObserver;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$900(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Ljava/util/Hashtable;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    :cond_3
    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method
