.class Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;
.super Ljava/lang/Object;
.source "UserGamercardRequest.java"


# instance fields
.field private final SERVICES_PATH_FORMAT:Ljava/lang/String;

.field private final SERVICES_PROFILE_SUBDOMAIN:Ljava/lang/String;

.field private final _container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

.field private final _requestId:I

.field private final _tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

.field private final metric:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILcom/microsoft/xbox/smartglass/controls/WebComponentContainer;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "container"    # Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;
    .param p3, "tracker"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, "profile"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->SERVICES_PROFILE_SUBDOMAIN:Ljava/lang/String;

    .line 13
    const-string v0, "/users/xuid(%s)/gamercard"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->SERVICES_PATH_FORMAT:Ljava/lang/String;

    .line 14
    const-string v0, "Information.GetUserGamercard"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->metric:Ljava/lang/String;

    .line 21
    iput p1, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_requestId:I

    .line 22
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    .line 23
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;

    .prologue
    .line 11
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_requestId:I

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;)Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    return-object v0
.end method


# virtual methods
.method public send(Ljava/lang/String;)V
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v6

    const-string v7, "Information.GetUserGamercard"

    iget v8, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_requestId:I

    invoke-virtual {v6, v7, v8}, Lcom/microsoft/xbox/smartglass/MetricsManager;->start(Ljava/lang/String;I)V

    .line 30
    :try_start_0
    const-string v6, "/users/xuid(%s)/gamercard"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 31
    .local v3, "path":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v7

    const-string v8, "profile"

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->getXliveServiceUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 34
    .local v5, "servicesUrl":Ljava/lang/String;
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    const-string v6, "GET"

    const/4 v7, 0x1

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v8

    iget-object v8, v8, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->xliveAudienceUri:Ljava/lang/String;

    invoke-direct {v1, v6, v5, v7, v8}, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 37
    .local v1, "jsonRequest":Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    new-instance v4, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    new-instance v6, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest$1;

    invoke-direct {v6, p0}, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;)V

    invoke-direct {v4, v1, v6}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;-><init>(Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;)V

    .line 65
    .local v4, "request":Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    iget-object v6, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->add(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 66
    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->runAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .end local v1    # "jsonRequest":Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "request":Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    .end local v5    # "servicesUrl":Ljava/lang/String;
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "exception":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to get user gamercard. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "message":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget v7, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_requestId:I

    invoke-interface {v6, v7, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v6

    const-string v7, "Information.GetUserGamercard"

    iget v8, p0, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->_requestId:I

    sget-object v9, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v6, v7, v8, v9, v2}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
