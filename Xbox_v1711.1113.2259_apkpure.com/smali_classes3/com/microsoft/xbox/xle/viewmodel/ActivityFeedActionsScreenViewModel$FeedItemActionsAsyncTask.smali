.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedActionsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FeedItemActionsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field private final model:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V
    .locals 1

    .prologue
    .line 849
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 850
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->model:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    .line 851
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$1;

    .prologue
    .line 849
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 855
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->model:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->getModel(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 874
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->model:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->getModel(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 865
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 860
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 861
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 879
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 880
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 849
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel$FeedItemActionsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 870
    return-void
.end method
