.class public Lcom/microsoft/cll/android/Cll;
.super Ljava/lang/Object;
.source "Cll.java"

# interfaces
.implements Lcom/microsoft/telemetry/IChannel;
.implements Lcom/microsoft/cll/android/ICll;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/cll/android/Cll$EventLatency;,
        Lcom/microsoft/cll/android/Cll$EventPersistence;
    }
.end annotation


# instance fields
.field protected cll:Lcom/microsoft/cll/android/ICll;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/PartA;Lcom/microsoft/cll/android/CorrelationVector;)V
    .locals 1
    .param p1, "iKey"    # Ljava/lang/String;
    .param p2, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p3, "eventDir"    # Ljava/lang/String;
    .param p4, "partA"    # Lcom/microsoft/cll/android/PartA;
    .param p5, "correlationVector"    # Lcom/microsoft/cll/android/CorrelationVector;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1, p2, p3, p4, p5}, Lcom/microsoft/cll/android/SingletonCll;->getInstance(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/PartA;Lcom/microsoft/cll/android/CorrelationVector;)Lcom/microsoft/cll/android/ICll;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    .line 25
    return-void
.end method


# virtual methods
.method public SubscribeCllEvents(Lcom/microsoft/cll/android/ICllEvents;)V
    .locals 1
    .param p1, "cllEvents"    # Lcom/microsoft/cll/android/ICllEvents;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ICll;->SubscribeCllEvents(Lcom/microsoft/cll/android/ICllEvents;)V

    .line 100
    return-void
.end method

.method public getAppUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0}, Lcom/microsoft/cll/android/ICll;->getAppUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCorrelationVector()Lcom/microsoft/cll/android/CorrelationVector;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    check-cast v0, Lcom/microsoft/cll/android/SingletonCll;

    iget-object v0, v0, Lcom/microsoft/cll/android/SingletonCll;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    return-object v0
.end method

.method public varargs log(Lcom/microsoft/cll/android/PreSerializedEvent;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/cll/android/PreSerializedEvent;
    .param p3, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/cll/android/PreSerializedEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1, p2, p3}, Lcom/microsoft/cll/android/ICll;->log(Lcom/microsoft/cll/android/PreSerializedEvent;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 55
    return-void
.end method

.method public varargs log(Lcom/microsoft/telemetry/Base;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/telemetry/Base;
    .param p3, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1, p2, p3}, Lcom/microsoft/cll/android/ICll;->log(Lcom/microsoft/telemetry/Base;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 65
    return-void
.end method

.method public log(Lcom/microsoft/telemetry/Base;Ljava/util/Map;)V
    .locals 1
    .param p1, "telemetry"    # Lcom/microsoft/telemetry/Base;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p2, "tags":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/cll/android/ICll;->log(Lcom/microsoft/telemetry/Base;Ljava/util/Map;)V

    .line 70
    return-void
.end method

.method public varargs log(Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/telemetry/Base;
    .param p2, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/cll/android/ICll;->log(Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 60
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0}, Lcom/microsoft/cll/android/ICll;->pause()V

    .line 40
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0}, Lcom/microsoft/cll/android/ICll;->resume()V

    .line 45
    return-void
.end method

.method public send()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0}, Lcom/microsoft/cll/android/ICll;->send()V

    .line 75
    return-void
.end method

.method public setAppUserId(Ljava/lang/String;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ICll;->setAppUserId(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public setDebugVerbosity(Lcom/microsoft/cll/android/Verbosity;)V
    .locals 1
    .param p1, "verbosity"    # Lcom/microsoft/cll/android/Verbosity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ICll;->setDebugVerbosity(Lcom/microsoft/cll/android/Verbosity;)V

    .line 50
    return-void
.end method

.method public setEndpointUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ICll;->setEndpointUrl(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public setExperimentId(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ICll;->setExperimentId(Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public setXuidCallback(Lcom/microsoft/cll/android/ITicketCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/cll/android/ITicketCallback;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ICll;->setXuidCallback(Lcom/microsoft/cll/android/ITicketCallback;)V

    .line 118
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0}, Lcom/microsoft/cll/android/ICll;->start()V

    .line 30
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0}, Lcom/microsoft/cll/android/ICll;->stop()V

    .line 35
    return-void
.end method

.method public synchronize()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0}, Lcom/microsoft/cll/android/ICll;->synchronize()V

    .line 95
    return-void
.end method

.method public useLegacyCS(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/cll/android/Cll;->cll:Lcom/microsoft/cll/android/ICll;

    invoke-interface {v0, p1}, Lcom/microsoft/cll/android/ICll;->useLegacyCS(Z)V

    .line 85
    return-void
.end method
