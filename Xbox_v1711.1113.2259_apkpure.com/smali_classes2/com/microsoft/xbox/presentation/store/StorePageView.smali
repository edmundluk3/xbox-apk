.class public Lcom/microsoft/xbox/presentation/store/StorePageView;
.super Lcom/microsoft/xbox/presentation/base/ReactActivityBase;
.source "StorePageView.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/base/ReactActivityBase;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/microsoft/xbox/presentation/store/StorePageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchOptions()Landroid/os/Bundle;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 26
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v0, "launchOptions":Landroid/os/Bundle;
    return-object v0
.end method

.method public getLayoutId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 33
    const/4 v0, 0x0

    return-object v0
.end method

.method public getModuleName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 20
    const-string v0, "StorePage"

    return-object v0
.end method

.method public onCreateFinished()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method
