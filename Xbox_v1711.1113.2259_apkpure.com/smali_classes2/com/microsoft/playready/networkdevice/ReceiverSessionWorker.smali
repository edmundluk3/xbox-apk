.class Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;
.super Ljava/lang/Object;
.source "ReceiverSessionWorker.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/microsoft/playready/networkdevice/IReceiverSession;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsComplete:Z

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPrevSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;

.field private final mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

.field private final mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/networkdevice/Receiver;Lcom/microsoft/playready/networkdevice/TransmitterInfo;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V
    .locals 1
    .param p1, "receiver"    # Lcom/microsoft/playready/networkdevice/Receiver;
    .param p2, "transmitter"    # Lcom/microsoft/playready/networkdevice/TransmitterInfo;
    .param p3, "session"    # Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    .line 35
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    .line 36
    iput-object p3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mPrevSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .line 37
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    .line 38
    return-void
.end method


# virtual methods
.method addReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;

    .prologue
    .line 46
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    monitor-enter v1

    .line 48
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mIsComplete:Z

    if-eqz v0, :cond_0

    .line 50
    invoke-interface {p1}, Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;->onComplete()V

    .line 56
    :goto_0
    monitor-exit v1

    .line 57
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public call()Lcom/microsoft/playready/networkdevice/IReceiverSession;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v2, 0x0

    .line 81
    .local v2, "session":Lcom/microsoft/playready/networkdevice/IReceiverSession;
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    invoke-virtual {v3}, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->tryToGetConnectionData()V

    .line 88
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mPrevSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    if-nez v3, :cond_0

    .line 90
    new-instance v2, Lcom/microsoft/playready/networkdevice/ReceiverSession;

    .end local v2    # "session":Lcom/microsoft/playready/networkdevice/IReceiverSession;
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    invoke-virtual {v3}, Lcom/microsoft/playready/networkdevice/Receiver;->getPlayReadyFactory()Lcom/microsoft/playready/networkdevice/IPRNDFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mTransmitter:Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    iget-object v5, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mReceiver:Lcom/microsoft/playready/networkdevice/Receiver;

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/playready/networkdevice/ReceiverSession;-><init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;Lcom/microsoft/playready/networkdevice/TransmitterInfo;Lcom/microsoft/playready/networkdevice/Receiver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 105
    .restart local v2    # "session":Lcom/microsoft/playready/networkdevice/IReceiverSession;
    :goto_0
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    monitor-enter v4

    .line 107
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mIsComplete:Z

    .line 109
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;

    .line 111
    .local v1, "listener":Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;
    invoke-interface {v1}, Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;->onComplete()V

    goto :goto_1

    .line 113
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 94
    :cond_0
    :try_start_2
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mPrevSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    check-cast v3, Lcom/microsoft/playready/networkdevice/ReceiverSession;

    invoke-virtual {v3}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->restart()V

    .line 95
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mPrevSession:Lcom/microsoft/playready/networkdevice/IReceiverSession;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 113
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 116
    return-object v2

    .line 105
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "session":Lcom/microsoft/playready/networkdevice/IReceiverSession;
    :catchall_1
    move-exception v3

    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    monitor-enter v4

    .line 107
    const/4 v5, 0x1

    :try_start_4
    iput-boolean v5, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mIsComplete:Z

    .line 109
    iget-object v5, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;

    .line 111
    .restart local v1    # "listener":Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;
    invoke-interface {v1}, Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;->onComplete()V

    goto :goto_2

    .line 113
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;
    :catchall_2
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v3
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->call()Lcom/microsoft/playready/networkdevice/IReceiverSession;

    move-result-object v0

    return-object v0
.end method

.method removeReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;

    .prologue
    .line 65
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    monitor-enter v1

    .line 67
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/ReceiverSessionWorker;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 68
    monitor-exit v1

    .line 69
    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
