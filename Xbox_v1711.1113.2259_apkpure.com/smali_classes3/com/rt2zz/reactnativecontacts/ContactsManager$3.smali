.class Lcom/rt2zz/reactnativecontacts/ContactsManager$3;
.super Ljava/lang/Object;
.source "ContactsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rt2zz/reactnativecontacts/ContactsManager;->getPhotoForId(Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rt2zz/reactnativecontacts/ContactsManager;

.field final synthetic val$callback:Lcom/facebook/react/bridge/Callback;

.field final synthetic val$contactId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/rt2zz/reactnativecontacts/ContactsManager;Ljava/lang/String;Lcom/facebook/react/bridge/Callback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/rt2zz/reactnativecontacts/ContactsManager;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;->this$0:Lcom/rt2zz/reactnativecontacts/ContactsManager;

    iput-object p2, p0, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;->val$contactId:Ljava/lang/String;

    iput-object p3, p0, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;->val$callback:Lcom/facebook/react/bridge/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 106
    iget-object v4, p0, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;->this$0:Lcom/rt2zz/reactnativecontacts/ContactsManager;

    invoke-static {v4}, Lcom/rt2zz/reactnativecontacts/ContactsManager;->access$200(Lcom/rt2zz/reactnativecontacts/ContactsManager;)Lcom/facebook/react/bridge/ReactApplicationContext;

    move-result-object v1

    .line 107
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 108
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;

    invoke-direct {v0, v2}, Lcom/rt2zz/reactnativecontacts/ContactsProvider;-><init>(Landroid/content/ContentResolver;)V

    .line 109
    .local v0, "contactsProvider":Lcom/rt2zz/reactnativecontacts/ContactsProvider;
    iget-object v4, p0, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;->val$contactId:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->getPhotoUriFromContactId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 111
    .local v3, "photoUri":Ljava/lang/String;
    iget-object v4, p0, Lcom/rt2zz/reactnativecontacts/ContactsManager$3;->val$callback:Lcom/facebook/react/bridge/Callback;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-interface {v4, v5}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    .line 112
    return-void
.end method
