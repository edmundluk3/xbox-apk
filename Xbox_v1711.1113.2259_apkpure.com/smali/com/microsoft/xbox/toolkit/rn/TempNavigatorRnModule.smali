.class final Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "TempNavigatorRnModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithPeopleHubPersonSummary;,
        Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;
    }
.end annotation


# static fields
.field private static final NAME:Ljava/lang/String; = "TempNavigator"


# direct methods
.method constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 38
    return-void
.end method

.method private goBack()V
    .locals 1

    .prologue
    .line 124
    invoke-static {}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$$Lambda$4;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 131
    return-void
.end method

.method private goToTournamentTeamDetails(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 2
    .param p1, "moduleName"    # Ljava/lang/String;
    .param p2, "props"    # Lcom/facebook/react/bridge/ReadableMap;

    .prologue
    .line 104
    const-string v1, "TournamentTeamDetailsPage"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 105
    .local v0, "isTeamDetailsPage":Z
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 107
    if-nez v0, :cond_0

    .line 121
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$$Lambda$3;->lambdaFactory$(Lcom/facebook/react/bridge/ReadableMap;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic lambda$goBack$3()V
    .locals 3

    .prologue
    .line 126
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :goto_0
    return-void

    .line 127
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_0
    move-exception v0

    .line 128
    .restart local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v1, "TempNavigator"

    const-string v2, "GoBack failed"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$goToTournamentTeamDetails$2(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 8
    .param p0, "props"    # Lcom/facebook/react/bridge/ReadableMap;

    .prologue
    .line 112
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView;

    const/4 v3, 0x1

    new-instance v4, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;

    const-string v0, "organizer"

    .line 116
    invoke-interface {p0, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "id"

    .line 117
    invoke-interface {p0, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v0, "teamId"

    .line 118
    invoke-interface {p0, v0}, Lcom/facebook/react/bridge/ReadableMap;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "teamId"

    invoke-interface {p0, v0}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v7, "isPendingSession"

    .line 119
    invoke-interface {p0, v7}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-direct {v4, v5, v6, v0, v7}, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 112
    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    return-void

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$messagePeople$1()V
    .locals 3

    .prologue
    .line 99
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 100
    return-void
.end method

.method static synthetic lambda$showShareDialog$0(Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 4
    .param p0, "mapWithItem"    # Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/toolkit/EmptyViewModel;->getInstance()Lcom/microsoft/xbox/toolkit/EmptyViewModel;

    move-result-object v1

    .line 55
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;->access$000(Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shareRoot:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Tournament:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 53
    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    return-void
.end method


# virtual methods
.method public dismissModule()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule;->goBack()V

    .line 80
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "TempNavigator"

    return-object v0
.end method

.method public messagePeople(Lcom/facebook/react/bridge/ReadableArray;)V
    .locals 6
    .param p1, "people"    # Lcom/facebook/react/bridge/ReadableArray;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 84
    new-instance v2, Lcom/microsoft/xbox/toolkit/MultiSelection;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/MultiSelection;-><init>()V

    .line 86
    .local v2, "selectedPeople":Lcom/microsoft/xbox/toolkit/MultiSelection;, "Lcom/microsoft/xbox/toolkit/MultiSelection<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 87
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/ReadableArray;->getMap(I)Lcom/facebook/react/bridge/ReadableMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithPeopleHubPersonSummary;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithPeopleHubPersonSummary;

    .line 89
    .local v1, "map":Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithPeopleHubPersonSummary;
    if-eqz v1, :cond_0

    .line 90
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    new-instance v4, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithPeopleHubPersonSummary;->access$100(Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithPeopleHubPersonSummary;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    .line 86
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    const-string v3, "Failed to get deserialize people"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1

    .line 96
    .end local v1    # "map":Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithPeopleHubPersonSummary;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$$Lambda$2;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 101
    return-void
.end method

.method public popModule()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule;->goBack()V

    .line 70
    return-void
.end method

.method public presentModule(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 0
    .param p1, "moduleName"    # Ljava/lang/String;
    .param p2, "props"    # Lcom/facebook/react/bridge/ReadableMap;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule;->goToTournamentTeamDetails(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;)V

    .line 75
    return-void
.end method

.method public pushModule(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 0
    .param p1, "moduleName"    # Ljava/lang/String;
    .param p2, "props"    # Lcom/facebook/react/bridge/ReadableMap;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule;->goToTournamentTeamDetails(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;)V

    .line 65
    return-void
.end method

.method public showShareDialog(Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 3
    .param p1, "feedItem"    # Lcom/facebook/react/bridge/ReadableMap;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;

    .line 49
    .local v0, "mapWithItem":Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;->access$000(Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 50
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;->access$000(Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    .line 51
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/rn/TempNavigatorRnModule$NativeMapWithProfileRecentItem;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 52
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t deserialize item correctly: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method
