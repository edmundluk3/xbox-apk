.class public Lcom/microsoft/xbox/xle/app/activity/TVEpisodeHeaderScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "TVEpisodeHeaderScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string v0, "TVEpisodeHeader"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 25
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeHeaderScreen;->onCreateContentView()V

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeHeaderScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 28
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f03024e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeHeaderScreen;->setContentView(I)V

    .line 33
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method
