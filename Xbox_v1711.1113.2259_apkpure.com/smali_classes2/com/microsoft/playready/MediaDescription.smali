.class public final Lcom/microsoft/playready/MediaDescription;
.super Ljava/lang/Object;
.source "MediaDescription.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaDescription"


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mStreamList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/MediaStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;)V
    .locals 5
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/playready/MediaDescription;->mStreamList:Ljava/util/ArrayList;

    .line 55
    iput-object p1, p0, Lcom/microsoft/playready/MediaDescription;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 56
    iget-object v2, p0, Lcom/microsoft/playready/MediaDescription;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    invoke-virtual {v2}, Lcom/microsoft/playready/Native_Class10;->method_4()I

    move-result v1

    .line 57
    .local v1, "streamCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 61
    return-void

    .line 59
    :cond_0
    iget-object v2, p0, Lcom/microsoft/playready/MediaDescription;->mStreamList:Ljava/util/ArrayList;

    new-instance v3, Lcom/microsoft/playready/MediaStream;

    iget-object v4, p0, Lcom/microsoft/playready/MediaDescription;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    invoke-direct {v3, v4, v0}, Lcom/microsoft/playready/MediaStream;-><init>(Lcom/microsoft/playready/Native_Class10;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method commitMediaSelectionChangeToNative()V
    .locals 5

    .prologue
    .line 42
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/playready/MediaDescription;->getStreamCount()I

    move-result v4

    if-lt v0, v4, :cond_0

    .line 52
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0, v0}, Lcom/microsoft/playready/MediaDescription;->getStreamAt(I)Lcom/microsoft/playready/MediaStream;

    move-result-object v3

    .line 45
    .local v3, "stream":Lcom/microsoft/playready/MediaStream;
    invoke-virtual {v3}, Lcom/microsoft/playready/MediaStream;->shouldBeSelected()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/playready/MediaStream;->select(Ljava/lang/Boolean;)V

    .line 46
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    invoke-virtual {v3}, Lcom/microsoft/playready/MediaStream;->getRepresentationCount()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {v3, v1}, Lcom/microsoft/playready/MediaStream;->getRepresentationAt(I)Lcom/microsoft/playready/MediaRepresentation;

    move-result-object v2

    .line 49
    .local v2, "rep":Lcom/microsoft/playready/MediaRepresentation;
    invoke-virtual {v2}, Lcom/microsoft/playready/MediaRepresentation;->shouldBeEnabled()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/microsoft/playready/MediaRepresentation;->enable(Z)V

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/playready/MediaDescription;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    invoke-virtual {v0}, Lcom/microsoft/playready/Native_Class10;->method_7()J

    move-result-wide v0

    return-wide v0
.end method

.method public getStreamAt(I)Lcom/microsoft/playready/MediaStream;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/playready/MediaDescription;->mStreamList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaStream;

    return-object v0
.end method

.method public getStreamCount()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/playready/MediaDescription;->mStreamList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public isLive()Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/playready/MediaDescription;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    invoke-virtual {v0}, Lcom/microsoft/playready/Native_Class10;->method_6()Z

    move-result v0

    return v0
.end method
