.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->onCreateSuccess(Lorg/webrtc/SessionDescription;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

.field final synthetic val$sdp:Lorg/webrtc/SessionDescription;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;Lorg/webrtc/SessionDescription;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    .prologue
    .line 949
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->val$sdp:Lorg/webrtc/SessionDescription;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 952
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/PeerConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 953
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v1

    const-string v2, "PCRTCClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set local SDP from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->val$sdp:Lorg/webrtc/SessionDescription;

    iget-object v4, v4, Lorg/webrtc/SessionDescription;->type:Lorg/webrtc/SessionDescription$Type;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->val$sdp:Lorg/webrtc/SessionDescription;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->fixSDP(Lorg/webrtc/SessionDescription;)Lorg/webrtc/SessionDescription;

    move-result-object v0

    .line 957
    .local v0, "newSdp":Lorg/webrtc/SessionDescription;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v2, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$2700(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/SessionDescription;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$2802(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)Z

    .line 959
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$2702(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lorg/webrtc/SessionDescription;)Lorg/webrtc/SessionDescription;

    .line 961
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/PeerConnection;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    iget-object v2, v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/webrtc/PeerConnection;->setLocalDescription(Lorg/webrtc/SdpObserver;Lorg/webrtc/SessionDescription;)V

    .line 963
    .end local v0    # "newSdp":Lorg/webrtc/SessionDescription;
    :cond_0
    return-void

    .line 957
    .restart local v0    # "newSdp":Lorg/webrtc/SessionDescription;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
