.class Lcom/microsoft/xbox/smartglass/RefTPtr;
.super Ljava/lang/Object;
.source "RefTPtr.java"


# instance fields
.field private _pointer:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/RefTPtr;->_pointer:J

    .line 12
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "pointer"    # J

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide p1, p0, Lcom/microsoft/xbox/smartglass/RefTPtr;->_pointer:J

    .line 16
    return-void
.end method


# virtual methods
.method public native delete(J)V
.end method

.method public finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->reset()V

    .line 28
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 29
    return-void
.end method

.method public get()J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/RefTPtr;->_pointer:J

    return-wide v0
.end method

.method public isNull()Z
    .locals 4

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/RefTPtr;->_pointer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 19
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/RefTPtr;->_pointer:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 20
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/RefTPtr;->_pointer:J

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->delete(J)V

    .line 21
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/RefTPtr;->_pointer:J

    .line 23
    :cond_0
    return-void
.end method
