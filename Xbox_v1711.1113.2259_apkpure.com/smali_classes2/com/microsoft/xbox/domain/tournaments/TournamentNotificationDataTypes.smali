.class public final Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes;
.super Ljava/lang/Object;
.source "TournamentNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamState;,
        Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
