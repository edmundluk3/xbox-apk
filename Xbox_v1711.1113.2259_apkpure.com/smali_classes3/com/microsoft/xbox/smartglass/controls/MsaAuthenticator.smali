.class public Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;
.super Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
.source "MsaAuthenticator.java"


# instance fields
.field private _clientIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/smartglass/Environment;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final _listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;",
            ">;"
        }
    .end annotation
.end field

.field private _policy:Ljava/lang/String;

.field private _sandboxId:Ljava/lang/String;

.field private _webView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/util/Map;)V
    .locals 2
    .param p1, "webView"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/smartglass/Environment;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 35
    .local p2, "clientIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/microsoft/xbox/smartglass/Environment;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_listeners:Ljava/util/HashSet;

    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A web view must always be specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_clientIds:Ljava/util/Map;

    .line 41
    sget-object v0, Lcom/microsoft/xbox/smartglass/AuthInfo;->DefaultPolicy:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_policy:Ljava/lang/String;

    .line 42
    const-string v0, "RETAIL"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_sandboxId:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_webView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_webView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->addListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V

    .line 45
    return-void
.end method

.method private cloneListeners()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_listeners:Ljava/util/HashSet;

    monitor-enter v2

    .line 179
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_listeners:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 180
    .local v0, "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;>;"
    monitor-exit v2

    .line 182
    return-object v0

    .line 180
    .end local v0    # "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private fireAuthenticationComplete(ZZ)V
    .locals 3
    .param p1, "isAuthenticated"    # Z
    .param p2, "hasError"    # Z

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;

    .line 187
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;
    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;->onAuthenticationComplete(ZZ)V

    goto :goto_0

    .line 189
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;
    :cond_0
    return-void
.end method

.method private static getParameter(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 203
    .local p0, "parameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static getQueryParameters(Landroid/net/Uri;)Ljava/util/Map;
    .locals 9
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 192
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 193
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v3

    const-string v5, "[#&]"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v0, v5, v3

    .line 194
    .local v0, "fragment":Ljava/lang/String;
    invoke-static {v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    const-string v7, "="

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 196
    .local v2, "parts":[Ljava/lang/String;
    aget-object v7, v2, v4

    const/4 v8, 0x1

    aget-object v8, v2, v8

    invoke-interface {v1, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 199
    .end local v0    # "fragment":Ljava/lang/String;
    .end local v2    # "parts":[Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method private static isOAuthCompletionUri(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 211
    invoke-virtual {p0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "oauth20_desktop.srf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSignInUri(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 215
    invoke-virtual {p0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    const-string v1, "access_token="

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;

    .prologue
    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 123
    monitor-exit v1

    .line 124
    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentClientId()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getEnvironment()Lcom/microsoft/xbox/smartglass/Environment;

    move-result-object v0

    .line 84
    .local v0, "current":Lcom/microsoft/xbox/smartglass/Environment;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_clientIds:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "No client ID is specified for environment \'%s\'"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_clientIds:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1
.end method

.method public getPolicy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_policy:Ljava/lang/String;

    return-object v0
.end method

.method public getSandboxId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_sandboxId:Ljava/lang/String;

    return-object v0
.end method

.method public getWebView()Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_webView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    return-object v0
.end method

.method public login()V
    .locals 5

    .prologue
    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_webView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->getCurrentClientId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_policy:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->getOAuthLoginUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->navigate(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error navigating to MSA login page: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    goto :goto_0
.end method

.method public logout()V
    .locals 4

    .prologue
    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_webView:Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->getCurrentClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->getOAuthLogoutUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->navigate(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error navigating to MSA logout page: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    goto :goto_0
.end method

.method public onNavigating(Ljava/lang/String;)V
    .locals 13
    .param p1, "uriString"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 138
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 139
    .local v9, "uri":Landroid/net/Uri;
    invoke-static {v9}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->isOAuthCompletionUri(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-static {v9}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->isSignInUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 144
    invoke-static {v9}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->getQueryParameters(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v8

    .line 146
    .local v8, "parameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "access_token"

    invoke-static {v8, v0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->getParameter(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "accessToken":Ljava/lang/String;
    const-string v0, "refresh_token"

    invoke-static {v8, v0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->getParameter(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "refreshToken":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v7, v10

    .line 152
    .local v7, "hasRefreshToken":Z
    :goto_1
    if-eqz v7, :cond_4

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->getCurrentClientId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 157
    .local v3, "clientId":Ljava/lang/String;
    :goto_2
    if-eqz v7, :cond_5

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_policy:Ljava/lang/String;

    .line 159
    .local v4, "policy":Ljava/lang/String;
    :goto_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    if-eqz v7, :cond_2

    .line 160
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTokenManager()Lcom/microsoft/xbox/smartglass/TokenManager;

    move-result-object v12

    new-instance v0, Lcom/microsoft/xbox/smartglass/AuthInfo;

    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_sandboxId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/smartglass/AuthInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v0, v10}, Lcom/microsoft/xbox/smartglass/TokenManager;->setAuthInfo(Lcom/microsoft/xbox/smartglass/AuthInfo;Z)V

    .line 163
    :cond_2
    invoke-direct {p0, v10, v11}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->fireAuthenticationComplete(ZZ)V

    goto :goto_0

    .end local v3    # "clientId":Ljava/lang/String;
    .end local v4    # "policy":Ljava/lang/String;
    .end local v7    # "hasRefreshToken":Z
    :cond_3
    move v7, v11

    .line 149
    goto :goto_1

    .line 152
    .restart local v7    # "hasRefreshToken":Z
    :cond_4
    :try_start_1
    const-string v3, ""
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 153
    :catch_0
    move-exception v6

    .line 154
    .local v6, "e":Ljava/lang/Exception;
    invoke-direct {p0, v11, v10}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->fireAuthenticationComplete(ZZ)V

    goto :goto_0

    .line 157
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v3    # "clientId":Ljava/lang/String;
    :cond_5
    const-string v4, ""

    goto :goto_3

    .line 165
    .end local v1    # "accessToken":Ljava/lang/String;
    .end local v2    # "refreshToken":Ljava/lang/String;
    .end local v3    # "clientId":Ljava/lang/String;
    .end local v7    # "hasRefreshToken":Z
    .end local v8    # "parameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_6
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTokenManager()Lcom/microsoft/xbox/smartglass/TokenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/TokenManager;->clearAuthInfo()V

    .line 166
    invoke-direct {p0, v11, v11}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->fireAuthenticationComplete(ZZ)V

    goto :goto_0
.end method

.method public onNavigationFailed(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "failingUrl"    # Ljava/lang/String;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 172
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->fireAuthenticationComplete(ZZ)V

    .line 173
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticatorListener;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 133
    monitor-exit v1

    .line 134
    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPolicy(Ljava/lang/String;)V
    .locals 0
    .param p1, "_policy"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_policy:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setSandboxId(Ljava/lang/String;)V
    .locals 0
    .param p1, "_sandboxId"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/MsaAuthenticator;->_sandboxId:Ljava/lang/String;

    .line 71
    return-void
.end method
