.class Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileFriendsListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpotlightViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private broadcastIconBackground:Landroid/view/View;

.field private broadcastIconImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private broadcastSubTitle:Landroid/widget/TextView;

.field private broadcasterName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private spotlightImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .line 133
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 135
    const v0, 0x7f0e067e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->spotlightImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 136
    const v0, 0x7f0e067f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastIconBackground:Landroid/view/View;

    .line 137
    const v0, 0x7f0e0680

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastIconImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 138
    const v0, 0x7f0e0681

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcasterName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 139
    const v0, 0x7f0e0682

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastSubTitle:Landroid/widget/TextView;

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;)V
    .locals 5
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f0201fb

    .line 146
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 148
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->spotlightImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 154
    :goto_0
    const-string v0, "Twitch"

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcastProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastIconBackground:Landroid/view/View;

    const v1, 0x7f0c0146

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastIconImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v1, 0x7f0201f9

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastSubTitle:Landroid/widget/TextView;

    const v1, 0x7f0705d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 164
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcasterName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705d7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcasterName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->spotlightImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->clearImage()V

    goto :goto_0

    .line 158
    :cond_2
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;->broadcastProvider:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastIconBackground:Landroid/view/View;

    const v1, 0x7f0c0023

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastIconImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v1, 0x7f020181

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->broadcastSubTitle:Landroid/widget/TextView;

    const v1, 0x7f0705d5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 124
    check-cast p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 170
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackSpotlightAction()V

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->launchBroadcast()V

    .line 172
    return-void
.end method
