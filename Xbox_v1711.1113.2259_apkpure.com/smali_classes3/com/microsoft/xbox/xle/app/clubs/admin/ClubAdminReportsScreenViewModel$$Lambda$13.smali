.class final synthetic Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Predicate;


# static fields
.field private static final instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;->instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/Predicate;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;->instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;

    return-object v0
.end method


# virtual methods
.method public test(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->lambda$downloadReportedItems$8(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)Z

    move-result v0

    return v0
.end method
