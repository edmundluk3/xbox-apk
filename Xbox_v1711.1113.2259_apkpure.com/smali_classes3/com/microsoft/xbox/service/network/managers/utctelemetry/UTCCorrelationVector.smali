.class public Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;
.super Ljava/lang/Object;
.source "UTCCorrelationVector.java"


# static fields
.field private static final CATASTROPHIC_FAILURE_CORRELATION_VECTOR_VALUE:Ljava/lang/String; = "B9ymkeUZe/IPc02+.2."


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getValue()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->getCorrelationVector()Lcom/microsoft/cll/android/CorrelationVector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/cll/android/CorrelationVector;->GetValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static increment()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "origCV":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->getCorrelationVector()Lcom/microsoft/cll/android/CorrelationVector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/cll/android/CorrelationVector;->Increment()Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "newCV":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->init()V

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->getValue()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 53
    .end local v1    # "newCV":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "B9ymkeUZe/IPc02+.2."

    goto :goto_0
.end method

.method public static init()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 16
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->getCll()Lcom/microsoft/cll/android/Internal/AndroidInternalCll;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->getCorrelationVector()Lcom/microsoft/cll/android/CorrelationVector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/cll/android/CorrelationVector;->Init()V

    .line 17
    const-string v0, "cV Initialized to [%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->getValue()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    return-void
.end method
