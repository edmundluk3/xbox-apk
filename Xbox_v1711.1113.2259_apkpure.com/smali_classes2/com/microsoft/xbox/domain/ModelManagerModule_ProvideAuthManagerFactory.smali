.class public final Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;
.super Ljava/lang/Object;
.source "ModelManagerModule_ProvideAuthManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/domain/ModelManagerModule;

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/domain/ModelManagerModule;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/domain/ModelManagerModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/ModelManagerModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->module:Lcom/microsoft/xbox/domain/ModelManagerModule;

    .line 24
    sget-boolean v0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->schedulerProvider:Ljavax/inject/Provider;

    .line 26
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/domain/ModelManagerModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/domain/ModelManagerModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/ModelManagerModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;-><init>(Lcom/microsoft/xbox/domain/ModelManagerModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideAuthManager(Lcom/microsoft/xbox/domain/ModelManagerModule;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/domain/ModelManagerModule;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/ModelManagerModule;->provideAuthManager(Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->module:Lcom/microsoft/xbox/domain/ModelManagerModule;

    iget-object v0, p0, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->schedulerProvider:Ljavax/inject/Provider;

    .line 31
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/domain/ModelManagerModule;->provideAuthManager(Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 30
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->get()Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    move-result-object v0

    return-object v0
.end method
