.class Lcom/microsoft/playready/networkdevice/PrndDrmProxy;
.super Ljava/lang/Object;
.source "PrndDrmProxy.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

.field private final mPrndReceiver:Lcom/microsoft/playready/networkdevice/IReceiverSession;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V
    .locals 2
    .param p1, "prFactory"    # Lcom/microsoft/playready/networkdevice/IPRNDFactory;
    .param p2, "receiverSession"    # Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-boolean v0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_0
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mPrndReceiver:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .line 47
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class6;

    invoke-direct {v0}, Lcom/microsoft/playready/networkdevice/Native_Class6;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    .line 49
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mPrndReceiver:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mPrndReceiver:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    invoke-interface {v0}, Lcom/microsoft/playready/networkdevice/IReceiverSession;->getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    new-instance v1, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$1;

    invoke-direct {v1, p0}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$1;-><init>(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/networkdevice/Native_Class6;->setHandler2(Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;)V

    .line 110
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    new-instance v1, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$2;

    invoke-direct {v1, p0}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$2;-><init>(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/networkdevice/Native_Class6;->setHandler3(Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;)V

    .line 153
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)Lcom/microsoft/playready/networkdevice/IReceiverSession;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mPrndReceiver:Lcom/microsoft/playready/networkdevice/IReceiverSession;

    return-object v0
.end method


# virtual methods
.method public initializeNativeDrmProxyPointer(Lcom/microsoft/playready/MediaPlayer;)V
    .locals 1
    .param p1, "mediaPlayer"    # Lcom/microsoft/playready/MediaPlayer;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/Native_Class6;->method_4(Ljava/lang/Object;)V

    .line 158
    return-void
.end method

.method public receiverFetchLicense(I[B[B[B)Z
    .locals 1
    .param p1, "contentIdentifierType"    # I
    .param p2, "contentID"    # [B
    .param p3, "customIDType"    # [B
    .param p4, "customData"    # [B

    .prologue
    .line 176
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/microsoft/playready/networkdevice/Native_Class6;->method_11(I[B[B[B)Z

    move-result v0

    return v0
.end method

.method public receiverProximityCheck()Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    invoke-virtual {v0}, Lcom/microsoft/playready/networkdevice/Native_Class6;->method_12()Z

    move-result v0

    return v0
.end method

.method public receiverStart(Ljava/lang/String;I[B[B[B)[B
    .locals 6
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "customDataTypeID"    # [B
    .param p4, "customData"    # [B
    .param p5, "sessionID"    # [B

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/playready/networkdevice/Native_Class6;->method_10(Ljava/lang/String;I[B[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public receiverStop()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    invoke-virtual {v0}, Lcom/microsoft/playready/networkdevice/Native_Class6;->method_13()V

    .line 187
    return-void
.end method

.method public receiverUpdateRevocationList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "revocationList"    # Ljava/lang/String;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class6;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/Native_Class6;->method_14(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
