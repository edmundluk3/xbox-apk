.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;
.super Ljava/lang/Object;
.source "UTCFacebookIntegration.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackAddFacebookUserToFriends(ZLjava/lang/String;)V
    .locals 1
    .param p0, "isFacebookFriend"    # Z
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$1;-><init>(ZLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 38
    return-void
.end method

.method public static trackFriendFinderView()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$2;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 50
    return-void
.end method

.method public static trackLoginStatus(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 1
    .param p0, "actionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p1, "status"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    .line 59
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$3;-><init>(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 69
    return-void
.end method

.method public static trackOptOut()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$5;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 96
    return-void
.end method

.method public static trackShareAction(Z)V
    .locals 1
    .param p0, "success"    # Z

    .prologue
    .line 77
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$4;-><init>(Z)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 87
    return-void
.end method

.method public static trackShareView()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$6;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$6;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 108
    return-void
.end method

.method public static trackUnlinkView()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$7;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration$7;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 120
    return-void
.end method
