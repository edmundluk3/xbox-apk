.class final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_RemoveMultiplayerMemberRequest;
.super Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;
.source "AutoValue_MultiplayerSessionDataTypes_RemoveMultiplayerMemberRequest.java"


# instance fields
.field private final members:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "members":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;-><init>()V

    .line 15
    if-nez p1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null members"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_RemoveMultiplayerMemberRequest;->members:Ljava/util/Map;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 29
    if-ne p1, p0, :cond_0

    .line 30
    const/4 v1, 0x1

    .line 36
    :goto_0
    return v1

    .line 32
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 33
    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;

    .line 34
    .local v0, "that":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_RemoveMultiplayerMemberRequest;->members:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;->members()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 36
    .end local v0    # "that":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x1

    .line 42
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 43
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_RemoveMultiplayerMemberRequest;->members:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 44
    return v0
.end method

.method public members()Ljava/util/Map;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_RemoveMultiplayerMemberRequest;->members:Ljava/util/Map;

    return-object v0
.end method
