.class public Lcom/microsoft/xbox/service/model/sls/Title;
.super Ljava/lang/Object;
.source "Title.java"


# static fields
.field private static final PLATFORM_XBOX:I = 0x0

.field private static final PLATFORM_XBOX360:I = 0x1

.field private static final XBOX_MUSIC_LAUNCH_PARAM:Ljava/lang/String; = "app:5848085B:MusicHomePage"

.field private static final imageUrlFormat:Ljava/lang/String; = "http://tiles.xbox.com/consoleAssets/%s/%s/largeboxart.jpg"


# instance fields
.field public currentAchievements:I

.field public currentGamerScore:I

.field public isApp:Z

.field public isGame:Z

.field public isTitleAvailableOnConsole:Z

.field private isXboxMusic:Z

.field private isXboxVideo:Z

.field public lastPlayed:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public platforms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public sequence:I

.field public titleId:J

.field public titleType:I

.field public totalAchievemenets:I

.field public totalGamerScore:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/sls/Title;)V
    .locals 3
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/sls/Title;

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iget v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->currentAchievements:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->currentAchievements:I

    .line 43
    iget v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->currentGamerScore:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->currentGamerScore:I

    .line 44
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->lastPlayed:Ljava/lang/String;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->lastPlayed:Ljava/lang/String;

    .line 45
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->name:Ljava/lang/String;

    if-nez v0, :cond_2

    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/sls/Title;->name:Ljava/lang/String;

    .line 46
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->platforms:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/sls/Title;->platforms:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->platforms:Ljava/util/List;

    .line 49
    :cond_0
    iget v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->sequence:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->sequence:I

    .line 50
    iget-wide v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->titleId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->titleId:J

    .line 51
    iget v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->titleType:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->titleType:I

    .line 52
    iget v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->totalAchievemenets:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->totalAchievemenets:I

    .line 53
    iget v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->totalGamerScore:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->totalGamerScore:I

    .line 54
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->isApp:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isApp:Z

    .line 55
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->isGame:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isGame:Z

    .line 56
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->isTitleAvailableOnConsole:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isTitleAvailableOnConsole:Z

    .line 57
    return-void

    .line 44
    :cond_1
    new-instance v0, Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/sls/Title;->lastPlayed:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_2
    new-instance v1, Ljava/lang/String;

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/sls/Title;->name:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getImageUrl(Ljava/lang/String;J)Ljava/lang/String;
    .locals 5
    .param p0, "locale"    # Ljava/lang/String;
    .param p1, "titleId"    # J

    .prologue
    .line 77
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "http://tiles.xbox.com/consoleAssets/%s/%s/largeboxart.jpg"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {p1, p2, v4}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public IsApplication()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isApp:Z

    return v0
.end method

.method public IsGame()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isGame:Z

    return v0
.end method

.method public IsLaunchableOnConsole()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isTitleAvailableOnConsole:Z

    return v0
.end method

.method public getCurrentAchievements()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->currentAchievements:I

    return v0
.end method

.method public getCurrentGamerScore()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->currentGamerScore:I

    return v0
.end method

.method public getImageUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->titleId:J

    invoke-static {p1, v0, v1}, Lcom/microsoft/xbox/service/model/sls/Title;->getImageUrl(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsXboxMusic()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isXboxMusic:Z

    return v0
.end method

.method public getIsXboxVideo()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isXboxVideo:Z

    return v0
.end method

.method public getLastPlayed()Ljava/util/Date;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->lastPlayed:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchParameter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/sls/Title;->getIsXboxMusic()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "app:5848085B:MusicHomePage"

    .line 133
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLaunchType()I
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/sls/Title;->getIsXboxMusic()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->UnknownLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/LaunchType;->getValue()I

    move-result v0

    .line 146
    :goto_0
    return v0

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/sls/Title;->IsGame()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->GameLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/LaunchType;->getValue()I

    move-result v0

    goto :goto_0

    .line 146
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/LaunchType;->getValue()I

    move-result v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->titleId:J

    return-wide v0
.end method

.method public getTitleType()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->titleType:I

    return v0
.end method

.method public getTotalAchievements()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->totalAchievemenets:I

    return v0
.end method

.method public getTotalGamerScore()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/microsoft/xbox/service/model/sls/Title;->totalGamerScore:I

    return v0
.end method

.method public setIsXboxMusic(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isXboxMusic:Z

    .line 118
    return-void
.end method

.method public setIsXboxVideo(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/sls/Title;->isXboxVideo:Z

    .line 126
    return-void
.end method
