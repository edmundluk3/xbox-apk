.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.source "ConversationsViewAllMembersScreenAdapter.java"


# instance fields
.field private addPeople:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private cancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private close:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;

.field private listView:Landroid/widget/AbsListView;

.field private memberList:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation
.end field

.field private viewMemberSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;-><init>()V

    .line 31
    const v0, 0x7f0e04ca

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->screenBody:Landroid/view/View;

    .line 32
    const v0, 0x7f0e04cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewMemberSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    .line 34
    const v0, 0x7f0e04cf

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listView:Landroid/widget/AbsListView;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V

    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->setOnClickListeners()V

    .line 37
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0300c9

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;-><init>(Landroid/app/Activity;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listView:Landroid/widget/AbsListView;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;
    .param p2, "parent"    # Landroid/widget/AdapterView;
    .param p3, "view"    # Landroid/view/View;
    .param p4, "position"    # I
    .param p5, "id"    # J

    .prologue
    .line 40
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;

    invoke-virtual {v1, p4}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 41
    .local v0, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->navigateToConversationMemberProfile(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method private setOnClickListeners()V
    .locals 2

    .prologue
    .line 46
    const v0, 0x7f0e04d2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->addPeople:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->addPeople:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    const v0, 0x7f0e04d3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->cancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->cancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v0, 0x7f0e04cc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->close:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->close:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewMemberSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->updateLoadingIndicator(Z)V

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewMemberSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->getListWithHeader()Ljava/util/ArrayList;

    move-result-object v0

    .line 88
    .local v0, "newData":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listView:Landroid/widget/AbsListView;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->memberList:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->clear()V

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersListAdapter;->addAll(Ljava/util/Collection;)V

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->listView:Landroid/widget/AbsListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setFastScrollAlwaysVisible(Z)V

    .line 92
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;->memberList:Ljava/util/Collection;

    .line 94
    :cond_0
    return-void
.end method
