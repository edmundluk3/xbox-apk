.class public Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;
.super Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;
.source "NowPlayingTvView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;


# instance fields
.field private oneGuideButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

.field private streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 33
    return-void
.end method

.method static synthetic lambda$attachToContainer$0(Landroid/view/View;)V
    .locals 5
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 51
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;

    .line 54
    .local v0, "destScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v1

    .line 58
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v2, "NowPlayingTvView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to navigate to screen \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$attachToContainer$1(Landroid/view/View;)V
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-static {v1, v1, v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->trackStreamLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static {v1, v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    const-string v0, "NowPlayingTvView"

    const-string v1, "Attempted to stream TV without any providers!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public attachToContainer()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 42
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->attachToContainer()V

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->context:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 45
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030198

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->panelButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->oneGuideButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 46
    const v1, 0x7f030199

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->panelButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->oneGuideButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPremiumLiveTVEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->panelButtons:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->oneGuideButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->oneGuideButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView$$Lambda$1;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    if-eqz v1, :cond_1

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->panelButtons:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-static {}, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView$$Lambda$2;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 78
    return-void
.end method

.method protected canLaunchDetails()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->cleanup()V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->oneGuideButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->oneGuideButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 103
    return-void
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 113
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->update()V

    .line 108
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->update()V

    .line 118
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 123
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->update()V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->shouldShowStreamingButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setVisibility(I)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTvView;->streamTvButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setVisibility(I)V

    goto :goto_0
.end method
