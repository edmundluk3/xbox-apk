.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubCustomizeImagePickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageViewHolder"
.end annotation


# instance fields
.field final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Landroid/view/ViewGroup;)V
    .locals 3
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    .line 267
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03007e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 269
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;Ljava/lang/String;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;
    .param p1, "imageUrl"    # Ljava/lang/String;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->access$200(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;->onImageSelected(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bindTo(Ljava/lang/String;II)V
    .locals 6
    .param p1, "imageUrl"    # Ljava/lang/String;
    .param p2, "position"    # I
    .param p3, "total"    # I

    .prologue
    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v1, 0x7f020122

    const v2, 0x7f020115

    invoke-virtual {v0, p1, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070d71

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v5, p2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 275
    return-void
.end method
