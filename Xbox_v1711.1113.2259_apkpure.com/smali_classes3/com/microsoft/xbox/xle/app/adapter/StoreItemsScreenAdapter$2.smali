.class Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;
.super Ljava/lang/Object;
.source "StoreItemsScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$102(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$302(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->clear()V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->setFilter(Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)V

    .line 117
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
