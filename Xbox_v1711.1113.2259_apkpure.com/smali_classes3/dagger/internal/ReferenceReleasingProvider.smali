.class public final Ldagger/internal/ReferenceReleasingProvider;
.super Ljava/lang/Object;
.source "ReferenceReleasingProvider.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation build Ldagger/internal/GwtIncompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NULL:Ljava/lang/Object;


# instance fields
.field private final provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile strongReference:Ljava/lang/Object;

.field private volatile weakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Ldagger/internal/ReferenceReleasingProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldagger/internal/ReferenceReleasingProvider;->$assertionsDisabled:Z

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldagger/internal/ReferenceReleasingProvider;->NULL:Ljava/lang/Object;

    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Ldagger/internal/ReferenceReleasingProvider;, "Ldagger/internal/ReferenceReleasingProvider<TT;>;"
    .local p1, "provider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    sget-boolean v0, Ldagger/internal/ReferenceReleasingProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 70
    :cond_0
    iput-object p1, p0, Ldagger/internal/ReferenceReleasingProvider;->provider:Ljavax/inject/Provider;

    .line 71
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ldagger/internal/ReferenceReleasingProviderManager;)Ldagger/internal/ReferenceReleasingProvider;
    .locals 2
    .param p1, "references"    # Ldagger/internal/ReferenceReleasingProviderManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljavax/inject/Provider",
            "<TT;>;",
            "Ldagger/internal/ReferenceReleasingProviderManager;",
            ")",
            "Ldagger/internal/ReferenceReleasingProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 158
    .local p0, "delegate":Ljavax/inject/Provider;, "Ljavax/inject/Provider<TT;>;"
    new-instance v0, Ldagger/internal/ReferenceReleasingProvider;

    .line 159
    invoke-static {p0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/inject/Provider;

    invoke-direct {v0, v1}, Ldagger/internal/ReferenceReleasingProvider;-><init>(Ljavax/inject/Provider;)V

    .line 160
    .local v0, "provider":Ldagger/internal/ReferenceReleasingProvider;, "Ldagger/internal/ReferenceReleasingProvider<TT;>;"
    invoke-virtual {p1, v0}, Ldagger/internal/ReferenceReleasingProviderManager;->addProvider(Ldagger/internal/ReferenceReleasingProvider;)V

    .line 161
    return-object v0
.end method

.method private currentValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 142
    .local p0, "this":Ldagger/internal/ReferenceReleasingProvider;, "Ldagger/internal/ReferenceReleasingProvider<TT;>;"
    iget-object v0, p0, Ldagger/internal/ReferenceReleasingProvider;->strongReference:Ljava/lang/Object;

    .line 143
    .local v0, "value":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 149
    .end local v0    # "value":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 146
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->weakReference:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    .line 147
    iget-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 125
    .local p0, "this":Ldagger/internal/ReferenceReleasingProvider;, "Ldagger/internal/ReferenceReleasingProvider<TT;>;"
    invoke-direct {p0}, Ldagger/internal/ReferenceReleasingProvider;->currentValue()Ljava/lang/Object;

    move-result-object v0

    .line 126
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_2

    .line 127
    monitor-enter p0

    .line 128
    :try_start_0
    invoke-direct {p0}, Ldagger/internal/ReferenceReleasingProvider;->currentValue()Ljava/lang/Object;

    move-result-object v0

    .line 129
    if-nez v0, :cond_1

    .line 130
    iget-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    .line 131
    if-nez v0, :cond_0

    .line 132
    sget-object v0, Ldagger/internal/ReferenceReleasingProvider;->NULL:Ljava/lang/Object;

    .line 134
    :cond_0
    iput-object v0, p0, Ldagger/internal/ReferenceReleasingProvider;->strongReference:Ljava/lang/Object;

    .line 136
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :cond_2
    sget-object v1, Ldagger/internal/ReferenceReleasingProvider;->NULL:Ljava/lang/Object;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    .end local v0    # "value":Ljava/lang/Object;
    :cond_3
    return-object v0

    .line 136
    .restart local v0    # "value":Ljava/lang/Object;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public releaseStrongReference()V
    .locals 3

    .prologue
    .line 78
    .local p0, "this":Ldagger/internal/ReferenceReleasingProvider;, "Ldagger/internal/ReferenceReleasingProvider<TT;>;"
    iget-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->strongReference:Ljava/lang/Object;

    .line 79
    .local v1, "value":Ljava/lang/Object;
    if-eqz v1, :cond_0

    sget-object v2, Ldagger/internal/ReferenceReleasingProvider;->NULL:Ljava/lang/Object;

    if-eq v1, v2, :cond_0

    .line 80
    monitor-enter p0

    .line 82
    move-object v0, v1

    .line 83
    .local v0, "storedValue":Ljava/lang/Object;, "TT;"
    :try_start_0
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Ldagger/internal/ReferenceReleasingProvider;->weakReference:Ljava/lang/ref/WeakReference;

    .line 84
    const/4 v2, 0x0

    iput-object v2, p0, Ldagger/internal/ReferenceReleasingProvider;->strongReference:Ljava/lang/Object;

    .line 85
    monitor-exit p0

    .line 87
    .end local v0    # "storedValue":Ljava/lang/Object;, "TT;"
    :cond_0
    return-void

    .line 85
    .restart local v0    # "storedValue":Ljava/lang/Object;, "TT;"
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public restoreStrongReference()V
    .locals 2

    .prologue
    .line 94
    .local p0, "this":Ldagger/internal/ReferenceReleasingProvider;, "Ldagger/internal/ReferenceReleasingProvider<TT;>;"
    iget-object v0, p0, Ldagger/internal/ReferenceReleasingProvider;->strongReference:Ljava/lang/Object;

    .line 95
    .local v0, "value":Ljava/lang/Object;
    iget-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->weakReference:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 96
    monitor-enter p0

    .line 97
    :try_start_0
    iget-object v0, p0, Ldagger/internal/ReferenceReleasingProvider;->strongReference:Ljava/lang/Object;

    .line 98
    iget-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->weakReference:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 99
    iget-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->weakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    .line 101
    iput-object v0, p0, Ldagger/internal/ReferenceReleasingProvider;->strongReference:Ljava/lang/Object;

    .line 102
    const/4 v1, 0x0

    iput-object v1, p0, Ldagger/internal/ReferenceReleasingProvider;->weakReference:Ljava/lang/ref/WeakReference;

    .line 105
    :cond_0
    monitor-exit p0

    .line 107
    :cond_1
    return-void

    .line 105
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
