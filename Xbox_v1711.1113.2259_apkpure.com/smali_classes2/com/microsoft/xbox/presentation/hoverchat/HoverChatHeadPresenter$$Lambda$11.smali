.class final synthetic Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/BiPredicate;


# static fields
.field private static final instance:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;->instance:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/BiPredicate;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;->instance:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;

    return-object v0
.end method


# virtual methods
.method public test(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    check-cast p2, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->equalsIgnoreIsMaximized(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)Z

    move-result v0

    return v0
.end method
