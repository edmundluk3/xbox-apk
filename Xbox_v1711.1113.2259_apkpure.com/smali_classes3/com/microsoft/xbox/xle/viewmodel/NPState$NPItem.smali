.class public interface abstract Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
.super Ljava/lang/Object;
.source "NPState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/NPState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NPItem"
.end annotation


# virtual methods
.method public abstract getItemId()Ljava/lang/String;
.end method

.method public abstract getLi()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
.end method

.method public abstract getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;
.end method

.method public abstract getProviderTitleId()J
.end method

.method public abstract getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;
.end method

.method public abstract isNPM()Z
.end method

.method public abstract isRecent()Z
.end method

.method public abstract isValid()Z
.end method

.method public abstract toRecent()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;
.end method
