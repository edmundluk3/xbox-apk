.class public Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;
.super Ljava/lang/Object;
.source "AuxiliaryStreamWriteStats.java"


# instance fields
.field public final queuedBytes:J

.field public final sentBytes:J


# direct methods
.method constructor <init>(JJ)V
    .locals 1
    .param p1, "sentBytes"    # J
    .param p3, "queuedBytes"    # J

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;->sentBytes:J

    .line 18
    iput-wide p3, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;->queuedBytes:J

    .line 19
    return-void
.end method
