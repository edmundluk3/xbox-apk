.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "ActivityParentScreenAdapter.java"


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 27
    const v0, 0x7f0e0833

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    .line 30
    const v0, 0x7f0e0835

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 31
    const v0, 0x7f0e0834

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03019e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->updateLoadingIndicator(Z)V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->items:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;->getData()Ljava/util/List;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->items:Ljava/util/List;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;->clear()V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->items:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;->addAll(Ljava/util/Collection;)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 56
    return-void
.end method
