.class Lcom/microsoft/xbox/xle/remote/RemoteControl$1;
.super Ljava/lang/Object;
.source "RemoteControl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/remote/RemoteControl;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/RemoteControl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$1;->this$0:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 111
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getTitleIdWithFocus()I

    move-result v6

    .line 112
    .local v6, "titleId":I
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "relativeId":Ljava/lang/String;
    const-string v0, "URC"

    const-string v1, "Remote"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "TitleId"

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->trackLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    .end local v5    # "relativeId":Ljava/lang/String;
    .end local v6    # "titleId":I
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$1;->this$0:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->access$000(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    .line 118
    return-void

    .line 114
    :catch_0
    move-exception v0

    goto :goto_0
.end method
