.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsActivityFeedPostFilter(JI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$clubId:J

.field final synthetic val$position:I


# direct methods
.method constructor <init>(JI)V
    .locals 1

    .prologue
    .line 1197
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;->val$clubId:J

    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 1200
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;->val$clubId:J

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 1202
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 1203
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ClubId"

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;->val$clubId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1205
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;->val$position:I

    packed-switch v1, :pswitch_data_0

    .line 1216
    const-string v1, "Clubs - Admin Settings Feed Post Members"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 1219
    :goto_0
    return-void

    .line 1207
    :pswitch_0
    const-string v1, "Clubs - Admin Settings Feed Post Members"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0

    .line 1210
    :pswitch_1
    const-string v1, "Clubs - Admin Settings Feed Post Admins"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0

    .line 1213
    :pswitch_2
    const-string v1, "Clubs - Admin Settings Feed Post Owner"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0

    .line 1205
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
