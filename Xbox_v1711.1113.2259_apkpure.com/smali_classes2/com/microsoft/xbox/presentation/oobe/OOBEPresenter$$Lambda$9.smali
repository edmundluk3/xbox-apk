.class final synthetic Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

.field private final arg$2:Lcom/microsoft/xbox/domain/auth/AuthStateManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;->arg$1:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;->arg$2:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;-><init>(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;->arg$1:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$9;->arg$2:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->lambda$null$1(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    move-result-object v0

    return-object v0
.end method
