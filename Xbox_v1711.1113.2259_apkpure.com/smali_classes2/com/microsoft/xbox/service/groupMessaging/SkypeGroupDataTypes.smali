.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ClientDescription;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GcmNode;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Transports;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Binding;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$EdfRegistrationResponse;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageContent;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponseData;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionReason;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionRequest;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$JoinGroupRequest;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$MuteRequest;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$TopicRenameRequest;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageNotificationRequest;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UpdateConsumptionHorizonRequest;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$Properties;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequestWithProperties;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequest;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
