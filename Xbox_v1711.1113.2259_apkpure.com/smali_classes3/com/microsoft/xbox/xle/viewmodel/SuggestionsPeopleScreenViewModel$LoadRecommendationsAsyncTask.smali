.class Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "SuggestionsPeopleScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRecommendationsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$1;

    .prologue
    .line 306
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 310
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshPeopleHubRecommendationsProfile()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPeopleHubRecommendations(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 316
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->onLoadRecommendationsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 318
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->onLoadRecommendationsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 329
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 306
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 322
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 323
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;Z)Z

    .line 324
    return-void
.end method
