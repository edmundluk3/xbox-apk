.class Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGameProgress360EarnedAchievementRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "titleModel"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "titleId"    # Ljava/lang/String;

    .prologue
    .line 1251
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1252
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 1253
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->xuid:Ljava/lang/String;

    .line 1254
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->titleId:Ljava/lang/String;

    .line 1255
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1259
    const/4 v3, 0x0

    .line 1260
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    const/4 v4, 0x0

    .line 1261
    .local v4, "temp":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    const/4 v1, 0x5

    .line 1262
    .local v1, "maxTryCount":I
    const/4 v0, 0x0

    .line 1264
    .local v0, "continuationToken":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->titleId:Ljava/lang/String;

    invoke-interface {v5, v6, v7, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgress360EarnedAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v4

    .line 1265
    if-eqz v4, :cond_0

    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1266
    if-nez v3, :cond_2

    .line 1267
    move-object v3, v4

    .line 1272
    :goto_1
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    if-eqz v5, :cond_0

    .line 1273
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 1276
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v2, v1, -0x1

    .end local v1    # "maxTryCount":I
    .local v2, "maxTryCount":I
    if-gtz v1, :cond_3

    move v1, v2

    .line 1278
    .end local v2    # "maxTryCount":I
    .restart local v1    # "maxTryCount":I
    :cond_1
    return-object v3

    .line 1270
    :cond_2
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .end local v1    # "maxTryCount":I
    .restart local v2    # "maxTryCount":I
    :cond_3
    move v1, v2

    .end local v2    # "maxTryCount":I
    .restart local v1    # "maxTryCount":I
    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1245
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1292
    const-wide/16 v0, 0xbde

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1287
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$800(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1288
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1283
    return-void
.end method
