.class public Lcom/microsoft/xbox/xle/app/module/RelatedListModule;
.super Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;
.source "RelatedListModule.java"


# instance fields
.field private listAdapter:Landroid/widget/ArrayAdapter;

.field private listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private mediaItemRelated:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const v0, 0x7f0300de

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->setContentView(I)V

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/module/RelatedListModule;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/module/RelatedListModule;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/module/RelatedListModule;)Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/module/RelatedListModule;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    return-object v0
.end method


# virtual methods
.method public getListView()Lcom/microsoft/xbox/toolkit/ui/XLEListView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    return-object v0
.end method

.method public getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 36
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->onFinishInflate()V

    .line 37
    const v1, 0x7f0e0508

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 38
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v2, Lcom/microsoft/xbox/xle/app/module/RelatedListModule$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/module/RelatedListModule$1;-><init>(Lcom/microsoft/xbox/xle/app/module/RelatedListModule;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03016c

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;-><init>(Landroid/app/Activity;IZ)V

    .line 46
    .local v0, "listAdapter":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->setShouldAlwaysShowTitle(Z)V

    .line 48
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listAdapter:Landroid/widget/ArrayAdapter;

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 50
    return-void
.end method

.method public setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 70
    check-cast p1, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    .end local p1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    .line 71
    return-void
.end method

.method public updateView()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->mediaItemRelated:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->getRelated()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->getRelated()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->mediaItemRelated:Ljava/util/ArrayList;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->mediaItemRelated:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listAdapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->mediaItemRelated:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->restoreListPosition()V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 65
    :cond_1
    return-void
.end method
