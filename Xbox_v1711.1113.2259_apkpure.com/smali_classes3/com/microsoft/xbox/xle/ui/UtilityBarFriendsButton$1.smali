.class Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton$1;
.super Ljava/lang/Object;
.source "UtilityBarFriendsButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton$1;->this$0:Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 38
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCHomeActionBar;->trackFriendsAction()V

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubPivotScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    goto :goto_0
.end method
