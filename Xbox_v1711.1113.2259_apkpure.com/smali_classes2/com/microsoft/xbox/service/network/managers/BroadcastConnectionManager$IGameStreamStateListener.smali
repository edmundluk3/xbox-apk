.class public interface abstract Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
.super Ljava/lang/Object;
.source "BroadcastConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IGameStreamStateListener"
.end annotation


# virtual methods
.method public abstract onBroadcastChannelConnectionStateChanged(Z)V
.end method

.method public abstract onGameStreamEnabledChanged(Z)V
.end method

.method public abstract onGameStreamError(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;)V
.end method

.method public abstract onGameStreamStateChanged(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;)V
.end method

.method public abstract onNetworkTestCompleted(ZLjava/lang/String;)V
.end method

.method public abstract onPairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;Lcom/microsoft/xbox/smartglass/SGResult;)V
.end method

.method public abstract onStreamConnectionStateChanged(Z)V
.end method
