.class public Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ActivityFeedActionsScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 9
    return-void
.end method


# virtual methods
.method public adjustBottomMargin(I)V
    .locals 0
    .param p1, "bottomMargin"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;->forceAdjustBottomMargin(I)V

    .line 27
    return-void
.end method

.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "Activity Detail"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;->onCreateContentView()V

    .line 15
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 16
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 20
    const v0, 0x7f030028

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedActionsScreen;->setContentView(I)V

    .line 21
    return-void
.end method
