.class public Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;
.source "TrendingTopicScreenAdapter.java"


# instance fields
.field private descriptionTextView:Landroid/widget/TextView;

.field private headerView:Landroid/view/View;

.field private titleTextView:Landroid/widget/TextView;

.field private viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;

    .line 24
    return-void
.end method


# virtual methods
.method public getHeader()Landroid/view/View;
    .locals 4

    .prologue
    .line 36
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->headerView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 37
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 38
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030239

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->headerView:Landroid/view/View;

    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->headerView:Landroid/view/View;

    const v2, 0x7f0e0aec

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->titleTextView:Landroid/widget/TextView;

    .line 40
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->headerView:Landroid/view/View;

    const v2, 0x7f0e0aed

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->descriptionTextView:Landroid/widget/TextView;

    .line 43
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->headerView:Landroid/view/View;

    return-object v1
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->updateViewOverride()V

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->titleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->descriptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 32
    return-void
.end method
