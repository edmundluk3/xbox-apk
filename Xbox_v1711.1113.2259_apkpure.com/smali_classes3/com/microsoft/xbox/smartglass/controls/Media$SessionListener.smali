.class Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;
.super Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.source "Media.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/Media;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/Media;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Media;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Media;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Media;Lcom/microsoft/xbox/smartglass/controls/Media$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Media;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/controls/Media$1;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Media;)V

    return-void
.end method


# virtual methods
.method public onMediaStateChanged(Lcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 4
    .param p1, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 151
    if-eqz p1, :cond_0

    .line 152
    new-instance v0, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    .line 153
    .local v0, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Media;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/smartglass/controls/Media;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Media;

    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;-><init>(Lcom/microsoft/xbox/smartglass/MediaState;)V

    invoke-static {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/Media;->access$102(Lcom/microsoft/xbox/smartglass/controls/Media;Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;)Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    .line 156
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Media;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/Media;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "mediaState"

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Media;

    invoke-static {v3}, Lcom/microsoft/xbox/smartglass/controls/Media;->access$100(Lcom/microsoft/xbox/smartglass/controls/Media;)Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v0    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :cond_0
    :goto_0
    return-void

    .line 157
    .restart local v0    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :catch_0
    move-exception v1

    goto :goto_0
.end method
