.class public final Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;
.super Ljava/lang/Object;
.source "LeaderboardsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LeaderboardUri"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final postUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->postUri:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 38
    if-ne p1, p0, :cond_0

    .line 39
    const/4 v0, 0x1

    .line 43
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .line 40
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;

    if-nez v0, :cond_1

    .line 41
    const/4 v0, 0x0

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->postUri:Ljava/lang/String;

    check-cast p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->postUri:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->hashCode:I

    if-nez v0, :cond_0

    .line 50
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->hashCode:I

    .line 51
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->postUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->hashCode:I

    .line 54
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
