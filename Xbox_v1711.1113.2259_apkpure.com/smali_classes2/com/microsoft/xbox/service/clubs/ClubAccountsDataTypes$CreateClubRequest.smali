.class public abstract Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;
.super Ljava/lang/Object;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CreateClubRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_CreateClubRequest$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_CreateClubRequest$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .param p1, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 101
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 102
    const-wide/16 v0, 0x4

    const-wide/32 v2, 0x7fffffff

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v4, v4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 103
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 105
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_CreateClubRequest;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_CreateClubRequest;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    return-object v0
.end method


# virtual methods
.method public abstract name()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
