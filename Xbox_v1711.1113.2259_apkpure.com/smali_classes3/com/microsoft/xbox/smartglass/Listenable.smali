.class abstract Lcom/microsoft/xbox/smartglass/Listenable;
.super Ljava/lang/Object;
.source "Listenable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final _listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 9
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Listenable;, "Lcom/microsoft/xbox/smartglass/Listenable<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/Listenable;->_listeners:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method public addListener(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Listenable;, "Lcom/microsoft/xbox/smartglass/Listenable<TT;>;"
    .local p1, "listener":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/Listenable;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 14
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/Listenable;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 15
    monitor-exit v1

    .line 16
    return-void

    .line 15
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected cloneListeners()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Listenable;, "Lcom/microsoft/xbox/smartglass/Listenable<TT;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/Listenable;->_listeners:Ljava/util/HashSet;

    monitor-enter v2

    .line 28
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/Listenable;->_listeners:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 29
    .local v0, "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<TT;>;"
    monitor-exit v2

    .line 31
    return-object v0

    .line 29
    .end local v0    # "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<TT;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeListener(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Listenable;, "Lcom/microsoft/xbox/smartglass/Listenable<TT;>;"
    .local p1, "listener":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/Listenable;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 20
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/Listenable;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 21
    monitor-exit v1

    .line 22
    return-void

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
