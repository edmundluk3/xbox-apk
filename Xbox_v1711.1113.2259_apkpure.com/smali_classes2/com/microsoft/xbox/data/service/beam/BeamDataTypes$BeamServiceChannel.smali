.class public abstract Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
.super Ljava/lang/Object;
.source "BeamDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/beam/BeamDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BeamServiceChannel"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;)Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
    .locals 6
    .param p0, "id"    # Ljava/lang/Integer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;
    .param p3, "viewersCurrent"    # Ljava/lang/Integer;
    .param p4, "audience"    # Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    .prologue
    .line 39
    new-instance v0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;)V

    return-object v0
.end method


# virtual methods
.method public abstract audience()Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract id()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract name()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract token()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract viewersCurrent()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
