.class public Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
.super Ljava/lang/Object;
.source "HoverChatModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideChatHeadManager(Landroid/content/Context;Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;)Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/inject/Named;
            value = "app_context"
        .end annotation
    .end param
    .param p2, "windowManagerContainer"    # Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
            ")",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;

    invoke-direct {v0, p1, p2}, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;-><init>(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadContainer;)V

    return-object v0
.end method

.method provideChatHeadViewAdapter(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;)Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/inject/Named;
            value = "app_context"
        .end annotation
    .end param
    .param p2, "circularDrawableFactory"    # Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;
    .param p3, "repository"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ")",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;)V

    return-object v0
.end method

.method provideWindowManagerContainer(Landroid/content/Context;)Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/inject/Named;
            value = "app_context"
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    invoke-direct {v0, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
