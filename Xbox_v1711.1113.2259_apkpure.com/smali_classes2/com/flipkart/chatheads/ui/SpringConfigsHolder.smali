.class public Lcom/flipkart/chatheads/ui/SpringConfigsHolder;
.super Ljava/lang/Object;
.source "SpringConfigsHolder.java"


# static fields
.field public static CAPTURING:Lcom/facebook/rebound/SpringConfig;

.field public static DRAGGING:Lcom/facebook/rebound/SpringConfig;

.field public static NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 9
    const-wide v0, 0x4067c00000000000L    # 190.0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/rebound/SpringConfig;->fromOrigamiTensionAndFriction(DD)Lcom/facebook/rebound/SpringConfig;

    move-result-object v0

    sput-object v0, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    .line 10
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/rebound/SpringConfig;->fromOrigamiTensionAndFriction(DD)Lcom/facebook/rebound/SpringConfig;

    move-result-object v0

    sput-object v0, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->CAPTURING:Lcom/facebook/rebound/SpringConfig;

    .line 11
    const-wide/16 v0, 0x0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/rebound/SpringConfig;->fromOrigamiTensionAndFriction(DD)Lcom/facebook/rebound/SpringConfig;

    move-result-object v0

    sput-object v0, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
