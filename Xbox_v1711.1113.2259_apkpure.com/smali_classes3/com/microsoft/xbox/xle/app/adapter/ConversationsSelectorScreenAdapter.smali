.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.source "ConversationsSelectorScreenAdapter.java"


# instance fields
.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private conversationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

.field private listView:Landroid/widget/AbsListView;

.field private saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private startNewConversation:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;-><init>()V

    .line 31
    const v0, 0x7f0e04b7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->screenBody:Landroid/view/View;

    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    .line 33
    const v0, 0x7f0e04be

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 35
    const v0, 0x7f0e04bf

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listView:Landroid/widget/AbsListView;

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V

    .line 38
    const v0, 0x7f0e04bc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 39
    const v0, 0x7f0e04bd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 40
    const v0, 0x7f0e04c0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->startNewConversation:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->startNewConversation:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0300c7

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;-><init>(Landroid/app/Activity;ILjava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listView:Landroid/widget/AbsListView;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dismissKeyboard()V

    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->confirm()V

    .line 49
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dismissKeyboard()V

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->confirm()V

    .line 55
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->navigateToFriendsPicker()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 64
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 65
    .local v0, "conversationSummary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->sendMessageToConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 66
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 3

    .prologue
    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->updateLoadingIndicator(Z)V

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getMembers()Ljava/util/ArrayList;

    move-result-object v0

    .line 75
    .local v0, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listView:Landroid/widget/AbsListView;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->conversationList:Ljava/util/List;

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->shouldForceUpdateSummaryAdapter()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->clearForceUpdateSummaryAdapter()V

    .line 78
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->conversationList:Ljava/util/List;

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->clear()V

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->conversationList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->addAll(Ljava/util/Collection;)V

    .line 85
    :cond_1
    :goto_0
    return-void

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
