.class public Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "RecentsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/recents/Recent;",
        ">;"
    }
.end annotation


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/MainActivity;ILjava/util/ArrayList;)V
    .locals 2
    .param p1, "activity"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p2, "rowViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 25
    return-void
.end method

.method private bind(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;
    .param p3, "position"    # I

    .prologue
    .line 35
    const v2, 0x7f0e09a2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 36
    .local v0, "recentName":Landroid/widget/TextView;
    const v2, 0x7f0e09a1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 37
    .local v1, "tileView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    if-eqz p2, :cond_1

    .line 38
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 39
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 40
    if-eqz v1, :cond_0

    .line 41
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;)V

    .line 42
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isApp(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getBoxArtBackgroundColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setBackgroundColor(I)V

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 48
    const-string v2, ""

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 29
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0301e2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 30
    .local v0, "view":Landroid/view/View;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/recents/Recent;

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/xle/app/adapter/RecentsListAdapter;->bind(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;I)V

    .line 31
    return-object v0

    .end local v0    # "view":Landroid/view/View;
    :cond_0
    move-object v0, p2

    .line 29
    goto :goto_0
.end method
