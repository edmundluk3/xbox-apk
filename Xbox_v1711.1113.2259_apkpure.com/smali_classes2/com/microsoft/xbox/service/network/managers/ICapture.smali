.class public interface abstract Lcom/microsoft/xbox/service/network/managers/ICapture;
.super Ljava/lang/Object;
.source "ICapture.java"


# virtual methods
.method public abstract getClipName()Ljava/lang/String;
.end method

.method public abstract getDate()Ljava/util/Date;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getDurationInSeconds()Ljava/lang/String;
.end method

.method public abstract getGamerTag()Ljava/lang/String;
.end method

.method public abstract getIdentifier()Ljava/lang/String;
.end method

.method public abstract getIsScreenshot()Ljava/lang/Boolean;
.end method

.method public abstract getItemRoot()Ljava/lang/String;
.end method

.method public abstract getSocialInfo()Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
.end method

.method public abstract getUserCaption()Ljava/lang/String;
.end method

.method public abstract getViews()I
.end method

.method public abstract getXuid()Ljava/lang/String;
.end method
