.class public Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
.super Ljava/lang/Object;
.source "ProfileStatisticsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;
    }
.end annotation


# static fields
.field public static final arrangeByScid:Ljava/lang/String; = "scid"

.field private static final arrangeByTitle:Ljava/lang/String; = "xuid"


# instance fields
.field public arrangebyfield:Ljava/lang/String;

.field public groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;",
            ">;"
        }
    .end annotation
.end field

.field public stats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;",
            ">;"
        }
    .end annotation
.end field

.field public xuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->groups:Ljava/util/List;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->xuids:Ljava/util/List;

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "groups":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;>;"
    .local p3, "stats":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    const-string/jumbo v0, "xuid"

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p4, "arrangedBy"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "groups":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;>;"
    .local p3, "stats":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->xuids:Ljava/util/List;

    .line 30
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->arrangebyfield:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->groups:Ljava/util/List;

    .line 32
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->stats:Ljava/util/List;

    .line 33
    return-void
.end method

.method public static getProfileStatisticsRequestBody(Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "profileStatisticsRequest"    # Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;

    .prologue
    .line 37
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 41
    :goto_0
    return-object v1

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ProfileStatisticsRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const/4 v1, 0x0

    goto :goto_0
.end method
