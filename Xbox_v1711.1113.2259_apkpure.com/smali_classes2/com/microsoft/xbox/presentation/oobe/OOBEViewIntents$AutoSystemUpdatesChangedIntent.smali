.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;
.super Ljava/lang/Object;
.source "OOBEViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AutoSystemUpdatesChangedIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;
    .locals 1
    .param p0, "enabled"    # Z

    .prologue
    .line 78
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_AutoSystemUpdatesChangedIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewIntents_AutoSystemUpdatesChangedIntent;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public abstract enabled()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
