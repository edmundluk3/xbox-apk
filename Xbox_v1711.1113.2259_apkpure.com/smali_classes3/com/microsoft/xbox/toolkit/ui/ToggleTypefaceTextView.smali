.class public Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;
.super Landroid/widget/TextView;
.source "ToggleTypefaceTextView.java"


# instance fields
.field private isPositive:Z

.field private negativeTypeface:Ljava/lang/String;

.field private positiveTypeface:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->isPositive:Z

    .line 26
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->ToggleTypeface:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 27
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->positiveTypeface:Ljava/lang/String;

    .line 28
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->negativeTypeface:Ljava/lang/String;

    .line 29
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->isPositive:Z

    .line 31
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->positiveTypeface:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->negativeTypeface:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 32
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "We need typefaces for both states"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 35
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->updateTypeface()V

    .line 37
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->setCursorVisible(Z)V

    .line 42
    return-void
.end method

.method private updateTypeface()V
    .locals 4

    .prologue
    .line 46
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->isPositive:Z

    if-eqz v1, :cond_0

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->positiveTypeface:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 52
    .local v0, "tf":Landroid/graphics/Typeface;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 53
    return-void

    .line 49
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->negativeTypeface:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .restart local v0    # "tf":Landroid/graphics/Typeface;
    goto :goto_0
.end method


# virtual methods
.method public setClickable(Z)V
    .locals 2
    .param p1, "clickable"    # Z

    .prologue
    .line 69
    if-eqz p1, :cond_0

    .line 70
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Click operation is not supported on this view type"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    return-void
.end method

.method public setIsPositive(Z)V
    .locals 1
    .param p1, "isPositive"    # Z

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->isPositive:Z

    if-eq v0, p1, :cond_0

    .line 57
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->isPositive:Z

    .line 58
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ToggleTypefaceTextView;->updateTypeface()V

    .line 60
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Click operation is not supported on this view type"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
