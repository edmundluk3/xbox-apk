.class public final Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;
.super Ljava/lang/Object;
.source "UserPostsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PostRequest"
.end annotation


# instance fields
.field private final linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

.field private final postText:Ljava/lang/String;

.field private final postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

.field private final postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

.field private final timelines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)V
    .locals 1
    .param p1, "postType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 147
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 149
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 150
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 151
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postText:Ljava/lang/String;

    .line 152
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->timelines:Ljava/util/List;

    .line 153
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    .line 154
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V
    .locals 2
    .param p1, "postType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "timelineOwner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 161
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 162
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 163
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 165
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 166
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 167
    iput-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postText:Ljava/lang/String;

    .line 168
    invoke-static {p3, p4}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->with(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->timelines:Ljava/util/List;

    .line 169
    iput-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    .line 170
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)V
    .locals 7
    .param p1, "postType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "postText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "timelineOwner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "linkPostData"    # Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 111
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V
    .locals 7
    .param p1, "postType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "postText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "timelineOwner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 119
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)V

    .line 120
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)V
    .locals 1
    .param p1, "postType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "postText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "timelineOwner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p6, "linkPostData"    # Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 129
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 130
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 131
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 134
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postText:Ljava/lang/String;

    .line 135
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    invoke-static {p3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 140
    :goto_0
    invoke-static {p4, p5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->with(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->timelines:Ljava/util/List;

    .line 141
    iput-object p6, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    .line 142
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
