.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Tutorial;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Tutorial"
.end annotation


# static fields
.field public static final FindFacebookFriends:Ljava/lang/String; = "Tutorial - Find Facebook friends"

.field public static final FriendSuggestions:Ljava/lang/String; = "Tutorial - Friend Suggestions"

.field public static final JoinClub:Ljava/lang/String; = "Tutorial - Join a club"

.field public static final LaunchOOBE:Ljava/lang/String; = "Tutorial - Launch OOBE"

.field public static final RedeemCode:Ljava/lang/String; = "Tutorial - Redeem a code"

.field public static final SetUpConsole:Ljava/lang/String; = "Tutorial - Set up a console"

.field public static final ShopForGames:Ljava/lang/String; = "Tutorial - Shop for games"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
