.class public Lcom/microsoft/xbox/xle/ui/TitleBarView;
.super Landroid/widget/RelativeLayout;
.source "TitleBarView.java"


# instance fields
.field private progressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 20
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 21
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f03022f

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 23
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f09011b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 24
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 26
    const v2, 0x7f0e01ea

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/TitleBarView;->progressBar:Landroid/widget/ProgressBar;

    .line 27
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/TitleBarView;->progressBar:Landroid/widget/ProgressBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 29
    const v2, 0x7f0e0ac4

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->setId(I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method


# virtual methods
.method public isLoading()Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/TitleBarView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateIsLoading(Z)V
    .locals 2
    .param p1, "isLoading"    # Z

    .prologue
    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TitleBarView;->progressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 40
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
