.class Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShareToFeedRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final caption:Ljava/lang/String;

.field private final itemLocator:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "xuid"    # Ljava/lang/String;
    .param p3, "itemLocator"    # Ljava/lang/String;
    .param p4, "caption"    # Ljava/lang/String;

    .prologue
    .line 4540
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4541
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->xuid:Ljava/lang/String;

    .line 4542
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->itemLocator:Ljava/lang/String;

    .line 4543
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->caption:Ljava/lang/String;

    .line 4544
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4535
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4552
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->xuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->itemLocator:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->caption:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->shareToFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/PostCommentResult;

    .line 4553
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4566
    const-wide/16 v0, 0x2330

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4558
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 4559
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->invalidateModels(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 4560
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->ItemSharedToFeed:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ShareToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 4562
    :cond_0
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4548
    return-void
.end method
