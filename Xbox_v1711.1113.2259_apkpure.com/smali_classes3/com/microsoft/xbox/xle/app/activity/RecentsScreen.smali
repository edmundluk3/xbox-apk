.class public Lcom/microsoft/xbox/xle/app/activity/RecentsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "RecentsScreen.java"


# static fields
.field private static final ACTIVITY_NAME:Ljava/lang/String; = "Recents"

.field private static final CHANNEL_NAME:Ljava/lang/String; = "Recents"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "Recents"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/RecentsScreen;->onCreateContentView()V

    .line 33
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/RecentsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 34
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f0301e3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/RecentsScreen;->setContentView(I)V

    .line 44
    return-void
.end method
