.class public Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;
.super Ljava/lang/Object;
.source "FeedPostPreviewDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LinkPreviewResponse"
.end annotation


# instance fields
.field public final description:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Description"
    .end annotation
.end field

.field private final imageUrl:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ImageUrl"
    .end annotation
.end field

.field public final link:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Link"
    .end annotation
.end field

.field public final linkData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LinkData"
    .end annotation
.end field

.field public final linkType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LinkType"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Title"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "link"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "title"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "imageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "linkType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "linkData"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->description:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->link:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->title:Ljava/lang/String;

    .line 84
    iput-object p4, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->imageUrl:Ljava/lang/String;

    .line 85
    iput-object p5, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->linkType:Ljava/lang/String;

    .line 86
    iput-object p6, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->linkData:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;

    .line 87
    return-void
.end method


# virtual methods
.method public getImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->imageUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;->imageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->ensureHttpsPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
