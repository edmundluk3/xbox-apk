.class public Lcom/microsoft/xbox/xle/test/automator/Automator;
.super Ljava/lang/Object;
.source "Automator.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/test/automator/IAutomator;


# static fields
.field private static appBarAnimationStateChanged:Lcom/microsoft/xle/test/interop/delegates/Action2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;",
            "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;",
            ">;"
        }
    .end annotation
.end field

.field private static instance:Lcom/microsoft/xbox/xle/test/automator/IAutomator;


# instance fields
.field private allowAutoConnect:Z

.field private appBarCallback:Lcom/microsoft/xle/test/interop/delegates/Action2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private cleanupCallback:Lcom/microsoft/xle/test/interop/delegates/Action;

.field private currentDialog:Landroid/app/Dialog;

.field private handleThrowableMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private logoutDesired:Z

.field private notifyActivityStateChangeMethod:Lcom/microsoft/xle/test/interop/delegates/Action2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            "Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;",
            ">;"
        }
    .end annotation
.end field

.field private onShowDialogMethod:Lcom/microsoft/xle/test/interop/delegates/Action2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private onShowErrorMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private onShowToastMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private takeSnapshotMethod:Lcom/microsoft/xle/test/interop/delegates/Action;

.field private viewModelAdapterMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;",
            ">;>;"
        }
    .end annotation
.end field

.field private viewModels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    sput-object v0, Lcom/microsoft/xbox/xle/test/automator/Automator;->instance:Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    .line 74
    sput-object v0, Lcom/microsoft/xbox/xle/test/automator/Automator;->appBarAnimationStateChanged:Lcom/microsoft/xle/test/interop/delegates/Action2;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->handleThrowableMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;

    .line 57
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->takeSnapshotMethod:Lcom/microsoft/xle/test/interop/delegates/Action;

    .line 59
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->notifyActivityStateChangeMethod:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->logoutDesired:Z

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->viewModelAdapterMap:Ljava/util/HashMap;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->viewModels:Ljava/util/HashMap;

    .line 66
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->onShowToastMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;

    .line 67
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->onShowErrorMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;

    .line 68
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->onShowDialogMethod:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 70
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->currentDialog:Landroid/app/Dialog;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->allowAutoConnect:Z

    .line 590
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->appBarCallback:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 592
    iput-object v1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->cleanupCallback:Lcom/microsoft/xle/test/interop/delegates/Action;

    .line 78
    return-void
.end method

.method private AutoLoginManualOverride()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/microsoft/xbox/xle/test/automator/Automator;->instance:Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/microsoft/xbox/xle/test/automator/Automator;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/test/automator/Automator;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/test/automator/Automator;->instance:Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    .line 85
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/test/automator/Automator;->instance:Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    return-object v0
.end method


# virtual methods
.method public OnActivityStateChange(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;)V
    .locals 0
    .param p1, "activity"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "change"    # Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;

    .prologue
    .line 205
    return-void
.end method

.method public cleanupListenerHooks()V
    .locals 0

    .prologue
    .line 611
    return-void
.end method

.method public clearFPSTracker()V
    .locals 2

    .prologue
    .line 441
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 442
    invoke-static {}, Lcom/microsoft/xbox/toolkit/FPSTool;->getInstance()Lcom/microsoft/xbox/toolkit/FPSTool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/FPSTool;->clearFPS()V

    .line 443
    return-void

    .line 441
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearLPSTracker()V
    .locals 2

    .prologue
    .line 461
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 462
    invoke-static {}, Lcom/microsoft/xbox/toolkit/TimeTool;->getInstance()Lcom/microsoft/xbox/toolkit/TimeTool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeTool;->clear()V

    .line 463
    return-void

    .line 461
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearLastSentKeys()V
    .locals 0

    .prologue
    .line 568
    return-void
.end method

.method public clearMostRecentlyPlayedSounds()V
    .locals 0

    .prologue
    .line 513
    return-void
.end method

.method public expireToken(Ljava/lang/String;)Z
    .locals 1
    .param p1, "audienceUri"    # Ljava/lang/String;

    .prologue
    .line 499
    const/4 v0, 0x0

    return v0
.end method

.method public getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;)",
            "Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;"
        }
    .end annotation

    .prologue
    .line 280
    .local p1, "viewModelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAppBarNowPlaying()Z
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    return v0
.end method

.method public getAppBarState()Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;
    .locals 1

    .prologue
    .line 547
    const/4 v0, 0x0

    return-object v0
.end method

.method public getApplicationBlockingTouch()Z
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    return v0
.end method

.method public getAverageFPS()I
    .locals 2

    .prologue
    .line 446
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 447
    invoke-static {}, Lcom/microsoft/xbox/toolkit/FPSTool;->getInstance()Lcom/microsoft/xbox/toolkit/FPSTool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/FPSTool;->getAverageFPS()I

    move-result v0

    return v0

    .line 446
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "expectedActivity":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDismissKeyboardMethod()Lcom/microsoft/xle/test/interop/delegates/Action;
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIsTablet()Z
    .locals 1

    .prologue
    .line 615
    const/4 v0, 0x0

    return v0
.end method

.method public getLastSentKeys()[Lcom/microsoft/xbox/service/model/LRCControlKey;
    .locals 1

    .prologue
    .line 561
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxMemoryLimitKb()I
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x0

    return v0
.end method

.method public getMinFPS()I
    .locals 2

    .prologue
    .line 451
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 452
    invoke-static {}, Lcom/microsoft/xbox/toolkit/FPSTool;->getInstance()Lcom/microsoft/xbox/toolkit/FPSTool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/FPSTool;->getMinFPS()I

    move-result v0

    return v0

    .line 451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMostRecentlyPlayedSounds()[Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 506
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPageStates()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 437
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResourceText(I)Ljava/lang/String;
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 346
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTestAllowsAutoConnect(Z)Z
    .locals 0
    .param p1, "original"    # Z

    .prologue
    .line 586
    return p1
.end method

.method public getTotalLeakedAdapterCount()I
    .locals 1

    .prologue
    .line 519
    const/4 v0, 0x0

    return v0
.end method

.method public getTotalPages()I
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x0

    return v0
.end method

.method public getUsedKb()I
    .locals 1

    .prologue
    .line 527
    const/4 v0, 0x0

    return v0
.end method

.method public getViewScreenRect(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 415
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v3, v6, :cond_0

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 417
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 418
    .local v0, "locationOnScreen":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 420
    aget v1, v0, v5

    .line 421
    .local v1, "x":I
    aget v2, v0, v4

    .line 423
    .local v2, "y":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-direct {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v3

    .end local v0    # "locationOnScreen":[I
    .end local v1    # "x":I
    .end local v2    # "y":I
    :cond_0
    move v3, v5

    .line 415
    goto :goto_0
.end method

.method public handleThrowable(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 128
    return-void
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    return v0
.end method

.method public leaveSession()V
    .locals 0

    .prologue
    .line 574
    return-void
.end method

.method public logOut()V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public onShowDialog(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 320
    const/4 v0, 0x0

    return v0
.end method

.method public onShowError(I)Z
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 301
    const/4 v0, 0x0

    return v0
.end method

.method public setAdapter(Ljava/lang/Class;Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;)V
    .locals 0
    .param p2, "adapter"    # Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 269
    .local p1, "viewModelClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;>;"
    return-void
.end method

.method public setAllowAutoConnect(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 580
    return-void
.end method

.method public setAllowDismissKeyboardHandler(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 165
    return-void
.end method

.method public setAppBarAnimationCallback(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;",
            "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 406
    .local p1, "callback":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;>;"
    return-void
.end method

.method public setAppBarItemHook(Lcom/microsoft/xle/test/interop/delegates/Action2;Lcom/microsoft/xle/test/interop/delegates/Action;)V
    .locals 0
    .param p2, "cleanup"    # Lcom/microsoft/xle/test/interop/delegates/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View$OnClickListener;",
            ">;",
            "Lcom/microsoft/xle/test/interop/delegates/Action;",
            ")V"
        }
    .end annotation

    .prologue
    .line 599
    .local p1, "appBarHook":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Ljava/lang/Integer;Landroid/view/View$OnClickListener;>;"
    return-void
.end method

.method public setAppbarAnimationState(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V
    .locals 0
    .param p1, "currentState"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;
    .param p2, "desiredState"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    .prologue
    .line 412
    return-void
.end method

.method public setAutomaticSignin(ZILcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .param p1, "automaticSignin"    # Z
    .param p2, "userIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Landroid/webkit/WebView;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 540
    .local p3, "signInDelegate":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Landroid/webkit/WebView;Ljava/lang/Integer;>;"
    invoke-static {p1, p2, p3}, Lcom/microsoft/xle/test/interop/TestInterop;->setAutomaticSignin(ZILcom/microsoft/xle/test/interop/delegates/Action2;)V

    .line 541
    return-void
.end method

.method public setCompanionSession(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;)V
    .locals 0
    .param p1, "companionSession"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .prologue
    .line 254
    return-void
.end method

.method public setCurrentDialog(Ljava/lang/String;Landroid/app/Dialog;)V
    .locals 0
    .param p1, "prompt"    # Ljava/lang/String;
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 101
    return-void
.end method

.method public setEDSServiceManager(Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;)V
    .locals 0
    .param p1, "serviceManager"    # Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    .prologue
    .line 247
    return-void
.end method

.method public setEnvironment(Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;)V
    .locals 1
    .param p1, "env"    # Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    .prologue
    .line 183
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->setEnvironment(Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;)V

    .line 184
    return-void
.end method

.method public setIsUsingToken(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 187
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 188
    return-void

    .line 187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListenerHook(ILandroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 605
    return-void
.end method

.method public setLogOutOnLaunch(Z)V
    .locals 0
    .param p1, "logoutOnLaunch"    # Z

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->logoutDesired:Z

    .line 171
    return-void
.end method

.method public setMembershipLevel(ZZ)V
    .locals 0
    .param p1, "overrideMembershipLevel"    # Z
    .param p2, "isGold"    # Z

    .prologue
    .line 376
    return-void
.end method

.method public setMonitorLPS(Z)V
    .locals 2
    .param p1, "monitor"    # Z

    .prologue
    .line 456
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 457
    invoke-static {p1}, Lcom/microsoft/xle/test/interop/TestInterop;->setMonitorLPS(Z)V

    .line 458
    return-void

    .line 456
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNoRedirectTestNetworkHttpClient(Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;

    .prologue
    .line 488
    return-void
.end method

.method public setNotifyOnActivityStateChange(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            "Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 196
    .local p1, "activityStateChangeAction":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->notifyActivityStateChangeMethod:Lcom/microsoft/xle/test/interop/delegates/Action2;

    .line 197
    return-void
.end method

.method public setNotifyOnServiceManagerActivityStateChange(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "action":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;>;"
    return-void
.end method

.method public setOnLoginStateChangedCallback(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/XLEException;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 391
    .local p1, "callback":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Ljava/lang/String;Lcom/microsoft/xbox/toolkit/XLEException;>;"
    return-void
.end method

.method public setOnShowDialog(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 311
    .local p1, "action":Lcom/microsoft/xle/test/interop/delegates/Action2;, "Lcom/microsoft/xle/test/interop/delegates/Action2<Ljava/lang/String;Ljava/lang/String;>;"
    return-void
.end method

.method public setOnShowError(Lcom/microsoft/xle/test/interop/delegates/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "action":Lcom/microsoft/xle/test/interop/delegates/Action1;, "Lcom/microsoft/xle/test/interop/delegates/Action1<Ljava/lang/Integer;>;"
    return-void
.end method

.method public setSLSServiceManager(Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;)V
    .locals 0
    .param p1, "serviceManager"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .prologue
    .line 260
    return-void
.end method

.method public setTakeScreenshot(Lcom/microsoft/xle/test/interop/delegates/Action;)V
    .locals 0
    .param p1, "action"    # Lcom/microsoft/xle/test/interop/delegates/Action;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->takeSnapshotMethod:Lcom/microsoft/xle/test/interop/delegates/Action;

    .line 136
    return-void
.end method

.method public setTestNetworkHttpClient(Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;

    .prologue
    .line 482
    return-void
.end method

.method public setTestSettings(Z)V
    .locals 0
    .param p1, "testEDSStub"    # Z

    .prologue
    .line 191
    return-void
.end method

.method public setTestTextureHttpClient(Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;

    .prologue
    .line 494
    return-void
.end method

.method public setThrowableHandler(Lcom/microsoft/xle/test/interop/delegates/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "action":Lcom/microsoft/xle/test/interop/delegates/Action1;, "Lcom/microsoft/xle/test/interop/delegates/Action1<Ljava/lang/Throwable;>;"
    invoke-static {p1}, Lcom/microsoft/xle/test/interop/CrashReporter;->setThrowableHandler(Lcom/microsoft/xle/test/interop/delegates/Action1;)V

    .line 118
    iput-object p1, p0, Lcom/microsoft/xbox/xle/test/automator/Automator;->handleThrowableMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;

    .line 119
    return-void
.end method

.method public setUseStub(Z)V
    .locals 1
    .param p1, "useStubData"    # Z

    .prologue
    .line 179
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->setStub(Z)V

    .line 180
    return-void
.end method

.method public setUserIsChild(ZZ)V
    .locals 0
    .param p1, "enableOverride"    # Z
    .param p2, "isChildAccount"    # Z

    .prologue
    .line 397
    return-void
.end method

.method public setVersion(III)V
    .locals 0
    .param p1, "minVersionRequired"    # I
    .param p2, "latestVersionAvailable"    # I
    .param p3, "currentVersion"    # I

    .prologue
    .line 469
    return-void
.end method

.method public setVersionToApplicationDefaults()V
    .locals 0

    .prologue
    .line 475
    return-void
.end method

.method public setViewModelForActivity(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/ViewGroup;",
            ">(TT;",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "activity":Landroid/view/ViewGroup;, "TT;"
    return-void
.end method

.method public takeScreenshot()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public testTearDown()V
    .locals 0

    .prologue
    .line 400
    return-void
.end method
