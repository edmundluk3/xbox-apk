.class public Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
.super Ljava/lang/Object;
.source "MultiplayerTelemetryService.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V
    .locals 2
    .param p1, "telemetryProvider"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    .line 38
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->getEvents()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
    .param p1, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->populateModel(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v0

    return-object v0
.end method

.method private handleEvents(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 188
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 203
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
    .param p1, "telemetryEvent"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->handleEvents(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    return-void
.end method

.method private populateModel(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .locals 5
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 212
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 213
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 214
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->getPayload()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 215
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->getPayload()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 216
    .local v2, "value":Ljava/lang/Object;
    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 218
    .end local v0    # "key":Ljava/lang/String;
    .end local v2    # "value":Ljava/lang/Object;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public LFGDetails()V
    .locals 1

    .prologue
    .line 77
    const-string v0, "Party - View LFG Details"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public cancelInvite()V
    .locals 1

    .prologue
    .line 175
    const-string v0, "Party - Cancel"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method public createLFG()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "Party - Looking for Group"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public goBack()V
    .locals 1

    .prologue
    .line 171
    const-string v0, "Party - Back"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public inviteOnly(Ljava/lang/String;)V
    .locals 1
    .param p1, "joinRestriction"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 91
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$2;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 102
    return-void
.end method

.method public inviteToParty()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "Party - Invite"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public joinParty(Ljava/lang/String;)V
    .locals 1
    .param p1, "partyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 50
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$1;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 62
    return-void
.end method

.method public leaveParty()V
    .locals 1

    .prologue
    .line 85
    const-string v0, "Party - Leave party"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 86
    const-string v0, "PartyId"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->removePersistentValue(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public muteMember(Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "isAudioEnabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p2, "partyMembers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;Ljava/util/Map;Z)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 137
    return-void
.end method

.method public muteParty(Z)V
    .locals 1
    .param p1, "mute"    # Z

    .prologue
    .line 105
    if-eqz p1, :cond_0

    const-string v0, "Party - Mute party"

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 106
    return-void

    .line 105
    :cond_0
    const-string v0, "Party - UnMute party"

    goto :goto_0
.end method

.method public removeMember(Ljava/lang/String;)V
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 141
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$4;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$4;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 153
    return-void
.end method

.method public sendChatMessage()V
    .locals 1

    .prologue
    .line 179
    const-string v0, "Party - Send chat message"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public startParty()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "Party - Start a party"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public viewChat()V
    .locals 1

    .prologue
    .line 81
    const-string v0, "Party - View chat"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public viewParty()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "Party - View party details"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public viewProfile(Ljava/lang/String;)V
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 156
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$5;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$5;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 168
    return-void
.end method
