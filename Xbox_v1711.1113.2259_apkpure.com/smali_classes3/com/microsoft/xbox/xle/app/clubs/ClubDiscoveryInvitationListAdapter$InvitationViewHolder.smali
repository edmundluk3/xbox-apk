.class public Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubDiscoveryInvitationListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InvitationViewHolder"
.end annotation


# instance fields
.field acceptButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0394
    .end annotation
.end field

.field clubBanner:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e038a
    .end annotation
.end field

.field clubGlyph:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e038f
    .end annotation
.end field

.field clubIcon:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e038e
    .end annotation
.end field

.field clubName:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e038c
    .end annotation
.end field

.field clubOwner:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0392
    .end annotation
.end field

.field clubPic:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e038b
    .end annotation
.end field

.field clubType:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0390
    .end annotation
.end field

.field headerView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0389
    .end annotation
.end field

.field ignoreButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0391
    .end annotation
.end field

.field inviteReason:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0393
    .end annotation
.end field

.field rootView:Landroid/view/View;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    .line 85
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 86
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->rootView:Landroid/view/View;

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->rootView:Landroid/view/View;

    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 88
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;Landroid/view/View;)V
    .locals 2
    .param p0, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubId()J

    move-result-wide v0

    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;->onProfileClick(J)V

    return-void
.end method

.method static synthetic lambda$bindTo$1(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;Landroid/view/View;)V
    .locals 2
    .param p0, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubId()J

    move-result-wide v0

    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;->onAcceptClick(J)V

    return-void
.end method

.method static synthetic lambda$bindTo$2(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;Landroid/view/View;)V
    .locals 2
    .param p0, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubId()J

    move-result-wide v0

    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;->onIgnoreClick(J)V

    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    .param p2, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;

    .prologue
    const v4, 0x7f0201fa

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->headerView:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubPic:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubPic()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v4}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubBanner:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubBanner()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v3, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubType:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->isTitleClub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubIcon:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubGlyph:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubGlyph:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubGlyph()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v4}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubOwner:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubOwner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->inviteReason:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getInviteReason()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->rootView:Landroid/view/View;

    invoke-static {p2, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->acceptButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->ignoreButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p2, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubIcon:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubGlyph:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
