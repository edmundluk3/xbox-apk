.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubAdminMembersScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;
    }
.end annotation


# static fields
.field private static final MAX_RESULTS:I = 0x64

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentFilter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

.field private currentSearchTerm:Ljava/lang/String;

.field private volatile isSearchingService:Z

.field private isViewerOwner:Z

.field private final latestSearchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final memberPresenceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;"
        }
    .end annotation
.end field

.field private final members:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final moderatorXuids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final moderators:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;",
            ">;"
        }
    .end annotation
.end field

.field private searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

.field private searchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;",
            ">;"
        }
    .end annotation
.end field

.field userSummaryDisposable:Lio/reactivex/disposables/Disposable;

.field userSummaryFromUserSearchResultDataMapper:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 96
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 98
    new-instance v0, Ljava/util/TreeSet;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$1;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    .line 99
    new-instance v0, Ljava/util/TreeSet;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$2;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    .line 100
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->memberPresenceList:Ljava/util/List;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->latestSearchResults:Ljava/util/List;

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminMembersScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 104
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->searchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 106
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V

    .line 107
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->onSearchResult(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateMemberLists(Ljava/util/List;)V

    return-void
.end method

.method private getFilteredRosterData()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    sget-object v4, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->MODERATORS:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    if-ne v3, v4, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 179
    .local v2, "unfilteredData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 190
    .end local v2    # "unfilteredData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    :goto_1
    return-object v2

    .line 175
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 182
    .restart local v2    # "unfilteredData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 184
    .local v0, "filteredData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 185
    .local v1, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->matchesSearchTerm(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 186
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    :cond_3
    move-object v2, v0

    .line 190
    goto :goto_1
.end method

.method private getFilteredSearchData()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->MODERATORS:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    if-ne v2, v3, :cond_1

    .line 159
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 161
    .local v1, "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 162
    .local v0, "member":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 163
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 170
    .end local v0    # "member":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .end local v1    # "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;>;"
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    :cond_2
    return-object v1
.end method

.method private getXuidsToLoad()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v6, 0x64

    .line 405
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 407
    .local v3, "xuidsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 408
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 409
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->moderator()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 410
    .local v2, "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    if-ge v4, v6, :cond_0

    .line 411
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 410
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 415
    .end local v1    # "i":I
    .end local v2    # "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->memberPresenceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    if-ge v4, v6, :cond_1

    .line 416
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->memberPresenceList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 415
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 419
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v4
.end method

.method static synthetic lambda$ban$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$demoteFromModerator$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$loadPersonSummaries$6(Ljava/util/ArrayList;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)V
    .locals 0
    .param p0, "collection"    # Ljava/util/ArrayList;
    .param p1, "item"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 392
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic lambda$loadPersonSummaries$7(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 397
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateMemberLists(Ljava/util/List;)V

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)I
    .locals 2
    .param p0, "lhs"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .param p1, "rhs"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;)I
    .locals 2
    .param p0, "lhs"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .param p1, "rhs"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic lambda$onSearchResult$8(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
    .param p1, "it"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 467
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryFromUserSearchResultDataMapper:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;

    iget-object v1, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;->apply(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$promoteToModerator$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$remove$5(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V

    return-void
.end method

.method private declared-synchronized loadPersonSummaries()V
    .locals 5

    .prologue
    .line 383
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->stopActiveTasks()V

    .line 385
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getXuidsToLoad()Ljava/util/List;

    move-result-object v0

    .line 387
    .local v0, "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    .line 389
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;->load(Ljava/util/Collection;)Lio/reactivex/Observable;

    move-result-object v2

    .line 390
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 391
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/BiConsumer;

    move-result-object v4

    .line 392
    invoke-virtual {v2, v3, v4}, Lio/reactivex/Observable;->collectInto(Ljava/lang/Object;Lio/reactivex/functions/BiConsumer;)Lio/reactivex/Single;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 393
    invoke-virtual {v2, v3, v4}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryDisposable:Lio/reactivex/disposables/Disposable;

    .line 388
    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    :goto_0
    monitor-exit p0

    return-void

    .line 400
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateMemberLists(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383
    .end local v0    # "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized onSearchResult(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V
    .locals 8
    .param p1, "response"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    .prologue
    .line 462
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->isSearchingService:Z

    .line 463
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 465
    if-eqz p1, :cond_0

    .line 466
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;->getResults()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 467
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 468
    invoke-virtual {v2}, Lio/reactivex/Observable;->blockingIterable()Ljava/lang/Iterable;

    move-result-object v1

    .line 469
    .local v1, "summaryIterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 470
    .local v0, "result":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->latestSearchResults:Ljava/util/List;

    new-instance v4, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-direct {v4, v0, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;-><init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 462
    .end local v0    # "result":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .end local v1    # "summaryIterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 474
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 475
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized removeLocalInstances(J)V
    .locals 5
    .param p1, "xuid"    # J

    .prologue
    .line 280
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 281
    .local v0, "listItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 282
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 283
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 284
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    .end local v0    # "listItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    :cond_1
    monitor-exit p0

    return-void

    .line 280
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private stopActiveTasks()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->userSummaryDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 326
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->stopSearchTask()V

    .line 327
    return-void
.end method

.method private stopSearchTask()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->cancel()V

    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    .line 334
    :cond_0
    return-void
.end method

.method private declared-synchronized updateMemberLists(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 423
    .local p1, "peopleList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->clear()V

    .line 424
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->clear()V

    .line 425
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->clear()V

    .line 427
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 428
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->moderator()Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 429
    .local v3, "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 423
    .end local v3    # "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 433
    :cond_0
    :try_start_1
    new-instance v4, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v4}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 434
    .local v4, "personSummaries":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    if-eqz p1, :cond_1

    .line 435
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 436
    .local v6, "summary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v6}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_1

    .line 440
    .end local v6    # "summary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->memberPresenceList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 441
    .local v2, "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 442
    .local v7, "xuid":Ljava/lang/Long;
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 444
    .local v5, "personSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    if-eqz v5, :cond_2

    .line 445
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-virtual {v5}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 446
    .local v0, "isModerator":Z
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    invoke-direct {v1, v5, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;-><init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)V

    .line 448
    .local v1, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-interface {v9, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 450
    if-eqz v0, :cond_2

    .line 451
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-interface {v9, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 452
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 457
    .end local v0    # "isModerator":Z
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    .end local v2    # "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    .end local v5    # "personSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .end local v7    # "xuid":Ljava/lang/Long;
    :cond_3
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    sget-object v8, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_3
    iput-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 458
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459
    monitor-exit p0

    return-void

    .line 457
    :cond_4
    :try_start_2
    sget-object v8, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method public declared-synchronized ban(J)V
    .locals 5
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 262
    monitor-enter p0

    const-wide/16 v0, 0x1

    :try_start_0
    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminBanMember(JLjava/lang/String;)V

    .line 265
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->removeLocalInstances(J)V

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    const v3, 0x7f07022a

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ban(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    monitor-exit p0

    return-void

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized demoteFromModerator(J)V
    .locals 5
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 245
    monitor-enter p0

    const-wide/16 v2, 0x0

    :try_start_0
    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 247
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminDemoteMember(JLjava/lang/String;)V

    .line 248
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 249
    .local v0, "listItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 250
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->setIsModerator(Z)V

    .line 251
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 258
    .end local v0    # "listItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v2, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v3

    const v4, 0x7f07022c

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    invoke-virtual {v1, p1, p2, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->demoteFromModerator(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    monitor-exit p0

    return-void

    .line 245
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public filterOptionSelected(I)V
    .locals 6
    .param p1, "position"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 208
    const-wide/16 v0, 0x0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    move-result-object v2

    array-length v2, v2

    int-to-long v2, v2

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackMemberFilter(JI)V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    move-result-object v1

    aget-object v1, v1, p1

    if-eq v0, v1, :cond_0

    .line 212
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    move-result-object v0

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    .line 213
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V

    .line 215
    :cond_0
    return-void
.end method

.method public declared-synchronized getFilterOptions()Ljava/util/List;
    .locals 8
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 196
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    move-result-object v5

    array-length v5, v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 197
    .local v2, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "Update this function if values are added"

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    move-result-object v6

    array-length v6, v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    :goto_0
    invoke-static {v5, v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 199
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    .line 200
    .local v0, "allMembersCount":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    .line 202
    .local v1, "moderatorsCount":I
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f0702a0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f0702a1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-object v2

    .end local v0    # "allMembersCount":I
    .end local v1    # "moderatorsCount":I
    :cond_0
    move v3, v4

    .line 197
    goto :goto_0

    .line 196
    .end local v2    # "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getFilteredData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->serviceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getFilteredSearchData()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 152
    :goto_0
    monitor-exit p0

    return-object v0

    .line 154
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getFilteredRosterData()Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSearchHint()I
    .locals 2
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->MODERATORS:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    if-ne v0, v1, :cond_0

    const v0, 0x7f0702ab

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0702aa

    goto :goto_0
.end method

.method public getSelectedFilter()I
    .locals 3

    .prologue
    .line 218
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 219
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;->values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$Filter;

    if-ne v1, v2, :cond_0

    .line 224
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 218
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->isSearchingService:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isViewerOwner()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->isViewerOwner:Z

    return v0
.end method

.method protected loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 338
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->ClubPresence:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 343
    return-void
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 352
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 379
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V

    .line 380
    return-void

    .line 356
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 358
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    .line 359
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->memberPresenceList:Ljava/util/List;

    monitor-enter v2

    .line 361
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->memberPresenceList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 362
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->memberPresenceList:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 363
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v1, v2}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->isViewerOwner:Z

    .line 366
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->loadPersonSummaries()V

    goto :goto_0

    .line 363
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 374
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 375
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club model changed to invalid state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 352
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 303
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminMembersScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 304
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 310
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 312
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 316
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 317
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->stopActiveTasks()V

    .line 318
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    .line 319
    return-void
.end method

.method public declared-synchronized promoteToModerator(J)V
    .locals 5
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 228
    monitor-enter p0

    const-wide/16 v2, 0x1

    :try_start_0
    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 230
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminPromoteMember(JLjava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;

    .line 232
    .local v0, "listItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->getXuid()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 233
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;->setIsModerator(Z)V

    .line 234
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderators:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 241
    .end local v0    # "listItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$ClubMembersListItem;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v2, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v3

    const v4, 0x7f07022f

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    invoke-virtual {v1, p1, p2, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->promoteToModerator(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    monitor-exit p0

    return-void

    .line 228
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized remove(J)V
    .locals 5
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 271
    monitor-enter p0

    const-wide/16 v0, 0x1

    :try_start_0
    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminRemoveMember(JLjava/lang/String;)V

    .line 274
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->removeLocalInstances(J)V

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    const v3, 0x7f070231

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->remove(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    monitor-exit p0

    return-void

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized report(JLjava/lang/String;Z)V
    .locals 7
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "gamerTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "isRealNameVisible"    # Z

    .prologue
    .line 293
    monitor-enter p0

    const-wide/16 v2, 0x1

    :try_start_0
    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 294
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminReportMember(JLjava/lang/String;)V

    .line 297
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 298
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    monitor-exit p0

    return-void

    .line 293
    .end local v0    # "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public serviceSearchEnabled()Z
    .locals 6

    .prologue
    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 142
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSearchTerm(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "term"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v4, 0x1

    .line 121
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->stopSearchTask()V

    .line 123
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    .line 125
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->serviceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->isSearchingService:Z

    .line 128
    monitor-enter p0

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 130
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    new-instance v0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->searchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->load(Z)V

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->updateAdapter()V

    .line 138
    return-void

    .line 123
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public showTooManyWarning()Z
    .locals 6

    .prologue
    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 147
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->members:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
