.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ConversationsSelectorListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
        ">;"
    }
.end annotation


# instance fields
.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILjava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "rowViewResourceId"    # I
    .param p4, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    .local p3, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->notifyDataSetChanged()V

    .line 35
    return-void
.end method

.method private getMessageText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;
    .locals 9
    .param p1, "conversation"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 84
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v1, :cond_1

    .line 86
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v0

    .line 88
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 98
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    .line 102
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :goto_1
    return-object v1

    .line 86
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    goto :goto_0

    .line 90
    .restart local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :pswitch_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070639

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v6, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberInitiator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getGroupMemberGamertag(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v6, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getGroupSenderGamertagText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 92
    :pswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07065a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v6, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getDeleteMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getGroupSenderGamertagText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 94
    :pswitch_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07063d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v6, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsSelectorScreenViewModel;->getGroupMemberGamertag(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->skypeMessageType:Ljava/lang/String;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageText:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 102
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_1
    :pswitch_3
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v9, 0x7f020125

    .line 39
    move-object v5, p2

    .line 41
    .local v5, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    instance-of v7, v7, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v7, :cond_4

    .line 42
    if-eqz v5, :cond_0

    instance-of v7, v5, Landroid/widget/LinearLayout;

    if-nez v7, :cond_1

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 44
    .local v6, "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f0300c7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 46
    .end local v6    # "vi":Landroid/view/LayoutInflater;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 47
    .local v1, "group":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v1, :cond_4

    .line 49
    invoke-virtual {v5, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 50
    const v7, 0x7f0e04c6

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 51
    .local v2, "groupName":Landroid/widget/TextView;
    const v7, 0x7f0e04c8

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 53
    .local v0, "description":Landroid/widget/TextView;
    const v7, 0x7f0e04c3

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 54
    .local v4, "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    const v7, 0x7f0e04c9

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 56
    .local v3, "iconSelect":Landroid/widget/TextView;
    if-eqz v2, :cond_2

    .line 57
    iget-boolean v7, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-eqz v7, :cond_5

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupTopic(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;

    move-result-object v7

    :goto_0
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    :cond_2
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->getMessageText(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->getIsSelected()Z

    move-result v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 64
    if-eqz v4, :cond_3

    .line 65
    iget-object v7, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    invoke-virtual {v4, v7, v9, v9}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 71
    :cond_3
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->getIsSelected()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 78
    .end local v0    # "description":Landroid/widget/TextView;
    .end local v1    # "group":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v2    # "groupName":Landroid/widget/TextView;
    .end local v3    # "iconSelect":Landroid/widget/TextView;
    .end local v4    # "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    :cond_4
    :goto_1
    return-object v5

    .line 57
    .restart local v0    # "description":Landroid/widget/TextView;
    .restart local v1    # "group":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .restart local v2    # "groupName":Landroid/widget/TextView;
    .restart local v3    # "iconSelect":Landroid/widget/TextView;
    .restart local v4    # "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    :cond_5
    iget-object v7, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto :goto_0

    .line 74
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0145

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1
.end method
