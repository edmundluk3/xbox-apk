.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "GameProfileAchievementsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;
    }
.end annotation


# instance fields
.field private achievementsFilter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

.field private allAchievements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private currentGamerScore:I

.field private earnedAchievements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private forceRefresh:Z

.field private friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

.field private gamerEarnedAchievements:I

.field private isLoading:Z

.field private isLoadingAchievementsFriendsWhoPlayStatus:Z

.field private isLoadingCompletedAchievements:Z

.field private isLoadingLeaderboardStats:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingLockedAchievements:Z

.field private isLoadingTitleData:Z

.field private load360AchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

.field private load360EarnedAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

.field private load360Task:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;

.field private loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

.field private loadStatLeadearboardRankTask:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;",
            ">;"
        }
    .end annotation
.end field

.field private loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

.field private loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

.field private peopleWhoPlayed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private statList:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;",
            ">;"
        }
    .end annotation
.end field

.field private titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

.field private titleId:J

.field private titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private titleName:Ljava/lang/String;

.field private totalGamerScore:I

.field private unearnedAchievements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private updateStatListView:Z

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    const/4 v3, 0x0

    .line 89
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 53
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 67
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    .line 68
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardStats:Ljava/util/Hashtable;

    .line 70
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateStatListView:Z

    .line 72
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->forceRefresh:Z

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 92
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 94
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    .line 96
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->xuid:Ljava/lang/String;

    .line 97
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 98
    return-void

    .line 94
    :cond_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0
.end method

.method static synthetic access$1002(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingTitleData:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->onLoad360AchievementDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->onLoadAchievementsDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V

    return-void
.end method

.method static synthetic access$1302(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLockedAchievements:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingCompletedAchievements:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->onLoadGameProfileAchievementsFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingAchievementsFriendsWhoPlayStatus:Z

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->onLoadStatLeaderboardCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)Ljava/util/Hashtable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardStats:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->onLoadAchievementDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$802(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoading:Z

    return p1
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->onLoadTitleDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private static buildTitleSatisticsTable(JLjava/util/List;)Ljava/util/Hashtable;
    .locals 16
    .param p0, "gameTitleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;",
            ">;"
        }
    .end annotation

    .prologue
    .line 790
    .local p2, "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    .line 792
    .local v4, "statList":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 793
    .local v2, "heroStats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;>;"
    invoke-static/range {p0 .. p1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    .line 795
    .local v11, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleStatisticsData()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v9

    .line 796
    .local v9, "statsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    if-eqz v9, :cond_c

    iget-object v12, v9, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    if-eqz v12, :cond_c

    .line 797
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v10

    .line 800
    .local v10, "titleId":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_6

    .line 801
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 802
    .local v5, "statType":Ljava/lang/String;
    iget-object v13, v9, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    if-eqz v13, :cond_0

    iget-object v13, v9, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_0

    .line 803
    iget-object v13, v9, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 804
    .local v7, "statisticsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v7, :cond_0

    .line 805
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-nez v13, :cond_1

    .line 806
    new-instance v13, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    invoke-direct {v13}, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;-><init>()V

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 809
    :cond_1
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 811
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    iget-object v13, v7, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 812
    .local v8, "stats":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    if-eqz v8, :cond_2

    iget-object v14, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 813
    iget-object v14, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v14, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    iget-object v14, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    invoke-virtual {v14, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 814
    sget-object v14, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v14}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 815
    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    if-nez v13, :cond_3

    .line 816
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    iput-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    .line 818
    :cond_3
    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 821
    :cond_4
    sget-object v14, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v14}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 822
    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    if-nez v13, :cond_5

    .line 823
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    iput-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    .line 825
    :cond_5
    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 836
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v5    # "statType":Ljava/lang/String;
    .end local v7    # "statisticsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v8    # "stats":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    :cond_6
    iget-object v12, v9, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_7
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;

    .line 837
    .local v1, "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    if-eqz v1, :cond_7

    iget-object v13, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->titleid:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 838
    iget-object v13, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->titleid:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 841
    iget-object v12, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_c

    .line 842
    iget-object v12, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 843
    .restart local v7    # "statisticsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v7, :cond_c

    iget-object v12, v7, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_c

    .line 844
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_8

    .line 845
    new-instance v12, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    invoke-direct {v12}, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;-><init>()V

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 848
    :cond_8
    const/4 v12, 0x0

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 850
    .restart local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    iget-object v12, v7, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_9
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 851
    .restart local v8    # "stats":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    if-eqz v8, :cond_a

    iget-object v13, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 852
    iget-object v13, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->titleid:Ljava/lang/String;

    iput-object v13, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    .line 854
    :cond_a
    if-eqz v3, :cond_9

    .line 855
    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    if-nez v13, :cond_b

    .line 856
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    iput-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    .line 858
    :cond_b
    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 869
    .end local v1    # "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v7    # "statisticsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v8    # "stats":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .end local v10    # "titleId":Ljava/lang/String;
    :cond_c
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_e

    .line 870
    const/4 v12, 0x0

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 871
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v0, :cond_e

    iget-object v12, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v12

    if-nez v12, :cond_e

    .line 872
    iget-object v12, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_d
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_e

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 873
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    if-eqz v3, :cond_d

    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_d

    .line 874
    new-instance v6, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    invoke-direct {v6}, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;-><init>()V

    .line 875
    .local v6, "statWithRank":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    iput-object v3, v6, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 876
    iget-object v13, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v4, v13, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 882
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .end local v6    # "statWithRank":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    :cond_e
    return-object v4
.end method

.method private isForceRefresh()Z
    .locals 1

    .prologue
    .line 291
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->forceRefresh:Z

    return v0
.end method

.method private loadAchievementsInternal(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->cancel()V

    .line 121
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->xuid:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;JLjava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->load(Z)V

    .line 143
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->cancel()V

    .line 147
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->load(Z)V

    .line 149
    return-void

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360AchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360AchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->cancel()V

    .line 127
    :cond_3
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->COMPLETED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360AchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360AchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->load(Z)V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360EarnedAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    if-eqz v0, :cond_4

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360EarnedAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->cancel()V

    .line 133
    :cond_4
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->LOCKED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360EarnedAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360EarnedAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->load(Z)V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360Task:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;

    if-eqz v0, :cond_5

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360Task:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->cancel()V

    .line 139
    :cond_5
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->xuid:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360Task:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360Task:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->load(Z)V

    goto :goto_0
.end method

.method private onLoad360AchievementDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 9
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v8, 0x0

    .line 942
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoading:Z

    .line 944
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 992
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 993
    return-void

    .line 948
    :pswitch_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->xuid:Ljava/lang/String;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    .line 950
    .local v3, "profileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v3, :cond_3

    .line 952
    iget-wide v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXbox360TitleSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    move-result-object v5

    .line 954
    .local v5, "titleProgress":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    sget-object v7, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    sget-object v8, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 956
    .local v4, "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-wide v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v6, v7, v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->buildTitleSatisticsTable(JLjava/util/List;)Ljava/util/Hashtable;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    .line 958
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 959
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v0

    .line 960
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    .line 962
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v8, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 963
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v8, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;

    .line 964
    .local v2, "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->cancel()V

    .line 965
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v8, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    .end local v2    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;

    iget-object v7, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-direct {v2, p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 969
    .restart local v2    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v8, v8, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v7, v8, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 970
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isForceRefresh()Z

    move-result v7

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->load(Z)V

    goto :goto_1

    .line 974
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;>;"
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    .end local v2    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    :cond_2
    if-eqz v5, :cond_3

    .line 975
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v6

    iput v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->currentGamerScore:I

    .line 976
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getEarnedAchievements()I

    move-result v6

    iput v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->gamerEarnedAchievements:I

    .line 977
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v6

    iput v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->totalGamerScore:I

    .line 978
    iget-object v6, v5, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->name:Ljava/lang/String;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleName:Ljava/lang/String;

    .line 982
    .end local v4    # "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "titleProgress":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateViewModelState()V

    goto/16 :goto_0

    .line 986
    .end local v3    # "profileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    :pswitch_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    if-nez v6, :cond_0

    .line 987
    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 944
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadAchievementDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 13
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v12, 0x0

    .line 653
    iput-boolean v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoading:Z

    .line 655
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 728
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 729
    return-void

    .line 659
    :pswitch_0
    iget-wide v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v10, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v7

    .line 660
    .local v7, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->xuid:Ljava/lang/String;

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    .line 662
    .local v5, "profileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v7, :cond_8

    if-eqz v5, :cond_8

    .line 664
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    sget-object v10, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    const/4 v10, 0x1

    sget-object v11, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 666
    .local v6, "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-wide v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v10, v11, v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->buildTitleSatisticsTable(JLjava/util/List;)Ljava/util/Hashtable;

    move-result-object v9

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    .line 668
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    invoke-virtual {v9}, Ljava/util/Hashtable;->size()I

    move-result v9

    if-lez v9, :cond_2

    .line 669
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    invoke-virtual {v9}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v2

    .line 670
    .local v2, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    .line 672
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v11, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v11, v11, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 673
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v11, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v11, v11, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;

    .line 674
    .local v4, "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->cancel()V

    .line 675
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v11, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v11, v11, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    .end local v4    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    :cond_1
    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;

    iget-object v10, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-direct {v4, p0, v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 679
    .restart local v4    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    iget-object v11, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v11, v11, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v10, v11, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isForceRefresh()Z

    move-result v10

    invoke-virtual {v4, v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->load(Z)V

    goto :goto_1

    .line 685
    .end local v2    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;>;"
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    .end local v4    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    :cond_2
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v9

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 687
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getTitleProgressData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    move-result-object v8

    .line 689
    .local v8, "titleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    if-eqz v8, :cond_3

    .line 690
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v9

    iput v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->currentGamerScore:I

    .line 691
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEarnedAchievements()I

    move-result v9

    iput v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->gamerEarnedAchievements:I

    .line 692
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v9

    iput v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->totalGamerScore:I

    .line 693
    iget-object v9, v8, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleName:Ljava/lang/String;

    .line 696
    :cond_3
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->xuid:Ljava/lang/String;

    invoke-virtual {v7, v9}, Lcom/microsoft/xbox/service/model/TitleModel;->getCompareAchievementData(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 697
    .local v1, "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    if-eqz v1, :cond_8

    .line 698
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    .line 699
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 700
    .local v0, "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->achievementType:Ljava/lang/String;

    const-string v11, "challenge"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progressState:Ljava/lang/String;

    sget-object v11, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 701
    :cond_5
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 706
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    :cond_6
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    .line 707
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    .line 708
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 709
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v10, v11, :cond_7

    .line 710
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 712
    :cond_7
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 718
    .end local v1    # "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v6    # "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "titleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :cond_8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateViewModelState()V

    goto/16 :goto_0

    .line 722
    .end local v5    # "profileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v7    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    :pswitch_1
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    if-nez v9, :cond_0

    .line 723
    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 655
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadAchievementsDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .prologue
    const/4 v2, 0x0

    .line 1064
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->COMPLETED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    if-ne p2, v1, :cond_0

    .line 1065
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingCompletedAchievements:Z

    .line 1068
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->LOCKED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    if-ne p2, v1, :cond_1

    .line 1069
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLockedAchievements:Z

    .line 1072
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1104
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 1105
    return-void

    .line 1077
    :pswitch_0
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    .line 1078
    .local v0, "model":Lcom/microsoft/xbox/service/model/TitleModel;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$AchievementsFilter:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1094
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 1080
    :pswitch_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360EarnedAchievements()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1081
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360EarnedAchievements()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->setEarnedAchievements(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 1085
    :pswitch_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1086
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360EarnedAchievements()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1087
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360EarnedAchievements()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->setEarnedAchievements(Ljava/util/ArrayList;)V

    .line 1089
    :cond_4
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->setUnearnedAchievements(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 1098
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/TitleModel;
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    if-nez v1, :cond_2

    .line 1099
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 1072
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 1078
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onLoadGameProfileAchievementsFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 467
    const-string v0, "GameProfileAchievementsScreenViewModel"

    const-string v1, "onLoadGameProfileAchievementsFriendsWhoPlayCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingAchievementsFriendsWhoPlayStatus:Z

    .line 469
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 480
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 481
    return-void

    .line 473
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getResult(J)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->peopleWhoPlayed:Ljava/util/ArrayList;

    .line 474
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 469
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onLoadStatLeaderboardCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "stat"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    .line 548
    const-string v4, "GameProfileAchievementsScreenViewModel"

    const-string v5, "onLoadStatLeaderboardCompleted"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    if-eqz p2, :cond_0

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 551
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardStats:Ljava/util/Hashtable;

    iget-object v5, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 552
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardStats:Ljava/util/Hashtable;

    iget-object v5, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 553
    .local v0, "isLoadingStatLeaderboard":Ljava/lang/Boolean;
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 554
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardStats:Ljava/util/Hashtable;

    iget-object v5, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    .end local v0    # "isLoadingStatLeaderboard":Ljava/lang/Boolean;
    :cond_0
    :goto_0
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 583
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 584
    return-void

    .line 557
    :cond_1
    const-string v4, "GameProfileAchievementsScreenViewModel"

    const-string v5, "Unexpected error"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 564
    :pswitch_0
    if-eqz p2, :cond_4

    .line 565
    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v4

    iget-object v5, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->getLeaderBoard(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    move-result-object v2

    .line 566
    .local v2, "leaderboard":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    :goto_2
    if-eqz v2, :cond_4

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 567
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->userList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .line 568
    .local v3, "user":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->xuid:Ljava/lang/String;

    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->xuid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 569
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    iget-object v6, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    .line 570
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    iget v5, v3, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->rank:I

    iput v5, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->rank:I

    goto :goto_3

    .line 565
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    .end local v2    # "leaderboard":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .end local v3    # "user":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 576
    :cond_4
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateStatListView:Z

    .line 577
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateViewModelState()V

    goto :goto_1

    .line 560
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onLoadTitleDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 733
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingTitleData:Z

    .line 734
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 749
    :goto_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->forceRefresh:Z

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadAchievementsInternal(Z)V

    .line 750
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 751
    return-void

    .line 738
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    goto :goto_0

    .line 734
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setEarnedAchievements(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1108
    .local p1, "achievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    .line 1114
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1115
    return-void

    .line 1111
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method private setForceRefresh(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 295
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->forceRefresh:Z

    .line 296
    return-void
.end method

.method private setUnearnedAchievements(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1118
    .local p1, "achievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    if-nez v3, :cond_3

    .line 1119
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    .line 1124
    :goto_0
    if-eqz p1, :cond_4

    .line 1125
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;

    .line 1126
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;
    const/4 v0, 0x0

    .line 1127
    .local v0, "earnedAchievementFound":Z
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 1128
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 1129
    .local v1, "earnedItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->id:Ljava/lang/String;

    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    if-ne v5, v6, :cond_1

    .line 1130
    const/4 v0, 0x1

    .line 1136
    .end local v1    # "earnedItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_2
    if-nez v0, :cond_0

    .line 1137
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1121
    .end local v0    # "earnedAchievementFound":Z
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 1142
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    if-nez v3, :cond_6

    .line 1143
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    .line 1148
    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    if-eqz v3, :cond_5

    .line 1149
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1152
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1153
    return-void

    .line 1145
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto :goto_2
.end method

.method private updateViewModelState()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 344
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isBusy()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 345
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 353
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardData()Z

    move-result v3

    if-nez v3, :cond_0

    .line 354
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->setForceRefresh(Z)V

    .line 356
    :cond_0
    return-void

    .line 347
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_2
    move v0, v3

    .line 348
    .local v0, "achievementsEmpty":Z
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getStatistics()Ljava/util/Collection;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getStatistics()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_3
    move v1, v3

    .line 350
    .local v1, "statisticsEmpty":Z
    :goto_2
    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_3
    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .end local v0    # "achievementsEmpty":Z
    .end local v1    # "statisticsEmpty":Z
    :cond_4
    move v0, v2

    .line 347
    goto :goto_1

    .restart local v0    # "achievementsEmpty":Z
    :cond_5
    move v1, v2

    .line 348
    goto :goto_2

    .line 350
    .restart local v1    # "statisticsEmpty":Z
    :cond_6
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_3
.end method


# virtual methods
.method public addPersonToSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 404
    return-void
.end method

.method public addPersonToUnSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 414
    return-void
.end method

.method public getAchievementsFilter()Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->achievementsFilter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->achievementsFilter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ALL:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    goto :goto_0
.end method

.method public getCurrentGamerScore()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->currentGamerScore:I

    return v0
.end method

.method public getData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$AchievementsFilter:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 184
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 177
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->allAchievements:Ljava/util/List;

    goto :goto_0

    .line 179
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->earnedAchievements:Ljava/util/List;

    goto :goto_0

    .line 181
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->unearnedAchievements:Ljava/util/List;

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getGamerEarnedAchievements()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->gamerEarnedAchievements:I

    return v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->COMPLETED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    if-ne v0, v1, :cond_0

    .line 226
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0705df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 231
    :goto_0
    return-object v0

    .line 227
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->LOCKED:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    if-ne v0, v1, :cond_1

    .line 228
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0705e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 231
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0705de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPeopleSelectedList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 361
    .local v2, "peopleSelectorItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingAchievementsFriendsWhoPlayStatus:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->peopleWhoPlayed:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 362
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->peopleWhoPlayed:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 363
    .local v0, "f":Lcom/microsoft/xbox/service/model/FollowersData;
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;-><init>()V

    .line 364
    .local v1, "p":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->displayName:Ljava/lang/String;

    .line 365
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->displayPicRaw:Ljava/lang/String;

    .line 366
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->gamertag:Ljava/lang/String;

    .line 367
    iget-boolean v4, v0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    iput-boolean v4, v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->isFavorite:Z

    .line 368
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->realName:Ljava/lang/String;

    .line 369
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    iput-object v4, v1, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->xuid:Ljava/lang/String;

    .line 370
    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    invoke-direct {v4, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;-><init>(Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 373
    .end local v0    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v1    # "p":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    :cond_0
    return-object v2
.end method

.method public getPeopleWhoPlayedCount()I
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->peopleWhoPlayed:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 218
    const/4 v0, 0x0

    .line 220
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->peopleWhoPlayed:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getRibbonViewColor(I)I
    .locals 2
    .param p1, "rank"    # I

    .prologue
    .line 279
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 280
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 286
    :goto_0
    return v0

    .line 281
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 282
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c012d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 283
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 284
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 286
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedPerson()Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    return-object v0
.end method

.method public getStatistics()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->statList:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalGamerScore()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->totalGamerScore:I

    return v0
.end method

.method public getUpdateStatListView()Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateStatListView:Z

    return v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingAchievementsFriendsWhoPlayStatus:Z

    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardData()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingCompletedAchievements:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLockedAchievements:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingTitleData:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 153
    :goto_0
    return v0

    .line 155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingAchievementData()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoading:Z

    return v0
.end method

.method public isLoadingLeaderboardData()Z
    .locals 3

    .prologue
    .line 162
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isLoadingLeaderboardStats:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 164
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Boolean;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 165
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 166
    .local v1, "element":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 167
    const/4 v2, 0x1

    .line 171
    .end local v1    # "element":Ljava/lang/Boolean;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->setForceRefresh(Z)V

    .line 105
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-nez v0, :cond_2

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->cancel()V

    .line 109
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->load(Z)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_2
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadAchievementsInternal(Z)V

    goto :goto_0
.end method

.method public navigateToAchievementDetails(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .prologue
    .line 235
    if-eqz p1, :cond_0

    .line 236
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 237
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 238
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putGameProgressAchievement(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    .line 240
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackShowAchievementAction()V

    .line 241
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsDetailsScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 243
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public navigateToPeopleSelectorActivity()V
    .locals 4

    .prologue
    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 247
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0705bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;ILjava/lang/String;)V

    .line 248
    return-void
.end method

.method public navigateToStatisticsLeaderboard(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 4
    .param p1, "data"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    .line 251
    if-eqz p1, :cond_0

    .line 252
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 253
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putUserStats(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 256
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackShowGameLeaderboardAction(J)V

    .line 258
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 260
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 305
    return-void
.end method

.method protected onStopOverride()V
    .locals 3

    .prologue
    .line 309
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

    if-eqz v2, :cond_0

    .line 310
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadFriendWhoPlayTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->cancel()V

    .line 313
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    if-eqz v2, :cond_1

    .line 314
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->cancel()V

    .line 317
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

    if-eqz v2, :cond_2

    .line 318
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadAchievementHeaderDataTask;->cancel()V

    .line 321
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    if-eqz v2, :cond_5

    .line 322
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 324
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;>;"
    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 325
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;

    .line 326
    .local v1, "task":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    if-eqz v1, :cond_3

    .line 327
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->cancel()V

    goto :goto_0

    .line 331
    .end local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->loadStatLeadearboardRankTask:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    .line 334
    .end local v0    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;>;"
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360AchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    if-eqz v2, :cond_6

    .line 335
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360AchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->cancel()V

    .line 338
    :cond_6
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360EarnedAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    if-eqz v2, :cond_7

    .line 339
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->load360EarnedAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->cancel()V

    .line 341
    :cond_7
    return-void
.end method

.method public removePersonFromSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 409
    return-void
.end method

.method public removePersonFromUnselected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 419
    return-void
.end method

.method public resetUpdateStatListView()V
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateStatListView:Z

    .line 268
    return-void
.end method

.method public selectionActivityCompleted(Z)V
    .locals 6
    .param p1, "accepted"    # Z

    .prologue
    .line 387
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    .line 389
    .local v0, "friendXuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 390
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedTitleId(J)V

    .line 391
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 394
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->titleId:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->trackCompareAction(Ljava/lang/String;J)V

    .line 396
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 399
    .end local v0    # "friendXuid":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setAchievementsFilter(Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->achievementsFilter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 191
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateViewModelState()V

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 194
    return-void
.end method

.method public setSelectedPerson(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 384
    return-void
.end method
