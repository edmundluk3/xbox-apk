.class public final enum Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
.super Ljava/lang/Enum;
.source "MediaItemBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/media/MediaItemBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OrderBy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_ALLTIMEPLAYCOUNT:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_DIGITALRELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_FREEANDPAIDCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_MOSTPOPULAR:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_NUMBERASCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_NUMBERDESCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_PAIDCOUNTALLTIME:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_PAIDCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_PLAYCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field public static final enum EDSV2ORDERBY_USERRATINGS:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

.field private static final intToTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 71
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_PLAYCOUNTDAILY"

    invoke-direct {v2, v3, v1, v6}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_PLAYCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 72
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_FREEANDPAIDCOUNTDAILY"

    invoke-direct {v2, v3, v6, v7}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_FREEANDPAIDCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 73
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_PAIDCOUNTALLTIME"

    invoke-direct {v2, v3, v7, v8}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_PAIDCOUNTALLTIME:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 74
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_PAIDCOUNTDAILY"

    invoke-direct {v2, v3, v8, v9}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_PAIDCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 75
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_DIGITALRELEASEDATE"

    const/4 v4, 0x5

    invoke-direct {v2, v3, v9, v4}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_DIGITALRELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 76
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_RELEASEDATE"

    const/4 v4, 0x5

    const/4 v5, 0x6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 77
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_USERRATINGS"

    const/4 v4, 0x6

    const/4 v5, 0x7

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_USERRATINGS:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 78
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_NUMBERASCENDING"

    const/4 v4, 0x7

    const/16 v5, 0x8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_NUMBERASCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 79
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_NUMBERDESCENDING"

    const/16 v4, 0x8

    const/16 v5, 0x9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_NUMBERDESCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 80
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_MOSTPOPULAR"

    const/16 v4, 0x9

    const/16 v5, 0xa

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_MOSTPOPULAR:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 81
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const-string v3, "EDSV2ORDERBY_ALLTIMEPLAYCOUNT"

    const/16 v4, 0xa

    const/16 v5, 0xb

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_ALLTIMEPLAYCOUNT:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 69
    const/16 v2, 0xb

    new-array v2, v2, [Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_PLAYCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_FREEANDPAIDCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_PAIDCOUNTALLTIME:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_PAIDCOUNTDAILY:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_DIGITALRELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_USERRATINGS:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_NUMBERASCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_NUMBERDESCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_MOSTPOPULAR:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_ALLTIMEPLAYCOUNT:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->$VALUES:[Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 85
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->intToTypeMap:Ljava/util/HashMap;

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->values()[Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 89
    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->intToTypeMap:Ljava/util/HashMap;

    iget v5, v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->value:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "number"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 96
    iput p3, p0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->value:I

    .line 97
    return-void
.end method

.method public static getOrderByFromInt(I)Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 104
    sget-object v1, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->intToTypeMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 105
    .local v0, "mediaType":Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    if-nez v0, :cond_0

    .line 106
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    .line 108
    .end local v0    # "mediaType":Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    const-class v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->$VALUES:[Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->value:I

    return v0
.end method
