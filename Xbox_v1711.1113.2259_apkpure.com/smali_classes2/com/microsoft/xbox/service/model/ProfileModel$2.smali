.class final Lcom/microsoft/xbox/service/model/ProfileModel$2;
.super Ljava/lang/Object;
.source "ProfileModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;->SortRecent360ProgressAndAchievementData(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)I
    .locals 4
    .param p1, "item1"    # Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .param p2, "item2"    # Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .prologue
    .line 1969
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .local v0, "item1_360":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    move-object v1, p2

    .line 1970
    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .line 1971
    .local v1, "item2_360":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->lastPlayed:Ljava/util/Date;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->lastPlayed:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v2

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1966
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast p2, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/model/ProfileModel$2;->compare(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)I

    move-result v0

    return v0
.end method
