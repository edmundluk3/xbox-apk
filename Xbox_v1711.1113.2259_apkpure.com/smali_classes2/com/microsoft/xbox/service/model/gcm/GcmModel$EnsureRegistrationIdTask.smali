.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;
.super Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnsureRegistrationIdTask"
.end annotation


# instance fields
.field registrationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 1

    .prologue
    .line 791
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;

    .prologue
    .line 791
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 791
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 802
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->registrationId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 803
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RegisterOnLoginTask - get registration id"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/iid/InstanceID;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/iid/InstanceID;

    move-result-object v1

    const-string v2, "86584527366"

    const-string v3, "GCM"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/iid/InstanceID;->getToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->registrationId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 811
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 806
    :catch_0
    move-exception v0

    .line 807
    .local v0, "ex":Ljava/io/IOException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GetRegIdAndRegisterNotificationTask - failed to register"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 791
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 816
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->registrationId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 817
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GetRegIdAndRegisterNotificationTask - post execution store registration id and run onLogin"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 820
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v1, "registration.id"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->registrationId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 821
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->registrationId:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1000(Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 822
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 824
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1100(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V

    .line 828
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 826
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GetRegIdAndRegisterNotificationTask - post execution without obtaining the registration id"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 796
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "registration.id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;->registrationId:Ljava/lang/String;

    .line 797
    return-void
.end method
