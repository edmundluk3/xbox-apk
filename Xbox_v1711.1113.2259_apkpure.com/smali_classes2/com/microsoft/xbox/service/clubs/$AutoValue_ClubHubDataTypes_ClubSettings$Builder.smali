.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.source "$AutoValue_ClubHubDataTypes_ClubSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

.field private feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

.field private lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

.field private profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

.field private roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

.field private viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;-><init>()V

    .line 128
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;-><init>()V

    .line 130
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .line 132
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .line 133
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .line 134
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .line 135
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .line 136
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .locals 7

    .prologue
    .line 169
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings;

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    iget-object v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)V

    return-object v0
.end method

.method public chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 0
    .param p1, "chat"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 144
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .line 145
    return-object p0
.end method

.method public feed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 0
    .param p1, "feed"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 139
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .line 140
    return-object p0
.end method

.method public lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 0
    .param p1, "lfg"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .line 150
    return-object p0
.end method

.method public profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 0
    .param p1, "profile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .line 160
    return-object p0
.end method

.method public roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 0
    .param p1, "roster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 154
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .line 155
    return-object p0
.end method

.method public viewerRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 0
    .param p1, "viewerRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 164
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .line 165
    return-object p0
.end method
