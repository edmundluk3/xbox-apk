.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$Error$Messaging;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Messaging"
.end annotation


# static fields
.field public static final GenericSendMessageError:Ljava/lang/String; = "Messaging - Failed to send message"

.field public static final PermissionsSendMessageError:Ljava/lang/String; = "Messaging - Failed to send message due to permissions or settings"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
