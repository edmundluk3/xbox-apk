.class Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ViewPagerWithTabs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SectionsPagerAdapter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

.field private final views:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 320
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)V
    .locals 1

    .prologue
    .line 323
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    .line 324
    return-void
.end method


# virtual methods
.method public addPage(Landroid/view/View;)Z
    .locals 2
    .param p1, "page"    # Landroid/view/View;

    .prologue
    .line 342
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 343
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->notifyDataSetChanged()V

    .line 346
    :cond_0
    return v0
.end method

.method public addPageAt(ILandroid/view/View;)Z
    .locals 1
    .param p1, "index"    # I
    .param p2, "page"    # Landroid/view/View;

    .prologue
    .line 350
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 352
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->notifyDataSetChanged()V

    .line 353
    const/4 v0, 0x1

    .line 355
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addPages(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "pages":Ljava/util/Collection;, "Ljava/util/Collection<+Landroid/view/View;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-result v0

    .line 335
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->notifyDataSetChanged()V

    .line 338
    :cond_0
    return v0
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 398
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$700(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 399
    :cond_0
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 400
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 412
    invoke-super {p0, p1}, Landroid/support/v4/view/PagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    .line 413
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 414
    .local v0, "position":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, -0x2

    .end local v0    # "position":I
    :cond_0
    return v0
.end method

.method public getPageAt(I)Landroid/view/View;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 327
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getCount()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 328
    :cond_0
    const/4 v0, 0x0

    .line 330
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public getPageIndex(Landroid/view/View;)I
    .locals 1
    .param p1, "page"    # Landroid/view/View;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 404
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$800(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$800(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/functions/GenericFunction;->eval(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 407
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 390
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$700(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    if-eq p1, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 391
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 392
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 393
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 385
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removePage(Landroid/view/View;)Z
    .locals 2
    .param p1, "page"    # Landroid/view/View;

    .prologue
    .line 359
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 360
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->notifyDataSetChanged()V

    .line 363
    :cond_0
    return v0
.end method

.method public removePageAt(I)Landroid/view/View;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 367
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 368
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->notifyDataSetChanged()V

    .line 371
    :cond_0
    return-object v0
.end method
