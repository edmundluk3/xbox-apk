.class public Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "PagesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/PagesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PageModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;,
        Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private continuationToken:Ljava/lang/String;

.field private lastRefreshPageActivityFeed:Ljava/util/Date;

.field private pageActivityFeedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field private pageActivityFeedLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private pageId:Ljava/lang/String;

.field private result:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/PagesModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/PagesModel;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/model/PagesModel;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->this$0:Lcom/microsoft/xbox/service/model/PagesModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 73
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageId:Ljava/lang/String;

    .line 74
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->onGetPageActivityFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V

    return-void
.end method

.method private onGetPageActivityFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V
    .locals 3
    .param p2, "isContinuationResult"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 118
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    .line 119
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->lastRefreshPageActivityFeed:Ljava/util/Date;

    .line 120
    if-eqz v0, :cond_0

    .line 121
    if-nez p2, :cond_1

    .line 122
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->activityItems:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedItems:Ljava/util/ArrayList;

    .line 130
    :goto_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->contToken:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->continuationToken:Ljava/lang/String;

    .line 133
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :cond_0
    return-void

    .line 124
    .restart local v0    # "data":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedItems:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 125
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->activityItems:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedItems:Ljava/util/ArrayList;

    goto :goto_0

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedItems:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->activityItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method


# virtual methods
.method public getContinuationToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->continuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getPageActivityFeedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedItems:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->result:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    return-object v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;-><init>(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;Lcom/microsoft/xbox/service/model/PagesModel$1;)V

    .line 98
    .local v0, "runner":Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageInfoRunner;
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadPageActivityFeed(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 105
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->lastRefreshPageActivityFeed:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->pageActivityFeedLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;

    invoke-direct {v6, p0, p2}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel$GetPageActivityFeedRunner;-><init>(Lcom/microsoft/xbox/service/model/PagesModel$PageModel;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->result:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldRefreshPageActivityFeed()Z
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->lastRefreshPageActivityFeed:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 110
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 111
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->result:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    .line 114
    :cond_0
    return-void

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
