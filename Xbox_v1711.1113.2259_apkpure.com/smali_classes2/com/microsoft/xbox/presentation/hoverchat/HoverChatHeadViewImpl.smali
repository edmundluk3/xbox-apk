.class public Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;
.super Ljava/lang/Object;
.source "HoverChatHeadViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;


# static fields
.field private static final CONTENT_DES_FORMAT:Ljava/lang/String; = "%s, %s"

.field private static final MAX_CACHE_SIZE:I = 0x8

.field private static final MAX_PARTICIPANTS_IN_DES:I = 0x4

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private backgroundPaint:Landroid/graphics/Paint;

.field private final chatHead:Lcom/flipkart/chatheads/ui/ChatHead;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation
.end field

.field private final drawableCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private firstAnimComplete:Z

.field private final glideAnimViewAdapter:Lcom/bumptech/glide/request/animation/GlideAnimation$ViewAdapter;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 74
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 76
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$1;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Lcom/flipkart/chatheads/ui/ChatHead;)V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->glideAnimViewAdapter:Lcom/bumptech/glide/request/animation/GlideAnimation$ViewAdapter;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->resources:Landroid/content/res/Resources;

    .line 96
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->drawableCache:Landroid/support/v4/util/LruCache;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->firstAnimComplete:Z

    return v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->firstAnimComplete:Z

    return p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Lcom/flipkart/chatheads/ui/ChatHead;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Lcom/bumptech/glide/request/animation/GlideAnimation$ViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->glideAnimViewAdapter:Lcom/bumptech/glide/request/animation/GlideAnimation$ViewAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Landroid/support/v4/util/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->drawableCache:Landroid/support/v4/util/LruCache;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;
    .param p1, "x1"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->setErrorBackground(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    return-void
.end method

.method private getImageObservable(Ljava/lang/String;I)Lio/reactivex/Observable;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v1

    .line 182
    invoke-virtual {v1, p1}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/bumptech/glide/load/Transformation;

    const/4 v3, 0x0

    new-instance v4, Ljp/wasabeef/glide/transformations/CropCircleTransformation;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 183
    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Ljp/wasabeef/glide/transformations/CropCircleTransformation;-><init>(Landroid/content/Context;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/DrawableTypeRequest;->bitmapTransform([Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v1

    .line 184
    invoke-virtual {v1, p2, p2}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(II)Lcom/bumptech/glide/request/FutureTarget;

    move-result-object v0

    .line 186
    .local v0, "b":Lcom/bumptech/glide/request/FutureTarget;, "Lcom/bumptech/glide/request/FutureTarget<Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;>;"
    invoke-static {v0}, Lio/reactivex/Observable;->fromFuture(Ljava/util/concurrent/Future;)Lio/reactivex/Observable;

    move-result-object v1

    return-object v1
.end method

.method static synthetic lambda$renderMultiImage$0(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;Ljava/util/List;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    .param p2, "images"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 176
    invoke-direct {p0, p2, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->setMultiImage(Ljava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    return-void
.end method

.method static synthetic lambda$renderMultiImage$1(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    .param p2, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->setErrorBackground(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    return-void
.end method

.method private renderFromCache(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v0, p1}, Lcom/flipkart/chatheads/ui/ChatHead;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 160
    return-void
.end method

.method private renderMultiImage(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
    .locals 6
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    .prologue
    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 165
    .local v0, "imageObservables":Ljava/util/List;, "Ljava/util/List<Lio/reactivex/Observable<Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;>;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v3

    div-int/lit8 v1, v3, 0x2

    .line 167
    .local v1, "size":I
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 168
    .local v2, "url":Ljava/lang/String;
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->getImageObservable(Ljava/lang/String;I)Lio/reactivex/Observable;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 171
    .end local v2    # "url":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Lio/reactivex/Observable;->merge(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v3

    .line 172
    invoke-virtual {v3}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object v3

    .line 173
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v3

    .line 174
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)Lio/reactivex/functions/Consumer;

    move-result-object v5

    .line 175
    invoke-virtual {v3, v4, v5}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 178
    return-void
.end method

.method private renderSingleImage(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
    .locals 11
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 242
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v5

    if-lez v5, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v4, v5

    .line 244
    .local v4, "url":Ljava/lang/String;
    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v5

    .line 245
    invoke-virtual {v5, v4}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 247
    .local v2, "builder":Lcom/bumptech/glide/DrawableRequestBuilder;, "Lcom/bumptech/glide/DrawableRequestBuilder<Ljava/lang/String;>;"
    const-string v5, "no_image_available"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const v3, 0x7f020180

    .line 252
    .local v3, "missingDrawable":I
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->Circle:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    if-ne v5, v8, :cond_2

    .line 253
    sget-object v5, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    .line 254
    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->get(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/bumptech/glide/DrawableRequestBuilder;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    .line 255
    invoke-virtual {v8, v3}, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->get(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/bumptech/glide/DrawableRequestBuilder;->error(Landroid/graphics/drawable/Drawable;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v5

    new-array v8, v6, [Lcom/bumptech/glide/load/Transformation;

    new-instance v9, Ljp/wasabeef/glide/transformations/CropCircleTransformation;

    iget-object v10, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 256
    invoke-virtual {v10}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Ljp/wasabeef/glide/transformations/CropCircleTransformation;-><init>(Landroid/content/Context;)V

    aput-object v9, v8, v7

    invoke-virtual {v5, v8}, Lcom/bumptech/glide/DrawableRequestBuilder;->bitmapTransform([Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    .line 263
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v5

    if-nez v5, :cond_3

    move v0, v6

    .line 264
    .local v0, "addBadge":Z
    :goto_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Right:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    if-ne v5, v8, :cond_4

    move v1, v6

    .line 266
    .local v1, "badgeOnRight":Z
    :goto_4
    if-eqz v0, :cond_5

    .line 267
    new-instance v5, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$3;

    invoke-direct {v5, p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$3;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    invoke-virtual {v2, v5}, Lcom/bumptech/glide/DrawableRequestBuilder;->listener(Lcom/bumptech/glide/request/RequestListener;)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v5

    new-instance v6, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;

    invoke-direct {v6, p0, v1, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    .line 278
    invoke-virtual {v5, v6}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Lcom/bumptech/glide/request/target/Target;)Lcom/bumptech/glide/request/target/Target;

    .line 303
    :goto_5
    return-void

    .line 242
    .end local v0    # "addBadge":Z
    .end local v1    # "badgeOnRight":Z
    .end local v2    # "builder":Lcom/bumptech/glide/DrawableRequestBuilder;, "Lcom/bumptech/glide/DrawableRequestBuilder<Ljava/lang/String;>;"
    .end local v3    # "missingDrawable":I
    .end local v4    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, ""

    goto :goto_0

    .line 247
    .restart local v2    # "builder":Lcom/bumptech/glide/DrawableRequestBuilder;, "Lcom/bumptech/glide/DrawableRequestBuilder<Ljava/lang/String;>;"
    .restart local v4    # "url":Ljava/lang/String;
    :cond_1
    const v3, 0x7f020125

    goto :goto_1

    .line 259
    .restart local v3    # "missingDrawable":I
    :cond_2
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->placeholder(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v5

    .line 260
    invoke-virtual {v5, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->error(I)Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    goto :goto_2

    :cond_3
    move v0, v7

    .line 263
    goto :goto_3

    .restart local v0    # "addBadge":Z
    :cond_4
    move v1, v7

    .line 264
    goto :goto_4

    .line 297
    .restart local v1    # "badgeOnRight":Z
    :cond_5
    iget-boolean v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->firstAnimComplete:Z

    if-eqz v5, :cond_6

    .line 298
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableRequestBuilder;->dontAnimate()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v5, v6}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    goto :goto_5

    .line 300
    :cond_6
    iput-boolean v6, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->firstAnimComplete:Z

    .line 301
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v2, v5}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    goto :goto_5
.end method

.method private setContentDescription(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "participants":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const v11, 0x7f070682

    const/4 v4, 0x4

    const/4 v7, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 141
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v4, :cond_0

    .line 142
    const-string v3, "%s, %s"

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, ", "

    .line 144
    invoke-static {v5, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 145
    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/ChatHead;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    .line 142
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "contentDescription":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v3, v0}, Lcom/flipkart/chatheads/ui/ChatHead;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    return-void

    .line 147
    .end local v0    # "contentDescription":Ljava/lang/String;
    :cond_0
    const-string v3, ", "

    invoke-interface {p1, v9, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    .line 148
    .local v2, "shortParticipants":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x4

    .line 149
    .local v1, "otherCount":I
    const-string v3, "%s, %s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 151
    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/ChatHead;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0700b1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v9

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 152
    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/ChatHead;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    .line 149
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "contentDescription":Ljava/lang/String;
    goto :goto_0
.end method

.method private setErrorBackground(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
    .locals 5
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    .prologue
    const v3, 0x7f020125

    .line 306
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHead;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    .line 308
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/Integer;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 309
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableTypeRequest;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 326
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->resources:Landroid/content/res/Resources;

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 314
    .local v1, "missing":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->Circle:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    if-ne v2, v3, :cond_1

    .line 315
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getCircleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 318
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 319
    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHead;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 321
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v2

    sget-object v4, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Right:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    if-ne v2, v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-direct {v0, v3, v1, v2}, Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Z)V

    .line 323
    .local v0, "chatBadgedGlideDrawable":Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v2, v0}, Lcom/flipkart/chatheads/ui/ChatHead;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 324
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->drawableCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2, p1, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 321
    .end local v0    # "chatBadgedGlideDrawable":Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setMultiImage(Ljava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
    .locals 16
    .param p2, "viewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;",
            ">;",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "drawables":Ljava/util/List;, "Ljava/util/List<Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v2

    .line 191
    .local v2, "badgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v11}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v12}, Lcom/flipkart/chatheads/ui/ChatHead;->getHeight()I

    move-result v12

    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v12, v13}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 192
    .local v6, "outputBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 194
    .local v3, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->backgroundPaint:Landroid/graphics/Paint;

    if-nez v11, :cond_0

    .line 195
    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->backgroundPaint:Landroid/graphics/Paint;

    .line 196
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->backgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->resources:Landroid/content/res/Resources;

    const v13, 0x7f0c00c1

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 197
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->backgroundPaint:Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setFlags(I)V

    .line 200
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v11}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v12}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v13}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v13

    div-int/lit8 v13, v13, 0x2

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v11}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v11

    div-int/lit8 v5, v11, 0x2

    .line 203
    .local v5, "largeRadius":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v11}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v4

    .line 204
    .local v4, "largeCircum":I
    int-to-double v12, v5

    const-wide v14, 0x40034fdf3b645a1dL    # 2.414

    div-double/2addr v12, v14

    double-to-int v9, v12

    .line 205
    .local v9, "smallRadius":I
    mul-int/lit8 v8, v9, 0x2

    .line 206
    .local v8, "smallCircum":I
    sub-int v7, v5, v8

    .line 208
    .local v7, "radiiPadding":I
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    add-int v12, v8, v7

    add-int v13, v8, v7

    invoke-virtual {v11, v7, v7, v12, v13}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->setBounds(IIII)V

    .line 209
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    invoke-virtual {v11, v3}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 211
    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    sub-int v12, v4, v7

    invoke-virtual {v11, v5, v7, v12, v5}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->setBounds(IIII)V

    .line 212
    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    invoke-virtual {v11, v3}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 214
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x3

    if-ne v11, v12, :cond_2

    .line 215
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    div-int/lit8 v12, v9, 0x2

    sub-int v12, v5, v12

    sub-int/2addr v12, v7

    div-int/lit8 v13, v9, 0x2

    sub-int v13, v5, v13

    add-int/2addr v13, v8

    sub-int/2addr v13, v7

    sub-int v14, v4, v7

    invoke-virtual {v11, v12, v5, v13, v14}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->setBounds(IIII)V

    .line 220
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    invoke-virtual {v11, v3}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 232
    :goto_0
    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v11

    if-nez v11, :cond_1

    .line 233
    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v13}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v14}, Lcom/flipkart/chatheads/ui/ChatHead;->getWidth()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 234
    .local v10, "totalBounds":Landroid/graphics/Rect;
    sget-object v11, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->INSTANCE:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isRight()Z

    move-result v12

    invoke-virtual {v11, v3, v10, v12}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->addBadgeToCanvas(Landroid/graphics/Canvas;Landroid/graphics/Rect;Z)V

    .line 237
    .end local v10    # "totalBounds":Landroid/graphics/Rect;
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v11, v6}, Lcom/flipkart/chatheads/ui/ChatHead;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->drawableCache:Landroid/support/v4/util/LruCache;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v12}, Lcom/flipkart/chatheads/ui/ChatHead;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v11, v0, v12}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    return-void

    .line 222
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x4

    if-ne v11, v12, :cond_3

    .line 223
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    add-int v12, v8, v7

    sub-int v13, v4, v7

    invoke-virtual {v11, v7, v5, v12, v13}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->setBounds(IIII)V

    .line 224
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    invoke-virtual {v11, v3}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 226
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    sub-int v12, v4, v7

    sub-int v13, v4, v7

    invoke-virtual {v11, v5, v5, v12, v13}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->setBounds(IIII)V

    .line 227
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    invoke-virtual {v11, v3}, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 229
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unexpected size: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public getKey()Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    return-object v0
.end method

.method public getParentX()D
    .locals 4

    .prologue
    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 113
    .local v0, "parent":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v1

    float-to-double v2, v1

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getX()F

    move-result v0

    float-to-double v0, v0

    return-wide v0
.end method

.method public getXChangedEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->chatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/RxHoverChat;->getChatHeadXPositionChangedEvents(Lcom/flipkart/chatheads/ui/ChatHead;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public render(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    .prologue
    .line 123
    sget-object v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "render: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->setContentDescription(Ljava/util/List;)V

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->drawableCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 129
    .local v0, "cachedDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 130
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->renderFromCache(Landroid/graphics/drawable/Drawable;)V

    .line 136
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 132
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->renderMultiImage(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    goto :goto_0

    .line 134
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->renderSingleImage(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V

    goto :goto_0
.end method
