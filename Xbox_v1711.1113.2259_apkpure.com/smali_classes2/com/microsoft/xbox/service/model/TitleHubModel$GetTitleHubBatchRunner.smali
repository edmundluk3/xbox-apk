.class Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleHubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleHubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTitleHubBatchRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleHubModel;

.field private titleIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleHubModel;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 341
    .local p2, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 342
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;->titleIds:Ljava/util/Set;

    .line 343
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 338
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 347
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getTitleHubService()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;->titleIds:Ljava/util/Set;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/titleHub/ITitleHubService;->getTitleSummaries(Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 361
    const-wide/16 v0, 0xfa7

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 356
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$GetTitleHubBatchRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/TitleHubModel;->updateTitleHubBatch(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 357
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 352
    return-void
.end method
