.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$ConsoleManger;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConsoleManger"
.end annotation


# static fields
.field public static final Close:Ljava/lang/String; = "ConsoleManager - Close"

.field public static final ConnectToXbox:Ljava/lang/String; = "ConsoleManager - Connect to Xbox"

.field public static final GotoConsoleManager:Ljava/lang/String; = "ConsoleManager - Navigate to console manager"

.field public static final SetupConsole:Ljava/lang/String; = "ConsoleManager - Set up a console"

.field public static final SwitchConsole:Ljava/lang/String; = "ConsoleManager - Switch console"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
