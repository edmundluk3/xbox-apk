.class public abstract Lcom/microsoft/xbox/xle/app/XLEUtil$BackgroundSetter;
.super Ljava/lang/Object;
.source "XLEUtil.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/XLEUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BackgroundSetter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isValidForBackground(Landroid/graphics/Bitmap;)Z
    .locals 2
    .param p0, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1052
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onAfterImageSet(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "img"    # Landroid/widget/ImageView;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1044
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/XLEUtil$BackgroundSetter;->isValidForBackground(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil$BackgroundSetter;->setBackground(Landroid/widget/ImageView;)V

    .line 1047
    :cond_0
    return-void
.end method

.method public final onBeforeImageSet(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "img"    # Landroid/widget/ImageView;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1039
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1040
    return-void
.end method

.method public abstract setBackground(Landroid/widget/ImageView;)V
.end method
