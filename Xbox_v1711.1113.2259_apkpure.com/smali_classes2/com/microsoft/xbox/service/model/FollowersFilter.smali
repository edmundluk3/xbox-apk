.class public final enum Lcom/microsoft/xbox/service/model/FollowersFilter;
.super Ljava/lang/Enum;
.source "FollowersFilter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/FollowersFilter;

.field public static final enum ALL:Lcom/microsoft/xbox/service/model/FollowersFilter;

.field public static final enum CLUB_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

.field public static final enum FAVORITES:Lcom/microsoft/xbox/service/model/FollowersFilter;

.field public static final enum FOLLOWERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

.field public static final enum FRIENDS_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

.field public static final enum RECENTPLAYERS:Lcom/microsoft/xbox/service/model/FollowersFilter;


# instance fields
.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 14
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    const-string v1, "ALL"

    const v2, 0x7f070b5a

    const-string v3, "All"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/service/model/FollowersFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->ALL:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    const-string v1, "FRIENDS_FIRST"

    const v2, 0x7f070a4f

    const-string v3, "FriendsFirst"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/service/model/FollowersFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->FRIENDS_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    const-string v1, "CLUB_FIRST"

    const v2, 0x7f070a4e

    const-string v3, "ClubsFirst"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/service/model/FollowersFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->CLUB_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    const-string v1, "FAVORITES"

    const v2, 0x7f070575

    const-string v3, "Favorites"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/service/model/FollowersFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->FAVORITES:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    const-string v1, "FOLLOWERS"

    const v2, 0x7f070576

    const-string v3, "RecentPlayers"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/service/model/FollowersFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->FOLLOWERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    const-string v1, "RECENTPLAYERS"

    const/4 v2, 0x5

    const v3, 0x7f070578

    const-string v4, "RecentPlayers"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/FollowersFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->RECENTPLAYERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 13
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/FollowersFilter;

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->ALL:Lcom/microsoft/xbox/service/model/FollowersFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->FRIENDS_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->CLUB_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->FAVORITES:Lcom/microsoft/xbox/service/model/FollowersFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->FOLLOWERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersFilter;->RECENTPLAYERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->$VALUES:[Lcom/microsoft/xbox/service/model/FollowersFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 28
    iput p3, p0, Lcom/microsoft/xbox/service/model/FollowersFilter;->resId:I

    .line 29
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/FollowersFilter;->telemetryName:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/FollowersFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/FollowersFilter;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersFilter;->$VALUES:[Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/FollowersFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/FollowersFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/service/model/FollowersFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
