.class Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "RecentlyPlayedModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetRecentlyPlayedRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final MAXIMUM_TITLES:I = 0x19


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;->this$0:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$1;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;-><init>(Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->access$100()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v0

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x19

    .line 61
    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/ITitleHubService;->getRecentlyPlayedTitles(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 73
    const-wide/16 v0, 0xfc8

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;->this$0:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->access$200(Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 69
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method
