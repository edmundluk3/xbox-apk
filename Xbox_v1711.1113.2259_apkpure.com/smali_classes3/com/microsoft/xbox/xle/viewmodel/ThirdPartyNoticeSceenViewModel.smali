.class public Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ThirdPartyNoticeSceenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 36
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ThirdPartyNoticeScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 12
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public onTapCloseButton()V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;->goBack()V

    .line 16
    return-void
.end method
