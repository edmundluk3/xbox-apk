.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;
.super Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;
.source "PeopleHubSocialListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PersonViewHolder"
.end annotation


# instance fields
.field friendImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0935
    .end annotation
.end field

.field primaryText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0936
    .end annotation
.end field

.field secondaryText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0937
    .end annotation
.end field

.field subText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0938
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    .line 208
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V

    .line 209
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 210
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;Lcom/microsoft/xbox/toolkit/generics/Action;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "itemAction"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "itemAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;>;"
    const v5, 0x7f0201fa

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 214
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 215
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 217
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 218
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    .line 220
    .local v0, "personListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->friendImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getPersonImage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 221
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->friendImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 223
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->primaryText:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getGamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getGamerRealName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->secondaryText:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getGamerRealName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->secondaryText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    :goto_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getPresonPresence()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->subText:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getPresonPresence()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->subText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;Lcom/microsoft/xbox/toolkit/generics/Action;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    .end local v0    # "personListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
    :goto_2
    return-void

    .line 229
    .restart local v0    # "personListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->secondaryText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 236
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$PersonViewHolder;->subText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 241
    .end local v0    # "personListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t bind PersonViewHolder to non-PersonListItem: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_2
.end method
