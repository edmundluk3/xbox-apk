.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
.super Ljava/lang/Enum;
.source "UTCConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

.field public static final enum AUTOMATIC:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

.field public static final enum IPADDRESS:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

.field public static final enum MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

.field public static final enum RETRY:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

.field public static final enum UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    const-string v1, "AUTOMATIC"

    invoke-direct {v0, v1, v6, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->AUTOMATIC:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    const-string v1, "MANUAL"

    invoke-direct {v0, v1, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    const-string v1, "RETRY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->RETRY:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    const-string v1, "IPADDRESS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->IPADDRESS:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .line 27
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->AUTOMATIC:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->RETRY:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->IPADDRESS:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->value:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->value:I

    return v0
.end method
