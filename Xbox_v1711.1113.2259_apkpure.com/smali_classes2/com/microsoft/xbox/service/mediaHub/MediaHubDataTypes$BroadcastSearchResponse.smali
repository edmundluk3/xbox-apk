.class public abstract Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;
.super Ljava/lang/Object;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BroadcastSearchResponse"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_BroadcastSearchResponse$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_BroadcastSearchResponse$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;
    .locals 2
    .param p0, "continuationToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BroadcastSearchResponse;"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "values":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 181
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_BroadcastSearchResponse;

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_BroadcastSearchResponse;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method


# virtual methods
.method public abstract continuationToken()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract values()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$MediaHubBroadcast;",
            ">;"
        }
    .end annotation
.end method
