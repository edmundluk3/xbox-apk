.class public abstract Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
.super Ljava/lang/Object;
.source "ApplicationBarManager.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;,
        Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;"
    }
.end annotation


# static fields
.field protected static final APP_BAR_BLOCK_TIMEOUT_MS:I = 0x1388

.field protected static final APP_BAR_HIDE_APPBAR_ANIMATION_NAME:Ljava/lang/String; = "AppBarHide"

.field protected static final APP_BAR_HIDE_APPBAR_BUTTON_ANIMATION_NAME:Ljava/lang/String; = "AppBarHideButton"

.field protected static final APP_BAR_SHOW_APPBAR_BUTTON_ANIMATION_NAME:Ljava/lang/String; = "AppBarShowButton"

.field protected static final APP_BAR_SHOW_APPBAR_FULL_ANIMATION_NAME:Ljava/lang/String; = "AppBarShowFull"


# instance fields
.field private animationState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

.field private cachedSessionState:I

.field protected collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

.field protected currentAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

.field private currentNowPlayingImageDefaultAspectRatio:[I

.field private currentNowPlayingImageDefaultRid:I

.field private currentNowPlayingImageUri:Ljava/lang/String;

.field private currentNowPlayingSecondaryImageDefaultRid:I

.field private currentNowPlayingSecondaryImageUri:Ljava/lang/String;

.field protected expandedAppBar:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

.field protected expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

.field protected globalIconButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private isBlocking:Z

.field private isConnectedToConsole:Z

.field private isPaused:Z

.field protected isShown:Z

.field private isTvEnabled:Z

.field protected lastAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

.field protected newButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private onAnimationEndRunnable:Ljava/lang/Runnable;

.field private onNewButtonsAddedRunnable:Ljava/lang/Runnable;

.field protected previousButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private profileBackgroundColorBitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

.field protected shouldShowNowPlaying:Z


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isShown:Z

    .line 91
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isBlocking:Z

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isPaused:Z

    .line 93
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isConnectedToConsole:Z

    .line 99
    iput v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageDefaultRid:I

    .line 101
    iput v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageDefaultRid:I

    .line 105
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isTvEnabled:Z

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getSmartGlassFoucsNowPlayingViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->updateTvView()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    return-void
.end method

.method private cleanUpAnimations()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 625
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 626
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 627
    return-void
.end method

.method private countTotalVisibleIcons()I
    .locals 3

    .prologue
    .line 615
    const/4 v1, 0x0

    .line 616
    .local v1, "totalVisibleIcons":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 617
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 618
    add-int/lit8 v1, v1, 0x1

    .line 616
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 621
    :cond_1
    return v1
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
    .locals 1

    .prologue
    .line 111
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManagerPhone;

    move-result-object v0

    return-object v0
.end method

.method private getIsTvEnabled()Z
    .locals 2

    .prologue
    .line 783
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 784
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-nez v0, :cond_0

    .line 785
    const/4 v1, 0x0

    .line 787
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method private getSmartGlassFoucsNowPlayingViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getFocusedTabViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v0

    return-object v0
.end method

.method private getSmartGlassNonFoucsNowPlayingViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .locals 1

    .prologue
    .line 764
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getNonFocusedTabViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v0

    return-object v0
.end method

.method private hasAppsRunning()Z
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getFocusedTabViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getNonFocusedTabViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initializeAnimation()V
    .locals 2

    .prologue
    .line 454
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->READY:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    .line 455
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    .line 457
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$15;->$SwitchMap$com$microsoft$xbox$xle$app$ApplicationBarManager$AppBarState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 475
    :goto_0
    return-void

    .line 459
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->lastAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    if-ne v0, v1, :cond_0

    .line 462
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SWAP_APPBAR_BUTTONS:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    goto :goto_0

    .line 466
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->SHOW_APPBAR_FULL:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    goto :goto_0

    .line 472
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->HIDE_APPBAR:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    goto :goto_0

    .line 457
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private observeModels()V
    .locals 1

    .prologue
    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 155
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 158
    :cond_0
    return-void
.end method

.method private onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V
    .locals 8
    .param p1, "state"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 478
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->animationState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    if-eq v3, p1, :cond_1

    .line 479
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->animationState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    .line 480
    const-string v3, "ApplicationBar"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Animation state updated: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const/4 v2, 0x0

    .line 484
    .local v2, "animation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    sget-object v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$15;->$SwitchMap$com$microsoft$xbox$xle$app$ApplicationBarManager$AppBarAnimationState:[I

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->animationState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 604
    :cond_0
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->animationState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    sget-object v5, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->DONE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->setAppbarAnimationState(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    .line 606
    if-eqz v2, :cond_1

    .line 607
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->disableButtons()V

    .line 609
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;->start()V

    .line 612
    .end local v2    # "animation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    :cond_1
    :goto_1
    return-void

    .line 487
    .restart local v2    # "animation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    :pswitch_0
    const-string v3, "ApplicationBar"

    const-string v4, "Playing show full app bar animation"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setVisibility(I)V

    .line 489
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 493
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->lastAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    sget-object v4, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->HIDE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    if-ne v3, v4, :cond_2

    .line 494
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getVolumeButton()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 495
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getRemoteControlButton()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 497
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;->HIDE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    .line 498
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v3

    const-string v4, "AppBarShowFull"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;->compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v2

    .line 499
    new-instance v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$7;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$7;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;->setOnAnimationEnd(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 511
    :pswitch_1
    const-string v3, "ApplicationBar"

    const-string v4, "Playing hide app bar animation"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 514
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v3

    const-string v4, "AppBarHide"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;->compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v2

    .line 515
    new-instance v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$8;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$8;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;->setOnAnimationEnd(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 527
    :pswitch_2
    const-string v3, "ApplicationBar"

    const-string v4, "Playing show app bar button animation"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setVisibility(I)V

    .line 529
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 530
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->addNewCollapsedButtonsToView()V

    .line 532
    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$9;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$9;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    .line 539
    .local v0, "afterShowAppbarButtons":Ljava/lang/Runnable;
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->countTotalVisibleIcons()I

    move-result v3

    if-nez v3, :cond_3

    .line 540
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    .line 543
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v3

    const-string v4, "AppBarShowButton"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;->compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v2

    .line 544
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;->setOnAnimationEnd(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 548
    .end local v0    # "afterShowAppbarButtons":Ljava/lang/Runnable;
    :pswitch_3
    const-string v3, "ApplicationBar"

    const-string v4, "Playing hide app bar button animation for swapping them out"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setVisibility(I)V

    .line 550
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 552
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowSwapButtonAnimation()Z

    move-result v3

    if-nez v3, :cond_4

    .line 554
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 555
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->addNewCollapsedButtonsToView()V

    .line 556
    sget-object v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->DONE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    goto/16 :goto_0

    .line 558
    :cond_4
    new-instance v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$10;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$10;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    .line 568
    .local v1, "afterSwapAppbarButtons":Ljava/lang/Runnable;
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->countTotalVisibleIcons()I

    move-result v3

    if-nez v3, :cond_5

    .line 569
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    .line 572
    :cond_5
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v3

    const-string v4, "AppBarHideButton"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;->compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v2

    .line 573
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;->setOnAnimationEnd(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 578
    .end local v1    # "afterSwapAppbarButtons":Ljava/lang/Runnable;
    :pswitch_4
    const-string v3, "ApplicationBar"

    const-string v4, "Playing hide app bar button animation"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setVisibility(I)V

    .line 581
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v3

    const-string v4, "AppBarHideButton"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;->compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v2

    .line 582
    new-instance v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$11;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$11;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;->setOnAnimationEnd(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 594
    :pswitch_5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->cleanUpAnimations()V

    .line 596
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->enableButtons()V

    .line 598
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onAnimationEndRunnable:Ljava/lang/Runnable;

    if-eqz v3, :cond_0

    .line 599
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onAnimationEndRunnable:Ljava/lang/Runnable;

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0

    .line 484
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private registerTvEventListener()V
    .locals 1

    .prologue
    .line 796
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 797
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 798
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 800
    :cond_0
    return-void
.end method

.method private setViewClickListener(ILandroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V
    .locals 4
    .param p1, "resId"    # I
    .param p2, "listener"    # Landroid/view/View$OnClickListener;
    .param p3, "longClickListener"    # Landroid/view/View$OnLongClickListener;

    .prologue
    .line 304
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 305
    .local v0, "collapsedButton":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 307
    invoke-virtual {v0, p3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 309
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 310
    .local v1, "expandedButton":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 311
    new-instance v2, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$6;

    invoke-direct {v2, p0, p2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$6;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;Landroid/view/View$OnClickListener;)V

    .line 318
    .local v2, "newListener":Landroid/view/View$OnClickListener;
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    invoke-virtual {v1, p3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 320
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v3

    invoke-interface {v3, p1, v2}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->setListenerHook(ILandroid/view/View$OnClickListener;)V

    .line 322
    .end local v2    # "newListener":Landroid/view/View$OnClickListener;
    :cond_1
    return-void
.end method

.method private stopObserveModels()V
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 162
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 163
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 165
    :cond_0
    return-void
.end method

.method private unRegisterTvEventListener()V
    .locals 1

    .prologue
    .line 803
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 804
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 805
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 807
    :cond_0
    return-void
.end method

.method private updateTvStatus()V
    .locals 1

    .prologue
    .line 791
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getIsTvEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isTvEnabled:Z

    .line 792
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->updateTvView()V

    .line 793
    return-void
.end method

.method private updateTvView()V
    .locals 2

    .prologue
    .line 840
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isTvEnabled:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 841
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getVolumeButton()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 842
    return-void

    .line 840
    .end local v0    # "visibility":I
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method protected addNewCollapsedButtonsToView()V
    .locals 5

    .prologue
    .line 400
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 402
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->newButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->newButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 403
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->newButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 404
    .local v0, "button":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->addIconButton(Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V

    .line 403
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 411
    .end local v0    # "button":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onNewButtonsAddedRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 412
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onNewButtonsAddedRunnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 414
    :cond_1
    return-void
.end method

.method public beginAnimation()V
    .locals 0

    .prologue
    .line 444
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->cleanUpAnimations()V

    .line 445
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->initializeAnimation()V

    .line 446
    return-void
.end method

.method protected disableButtons()V
    .locals 3

    .prologue
    .line 417
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ApplicationBar:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x1388

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isBlocking:Z

    .line 419
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setEnabled(Z)V

    .line 420
    return-void
.end method

.method protected enableButtons()V
    .locals 2

    .prologue
    .line 423
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ApplicationBar:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    .line 424
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isBlocking:Z

    .line 425
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setEnabled(Z)V

    .line 426
    return-void
.end method

.method public expandAppBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 429
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isBlocking:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->hasAppsRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Now Playing"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Now Playing"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBar:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showAppBarMenu(Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;)V

    .line 439
    :cond_0
    return-void
.end method

.method public getAppBarState()Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentAppBarState:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;

    return-object v0
.end method

.method public getCollapsedAppBarView()Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    return-object v0
.end method

.method public getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBar:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    return-object v0
.end method

.method public getExpandedAppBarView()Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    return-object v0
.end method

.method public getFocusLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 1

    .prologue
    .line 773
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getFocusedTabLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    return-object v0
.end method

.method public getIsApplicationBarShown()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isShown:Z

    return v0
.end method

.method public getIsBlocking()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isBlocking:Z

    return v0
.end method

.method protected abstract getNowPlayingSecondaryText()Ljava/lang/String;
.end method

.method public abstract getPageIndicator()Lcom/microsoft/xbox/toolkit/ui/PageIndicator;
.end method

.method public getShouldShowNowPlaying()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowNowPlaying:Z

    return v0
.end method

.method public hide()V
    .locals 3

    .prologue
    .line 362
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    if-eqz v1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setVisibility(I)V

    .line 370
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isShown:Z

    if-eqz v1, :cond_1

    .line 371
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isShown:Z

    .line 375
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 376
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_1

    .line 377
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->resetBottomMargin()V

    .line 380
    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_1
    return-void
.end method

.method public loadAnimations()V
    .locals 2

    .prologue
    .line 638
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v0

    const-string v1, "AppBarShowFull"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 639
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v0

    const-string v1, "AppBarHide"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 640
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v0

    const-string v1, "AppBarShowButton"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 641
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v0

    const-string v1, "AppBarHideButton"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 643
    return-void
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 1
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 826
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onTvEvent(Z)V

    .line 827
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 822
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0300b7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    .line 170
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0300b6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    const v1, 0x7f0e005f

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setId(I)V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setVisibility(I)V

    .line 175
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;-><init>(Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBar:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBar:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    new-instance v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$1;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 188
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->observeModels()V

    .line 190
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->updateTvStatus()V

    .line 191
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 832
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 837
    return-void
.end method

.method public onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 248
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isPaused:Z

    if-eqz v2, :cond_1

    .line 249
    const-string v2, "ApplicationBarManager"

    const-string/jumbo v3, "the applicationbar manager is paused, ignore onPause request"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->unRegisterTvEventListener()V

    .line 257
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->previousButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v2, :cond_2

    .line 258
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->previousButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 259
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->previousButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    aget-object v2, v2, v0

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 262
    .end local v0    # "i":I
    :cond_2
    const-string v2, "ApplicationBarManager"

    const-string v3, "onPause called "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    const v2, 0x7f0e0457

    invoke-virtual {p0, v2, v4}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setButtonClickListener(ILandroid/view/View$OnClickListener;)V

    .line 266
    const v2, 0x7f0e0456

    invoke-virtual {p0, v2, v4}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setButtonClickListener(ILandroid/view/View$OnClickListener;)V

    .line 268
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->cleanup()V

    .line 269
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->cleanup()V

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->cleanupListenerHooks()V

    .line 271
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowNowPlaying:Z

    .line 273
    iput v5, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageDefaultRid:I

    .line 274
    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageUri:Ljava/lang/String;

    .line 275
    iput v5, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageDefaultRid:I

    .line 276
    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageUri:Ljava/lang/String;

    .line 277
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->onPause()V

    .line 278
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->onPause()V

    .line 279
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isShown:Z

    .line 282
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->stopObserveModels()V

    .line 283
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isPaused:Z

    .line 285
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getRecordThatButton()Landroid/widget/Button;

    move-result-object v1

    .line 286
    .local v1, "recordButton":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 287
    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 194
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isPaused:Z

    if-nez v2, :cond_0

    .line 195
    const-string v2, "ApplicationBarManager"

    const-string/jumbo v3, "the applicationbar manager is not paused, ignore onResume request"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :goto_0
    return-void

    .line 199
    :cond_0
    const-string v2, "ApplicationBarManager"

    const-string v3, "onResume called "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$2;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    .line 207
    .local v0, "listener":Landroid/view/View$OnClickListener;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    const v2, 0x7f0e0457

    new-instance v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$3;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$3;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setButtonClickListener(ILandroid/view/View$OnClickListener;)V

    .line 215
    const v2, 0x7f0e0456

    new-instance v3, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$4;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$4;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setButtonClickListener(ILandroid/view/View$OnClickListener;)V

    .line 224
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getRecordThatButton()Landroid/widget/Button;

    move-result-object v1

    .line 225
    .local v1, "recordButton":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 226
    new-instance v2, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$5;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$5;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    :cond_1
    iput v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->cachedSessionState:I

    .line 238
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->observeModels()V

    .line 239
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->onResume()V

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->onResume()V

    .line 242
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->registerTvEventListener()V

    .line 244
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isPaused:Z

    goto :goto_0
.end method

.method public onTvEvent(Z)V
    .locals 1
    .param p1, "tvEnabled"    # Z

    .prologue
    .line 810
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isTvEnabled:Z

    .line 811
    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$14;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$14;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 817
    return-void
.end method

.method public setButtonClickListener(ILandroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 300
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setViewClickListener(ILandroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 301
    return-void
.end method

.method public setButtonEnabled(IZ)V
    .locals 3
    .param p1, "resId"    # I
    .param p2, "isEnabled"    # Z

    .prologue
    .line 325
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 326
    .local v0, "collapsedButton":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 329
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 330
    .local v1, "expandedButton":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 331
    invoke-virtual {v1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 333
    :cond_1
    return-void
.end method

.method public setButtonText(ILjava/lang/String;)V
    .locals 3
    .param p1, "resId"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 347
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 348
    .local v0, "collapsedButton":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 352
    .local v1, "expandedButton":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 353
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 355
    :cond_1
    return-void
.end method

.method public setButtonVisibility(II)V
    .locals 3
    .param p1, "resId"    # I
    .param p2, "visibility"    # I

    .prologue
    .line 336
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 337
    .local v0, "collapsedButton":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 340
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 341
    .local v1, "expandedButton":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 342
    invoke-virtual {v1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 344
    :cond_1
    return-void
.end method

.method public abstract setCurrentPage(I)V
.end method

.method public setOnAnimationEndRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "onAnimationEnd"    # Ljava/lang/Runnable;

    .prologue
    .line 630
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onAnimationEndRunnable:Ljava/lang/Runnable;

    .line 631
    return-void
.end method

.method public setOnNewButtonsAddedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "onButtonsAdded"    # Ljava/lang/Runnable;

    .prologue
    .line 634
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onNewButtonsAddedRunnable:Ljava/lang/Runnable;

    .line 635
    return-void
.end method

.method public abstract setPageIndicatorBackground(I)V
.end method

.method public abstract setPageIndicatorDrawables(II)V
.end method

.method public setShouldShowNowPlaying(Z)V
    .locals 3
    .param p1, "shouldShowNowPlaying"    # Z

    .prologue
    .line 137
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowNowPlaying:Z

    if-eq v1, p1, :cond_0

    .line 138
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowNowPlaying:Z

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowNowPlaying:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setNowPlayingEnabled(Z)V

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowNowPlaying:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->setNowPlayingEnabled(Z)V

    .line 143
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->shouldShowNowPlaying:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 144
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setNowPlayingVisibility(I)V

    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->setNowPlayingVisibility(I)V

    .line 147
    .end local v0    # "visibility":I
    :cond_0
    return-void

    .line 143
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public abstract setTotalPageCount(I)V
.end method

.method protected abstract shouldShowSwapButtonAnimation()Z
.end method

.method public show()V
    .locals 2

    .prologue
    .line 383
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getShouldShowAppbar()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isShown:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isConnectedToConsole:Z

    if-eqz v0, :cond_0

    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isShown:Z

    .line 386
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setVisibility(I)V

    .line 391
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->resetBottomMargin()V

    .line 397
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    const-string v0, "ApplicationBarManager"

    const-string v1, "current screen does not show appbar, ignore"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 647
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 648
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    .line 650
    .local v2, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v4, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$15;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 677
    .end local v2    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_0
    return-void

    .line 652
    .restart local v2    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    const/4 v0, 0x1

    .line 653
    .local v0, "isCurrentlyConnected":Z
    :goto_1
    if-nez v0, :cond_1

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isConnectedToConsole:Z

    if-eqz v4, :cond_1

    .line 655
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->expandedAppBarView:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->reset()V

    .line 656
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 658
    :cond_1
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isConnectedToConsole:Z

    .line 659
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->isConnectedToConsole:Z

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v4

    instance-of v4, v4, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    if-nez v4, :cond_3

    .line 660
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->show()V

    goto :goto_0

    .line 652
    .end local v0    # "isCurrentlyConnected":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 662
    .restart local v0    # "isCurrentlyConnected":Z
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->hide()V

    goto :goto_0

    .line 667
    .end local v0    # "isCurrentlyConnected":Z
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 668
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getSmartGlassFoucsNowPlayingViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v3

    .line 669
    .local v3, "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v1

    .line 670
    .local v1, "nowPlayingTile":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    .line 671
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->setImageBackgroundColor(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 650
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateNowPlayingBarContent()V
    .locals 14

    .prologue
    const v13, 0x7f0e001d

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 680
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getSmartGlassFoucsNowPlayingViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v6

    .line 681
    .local v6, "vm":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    const/4 v5, 0x0

    .line 682
    .local v5, "showRecordButton":Z
    if-eqz v6, :cond_8

    .line 683
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageUri:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingBarFocusedImageUri()Ljava/lang/String;

    move-result-object v11

    if-ne v8, v11, :cond_0

    iget v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageDefaultRid:I

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingDefaultImageRid()I

    move-result v11

    if-ne v8, v11, :cond_0

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageDefaultAspectRatio:[I

    .line 684
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingDefaultAspectRatio()[I

    move-result-object v11

    invoke-static {v8, v11}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 685
    :cond_0
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingDefaultAspectRatio()[I

    move-result-object v8

    iput-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageDefaultAspectRatio:[I

    .line 686
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingDefaultImageRid()I

    move-result v8

    iput v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageDefaultRid:I

    .line 687
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingBarFocusedImageUri()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageUri:Ljava/lang/String;

    .line 689
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v2

    .line 690
    .local v2, "nowPlayingTile":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    invoke-virtual {v2, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 692
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowOOBE()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 693
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getOOBEImageResourceId()I

    move-result v8

    invoke-static {v2, v8}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;I)V

    .line 711
    .end local v2    # "nowPlayingTile":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_1
    :goto_0
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowRecordThat()Z

    move-result v5

    .line 712
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingPrimaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 713
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingSecondaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 714
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingPrimaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v8

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 715
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingSecondaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getNowPlayingSecondaryText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 716
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingSecondaryTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 723
    :goto_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getRecordThatButton()Landroid/widget/Button;

    move-result-object v4

    .line 724
    .local v4, "recordButton":Landroid/view/View;
    if-eqz v4, :cond_2

    .line 725
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 726
    .local v3, "parent":Landroid/view/View;
    if-eqz v5, :cond_9

    move v8, v9

    :goto_2
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 729
    .end local v3    # "parent":Landroid/view/View;
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getSmartGlassNonFoucsNowPlayingViewModel()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    move-result-object v7

    .line 730
    .local v7, "vm2":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    if-eqz v7, :cond_a

    .line 731
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingSecondaryTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 732
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageUri:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingBarNonFocusedImageUri()Ljava/lang/String;

    move-result-object v10

    if-ne v8, v10, :cond_3

    iget v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageDefaultRid:I

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingDefaultImageRid()I

    move-result v10

    if-eq v8, v10, :cond_4

    .line 733
    :cond_3
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingDefaultImageRid()I

    move-result v8

    iput v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageDefaultRid:I

    .line 734
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->nowPlayingBarNonFocusedImageUri()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageUri:Ljava/lang/String;

    .line 735
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingSecondaryTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v0

    .line 736
    .local v0, "imageTile":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    invoke-virtual {v0, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 737
    new-instance v8, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$13;

    invoke-direct {v8, p0, v7}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$13;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V

    invoke-virtual {v0, v13, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 743
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageUri:Ljava/lang/String;

    sget v9, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    iget v10, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingSecondaryImageDefaultRid:I

    invoke-virtual {v0, v8, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 749
    .end local v0    # "imageTile":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_4
    :goto_3
    if-nez v6, :cond_5

    if-nez v7, :cond_5

    .line 750
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getExpandedAppBar()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 752
    :cond_5
    return-void

    .line 695
    .end local v4    # "recordButton":Landroid/view/View;
    .end local v7    # "vm2":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .restart local v2    # "nowPlayingTile":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_6
    invoke-virtual {v2, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 696
    new-instance v8, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$12;

    invoke-direct {v8, p0, v6}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$12;-><init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V

    invoke-virtual {v2, v13, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 702
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 703
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_7

    .line 704
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    .line 708
    :goto_4
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageUri:Ljava/lang/String;

    sget v11, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    iget v12, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->currentNowPlayingImageDefaultRid:I

    invoke-virtual {v2, v8, v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 706
    :cond_7
    sget v8, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    goto :goto_4

    .line 718
    .end local v1    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v2    # "nowPlayingTile":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_8
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 719
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingPrimaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 720
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingSecondaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto/16 :goto_1

    .restart local v3    # "parent":Landroid/view/View;
    .restart local v4    # "recordButton":Landroid/view/View;
    :cond_9
    move v8, v10

    .line 726
    goto/16 :goto_2

    .line 746
    .end local v3    # "parent":Landroid/view/View;
    .restart local v7    # "vm2":Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    :cond_a
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getNowPlayingSecondaryTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-result-object v8

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    goto :goto_3
.end method
