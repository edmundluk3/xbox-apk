.class public Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;
.super Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;
.source "URCResourceGroupView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "URCResourceGroupView"


# instance fields
.field private mHandler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->init(Landroid/util/AttributeSet;I)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->init(Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->init(Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    .line 33
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->mHandler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    .line 34
    return-void
.end method

.method private setNarratorContent(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 62
    if-nez p1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    instance-of v6, p1, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    if-eqz v6, :cond_3

    move-object v4, p1

    .line 66
    check-cast v4, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    .line 67
    .local v4, "pv":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getCommand()Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 69
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getInstance(Landroid/content/Context;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getNarratorContentIdFromButtonId(Ljava/lang/String;)I

    move-result v5

    .line 70
    .local v5, "stringId":I
    if-eqz v5, :cond_0

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 74
    .end local v5    # "stringId":I
    :cond_2
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildCount()I

    move-result v0

    .line 75
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 76
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->setNarratorContent(Landroid/view/View;)V

    .line 75
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 79
    .end local v0    # "childCount":I
    .end local v1    # "i":I
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "pv":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;
    :cond_3
    instance-of v6, p1, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;

    if-eqz v6, :cond_0

    move-object v2, p1

    .line 80
    check-cast v2, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;

    .line 81
    .local v2, "ib":Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;->getCommand()Ljava/lang/String;

    move-result-object v3

    .line 82
    .restart local v3    # "id":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getInstance(Landroid/content/Context;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getNarratorContentIdFromButtonId(Ljava/lang/String;)I

    move-result v5

    .line 83
    .restart local v5    # "stringId":I
    if-eqz v5, :cond_0

    .line 84
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 92
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->localizeStrings(Landroid/view/View;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->setNarratorContent(Landroid/view/View;)V

    .line 94
    return-void
.end method

.method protected localizeStrings(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 38
    instance-of v5, p1, Landroid/view/ViewGroup;

    if-eqz v5, :cond_0

    move-object v4, p1

    .line 39
    check-cast v4, Landroid/view/ViewGroup;

    .line 40
    .local v4, "vg":Landroid/view/ViewGroup;
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 41
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 42
    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->localizeStrings(Landroid/view/View;)V

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    .end local v0    # "childCount":I
    .end local v1    # "i":I
    .end local v4    # "vg":Landroid/view/ViewGroup;
    :cond_0
    instance-of v5, p1, Landroid/widget/TextView;

    if-eqz v5, :cond_1

    move-object v3, p1

    .line 48
    check-cast v3, Landroid/widget/TextView;

    .line 49
    .local v3, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getInstance(Landroid/content/Context;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v5

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getStringIdFromTextId(I)I

    move-result v2

    .line 50
    .local v2, "stringId":I
    if-eqz v2, :cond_2

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->isInEditMode()Z

    move-result v5

    if-nez v5, :cond_1

    .line 52
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .end local v2    # "stringId":I
    .end local v3    # "tv":Landroid/widget/TextView;
    :cond_1
    :goto_1
    return-void

    .line 55
    .restart local v2    # "stringId":I
    .restart local v3    # "tv":Landroid/widget/TextView;
    :cond_2
    const-string v5, "URCResourceGroupView"

    const-string v6, "Attempted to localize text for unknown text view!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const-string v5, ""

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->mHandler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->onAttachedToWindow()V

    .line 99
    invoke-super {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->onAttachedToWindow()V

    .line 100
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->onDetachedFromWindow()V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCResourceGroupView;->mHandler:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->onDetachedFromWindow()V

    .line 106
    return-void
.end method
