.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "PeopleActivityFeedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$ViewHolderHeader;,
        Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

.field private header:Landroid/view/View;

.field private final inflater:Landroid/view/LayoutInflater;

.field private final mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

.field private onScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p3, "floatingRowHelper"    # Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 48
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 49
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .line 50
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->onScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    .line 53
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    return-object v0
.end method

.method private getHeaderPosition()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->header:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getItemViewType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 246
    if-eqz p0, :cond_3

    .line 247
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getIsHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Hidden:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    .line 257
    :goto_0
    return-object v0

    .line 249
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isRecommendation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Recommendations:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 251
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isTrending()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Trending:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 253
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Shared:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 257
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Normal:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0
.end method

.method private getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getHeaderPosition()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 221
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Header:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    .line 242
    :goto_0
    return-object v0

    .line 224
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getStatusPostPosition()I

    move-result v0

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->isStatusPostEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->EntryText:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 228
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getModelStatePosition()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 229
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 238
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getModelStatePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItemViewType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v0

    goto :goto_0

    .line 231
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Error:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 233
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->Loading:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 235
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->NoData:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    goto :goto_0

    .line 242
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getModelStatePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItemViewType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v0

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getModelStatePosition()I
    .locals 2

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getStatusPostPosition()I

    move-result v0

    .line 184
    .local v0, "statusPostPosition":I
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->isStatusPostEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 185
    :cond_0
    return v0
.end method

.method private getStatusPostPosition()I
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getHeaderPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private isStatusPostEnabled()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isStatusPostEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$onBindViewHolder$0(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;
    .param p1, "v1"    # Landroid/view/View;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchStatusPost()V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$1(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;
    .param p1, "v1"    # Landroid/view/View;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchUnsharedFeed()V

    return-void
.end method

.method static synthetic lambda$onBindViewHolder$2(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;
    .param p1, "v1"    # Landroid/view/View;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchFilterSelection()V

    return-void
.end method


# virtual methods
.method public addAll(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v0, "activityFeedData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 170
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v1, :cond_0

    .line 171
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_1
    invoke-super {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;->addAll(Ljava/util/Collection;)V

    .line 176
    return-void
.end method

.method public getItem(I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 198
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getHeaderPosition()I

    move-result v2

    if-ne p1, v2, :cond_1

    .line 216
    :cond_0
    :goto_0
    :pswitch_0
    return-object v1

    .line 202
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getStatusPostPosition()I

    move-result v2

    if-ne p1, v2, :cond_2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->isStatusPostEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 206
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getModelStatePosition()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 207
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 215
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getModelStatePosition()I

    move-result v2

    sub-int v0, p1, v2

    .line 216
    .local v0, "dataIndex":I
    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getDataCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 155
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 162
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getModelStatePosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getDataCount()I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    .line 159
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getModelStatePosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getOnScrollListener()Landroid/support/v7/widget/RecyclerView$OnScrollListener;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->onScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    return-object v0
.end method

.method public onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3
    .param p1, "view"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;->onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 62
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->EntryText:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 63
    return-void
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 13
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v12, 0x0

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v3

    aget-object v11, v0, v3

    .line 90
    .local v11, "viewType":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    const/4 v2, 0x0

    .line 91
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$PeopleActivityFeedListAdapterUtil$ViewType:[I

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 146
    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_0
    :goto_0
    return-void

    .line 93
    .restart local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :pswitch_0
    iget-object v10, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 94
    .local v10, "v":Landroid/view/View;
    const v0, 0x7f0e08ac

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 95
    .local v8, "postStatusView":Landroid/view/View;
    const v0, 0x7f0e08ad

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 96
    .local v9, "shareButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setBackgroundColor(I)V

    .line 97
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->floatingRowHelper:Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/FloatingRowHelper;->isFloatingHeaderVisible()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-static {v10, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 101
    const v0, 0x7f0e08ae

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 102
    .local v7, "filterButton":Landroid/widget/Button;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isFilterEnabled()Z

    move-result v0

    invoke-static {v7, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filtersAreActive()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 108
    const v0, 0x7f070ff6

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .end local v7    # "filterButton":Landroid/widget/Button;
    :cond_2
    move v0, v12

    .line 99
    goto :goto_1

    .line 110
    .restart local v7    # "filterButton":Landroid/widget/Button;
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0142

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 111
    const v0, 0x7f070f34

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0

    .line 118
    .end local v7    # "filterButton":Landroid/widget/Button;
    .end local v8    # "postStatusView":Landroid/view/View;
    .end local v9    # "shareButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v10    # "v":Landroid/view/View;
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItem(I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 119
    if-eqz v2, :cond_0

    move-object v1, p1

    .line 120
    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .line 121
    .local v1, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 122
    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->detachItemListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->ACTIVITY_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->NAV_FEED:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    .line 124
    iget-object v0, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-static {v1, v2, v0, v3, v12}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->attachSocialBarListeners(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)V

    .line 127
    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->attachActivityFeedViewAccessibilityDelegate(Landroid/view/View;)V

    .line 128
    invoke-static {v1, v11}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->setNarratorContent(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;)V

    goto/16 :goto_0

    .line 132
    .end local v1    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItem(I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 133
    if-eqz v2, :cond_0

    .line 134
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-static {p1, v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindTrendingView(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V

    goto/16 :goto_0

    .line 138
    .restart local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItem(I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    .line 139
    if-eqz v2, :cond_0

    .line 140
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    .end local p1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-static {p1, v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindSocialRecommendationView(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V

    goto/16 :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 67
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v3

    aget-object v2, v3, p2

    .line 68
    .local v2, "vt":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    const/4 v0, 0x0

    .line 70
    .local v0, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$PeopleActivityFeedListAdapterUtil$ViewType:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 75
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->inflater:Landroid/view/LayoutInflater;

    invoke-static {p1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createViewHolder(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;Landroid/view/LayoutInflater;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    move-result-object v0

    .line 76
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;->NoData:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    if-ne v2, v3, :cond_0

    .line 77
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v4, 0x7f0e06d5

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 78
    .local v1, "noDataTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v1, :cond_0

    .line 79
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getNoContentText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    .end local v1    # "noDataTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_0
    :goto_0
    return-object v0

    .line 72
    :pswitch_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$ViewHolderHeader;

    .end local v0    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->header:Landroid/view/View;

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$ViewHolderHeader;-><init>(Landroid/view/View;)V

    .line 73
    .restart local v0    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setHeader(Landroid/view/View;)V
    .locals 0
    .param p1, "header"    # Landroid/view/View;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->header:Landroid/view/View;

    .line 57
    return-void
.end method
