.class public Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;
.super Lio/reactivex/Observable;
.source "RxWebSocket.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable",
        "<",
        "Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private client:Lokhttp3/OkHttpClient;

.field private outgoingMessages:Lio/reactivex/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/ReplaySubject",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private socket:Lokhttp3/WebSocket;

.field private final subProtocol:Ljava/lang/String;

.field private final webSocketEndpoint:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lokhttp3/OkHttpClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "httpClient"    # Lokhttp3/OkHttpClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "webSocketEndpoint"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "subProtocol"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 42
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 43
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->client:Lokhttp3/OkHttpClient;

    .line 46
    invoke-static {}, Lio/reactivex/subjects/ReplaySubject;->create()Lio/reactivex/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->outgoingMessages:Lio/reactivex/subjects/ReplaySubject;

    .line 47
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->webSocketEndpoint:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->subProtocol:Ljava/lang/String;

    .line 49
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;)Lio/reactivex/subjects/ReplaySubject;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->outgoingMessages:Lio/reactivex/subjects/ReplaySubject;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;)Lokhttp3/WebSocket;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->socket:Lokhttp3/WebSocket;

    return-object v0
.end method


# virtual methods
.method public isConnected()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->socket:Lokhttp3/WebSocket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendData(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 108
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->outgoingMessages:Lio/reactivex/subjects/ReplaySubject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    .line 110
    return-void
.end method

.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->client:Lokhttp3/OkHttpClient;

    new-instance v1, Lokhttp3/Request$Builder;

    invoke-direct {v1}, Lokhttp3/Request$Builder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->webSocketEndpoint:Ljava/lang/String;

    .line 59
    invoke-virtual {v1, v2}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    const-string v2, "Sec-WebSocket-Protocol"

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->subProtocol:Ljava/lang/String;

    .line 60
    invoke-virtual {v1, v2, v3}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket$1;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket$1;-><init>(Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;Lio/reactivex/Observer;)V

    .line 57
    invoke-virtual {v0, v1, v2}, Lokhttp3/OkHttpClient;->newWebSocket(Lokhttp3/Request;Lokhttp3/WebSocketListener;)Lokhttp3/WebSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->socket:Lokhttp3/WebSocket;

    .line 98
    return-void
.end method
