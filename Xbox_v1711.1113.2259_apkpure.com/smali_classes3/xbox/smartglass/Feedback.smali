.class public Lxbox/smartglass/Feedback;
.super LMicrosoft/Telemetry/Data;
.source "Feedback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxbox/smartglass/Feedback$Schema;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LMicrosoft/Telemetry/Data",
        "<",
        "Lxbox/smartglass/CommonData;",
        ">;"
    }
.end annotation


# instance fields
.field private comment:Ljava/lang/String;

.field private logData:Ljava/lang/String;

.field private pageName:Ljava/lang/String;

.field private rating:I

.field private ratingType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 299
    invoke-direct {p0}, LMicrosoft/Telemetry/Data;-><init>()V

    .line 301
    return-void
.end method

.method public static getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lxbox/smartglass/Feedback$Schema;->schemaDef:Lcom/microsoft/bond/SchemaDef;

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/microsoft/bond/BondSerializable;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lxbox/smartglass/Feedback;->clone()Lcom/microsoft/bond/BondSerializable;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Lcom/microsoft/bond/StructDef;)Lcom/microsoft/bond/BondMirror;
    .locals 1
    .param p1, "structDef"    # Lcom/microsoft/bond/StructDef;

    .prologue
    .line 278
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getField(Lcom/microsoft/bond/FieldDef;)Ljava/lang/Object;
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;

    .prologue
    .line 233
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 245
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 235
    :sswitch_0
    iget v0, p0, Lxbox/smartglass/Feedback;->rating:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 237
    :sswitch_1
    iget v0, p0, Lxbox/smartglass/Feedback;->ratingType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 239
    :sswitch_2
    iget-object v0, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    goto :goto_0

    .line 241
    :sswitch_3
    iget-object v0, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    goto :goto_0

    .line 243
    :sswitch_4
    iget-object v0, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    goto :goto_0

    .line 233
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public final getLogData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    return-object v0
.end method

.method public final getPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    return-object v0
.end method

.method public final getRating()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lxbox/smartglass/Feedback;->rating:I

    return v0
.end method

.method public final getRatingType()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lxbox/smartglass/Feedback;->ratingType:I

    return v0
.end method

.method public getSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 286
    invoke-static {}, Lxbox/smartglass/Feedback;->getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v0

    return-object v0
.end method

.method public marshal(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 0
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 458
    invoke-static {p0, p1}, Lcom/microsoft/bond/internal/Marshaler;->marshal(Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/ProtocolWriter;)V

    .line 459
    return-void
.end method

.method public memberwiseCompare(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 517
    if-nez p1, :cond_1

    .line 523
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 521
    check-cast v0, Lxbox/smartglass/Feedback;

    .line 523
    .local v0, "that":Lxbox/smartglass/Feedback;
    invoke-virtual {p0, v0}, Lxbox/smartglass/Feedback;->memberwiseCompareQuick(Lxbox/smartglass/Feedback;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lxbox/smartglass/Feedback;->memberwiseCompareDeep(Lxbox/smartglass/Feedback;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected memberwiseCompareDeep(Lxbox/smartglass/Feedback;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/Feedback;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 541
    const/4 v0, 0x1

    .line 542
    .local v0, "equals":Z
    if-eqz v0, :cond_3

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareDeep(LMicrosoft/Telemetry/Data;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 543
    :goto_0
    if-eqz v0, :cond_5

    iget-object v3, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    if-nez v3, :cond_4

    :cond_0
    move v0, v1

    .line 544
    :goto_1
    if-eqz v0, :cond_7

    iget-object v3, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    if-nez v3, :cond_6

    :cond_1
    move v0, v1

    .line 545
    :goto_2
    if-eqz v0, :cond_9

    iget-object v3, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    if-nez v3, :cond_8

    :cond_2
    move v0, v1

    .line 546
    :goto_3
    return v0

    :cond_3
    move v0, v2

    .line 542
    goto :goto_0

    .line 543
    :cond_4
    iget-object v3, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_5
    move v0, v2

    goto :goto_1

    .line 544
    :cond_6
    iget-object v3, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_7
    move v0, v2

    goto :goto_2

    .line 545
    :cond_8
    iget-object v3, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_9
    move v0, v2

    goto :goto_3
.end method

.method protected memberwiseCompareQuick(Lxbox/smartglass/Feedback;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/Feedback;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 527
    const/4 v0, 0x1

    .line 528
    .local v0, "equals":Z
    if-eqz v0, :cond_3

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareQuick(LMicrosoft/Telemetry/Data;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v2

    .line 529
    :goto_0
    if-eqz v0, :cond_4

    iget v1, p0, Lxbox/smartglass/Feedback;->rating:I

    iget v4, p1, Lxbox/smartglass/Feedback;->rating:I

    if-ne v1, v4, :cond_4

    move v0, v2

    .line 530
    :goto_1
    if-eqz v0, :cond_5

    iget v1, p0, Lxbox/smartglass/Feedback;->ratingType:I

    iget v4, p1, Lxbox/smartglass/Feedback;->ratingType:I

    if-ne v1, v4, :cond_5

    move v0, v2

    .line 531
    :goto_2
    if-eqz v0, :cond_8

    iget-object v1, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    iget-object v4, p1, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    if-nez v4, :cond_7

    move v4, v2

    :goto_4
    if-ne v1, v4, :cond_8

    move v0, v2

    .line 532
    :goto_5
    if-eqz v0, :cond_a

    iget-object v1, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    if-nez v1, :cond_9

    :cond_0
    move v0, v2

    .line 533
    :goto_6
    if-eqz v0, :cond_d

    iget-object v1, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    if-nez v1, :cond_b

    move v1, v2

    :goto_7
    iget-object v4, p1, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    if-nez v4, :cond_c

    move v4, v2

    :goto_8
    if-ne v1, v4, :cond_d

    move v0, v2

    .line 534
    :goto_9
    if-eqz v0, :cond_f

    iget-object v1, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    if-nez v1, :cond_e

    :cond_1
    move v0, v2

    .line 535
    :goto_a
    if-eqz v0, :cond_12

    iget-object v1, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    if-nez v1, :cond_10

    move v1, v2

    :goto_b
    iget-object v4, p1, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    if-nez v4, :cond_11

    move v4, v2

    :goto_c
    if-ne v1, v4, :cond_12

    move v0, v2

    .line 536
    :goto_d
    if-eqz v0, :cond_14

    iget-object v1, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    if-nez v1, :cond_13

    :cond_2
    move v0, v2

    .line 537
    :goto_e
    return v0

    :cond_3
    move v0, v3

    .line 528
    goto :goto_0

    :cond_4
    move v0, v3

    .line 529
    goto :goto_1

    :cond_5
    move v0, v3

    .line 530
    goto :goto_2

    :cond_6
    move v1, v3

    .line 531
    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_4

    :cond_8
    move v0, v3

    goto :goto_5

    .line 532
    :cond_9
    iget-object v1, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_0

    :cond_a
    move v0, v3

    goto :goto_6

    :cond_b
    move v1, v3

    .line 533
    goto :goto_7

    :cond_c
    move v4, v3

    goto :goto_8

    :cond_d
    move v0, v3

    goto :goto_9

    .line 534
    :cond_e
    iget-object v1, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_1

    :cond_f
    move v0, v3

    goto :goto_a

    :cond_10
    move v1, v3

    .line 535
    goto :goto_b

    :cond_11
    move v4, v3

    goto :goto_c

    :cond_12
    move v0, v3

    goto :goto_d

    .line 536
    :cond_13
    iget-object v1, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_2

    :cond_14
    move v0, v3

    goto :goto_e
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readBegin()V

    .line 338
    invoke-virtual {p0, p1}, Lxbox/smartglass/Feedback;->readNested(Lcom/microsoft/bond/ProtocolReader;)V

    .line 339
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readEnd()V

    .line 340
    return-void
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 358
    return-void
.end method

.method public readNested(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 346
    sget-object v0, Lcom/microsoft/bond/ProtocolCapability;->TAGGED:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v0}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 347
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Feedback;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Feedback;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    invoke-static {p1}, Lcom/microsoft/bond/internal/ReadHelper;->skipPartialStruct(Lcom/microsoft/bond/ProtocolReader;)V

    goto :goto_0
.end method

.method protected readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z
    .locals 5
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 403
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 405
    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 450
    :goto_0
    return v2

    .line 422
    .local v0, "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :sswitch_0
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/Feedback;->rating:I

    .line 444
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldEnd()V

    .line 412
    .end local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldBegin()Lcom/microsoft/bond/ProtocolReader$FieldTag;

    move-result-object v0

    .line 414
    .restart local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP:Lcom/microsoft/bond/BondDataType;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_3

    .line 416
    :cond_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_2

    .line 447
    .local v1, "isPartial":Z
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    move v2, v1

    .line 450
    goto :goto_0

    .end local v1    # "isPartial":Z
    :cond_2
    move v1, v2

    .line 416
    goto :goto_2

    .line 420
    :cond_3
    iget v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->id:I

    sparse-switch v3, :sswitch_data_0

    .line 440
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-virtual {p1, v3}, Lcom/microsoft/bond/ProtocolReader;->skip(Lcom/microsoft/bond/BondDataType;)V

    goto :goto_1

    .line 426
    :sswitch_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/Feedback;->ratingType:I

    goto :goto_1

    .line 430
    :sswitch_2
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    goto :goto_1

    .line 434
    :sswitch_3
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    goto :goto_1

    .line 437
    :sswitch_4
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    goto :goto_1

    .line 420
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method protected readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 361
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 363
    .local v0, "canOmitFields":Z
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 364
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 366
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 367
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/Feedback;->rating:I

    .line 374
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 375
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/Feedback;->ratingType:I

    .line 382
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_5

    .line 383
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    .line 390
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_7

    .line 391
    :cond_6
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    .line 394
    :cond_7
    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_9

    .line 395
    :cond_8
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    .line 397
    :cond_9
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    .line 398
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 307
    const-string v0, "Feedback"

    const-string v1, "xbox.smartglass.Feedback"

    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/Feedback;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    return-void
.end method

.method protected reset(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "qualifiedName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 311
    invoke-super {p0, p1, p2}, LMicrosoft/Telemetry/Data;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    iput v0, p0, Lxbox/smartglass/Feedback;->rating:I

    .line 313
    iput v0, p0, Lxbox/smartglass/Feedback;->ratingType:I

    .line 314
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    .line 315
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    .line 316
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    .line 317
    return-void
.end method

.method public final setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setField(Lcom/microsoft/bond/FieldDef;Ljava/lang/Object;)V
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 254
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 271
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    return-void

    .line 256
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/Feedback;->rating:I

    goto :goto_0

    .line 259
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_1
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/Feedback;->ratingType:I

    goto :goto_0

    .line 262
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_2
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    goto :goto_0

    .line 265
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_3
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    goto :goto_0

    .line 268
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_4
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    goto :goto_0

    .line 254
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public final setLogData(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public final setPageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public final setRating(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 55
    iput p1, p0, Lxbox/smartglass/Feedback;->rating:I

    .line 56
    return-void
.end method

.method public final setRatingType(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 69
    iput p1, p0, Lxbox/smartglass/Feedback;->ratingType:I

    .line 70
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    invoke-static {p1, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V

    .line 324
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    check-cast p2, Lcom/microsoft/bond/SchemaDef;

    .end local p2    # "schema":Lcom/microsoft/bond/BondSerializable;
    invoke-static {p1, p2, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/BondSerializable;)V

    .line 331
    return-void
.end method

.method public write(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 2
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 465
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeBegin()V

    .line 467
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->getFirstPassWriter()Lcom/microsoft/bond/ProtocolWriter;

    move-result-object v0

    .local v0, "firstPassWriter":Lcom/microsoft/bond/ProtocolWriter;
    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/Feedback;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 470
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Feedback;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 476
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeEnd()V

    .line 477
    return-void

    .line 474
    :cond_0
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Feedback;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    goto :goto_0
.end method

.method public writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V
    .locals 6
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x32

    const/16 v4, 0x28

    .line 480
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 481
    .local v0, "canOmitFields":Z
    sget-object v1, Lxbox/smartglass/Feedback$Schema;->metadata:Lcom/microsoft/bond/Metadata;

    invoke-virtual {p1, v1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructBegin(Lcom/microsoft/bond/BondSerializable;Z)V

    .line 482
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 484
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0xa

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 485
    iget v1, p0, Lxbox/smartglass/Feedback;->rating:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 486
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 488
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x14

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 489
    iget v1, p0, Lxbox/smartglass/Feedback;->ratingType:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 490
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 492
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x1e

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 493
    iget-object v1, p0, Lxbox/smartglass/Feedback;->pageName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 494
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 496
    if-eqz v0, :cond_0

    iget-object v1, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Metadata;->getDefault_value()Lcom/microsoft/bond/Variant;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Variant;->getWstring_value()Ljava/lang/String;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 497
    :cond_0
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v4, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 498
    iget-object v1, p0, Lxbox/smartglass/Feedback;->logData:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 499
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 504
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$400()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Metadata;->getDefault_value()Lcom/microsoft/bond/Variant;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Variant;->getWstring_value()Ljava/lang/String;

    move-result-object v2

    if-eq v1, v2, :cond_3

    .line 505
    :cond_1
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$400()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v5, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 506
    iget-object v1, p0, Lxbox/smartglass/Feedback;->comment:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 507
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 512
    :goto_1
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructEnd(Z)V

    .line 513
    return-void

    .line 501
    :cond_2
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v4, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldOmitted(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    goto :goto_0

    .line 509
    :cond_3
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Feedback$Schema;->access$400()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v5, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldOmitted(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    goto :goto_1
.end method
