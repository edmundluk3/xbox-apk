.class Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "HomeScreenNowPlayingViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadXboxOneTitleSummaryAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final isGame:Z

.field private final strTitleId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

.field private final titleId:I

.field private final titleModel:Lcom/microsoft/xbox/service/model/TitleModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;IZ)V
    .locals 3
    .param p2, "titleId"    # I
    .param p3, "isGame"    # Z

    .prologue
    .line 506
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 507
    iput p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleId:I

    .line 508
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->isGame:Z

    .line 509
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleId:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->strTitleId:Ljava/lang/String;

    .line 510
    int-to-long v0, p2

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 511
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Created LoadXboxOneTitleSummaryAsyncTask with titleId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->strTitleId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 520
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 521
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->strTitleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshTitleProgress(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getTitleId()I
    .locals 1

    .prologue
    .line 515
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleId:I

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 559
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->strTitleId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 561
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->strTitleId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadTitleProgress(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 562
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 563
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to get title details"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    :cond_0
    :goto_1
    return-object v0

    .line 559
    .end local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 567
    .restart local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->loadTitleStatistics(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 568
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to get hero stats for title"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_3
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->isGame:Z

    if-eqz v1, :cond_0

    .line 573
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->loadTitleImages(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 574
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to get title image details"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 554
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 5

    .prologue
    .line 526
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 527
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->cancelled:Z

    if-nez v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleId:I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->strTitleId:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ILjava/lang/String;Lcom/microsoft/xbox/service/model/TitleModel;)V

    .line 533
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    .line 534
    return-void

    .line 530
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "The LoadXboxOneTitleSummaryAsyncTask has been cancelled"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 543
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->cancelled:Z

    if-nez v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleId:I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->strTitleId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ILjava/lang/String;Lcom/microsoft/xbox/service/model/TitleModel;)V

    .line 549
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;

    .line 550
    return-void

    .line 546
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "The LoadXboxOneTitleSummaryAsyncTask has been cancelled"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 500
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel$LoadXboxOneTitleSummaryAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 538
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 539
    return-void
.end method
