.class public Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;
.super Landroid/view/View;
.source "ChatHeadOverlayView.java"


# static fields
.field private static final ANIMATION_DURATION:J = 0x258L


# instance fields
.field private OVAL_RADIUS:F

.field private STAMP_SPACING:F

.field private animator:Landroid/animation/ObjectAnimator;

.field private arrowDashedPath:Landroid/graphics/Path;

.field private paint:Landroid/graphics/Paint;

.field private pathDashEffect:Landroid/graphics/PathEffect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->paint:Landroid/graphics/Paint;

    .line 31
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->init(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->paint:Landroid/graphics/Paint;

    .line 36
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->init(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->paint:Landroid/graphics/Paint;

    .line 51
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->init(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method private animatePath()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 95
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 40
    const/16 v0, 0x14

    invoke-static {p1, v0}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->STAMP_SPACING:F

    .line 41
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->OVAL_RADIUS:F

    .line 42
    const-string v0, "phase"

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    iget v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->STAMP_SPACING:F

    neg-float v2, v2

    aput v2, v1, v4

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animator:Landroid/animation/ObjectAnimator;

    .line 43
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 44
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 45
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animator:Landroid/animation/ObjectAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 46
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 47
    return-void
.end method

.method private makeDot(F)Landroid/graphics/Path;
    .locals 3
    .param p1, "radius"    # F

    .prologue
    const/4 v2, 0x0

    .line 67
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 68
    .local v0, "p":Landroid/graphics/Path;
    sget-object v1, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v2, v2, p1, v1}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 69
    return-object v0
.end method

.method private setPhase(F)V
    .locals 4
    .param p1, "phase"    # F

    .prologue
    .line 87
    new-instance v0, Landroid/graphics/PathDashPathEffect;

    iget v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->OVAL_RADIUS:F

    invoke-direct {p0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->makeDot(F)Landroid/graphics/Path;

    move-result-object v1

    iget v2, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->STAMP_SPACING:F

    sget-object v3, Landroid/graphics/PathDashPathEffect$Style;->ROTATE:Landroid/graphics/PathDashPathEffect$Style;

    invoke-direct {v0, v1, v2, p1, v3}, Landroid/graphics/PathDashPathEffect;-><init>(Landroid/graphics/Path;FFLandroid/graphics/PathDashPathEffect$Style;)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->pathDashEffect:Landroid/graphics/PathEffect;

    .line 88
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->invalidate()V

    .line 89
    return-void
.end method


# virtual methods
.method public clearPath()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->arrowDashedPath:Landroid/graphics/Path;

    .line 100
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->invalidate()V

    .line 101
    return-void
.end method

.method public drawPath(FFFF)V
    .locals 3
    .param p1, "fromX"    # F
    .param p2, "fromY"    # F
    .param p3, "toX"    # F
    .param p4, "toY"    # F

    .prologue
    .line 73
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->arrowDashedPath:Landroid/graphics/Path;

    .line 74
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->arrowDashedPath:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 75
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->arrowDashedPath:Landroid/graphics/Path;

    invoke-virtual {v0, p3, p4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 76
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->paint:Landroid/graphics/Paint;

    const-string v1, "#77FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 77
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->OVAL_RADIUS:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 78
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->animatePath()V

    .line 79
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->invalidate()V

    .line 80
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 57
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->arrowDashedPath:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->paint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->pathDashEffect:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 59
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->arrowDashedPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 61
    :cond_0
    return-void
.end method
