.class public Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
.super Ljava/lang/Object;
.source "TitleLeaderBoard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "User"
.end annotation


# instance fields
.field private gamerRealName:Ljava/lang/String;

.field public gamertag:Ljava/lang/String;

.field public rank:I

.field private userProfilePicUri:Ljava/lang/String;

.field public values:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getGamerRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->gamerRealName:Ljava/lang/String;

    return-object v0
.end method

.method public getUserProfilePicUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->userProfilePicUri:Ljava/lang/String;

    return-object v0
.end method

.method public setGamerRealName(Ljava/lang/String;)V
    .locals 0
    .param p1, "realName"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->gamerRealName:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setUserProfilePicUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->userProfilePicUri:Ljava/lang/String;

    .line 39
    return-void
.end method
