.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
.source "ClubAdminReportsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "MessageViewHolder"
.end annotation


# instance fields
.field private final gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamertag:Landroid/widget/TextView;

.field private final message:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

.field private final timestamp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .line 349
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V

    .line 351
    const v0, 0x7f0e02ec

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 352
    const v0, 0x7f0e02ee

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->gamertag:Landroid/widget/TextView;

    .line 353
    const v0, 0x7f0e02f1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->timestamp:Landroid/widget/TextView;

    .line 354
    const v0, 0x7f0e02f2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->message:Landroid/widget/TextView;

    .line 355
    return-void
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;
    .param p1, "messageListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->deleteItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method

.method static synthetic lambda$onBind$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;
    .param p1, "messageListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 376
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->removeDecorator(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected getInnerViewId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 360
    const v0, 0x7f03006f

    return v0
.end method

.method public onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v3, 0x7f0201fa

    .line 365
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    move-object v0, p1

    .line 367
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;

    .line 369
    .local v0, "messageListItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;->hasValidContent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->hideErrorView()V

    .line 372
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 373
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->gamertag:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;->getGamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->timestamp:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;->getTimestamp()Ljava/util/Date;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->message:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->parseMessage(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->delete:Landroid/view/View;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->showErrorView()V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 341
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$MessageViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method
