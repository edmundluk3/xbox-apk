.class public Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "AboutActivityViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 17
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAboutActivityAdapter(Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 18
    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 40
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAboutActivityAdapter(Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 31
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method
