.class Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$2;
.super Ljava/lang/Object;
.source "MultiplayerTelemetryService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->inviteOnly(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

.field final synthetic val$joinRestriction:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$2;->this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iput-object p2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$2;->val$joinRestriction:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$2;->val$joinRestriction:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 97
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->Local:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$2;->val$joinRestriction:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 99
    .local v0, "enable":Z
    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "Party - Make party invite only"

    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 100
    return-void

    .line 97
    .end local v0    # "enable":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 99
    .restart local v0    # "enable":Z
    :cond_1
    const-string v1, "Party - Make party joinable"

    goto :goto_1
.end method
