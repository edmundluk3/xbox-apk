.class final synthetic Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final arg$1:Ljava/lang/String;

.field private final arg$2:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final arg$3:Z

.field private final arg$4:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/model/ProfileModel;ZLcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$1:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$2:Lcom/microsoft/xbox/service/model/ProfileModel;

    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$3:Z

    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$4:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    return-void
.end method

.method public static lambdaFactory$(Ljava/lang/String;Lcom/microsoft/xbox/service/model/ProfileModel;ZLcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/model/ProfileModel;ZLcom/microsoft/xbox/toolkit/ui/NavigationManager;)V

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$1:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$2:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$3:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->arg$4:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->lambda$navigateToUserProfile$15(Ljava/lang/String;Lcom/microsoft/xbox/service/model/ProfileModel;ZLcom/microsoft/xbox/toolkit/ui/NavigationManager;)V

    return-void
.end method
