.class public abstract Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;
.super Ljava/lang/Object;
.source "RxWebSocketDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RxWebSocketStringMessageEvent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22
    new-instance v0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketStringMessageEvent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketStringMessageEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract text()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
