.class public Lcom/microsoft/xbox/domain/ModelManagerModule;
.super Ljava/lang/Object;
.source "ModelManagerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideAuthManager(Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .locals 1
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;-><init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    return-object v0
.end method

.method provideClubModelManager()Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-object v0
.end method
