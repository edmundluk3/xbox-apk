.class public Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "TrendingTopicScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "TrendingTopic"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreen;->onCreateContentView()V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 23
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f03023b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreen;->setContentView(I)V

    .line 28
    return-void
.end method
