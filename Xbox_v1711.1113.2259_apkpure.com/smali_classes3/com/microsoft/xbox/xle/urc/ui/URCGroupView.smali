.class public Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;
.super Landroid/view/ViewGroup;
.source "URCGroupView.java"


# instance fields
.field protected cols_count:I

.field protected groupType:I

.field protected item_height:I

.field protected item_width:I

.field protected rows_count:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 13
    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    .line 14
    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    .line 15
    iput v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->rows_count:I

    .line 16
    const/4 v0, 0x3

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->groupType:I

    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->init(Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    .line 14
    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    .line 15
    iput v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->rows_count:I

    .line 16
    const/4 v0, 0x3

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->groupType:I

    .line 30
    invoke-direct {p0, p2, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->init(Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x1

    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 13
    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    .line 14
    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->rows_count:I

    .line 16
    const/4 v0, 0x3

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->groupType:I

    .line 35
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->init(Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method private createTestButton()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;-><init>(Landroid/content/Context;)V

    .line 51
    .local v0, "v":Lcom/microsoft/xbox/xle/urc/ui/BranchButton;
    const-string v1, "Test"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;->setText(Ljava/lang/CharSequence;)V

    .line 52
    const-string v1, "btn.dvr"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;->setCommand(Ljava/lang/String;)V

    .line 53
    sget v1, Lcom/microsoft/xbox/xle/urc/R$drawable;->btn_small_rect:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;->setBackgroundResource(I)V

    .line 54
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->addView(Landroid/view/View;)V

    .line 55
    return-void
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    .line 39
    const/4 v1, 0x3

    iput v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCGroupView:[I

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCGroupView_urc_group:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->groupType:I

    .line 46
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 47
    return-void
.end method


# virtual methods
.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 169
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 179
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 174
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getGroupType()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->groupType:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 15
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 59
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->rows_count:I

    const/4 v2, 0x1

    if-ge v0, v2, :cond_1

    .line 94
    :cond_0
    return-void

    .line 62
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getChildCount()I

    move-result v7

    .line 65
    .local v7, "count":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getPaddingLeft()I

    move-result v11

    .line 66
    .local v11, "pl":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getPaddingTop()I

    move-result v13

    .line 67
    .local v13, "pt":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getPaddingRight()I

    move-result v12

    .line 71
    .local v12, "pr":I
    sub-int v0, p4, v12

    sub-int v0, v0, p2

    sub-int/2addr v0, v11

    iget v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    iget v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    mul-int/2addr v2, v3

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int p2, v11, v0

    .line 72
    move/from16 p3, v13

    .line 74
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v7, :cond_0

    .line 76
    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 77
    .local v1, "child":Landroid/view/View;
    if-nez v1, :cond_2

    .line 74
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 81
    :cond_2
    move v9, v8

    .line 83
    .local v9, "index":I
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    rem-int v6, v9, v0

    .line 84
    .local v6, "col":I
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    div-int v14, v9, v0

    .line 85
    .local v14, "row":I
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->rows_count:I

    if-ge v14, v0, :cond_0

    .line 88
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v3, 0x0

    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 90
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 91
    .local v10, "lps":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    mul-int/2addr v0, v6

    add-int v0, v0, p2

    iget v2, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    mul-int/2addr v2, v14

    add-int v2, v2, p3

    iget v3, v10, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    add-int/lit8 v4, v6, 0x1

    mul-int/2addr v3, v4

    add-int v3, v3, p2

    iget v4, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    add-int/lit8 v5, v14, 0x1

    mul-int/2addr v4, v5

    add-int v4, v4, p3

    iget v5, v10, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 20
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 99
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->isInEditMode()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getChildCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 100
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->createTestButton()V

    .line 104
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v15

    .line 105
    .local v15, "mw":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    .line 106
    .local v14, "mh":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v19

    .line 107
    .local v19, "sw":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v18

    .line 110
    .local v18, "sh":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getPaddingLeft()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getPaddingRight()I

    move-result v5

    add-int v17, v3, v5

    .line 111
    .local v17, "pw":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getPaddingTop()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getPaddingBottom()I

    move-result v5

    add-int v16, v3, v5

    .line 113
    .local v16, "ph":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getChildCount()I

    move-result v10

    .line 118
    .local v10, "count":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    add-int/2addr v3, v10

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    div-int/2addr v3, v5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->rows_count:I

    .line 120
    sub-int v12, v19, v17

    .line 121
    .local v12, "dw":I
    if-nez v15, :cond_1

    if-gez v12, :cond_1

    .line 122
    const/4 v12, 0x0

    .line 123
    move/from16 v19, v17

    .line 126
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->cols_count:I

    div-int v3, v12, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    .line 127
    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    .line 130
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    if-ge v13, v10, :cond_4

    .line 131
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 132
    .local v4, "child":Landroid/view/View;
    if-nez v4, :cond_3

    .line 130
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 136
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    const/4 v5, 0x0

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    const/4 v7, 0x0

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 138
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 139
    .local v9, "cmw":I
    if-eqz v9, :cond_2

    .line 143
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    mul-int/2addr v3, v5

    div-int/2addr v3, v9

    move-object/from16 v0, p0

    iput v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    .line 147
    .end local v4    # "child":Landroid/view/View;
    .end local v9    # "cmw":I
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->rows_count:I

    mul-int v11, v3, v5

    .line 148
    .local v11, "dh":I
    if-nez v14, :cond_6

    .line 149
    add-int v18, v16, v11

    .line 155
    :cond_5
    :goto_1
    const/4 v13, 0x0

    :goto_2
    if-ge v13, v10, :cond_8

    .line 156
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 157
    .restart local v4    # "child":Landroid/view/View;
    if-nez v4, :cond_7

    .line 155
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 150
    .end local v4    # "child":Landroid/view/View;
    :cond_6
    const/high16 v3, -0x80000000

    if-ne v14, v3, :cond_5

    .line 151
    add-int v3, v16, v11

    move/from16 v0, v18

    if-le v0, v3, :cond_5

    .line 152
    add-int v18, v16, v11

    goto :goto_1

    .line 161
    .restart local v4    # "child":Landroid/view/View;
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_width:I

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->item_height:I

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->measureChildWithMargins(Landroid/view/View;IIII)V

    goto :goto_3

    .line 164
    .end local v4    # "child":Landroid/view/View;
    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCGroupView;->setMeasuredDimension(II)V

    .line 165
    return-void
.end method
