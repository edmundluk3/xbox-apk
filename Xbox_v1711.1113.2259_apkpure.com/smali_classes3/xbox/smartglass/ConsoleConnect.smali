.class public Lxbox/smartglass/ConsoleConnect;
.super LMicrosoft/Telemetry/Data;
.source "ConsoleConnect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxbox/smartglass/ConsoleConnect$Schema;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LMicrosoft/Telemetry/Data",
        "<",
        "Lxbox/smartglass/CommonData;",
        ">;"
    }
.end annotation


# instance fields
.field private consoleLanguage:Ljava/lang/String;

.field private consoleOsVersion:Ljava/lang/String;

.field private hResult:I

.field private latency:I

.field private mode:I

.field private sessionId:Ljava/lang/String;

.field private start:Ljava/lang/String;

.field private tvProviderID:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 408
    invoke-direct {p0}, LMicrosoft/Telemetry/Data;-><init>()V

    .line 410
    return-void
.end method

.method public static getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lxbox/smartglass/ConsoleConnect$Schema;->schemaDef:Lcom/microsoft/bond/SchemaDef;

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/microsoft/bond/BondSerializable;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lxbox/smartglass/ConsoleConnect;->clone()Lcom/microsoft/bond/BondSerializable;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Lcom/microsoft/bond/StructDef;)Lcom/microsoft/bond/BondMirror;
    .locals 1
    .param p1, "structDef"    # Lcom/microsoft/bond/StructDef;

    .prologue
    .line 387
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getConsoleLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public final getConsoleOsVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getField(Lcom/microsoft/bond/FieldDef;)Ljava/lang/Object;
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;

    .prologue
    .line 327
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 345
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 329
    :sswitch_0
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 331
    :sswitch_1
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 333
    :sswitch_2
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 335
    :sswitch_3
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    goto :goto_0

    .line 337
    :sswitch_4
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 339
    :sswitch_5
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    goto :goto_0

    .line 341
    :sswitch_6
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    goto :goto_0

    .line 343
    :sswitch_7
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 327
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x46 -> :sswitch_6
        0x50 -> :sswitch_7
    .end sparse-switch
.end method

.method public final getHResult()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    return v0
.end method

.method public final getLatency()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    return v0
.end method

.method public final getMode()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    return v0
.end method

.method public getSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 395
    invoke-static {}, Lxbox/smartglass/ConsoleConnect;->getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v0

    return-object v0
.end method

.method public final getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    return-object v0
.end method

.method public final getTvProviderID()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    return v0
.end method

.method public marshal(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 0
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 616
    invoke-static {p0, p1}, Lcom/microsoft/bond/internal/Marshaler;->marshal(Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/ProtocolWriter;)V

    .line 617
    return-void
.end method

.method public memberwiseCompare(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 679
    if-nez p1, :cond_1

    .line 685
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 683
    check-cast v0, Lxbox/smartglass/ConsoleConnect;

    .line 685
    .local v0, "that":Lxbox/smartglass/ConsoleConnect;
    invoke-virtual {p0, v0}, Lxbox/smartglass/ConsoleConnect;->memberwiseCompareQuick(Lxbox/smartglass/ConsoleConnect;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lxbox/smartglass/ConsoleConnect;->memberwiseCompareDeep(Lxbox/smartglass/ConsoleConnect;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected memberwiseCompareDeep(Lxbox/smartglass/ConsoleConnect;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/ConsoleConnect;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 707
    const/4 v0, 0x1

    .line 708
    .local v0, "equals":Z
    if-eqz v0, :cond_4

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareDeep(LMicrosoft/Telemetry/Data;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    .line 709
    :goto_0
    if-eqz v0, :cond_6

    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    if-nez v3, :cond_5

    :cond_0
    move v0, v1

    .line 710
    :goto_1
    if-eqz v0, :cond_8

    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    if-nez v3, :cond_7

    :cond_1
    move v0, v1

    .line 711
    :goto_2
    if-eqz v0, :cond_a

    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    if-nez v3, :cond_9

    :cond_2
    move v0, v1

    .line 712
    :goto_3
    if-eqz v0, :cond_c

    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    if-nez v3, :cond_b

    :cond_3
    move v0, v1

    .line 713
    :goto_4
    return v0

    :cond_4
    move v0, v2

    .line 708
    goto :goto_0

    .line 709
    :cond_5
    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_6
    move v0, v2

    goto :goto_1

    .line 710
    :cond_7
    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_8
    move v0, v2

    goto :goto_2

    .line 711
    :cond_9
    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_a
    move v0, v2

    goto :goto_3

    .line 712
    :cond_b
    iget-object v3, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_c
    move v0, v2

    goto :goto_4
.end method

.method protected memberwiseCompareQuick(Lxbox/smartglass/ConsoleConnect;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/ConsoleConnect;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 689
    const/4 v0, 0x1

    .line 690
    .local v0, "equals":Z
    if-eqz v0, :cond_4

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareQuick(LMicrosoft/Telemetry/Data;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v0, v2

    .line 691
    :goto_0
    if-eqz v0, :cond_7

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_1
    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    if-nez v4, :cond_6

    move v4, v2

    :goto_2
    if-ne v1, v4, :cond_7

    move v0, v2

    .line 692
    :goto_3
    if-eqz v0, :cond_9

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    if-nez v1, :cond_8

    :cond_0
    move v0, v2

    .line 693
    :goto_4
    if-eqz v0, :cond_a

    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    iget v4, p1, Lxbox/smartglass/ConsoleConnect;->mode:I

    if-ne v1, v4, :cond_a

    move v0, v2

    .line 694
    :goto_5
    if-eqz v0, :cond_b

    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    iget v4, p1, Lxbox/smartglass/ConsoleConnect;->latency:I

    if-ne v1, v4, :cond_b

    move v0, v2

    .line 695
    :goto_6
    if-eqz v0, :cond_e

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    if-nez v1, :cond_c

    move v1, v2

    :goto_7
    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    if-nez v4, :cond_d

    move v4, v2

    :goto_8
    if-ne v1, v4, :cond_e

    move v0, v2

    .line 696
    :goto_9
    if-eqz v0, :cond_10

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    if-nez v1, :cond_f

    :cond_1
    move v0, v2

    .line 697
    :goto_a
    if-eqz v0, :cond_11

    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    iget v4, p1, Lxbox/smartglass/ConsoleConnect;->hResult:I

    if-ne v1, v4, :cond_11

    move v0, v2

    .line 698
    :goto_b
    if-eqz v0, :cond_14

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    if-nez v1, :cond_12

    move v1, v2

    :goto_c
    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    if-nez v4, :cond_13

    move v4, v2

    :goto_d
    if-ne v1, v4, :cond_14

    move v0, v2

    .line 699
    :goto_e
    if-eqz v0, :cond_16

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    if-nez v1, :cond_15

    :cond_2
    move v0, v2

    .line 700
    :goto_f
    if-eqz v0, :cond_19

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    if-nez v1, :cond_17

    move v1, v2

    :goto_10
    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    if-nez v4, :cond_18

    move v4, v2

    :goto_11
    if-ne v1, v4, :cond_19

    move v0, v2

    .line 701
    :goto_12
    if-eqz v0, :cond_1b

    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    if-nez v1, :cond_1a

    :cond_3
    move v0, v2

    .line 702
    :goto_13
    if-eqz v0, :cond_1c

    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    iget v4, p1, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    if-ne v1, v4, :cond_1c

    move v0, v2

    .line 703
    :goto_14
    return v0

    :cond_4
    move v0, v3

    .line 690
    goto :goto_0

    :cond_5
    move v1, v3

    .line 691
    goto :goto_1

    :cond_6
    move v4, v3

    goto :goto_2

    :cond_7
    move v0, v3

    goto :goto_3

    .line 692
    :cond_8
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_0

    :cond_9
    move v0, v3

    goto :goto_4

    :cond_a
    move v0, v3

    .line 693
    goto :goto_5

    :cond_b
    move v0, v3

    .line 694
    goto :goto_6

    :cond_c
    move v1, v3

    .line 695
    goto :goto_7

    :cond_d
    move v4, v3

    goto :goto_8

    :cond_e
    move v0, v3

    goto :goto_9

    .line 696
    :cond_f
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_1

    :cond_10
    move v0, v3

    goto :goto_a

    :cond_11
    move v0, v3

    .line 697
    goto :goto_b

    :cond_12
    move v1, v3

    .line 698
    goto :goto_c

    :cond_13
    move v4, v3

    goto :goto_d

    :cond_14
    move v0, v3

    goto :goto_e

    .line 699
    :cond_15
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_2

    :cond_16
    move v0, v3

    goto :goto_f

    :cond_17
    move v1, v3

    .line 700
    goto :goto_10

    :cond_18
    move v4, v3

    goto :goto_11

    :cond_19
    move v0, v3

    goto :goto_12

    .line 701
    :cond_1a
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_3

    :cond_1b
    move v0, v3

    goto :goto_13

    :cond_1c
    move v0, v3

    .line 702
    goto :goto_14
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 449
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readBegin()V

    .line 450
    invoke-virtual {p0, p1}, Lxbox/smartglass/ConsoleConnect;->readNested(Lcom/microsoft/bond/ProtocolReader;)V

    .line 451
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readEnd()V

    .line 452
    return-void
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 470
    return-void
.end method

.method public readNested(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 458
    sget-object v0, Lcom/microsoft/bond/ProtocolCapability;->TAGGED:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v0}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 459
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleConnect;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleConnect;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    invoke-static {p1}, Lcom/microsoft/bond/internal/ReadHelper;->skipPartialStruct(Lcom/microsoft/bond/ProtocolReader;)V

    goto :goto_0
.end method

.method protected readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z
    .locals 5
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 547
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 549
    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 608
    :goto_0
    return v2

    .line 566
    .local v0, "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :sswitch_0
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    .line 602
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldEnd()V

    .line 556
    .end local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldBegin()Lcom/microsoft/bond/ProtocolReader$FieldTag;

    move-result-object v0

    .line 558
    .restart local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP:Lcom/microsoft/bond/BondDataType;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_3

    .line 560
    :cond_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_2

    .line 605
    .local v1, "isPartial":Z
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    move v2, v1

    .line 608
    goto :goto_0

    .end local v1    # "isPartial":Z
    :cond_2
    move v1, v2

    .line 560
    goto :goto_2

    .line 564
    :cond_3
    iget v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->id:I

    sparse-switch v3, :sswitch_data_0

    .line 598
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-virtual {p1, v3}, Lcom/microsoft/bond/ProtocolReader;->skip(Lcom/microsoft/bond/BondDataType;)V

    goto :goto_1

    .line 570
    :sswitch_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    goto :goto_1

    .line 574
    :sswitch_2
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    goto :goto_1

    .line 578
    :sswitch_3
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    goto :goto_1

    .line 582
    :sswitch_4
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    goto :goto_1

    .line 586
    :sswitch_5
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    goto :goto_1

    .line 590
    :sswitch_6
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    goto :goto_1

    .line 594
    :sswitch_7
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    goto :goto_1

    .line 564
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x46 -> :sswitch_6
        0x50 -> :sswitch_7
    .end sparse-switch
.end method

.method protected readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 473
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 475
    .local v0, "canOmitFields":Z
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 476
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 478
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 479
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    .line 486
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 487
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    .line 494
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_5

    .line 495
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    .line 502
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_7

    .line 503
    :cond_6
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    .line 510
    :cond_7
    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_9

    .line 511
    :cond_8
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    .line 518
    :cond_9
    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_b

    .line 519
    :cond_a
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    .line 526
    :cond_b
    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_d

    .line 527
    :cond_c
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    .line 534
    :cond_d
    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_f

    .line 535
    :cond_e
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    .line 541
    :cond_f
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    .line 542
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 416
    const-string v0, "ConsoleConnect"

    const-string v1, "xbox.smartglass.ConsoleConnect"

    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/ConsoleConnect;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    return-void
.end method

.method protected reset(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "qualifiedName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 420
    invoke-super {p0, p1, p2}, LMicrosoft/Telemetry/Data;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    .line 422
    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    .line 423
    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    .line 424
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    .line 425
    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    .line 426
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    .line 427
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    .line 428
    iput v1, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    .line 429
    return-void
.end method

.method public final setConsoleLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public final setConsoleOsVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setField(Lcom/microsoft/bond/FieldDef;Ljava/lang/Object;)V
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 354
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 380
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    return-void

    .line 356
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 359
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_1
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    goto :goto_0

    .line 362
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_2
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    goto :goto_0

    .line 365
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_3
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    goto :goto_0

    .line 368
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_4
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    goto :goto_0

    .line 371
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_5
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    goto :goto_0

    .line 374
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_6
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    goto :goto_0

    .line 377
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_7
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    goto :goto_0

    .line 354
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x46 -> :sswitch_6
        0x50 -> :sswitch_7
    .end sparse-switch
.end method

.method public final setHResult(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 120
    iput p1, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    .line 121
    return-void
.end method

.method public final setLatency(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 92
    iput p1, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    .line 93
    return-void
.end method

.method public final setMode(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 78
    iput p1, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    .line 79
    return-void
.end method

.method public final setSessionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public final setStart(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public final setTvProviderID(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 162
    iput p1, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    .line 163
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 435
    invoke-static {p1, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V

    .line 436
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 442
    check-cast p2, Lcom/microsoft/bond/SchemaDef;

    .end local p2    # "schema":Lcom/microsoft/bond/BondSerializable;
    invoke-static {p1, p2, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/BondSerializable;)V

    .line 443
    return-void
.end method

.method public write(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 2
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 623
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeBegin()V

    .line 625
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->getFirstPassWriter()Lcom/microsoft/bond/ProtocolWriter;

    move-result-object v0

    .local v0, "firstPassWriter":Lcom/microsoft/bond/ProtocolWriter;
    if-eqz v0, :cond_0

    .line 627
    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/ConsoleConnect;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 628
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleConnect;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 634
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeEnd()V

    .line 635
    return-void

    .line 632
    :cond_0
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/ConsoleConnect;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    goto :goto_0
.end method

.method public writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V
    .locals 4
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 638
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 639
    .local v0, "canOmitFields":Z
    sget-object v1, Lxbox/smartglass/ConsoleConnect$Schema;->metadata:Lcom/microsoft/bond/Metadata;

    invoke-virtual {p1, v1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructBegin(Lcom/microsoft/bond/BondSerializable;Z)V

    .line 640
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 642
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0xa

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 643
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->sessionId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 644
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 646
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x14

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 647
    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->mode:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 648
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 650
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x1e

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 651
    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->latency:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 652
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 654
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x28

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 655
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->start:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 656
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 658
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_INT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x32

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$400()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 659
    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->hResult:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeInt32(I)V

    .line 660
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 662
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x3c

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$500()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 663
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleLanguage:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 664
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 666
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x46

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$600()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 667
    iget-object v1, p0, Lxbox/smartglass/ConsoleConnect;->consoleOsVersion:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 668
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 670
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x50

    invoke-static {}, Lxbox/smartglass/ConsoleConnect$Schema;->access$700()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 671
    iget v1, p0, Lxbox/smartglass/ConsoleConnect;->tvProviderID:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 672
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 674
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructEnd(Z)V

    .line 675
    return-void
.end method
