.class public interface abstract Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;
.super Ljava/lang/Object;
.source "DLAssetsService.java"


# static fields
.field public static final GAMERPIC_URL_FORMAT:Ljava/lang/String; = "https://dlassets-ssl.xboxlive.com/public/content/ppl/gamerpics/%s-md.png"


# virtual methods
.method public abstract getClubpicsManifest()Lretrofit2/Call;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "gamerpics/clubpicsmanifest.json"
    .end annotation
.end method

.method public abstract getGamerpicsManifest()Lretrofit2/Call;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "gamerpics/gamerpicsmanifest.json"
    .end annotation
.end method
