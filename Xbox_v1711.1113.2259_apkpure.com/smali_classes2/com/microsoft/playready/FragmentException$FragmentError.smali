.class public final enum Lcom/microsoft/playready/FragmentException$FragmentError;
.super Ljava/lang/Enum;
.source "FragmentException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/FragmentException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FragmentError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/FragmentException$FragmentError;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BeginningOfStream:Lcom/microsoft/playready/FragmentException$FragmentError;

.field private static final synthetic ENUM$VALUES:[Lcom/microsoft/playready/FragmentException$FragmentError;

.field public static final enum EndOfStream:Lcom/microsoft/playready/FragmentException$FragmentError;


# instance fields
.field private final mMsg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/microsoft/playready/FragmentException$FragmentError;

    const-string v1, "EndOfStream"

    const-string v2, "End of known Fragments"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/playready/FragmentException$FragmentError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/playready/FragmentException$FragmentError;->EndOfStream:Lcom/microsoft/playready/FragmentException$FragmentError;

    .line 16
    new-instance v0, Lcom/microsoft/playready/FragmentException$FragmentError;

    const-string v1, "BeginningOfStream"

    const-string v2, "Fragment index before begining of stream"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/playready/FragmentException$FragmentError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/playready/FragmentException$FragmentError;->BeginningOfStream:Lcom/microsoft/playready/FragmentException$FragmentError;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/playready/FragmentException$FragmentError;

    sget-object v1, Lcom/microsoft/playready/FragmentException$FragmentError;->EndOfStream:Lcom/microsoft/playready/FragmentException$FragmentError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/playready/FragmentException$FragmentError;->BeginningOfStream:Lcom/microsoft/playready/FragmentException$FragmentError;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/playready/FragmentException$FragmentError;->ENUM$VALUES:[Lcom/microsoft/playready/FragmentException$FragmentError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput-object p3, p0, Lcom/microsoft/playready/FragmentException$FragmentError;->mMsg:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/FragmentException$FragmentError;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/microsoft/playready/FragmentException$FragmentError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/FragmentException$FragmentError;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/FragmentException$FragmentError;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/microsoft/playready/FragmentException$FragmentError;->ENUM$VALUES:[Lcom/microsoft/playready/FragmentException$FragmentError;

    array-length v1, v0

    new-array v2, v1, [Lcom/microsoft/playready/FragmentException$FragmentError;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/playready/FragmentException$FragmentError;->mMsg:Ljava/lang/String;

    return-object v0
.end method
