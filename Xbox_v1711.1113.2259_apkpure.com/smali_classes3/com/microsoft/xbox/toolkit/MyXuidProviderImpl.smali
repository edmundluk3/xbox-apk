.class public Lcom/microsoft/xbox/toolkit/MyXuidProviderImpl;
.super Ljava/lang/Object;
.source "MyXuidProviderImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMyXuid()J
    .locals 2

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MyXuidProviderImpl;->getMyXuidString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMyXuidString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
