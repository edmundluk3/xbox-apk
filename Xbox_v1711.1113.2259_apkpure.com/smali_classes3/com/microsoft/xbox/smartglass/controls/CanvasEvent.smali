.class final Lcom/microsoft/xbox/smartglass/controls/CanvasEvent;
.super Ljava/lang/Object;
.source "CanvasEvent.java"


# static fields
.field public static final Accelerometer:Ljava/lang/String; = "accelerometer"

.field public static final ClientsUpdated:Ljava/lang/String; = "clientsUpdated"

.field public static final ConnectionStateChanged:Ljava/lang/String; = "connectionStateChanged"

.field public static final Error:Ljava/lang/String; = "error"

.field public static final Gyroscope:Ljava/lang/String; = "gyroscope"

.field public static final Loaded:Ljava/lang/String; = "loaded"

.field public static final MediaState:Ljava/lang/String; = "mediaState"

.field public static final PairedIdentityStateChanged:Ljava/lang/String; = "pairedIdentityStateChanged"

.field public static final Received:Ljava/lang/String; = "received"

.field public static final TitleChanged:Ljava/lang/String; = "titleChanged"

.field public static final TitleChannelChanged:Ljava/lang/String; = "titleChannelChanged"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
